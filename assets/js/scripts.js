jQuery(document).ready(function($) {

	$(".clhomemenu").addClass('height-1');

	//check vip tabs
	$('.tabbable').each(function() {
		if($(this).children('.dailystrip-content').children('.tab-pane').first().children().length == 0) {
			$(this).children('ul').children('li').first().css('display','none');
			$(this).children('ul').children('li').removeClass('col-md-3').addClass('col-md-4');
		}
	});

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$('.tabbable').each(function() {
			if($(this).children('.dailystrip-content').children('.tab-pane').first().children().length == 0) {
				$(this).children('ul.mobile').children('li').first().css('display','none');
				$(this).children('ul.mobile').children('li').removeClass('col-xs-3').addClass('col-xs-4');
			}
		});
	}


	//show release date on upcoming header
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$('.upcom-li').each(function () {
			if($(this).hasClass('active')) {
				$('#upcoming_date').text($(this).attr('r-date'));
			}
		});

		var counter = $('.upcom-li').length;

		$('.upcom-li').click(function() {
			$('#upcoming_date').text($(this).attr('r-date'));
		});

		$('.upcom-prev').click(function() {
			$('.upcom-li').each(function () {
				if($(this).hasClass('active')) {
					var order = $(this).attr('order');
					if(order <= 1) {
						$('#upcoming_date').text($('.upcom-li').last().attr('r-date'));
					}
					else {
						$('#upcoming_date').text($(this).prev().attr('r-date'));
					}	
				}
			});
		});

		$('.upcom-next').click(function() {
			$('.upcom-li').each(function () {
				if($(this).hasClass('active')) {
					var order = $(this).attr('order');
					if(order >= counter) {
						$('#upcoming_date').text($('.upcom-li').first().attr('r-date'));
					}
					else {
						$('#upcoming_date').text($(this).next().attr('r-date'));
					}		
				}
			});
		});
    }

    //for tab-pane (other site released)

	$(document).on('click', '.daily-strip-tap-menu > li > a', function() {
		$(this).parent().parent().children().each(function() {
			$(this).children().removeClass('tab-active');
		});
		$(this).addClass('tab-active');
		$(this).parent().removeClass('active');
	});

	
    $('.tab-action').click(function () {
    	var link = $(this).children().attr("href");
    	link = link.substring(1,link.length);
    	$('#' + link).parent().css('border-bottom-color',$('#' + link).attr('color'));
    })

    // For tweet feeds
    if($('#block-follow-news').length > 0) {
    	!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    }
    
    if($("iframe#twitter-widget-0").length > 0 ) {
    	$("iframe#twitter-widget-0").waitUntilExists(function(){
	        $("iframe#twitter-widget-0").contents().find('head').append('<style>::-webkit-scrollbar { width: 12px; } ::-webkit-scrollbar-track { -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); border-radius: 10px; } ::-webkit-scrollbar-thumb { border-radius: 10px; -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); } @media (max-width: 480px) {li.h-entry {height:auto ! important;}}</style>');     
	    });
    }

    $(".navbar-header .navbar-toggle").click(function(event) {
		$("#homemenu").removeClass('height-1');
    });


    $(document).on('change','#ddViewGallery', function() {
    	$('input[name=view]').val($(this).val());
    	$('#fViewGallery').attr('action','/index.cfm/gallery.default?view=' + $('input[name=view]').val() + '&pMonth=' + $('input[name=pMonth]').val() + '&pYear=' + $('input[name=pYear]').val());
    	$('#fViewGallery').submit();
    })

    $(document).on('change', '#galTypeSelect', function() {
    	mSearchFilter($('#hdModelId').val(),$(this).val());
    })

    $(document).on('change', '#ddPreMonths', function() {
    	window.location = $(this).val();
    })

    $('#div_otherSitesLabel').on('click', function() {
    	showMoreSites($('#hdModelLinkCount').val());
    })

 	$(document).on('change', '#ddSet', function() {
    	window.location = $(this).val();
    })

    $(document).on('click', '.available-month-nav', function (e) {
    	e.preventDefault();

    	$('#current-available-month').text($(this).attr('data-year'));
    	$('#hd-current-available-month').val($(this).attr('data-year'));

    	$.ajax({
    	        type: "post",
    	        url: "/index.cfm/gallery.getAvailableMonthsByYear",
    	        data: {
    	        	view: $(this).attr('data-view'),
    	        	year: $(this).attr('data-year')
    	        },
    	        dataType: "JSON",
    	        success: function (data) {
    	        	$('#calendar-content').empty();
	        		var strHTML = "";
	        		var currentyear = $('#hd-current-available-month').val() * 1;
    	        	if(data.length > 0) {
        				for(var j = 0; j <= 11; j++) {
        					if (j == 0) {
      							strHTML += '<tr>';
	      					}
	      					if (data[j].DISABLED == 1) {
	      						strHTML += '<td><div>'+ getFormatedMonth(j) + '</div></td>';
			      			}
			      			else {
			      				var classactive = (data[j].MONTH == $('#pMonth').val() && data[j].YEAR == $('#pYear').val()) ? 'active' : '';
			      				strHTML += '<td class="' + classactive + '"><a href="/index.cfm/gallery.default?pMonth=' + data[j].MONTH + '&pYear=' + currentyear + '&view=' + $('#ddViewGallery').val() + '" data-month="'+ data[j].MONTH +'" data-year="' + currentyear + '">' + getFormatedMonth(j) + '</a></td>';
	      					}
	      					if (j % 4 == 3) {
	      						strHTML += '</tr><tr>';
	      					}
	      					if(j == 11) {
	      						strHTML += '</tr>';
	      					}
        				}
    	        	}
    	        	else {
    	        		for(var j = 0; j<=11; j++) {
        					if (j == 0) {
      							strHTML += '<tr>';
	      					}
			      			strHTML += '<td><div>'+ getFormatedMonth(j) + '</div></td>';
	      					if (j % 4 == 3) {
	      						strHTML += '</tr><tr>';
	      					}
	      					if(j == 11) {
	      						strHTML += '</tr>';
	      					}
        				}
    	        	}
	    	       	$(strHTML).hide()
			                .prependTo("#calendar-content")
			                .fadeIn();
    	        },
    	        error: function () {
    	        	//TO DO
    	        } 
    	});
		var datatype = $(this).attr('data-type');
    	if(datatype == "next") {
    		$(this).attr('data-year', $('#hd-current-available-month').val()*1 + 1);
    		$('a[data-type="prev"]').attr('data-year', $('#hd-current-available-month').val()*1 - 1);
    	}
    	else {
    		$(this).attr('data-year', $('#hd-current-available-month').val()*1 - 1);
    		$('a[data-type="next"]').attr('data-year', $('#hd-current-available-month').val()*1 + 1);
    	}
    })

	$(document).on('click','#div_suggestButton', function (e) {
		e.preventDefault();
		$(".div_suggestButton").css("display","none");
		$("#div_suggestForm").css("display","block");
	})

	$(document).on('click','#btn-cancel-suggest', function (e) {
		e.preventDefault();
		$(".div_suggestButton").css("display","block");
		$("#div_suggestForm").css("display","none");
	})

	$(document).on('click', '#btn-add-img-cus-set', function (e) {
		e.preventDefault();
		if($("#customgal-div").hasClass('hide')) {
			$("#customgal-div").removeClass('hide');
		}
	})

	$(document).on('click', '#btn-refresh-capcha', function (e) {
		e.preventDefault();
		$('#iframe_captcha').location.reload();
	})

	$(document).on('click', '#x-close', function (e) {
		e.preventDefault();
		if(! $("#customgal-div").hasClass('hide')) {
			$("#customgal-div").addClass('hide');
		}
	})

	if($('.modelprofile-content').length > 0) {
		jwplayer('mediaspace').setup({
		'flashplayer': 'http://models.onlytease.com/flv_files/player.swf',
		'file': $('#flv-video-path').val(),
		'image': '/images/flashprofile-cover-hd.jpg',
		'backcolor': 'EEEEEE',
		'frontcolor': '3333FF',
		'lightcolor': 'FF00FF',
		'screencolor': 'FFFFFF',
		'controlbar': 'bottom',
		'width': '398',
		'height': '224'});
	}

	$(document).on('change', '#ddFiler-mobile-search', function() {
		galFilterType($(this).val());
	})

	$(document).on('change', '#ddSort-mobile-search', function() {
		filterUpdate($(this).val());
	})

	if($('#post_topic_comment').length > 0) {
		CKEDITOR.replace('post_topic_comment');
	}

	if($('#reply_topic_comment').length > 0) {
		CKEDITOR.replace('reply_topic_comment');
	}

	$(document).on('click', '#post_topic_addmore', function(e) {
		e.preventDefault();
		var length = $('.post-topic-poll-options').length;
		for (var i = 1; i <= length; i++) {
			if($('.post-topic-poll-options')[i].hasClass('hide')) {
				$('.post-topic-poll-options')[i].removeClass('hide');
				break;
			}
		}
	})
});

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function mSearchFilter(model_id,gType) {
	window.location = '/index.cfm/model.model_set?model_id='+model_id+'&gType='+gType;
}

function showMoreSites(int_sites) {
	var div_otherSitesLabel = $("#div_otherSitesLabel");
	for (i=51; i<=int_sites; i++) {
		div_otherSite = $("#div_otherSite_"+i);
		if (div_otherSite.style.display == "none") {
			div_otherSite.style.display = "block";
			div_otherSitesLabel.value = "Show Less";
		} else {
			div_otherSite.style.display = "none";
			div_otherSitesLabel.value = "Show More";
		}
	}
}

function getFormatedMonth(month) {
	var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	return months[month];
}

function ConfirmDelete(id_val,currentPage)
{
	if (window.confirm(" Do you really want to remove this Gallery from your My Favourites list?"))
	 {
		self.location="/members/processes/favourites_functions.cfm?action=deletegallery&from=view&CurrentPage=#CurrentPage#&gid="+id_val;
	}
}

function ConfirmReminderDelete(del_val,id_val,currentPage)
{
	if (window.confirm(" Do you really want to remove this gallery from your My Reminders list?"))
	 {
		self.location="/members/processes/reminders_functions.cfm?action=delete&from=viewgallery&CurrentPage=#CurrentPage#&mrid="+del_val+"&id="+id_val;
	}
}

function showMoreSites(int_sites) {
	var div_otherSitesLabel = $("#div_otherSitesLabel");
	for (i=51; i<=int_sites; i++) {
		div_otherSite = $("#div_otherSite_"+i);
		if (div_otherSite.style.display == "none") {
			div_otherSite.style.display = "block";
			div_otherSitesLabel.value = "Show Less";
		} else {
			div_otherSite.style.display = "none";
			div_otherSitesLabel.value = "Show More";
		}
	}
}

function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}
function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}						
function valForm(thisForm) {
	if (trim(thisForm.site_name.value).length == 0) {
		alert("Please enter the name of the site this model appears on.");
		thisForm.site_name.value = "";
		thisForm.site_name.focus();
		return false;
	}
	if (trim(thisForm.site_url.value).length == 0) {
		alert("Please enter the url/address of the site this model appears on.");
		thisForm.site_url.value = "";
		thisForm.site_url.focus();
		return false;
	}
}
function clearField(thisField) {
	thisField.value = "";
	thisField.style.color = "";
	thisField.style.fontSize = "";
	thisField.style.fontStyle = "";
}

function galFilterType(galType) {
	document.getElementById('galType').value = galType;
	filterUpdate(1);
}

function filterUpdate(pageNumber,excludeID) {

	var gType = document.getElementById('galType').value;
	var fallBack = 0;
	if (gType == "load") {
		document.getElementById('galType').value = "a";
		if (document.getElementById('url_keyword_id').value > "") {
			fallBack = document.getElementById('url_keyword_id').value;
			gType = "a";
		}
	}
	
	var sortOrder = 0;
	var sortOrders = document.getElementsByName('sortOrder');
	for (var i = 0, l = sortOrders.length; i < l; i++)
    {
        if (sortOrders[i].checked)
        {
            sortOrder = sortOrders[i].value;
        }
    }
	
	var qryString = '/index.cfm/search.search_update?gType='+gType+'&order='+sortOrder+'&CurrentPage='+pageNumber;
	var i = 1;
	
	while (curEle = document.getElementById('keyword_id_'+i)) {
		include = 1;
		if (excludeID && excludeID == i) include = 0;
		if (include == 1 && curEle.value > 0) {
			qryString = qryString + "&filter_"+i+"="+curEle.value;
		}
		i++;
	} 

	if (i == 1 && fallBack > 0) {
		qryString = qryString + "&filter_1="+fallBack;
	}

	// Display loading thing
	document.getElementById('search-results').style.display = 'none';
	document.getElementById('loadingDiv-search').style.display = 'block';

	makeRequest(qryString,'search');
}

function makeRequest(url,process) {
	http_request = false;
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType) {
			http_request.overrideMimeType('text/xml');
			// See note below about this line
		}
	} else if (window.ActiveXObject) { // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}
	if (!http_request) {
		alert('Giving up :( Cannot create an XMLHTTP instance');
		return false;
	}
	if (process == 1)
		http_request.onreadystatechange = processRepsonse;
	else if (process == 'search')
		http_request.onreadystatechange = processFilterResponse;
	else if (process == 'get_tooltip')
		http_request.onreadystatechange = tt_response;
	else if (process !== 'customgal_update')
		http_request.onreadystatechange = processPermRepsonse;
	
	http_request.open('POST', url, true);
	http_request.send(null);
}

function processFilterResponse () {
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			hideLoading = true;
			var filtersPerBlock = 3;
			var xmlDoc = http_request.responseXML;
			var filterContainer = document.getElementById('filter_container');
			var blockSort = document.getElementsByClassName('blockSort')[0];

			if(blockSort.id == "blockSortMobi") {
				while (filterContainer.hasChildNodes() && filterContainer.children.length > 0) {
					for(var n = 0; n < filterContainer.children.length; n++) {
						if(filterContainer.children[n].hasClass('filter-node')) {
							jQuery(filterContainer.children[n]).remove();
						}
					}
				}
			}
			else {
				while (filterContainer.hasChildNodes() && filterContainer.children.length > 1) {
					for(var n = 0; n < filterContainer.children.length; n++) {
						if(filterContainer.children[n].hasClass('filter-node')) {
							jQuery(filterContainer.children[n]).remove();
						}
					}
				}
			}
			

			if(xmlDoc.getElementsByTagName("searchRequest").length == 1) {
				var rootNode_response = xmlDoc.getElementsByTagName('searchRequest')[0];
				
				var responResults = rootNode_response.getElementsByTagName('results')[0];
				if (responResults) {
					var resultLists = responResults.getElementsByTagName('g');
					if (responResults.attributes.getNamedItem("total_results")) {
						var total_results = responResults.attributes.getNamedItem("total_results").value;
					}
				}

				//filter(s)
				var filters = rootNode_response.getElementsByTagName('filter');
				if (filters.length > 0) {
					for (cFilter = 0; cFilter < filters.length; cFilter++) {
						
						// Filter data
						var filter_root = filters[cFilter];
						var filterNumber = filter_root.attributes.getNamedItem("number").value;
						var filterOptSelected = filter_root.attributes.getNamedItem("selected").value;
						var filterOpts = filter_root.getElementsByTagName('opt');

						if (!document.getElementById("keyword_id_" + filterNumber)) {
							
							// If need be add and populate a new row
							// Don't bother to add in a new filter if there's only one result
							if ((filterOpts.length > 0 && total_results && total_results > 1) || (filterNumber == 1 && keyword_first && keyword_first.length >  0) || filterNumber < filters.length) {
								
								if ((filterNumber-1)%filtersPerBlock == 0) {
									var curBlock = document.getElementsByClassName('filter-node').length;
									for (i = 0; i < filtersPerBlock; i++) {
										var newFilterDiv = document.createElement('div');
										newFilterDiv.id = "filter_" + ((curBlock)+i+1);
										newFilterDiv.className = "form-group filter-node";
										filterContainer.appendChild(newFilterDiv);
									} 	
								}

								var newFilter = document.getElementById('filter_' + filterNumber);
								newFilter.id = "filter_" + filterNumber;

								newSelect = document.createElement('select');
								newSelect.id = 'keyword_id_' + filterNumber;
								newSelect.className = 'form-control left';
								newSelect.onchange = function(){filterUpdate(1);this.blur(); document.body.focus();};
								newOption = document.createElement('option');
								newOption.value = 0;
								// Have to assign as innerHTML to work with IE - doesn't like adding .text
								newOption.innerHTML = (filterNumber == 1) ? 'Please select first filter' : 'Please select next filter';
								newSelect.appendChild(newOption);
								
								if (filterNumber == 1) {
									for (i = 0; i < keyword_first.length; i++) {
										newOption = document.createElement('option');
										newOption.value = keyword_first[i];
										if (filterOptSelected && filterOptSelected == newOption.value) {
											newOption.selected = 'selected';
										}
										// Have to assign as innerHTML to work with IE - doesn't like adding .text
										newOption.innerHTML = keyword_ref[newOption.value];
										newSelect.appendChild(newOption);
									}
								} else {
									for (i = 0; i < filterOpts.length; i++) {
										newOption = document.createElement('option');
										newOption.value = filterOpts[i].firstChild.data;
										if (filterOptSelected && filterOptSelected == newOption.value) {
											newOption.selected = 'selected';
										}
										// Have to assign as innerHTML to work with IE - doesn't like adding .text
										newOption.innerHTML = keyword_ref[newOption.value];
										newSelect.appendChild(newOption);
									}
								}


								newFilter.appendChild(newSelect);

								if (filterOptSelected) {
									var remImage = document.createElement('a');
									remImage.href = "";
									if(blockSort.id == "blockSortMobi") {
										remImage.className = "right iconRemove";
									}
									else {
										remImage.className = "right";
									}
									
									remImage.id = filterNumber;
									remImage.alt = "Remove filter option";
									remImage.innerHTML = "<i class='fa fa-minus-circle'></i>";
									remImage.onclick = function(e) {e.preventDefault();filterUpdate(1,this.id);}

									document.getElementById("filter_" + filterNumber).appendChild(remImage);
								}						
							}
						}
					}
				}

				if (total_results > 0) {
					results_per_page = responResults.attributes.getNamedItem("results_page").value;
					document.getElementById("search-results").innerHTML = '';
					
					document.getElementById("galType").value = responResults.attributes.getNamedItem("gallery_type").value;
					if( blockSort.id == "blockSortMobi") {
						// Set gallery type
						var sort = document.getElementById('ddFiler-mobile-search');
						for(var s=0; s<document.getElementById('ddFiler-mobile-search').children.length; s++) {
							if(document.getElementById('ddFiler-mobile-search').children[s].value == document.getElementById("galType").value) {
								document.getElementById('ddFiler-mobile-search').children[s].selected = "selected";
								break;
							}
						}
						// Set sort order
						var sort = document.getElementById('ddSort-mobile-search');
						for(var s=0; s<document.getElementById('ddSort-mobile-search').children.length; s++) {
							if(document.getElementById('ddSort-mobile-search').children[s].value == responResults.attributes.getNamedItem("gallery_sort").value) {
								document.getElementById('ddSort-mobile-search').children[s].selected = "selected";
								break;
							}
						}
					}
					else {
						// Set gallery type
						document.getElementById("gt_"+document.getElementById("galType").value).checked = true;
						// Set sort order
						var sortSelect = document.getElementsByName("sortOrder");
						for (i=0;i<sortSelect.length;i++) {
							if (sortSelect[i].value == responResults.attributes.getNamedItem("gallery_sort").value) {
								sortSelect[i].checked = "checked";
								break;
							}	
						}
					}

					//show paging
					var pagingContainer = document.getElementsByClassName('paging-search');
					for(var l=0; l<pagingContainer.length; l++) {
						pagingContainer[l].innerHTML = '';
						var pagingChildDivUL = document.createElement('div');
						pagingChildDivUL.className = "col-md-12 col-sm-12 right desktop marginTop";
						if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
							var tmp = jQuery(rootNode_response.getElementsByTagName('paging_content_desk')[0].getElementsByTagName('ul')[0]);
							setHTML(pagingChildDivUL,tmp);
						}
						else {
							setHTML(pagingChildDivUL,rootNode_response.getElementsByTagName('paging_content_desk')[0].innerHTML);
						}
						pagingContainer[l].appendChild(pagingChildDivUL);

						var pagingChildDivDD = document.createElement('div');
						pagingChildDivDD.className = "col-md-12 col-sm-12 col-xs-12 mobile marginTop";
						pagingChildDivDD.style = "padding-right:10px;";
						for(var k=0; k<rootNode_response.getElementsByTagName('paging_content_mobi').length;k++) {
							setHTML(pagingChildDivDD,rootNode_response.getElementsByTagName('paging_content_mobi')[k].innerHTML);
						}
						pagingContainer[l].appendChild(pagingChildDivDD);
					}

					for (i = 0; i < resultLists.length; i++) {
						if (getSQLField(resultLists[i],'IMAGE_SET') == "1") {
							image_set = true;
							viewfull = "set";
						}
						else {
							image_set = false;
							viewfull = "video";
						}

						var newset_container = document.createElement('div');
						newset_container.className = "col-md-3 col-sm-3 col-xs-6 release-pic release-pic-models";

						var image_container = document.createElement('div');
						image_container.className = "width-img";

						var image_link = document.createElement('a');
						image_link.href = '/index.cfm/model.view_gallery?gallery_id=' + getSQLField(resultLists[i],'GALLERY_ID');
						image_link.innerHTML = '<img width="260px" class="img-responsive" src="' + '/images/v2/tour_images/' + getSQLField(resultLists[i],'TOUR_PIC1_THUMB').toString().replace('tn','tn260') + '"/>';

						var image_link1 = document.createElement('a');
						image_link1.href = '/index.cfm/model.view_gallery?gallery_id=' + getSQLField(resultLists[i],'GALLERY_ID');
						image_link1.innerHTML = '<img width="260px" class="img-responsive" src="' + '/images/v2/tour_images/' + getSQLField(resultLists[i],'TOUR_PIC2_THUMB').toString().replace('tn','tn260') + '" style="opacity:0.4;margin-top: -30px;"/>';

						var image_link2 = document.createElement('a');
						image_link2.href = '/index.cfm/model.view_gallery?gallery_id=' + getSQLField(resultLists[i],'GALLERY_ID');
						image_link2.innerHTML = '<img width="260px" class="img-responsive" src="' + '/images/v2/tour_images/' + getSQLField(resultLists[i],'TOUR_PIC1_THUMB').toString().replace('tn','tn260') + '"/>';

						var image_link3 = document.createElement('a');
						image_link3.href = '/index.cfm/model.view_gallery?gallery_id=' + getSQLField(resultLists[i],'GALLERY_ID');
						image_link3.innerHTML = '<img width="260px" class="img-responsive" src="' + '/images/v2/tour_images/' + getSQLField(resultLists[i],'TOUR_PIC3_THUMB').toString().replace('tn','tn260') + '" style="opacity:0.4"/>';

						if(image_set == false) {
							image_container.appendChild(image_link1);
							image_container.appendChild(image_link2);
							image_container.appendChild(image_link3);
						}
						else {
							image_container.appendChild(image_link);
						}
						newset_container.appendChild(image_container);

						var info_container = document.createElement("div");
						info_container.className = "caption caption-updates caption-search";

						and_switch = false;
						model_names = '';
						model_multiple = false;
						
						if (getSQLField(resultLists[i],'FOURTH_MODEL_NAME')) {
							model_names = ' and <a href="/index.cfm/model.model_set?model_id=' + getSQLField(resultLists[i],'FOURTH_MODEL_ID') + '" title=""Click to view all sets of this model"">' + getSQLField(resultLists[i],'FOURTH_MODEL_NAME') + '</a>';
							and_switch = true;
							model_multiple = true;
						}
						
						if (getSQLField(resultLists[i],'THIRD_MODEL_NAME')) {
							if (and_switch == true) {
								model_names = ', <a href="/index.cfm/model.model_set?model_id=' + getSQLField(resultLists[i],'THIRD_MODEL_ID') + '" title=""Click to view all sets of this model"">' + getSQLField(resultLists[i],'THIRD_MODEL_NAME') + '</a>' + model_names;
							} else {
								model_names = ' and <a href="/index.cfm/model.model_set?model_id=' + getSQLField(resultLists[i],'THIRD_MODEL_ID') + '" title=""Click to view all sets of this model"">' + getSQLField(resultLists[i],'THIRD_MODEL_NAME') + '</a>';
								and_switch = true;
								model_multiple = true;
							}
						}
						
						if (getSQLField(resultLists[i],'SECOND_MODEL_NAME')) {
							if (and_switch == true) {
								model_names = ', <a href="/index.cfm/model.model_set?model_id=' + getSQLField(resultLists[i],'SECOND_MODEL_ID') + '" title="Click to view all sets of this model">' + getSQLField(resultLists[i],'SECOND_MODEL_NAME') + '</a>' + model_names;
							} else {
								model_names = ' and <a href="/index.cfm/model.model_set?model_id=' + getSQLField(resultLists[i],'SECOND_MODEL_ID') + '" title="Click to view all sets of this model">' + getSQLField(resultLists[i],'SECOND_MODEL_NAME') + '</a>';
								and_switch = true;
								model_multiple = true;
							}
						}
						
						model_names = '<a href="/index.cfm/model.model_set?model_id=' + getSQLField(resultLists[i],'MODEL_ID') + '" title="Click to view all sets of this model">' + getSQLField(resultLists[i],'MODEL_DISPLAY_NAME') + '</a>' + model_names;

						if(image_set == true) {
							imagecontent = '<span class="border-left-blue-model"><i class="fa fa-camera"></i>' + getSQLField(resultLists[i],'IMAGE_COUNT') + '</span>';
						}
						else {
							imagecontent = '<span class="border-left-blue-model"><i class="fa fa-film"></i>' + getSQLField(resultLists[i],'VIDEO_LENGTH') + '</span>';
						}

						var info_content = '<p class="text-dateTime">' + getSQLField(resultLists[i],'RELEASE_DATE_FORMARTED_2') + '</p>' + 
											'<p class="text-blue blue-info-model desktop">' + 
												'<span class="border-left-blue-model">' + model_names + '</span>' + 
												imagecontent +
											'</p>';
						if(getSQLField(resultLists[i],'GALLERY_RELEASE') == 0) {
							info_content += '<p class="text-center">' + 
												'<a class="btn btn-blue-model no-border" href="' + '/index.cfm/model.view_gallery?gallery_id=' + getSQLField(resultLists[i],'GALLERY_ID') + '">View full' + viewfull + '</a>' + 	
											'</p>';
						}
						else {
							curDate = new Date();
							if (getSQLField(resultLists[i],'FT_PREF_ID') && getSQLField(resultLists[i],'FT_MONTH') == curDate.getMonth() + 1 && getSQLField(resultLists[i],'FT_YEAR') == curDate.getFullYear()) {
								info_content += '<p class="text-center">' +
													'<div class="fastrack-text">' + 
														'<a href="' + '/index.cfm/model.view_gallery?gallery_id=' + getSQLField(resultLists[i],'GALLERY_ID') + '" alt="Click to view this fasttracked set">Fasttrack This Set<i class="fa fa-fast-forward"></i></a>' +
													'</div>' +
												'</p>';
							}
							else if (getSQLField(resultLists[i],'PFT_PREF_ID')) {
								info_content += '<p class="text-center">' +
													'<div class="fastrack-text">' + 
														'<a href="' + '/index.cfm/model.view_gallery?gallery_id=' + getSQLField(resultLists[i],'GALLERY_ID') + '" alt="Click to view this fasttracked set">Permanent Fasttrack This Set<i class="fa fa-fast-forward"></i></a>' +
													'</div>' +
												'</p>';
							}
							else {
								if (getSQLField(resultLists[i],'GALLERY_TYPE') == 7) {
									info_content += '<p class="text-center">' + '<a class="btn btn-blue-model no-border" href="' + '/index.cfm/model.view_gallery?gallery_id=' + getSQLField(resultLists[i],'GALLERY_ID') + '">Prestige set</a>' + 	
													'</p>';
														
								}
								else {
									if (sets_root.attributes.getNamedItem("allow_fasttrack").value == 'TRUE') {
										info_content += '<p class="text-center">' +
															'<div class="fastrack-text">' + 
																'<a href="" onclick="function(){fastrackRequest(' + sets_root.attributes.getNamedItem("pref_id").value + ',' + getSQLField(resultLists[i],'GALLERY_ID') + ');}' + '" alt="Click to view this fasttrack set">Fasttrack This Set<i class="fa fa-fast-forward"></i></a>' +
															'</div>' +
														'</p>';
									}
									if (sets_root.attributes.getNamedItem("allow_permfasttrack").value == 'TRUE') {
										info_content += '<p class="text-center marginTop5">' +
															'<div class="fastrack-text">' + 
																'<a href="" onclick="function(){permFastrackRequest(' + sets_root.attributes.getNamedItem("pref_id").value + ',' + getSQLField(resultLists[i],'GALLERY_ID') + ');}' + '" alt="Click here to permanently fasttrack this set">Permanent Fasttrack This Set<i class="fa fa-fast-forward"></i></a>' +
															'</div>' +
														'</p>';
									}
									else {
										info_content += '<p class="text-center marginTop5">' +
															'<div class="fastrack-text">' + 
																'<a href="http://members.onlytease.com/members/help.cfm?type=PFT" alt="Click here to buy permanent fasttracks">Permanent Fasttrack This Set<i class="fa fa-fast-forward"></i></a>' +
															'</div>' +
														'</p>';
									}
								}
							}

						}
											

						info_container.innerHTML = info_content;
						newset_container.appendChild(info_container);
						document.getElementById("search-results").appendChild(newset_container);
					}

				}
				else {
					var pagingContainer = document.getElementsByClassName('paging-search');
					for(var le=0; le<pagingContainer.length; le++) {
						pagingContainer[le].innerHTML = '';
					}
					var rs_Node = document.getElementById("search-results");
					rs_Node.innerHTML = '<div class="col-md-12 col-sm-12 col-xs-12 rs-text">Please select from the categories above to search Only-Tease by category</div>';
				}
			}
			else {
				var pagingContainer = document.getElementsByClassName('paging-search');
				for(var le=0; le<pagingContainer.length; le++) {
					pagingContainer[le].innerHTML = '';
				}
				var rs_Node = document.getElementById("search-results");
				rs_Node.innerHTML = '<div class="col-md-12 col-sm-12 col-xs-12 rs-text">Currently there are no sets matching the selected filter</div>';
			}
		}
		else {
			var rs_Node = document.getElementById("search-results");
			rs_Node.innerHTML = '<div class="col-md-12 col-sm-12 col-xs-12 rs-text">Currently there are no sets matching the selected filter</div>';
		}
		if (hideLoading) {
			document.getElementById('loadingDiv-search').style.display = 'none';
			document.getElementById('search-results').style.display = 'block';
		}
	}
}

function getSQLField(dataSet,field) {
	if (dataSet.getElementsByTagName(field).item(0).firstChild) {
		return dataSet.getElementsByTagName(field).item(0).firstChild.data;
	} else {
		return false;
	}
	return false;
}

function setHTML(parent,child) {
	jQuery(parent).html(child);
}