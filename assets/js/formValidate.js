function validateEmailAddress(obj, formObj, formValue) {
	var regex = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
	return regex.test(formValue);
}