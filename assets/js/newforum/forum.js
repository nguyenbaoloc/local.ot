function disp_prompt() {
	var check=0;
	while (check==0) {
		check=1;
		var url=prompt("Please enter the URL to link to","http://");
		if (url!=null) {
			var title=prompt("Please enter the title of your link (the text that displays in the post)","");
			if (title!=null) {	
				if (url!="" && url!="http://" && title!="") {
					var text = document.getElementById('Body').value;
					document.getElementById('Body').value = text + ' <a href="'+url+'" target="_blank">'+title+'</a>';
				}
				else {
					alert('An invalid URL or no title for you link has been provided, please try again');
					check=0;
				}
			}	
		}
	}
}
var Form=document.forms[0];
function formCheck(){       
    if (Form.Author.value == ""){
        alert("Please put in your Name.");
        Form.Author.focus();
        return false;
    }
	if (Form.Email.value == ""){
        alert("Please include your Email Address.");
		Form.Email.focus();
        return false;
	}
    else if (!IsEmail(Form.Email)){
		Form.Email.focus();
        return false;
	}
	if (Form.Subject.value == ""){
        alert("Please put in a Subject for this message.");
		Form.Subject.focus();
        return false;
	}
	if (Form.Body.value == ""){
        alert("Please put in some Comments for this message.");
		Form.Body.focus();
        return false;
	}
}
function IsEmail(field){
    var emailStr=field.value
    var checkTLD=1;
    var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
    var emailPat=/^(.+)@(.+)$/;
    var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
    var validChars="\[^\\s" + specialChars + "\]";
    var quotedUser="(\"[^\"]*\")";
    var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
    var atom=validChars + '+';
    var word="(" + atom + "|" + quotedUser + ")";
    var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
    var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
    var matchArray=emailStr.match(emailPat);
    if (matchArray==null) {
        alert("Email address seems incorrect (check @ and .'s)");
        return false;
    }
    var user=matchArray[1];
    var domain=matchArray[2];
    for (i=0; i<user.length; i++) {
        if (user.charCodeAt(i)>127) {
            alert("Ths username contains invalid characters.");
            return false;
        }
    }
    for (i=0; i<domain.length; i++) {
        if (domain.charCodeAt(i)>127) {
            alert("Ths domain name contains invalid characters.");
            return false;
        }
    }
    if (user.match(userPat)==null) {
        alert("The username doesn't seem to be valid.");
        return false;
    }
    var IPArray=domain.match(ipDomainPat);
    if (IPArray!=null) {
        for (var i=1;i<=4;i++) {
            if (IPArray[i]>255) {
                alert("Destination IP address is invalid!");
                return false;
            }
        }
    }
    var atomPat=new RegExp("^" + atom + "$");
    var domArr=domain.split(".");
    var len=domArr.length;
    for (i=0;i<len;i++) {
        if (domArr[i].search(atomPat)==-1) {
            alert("The domain name does not seem to be valid.");
            return false;
        }
    }
    if (checkTLD && domArr[domArr.length-1].length!=2 && domArr[domArr.length-1].search(knownDomsPat)==-1) {
        alert("The address must end in a well-known domain or two letter " + "country.");
        return false;
    }
    if (len<2) {
        alert("This address is missing a hostname!");
        return false;
    }
	return true;
}