/*
 * Packer
 * Javascript Compressor
 * http://dean.edwards.name/
 * http://www.smallsharptools.com/Projects/Packer/
 */
// slider_c.js
var newSlider = new Class({
    Implements: [Options, Events],
    options: {
        width: "",
        height: "",
        itemWidth: "",
        itemHeight: "",
        itemPadding: 0,
        itemBorderWidth: 0,
        itemBorderColour: "",
        handleWidth: "",
        handleHeight: "",
        handleLeftImage: "",
        handleRightImage: "",
        handleLeftImageOver: "",
        handleRightImageOver: "",
        handleBackground: false,
        shiftAmount: 1,
        imageNameAppend: "",
        targetDiv: "",
        name: "",
        onclickLink: "",
        forceInitialItemClick: false,
        clickFunction: function () {},
        addFunction: function () {}
    },
    initialize: function (A) {
        this.setOptions(A);
        this.itemArray = [];
        this.currentPosition = 0;
        this.currentDisplayWidth = 0;
        this.itemCount = 0;
        this.currentlyShifting = false;
        this.availableVisible = ((this.options.width.toInt()) / (this.options.itemWidth.toInt() + this.options.itemPadding.toInt() + (this.options.itemBorderWidth.toInt() * 2)));
        if ($(this.options.targetDiv).getStyle("position") != "absolute" && $(this.options.targetDiv).getStyle("position") != "relative") {
            $(this.options.targetDiv).setStyle("position", "relative")
        }
        var E = new Image();
        E.src = this.options.handleLeftImage;
        if (this.options.handleLeftImageOver != "") {
            var D = new Image();
            D.src = this.options.handleLeftImageOver
        }
        var G = new Image();
        G.src = this.options.handleRightImage;
        if (this.options.handleRightImageOver != "") {
            var F = new Image();
            F.src = this.options.handleRightImageOver
        }
        this.slideContainer = new Element("DIV", {
            styles: {
                width: this.options.width,
                height: this.options.height + (this.options.itemBorderWidth * 2),
                position: "relative"
            },
            id: "slideContainer_" + this.options.name
        });
        var B = new Element("DIV", {
            styles: {
                width: this.options.handleWidth,
                height: this.options.handleHeight,
                position: "absolute",
                top: "0",
                "z-index": "2",
                background: "#ddd",
                opacity: "0.3"
            }
        });
        if (this.options.handleBackground) {
            this.leftSlideNavBacker = B.clone().cloneEvents(B);
            this.rightSlideNavBacker = B.clone().cloneEvents(B);
            this.leftSlideNavBacker.set({
                styles: {
                    left: "0",
                    display: "none"
                }
            });
            this.rightSlideNavBacker.set({
                styles: {
                    right: "0"
                }
            })
        }
        var C = new Element("DIV", {
            styles: {
                width: this.options.handleWidth,
                height: this.options.handleHeight,
                position: "absolute",
                top: "0",
                "z-index": "3"
            },
            id: "slideNavHolder_" + this.options.name
        });
        this.leftSlideNav = C.clone().cloneEvents(C);
        this.rightSlideNav = C.clone().cloneEvents(C);
        this.leftSlideNav.set({
            styles: {
                left: -40,
                background: "url(" + this.options.handleLeftImage + ") no-repeat center center",
                display: "none",
                opacity: 0.5
            },
            id: "leftNav_" + this.options.name
        });
        $(this.leftSlideNav).store("classInstance", this);
        $(this.leftSlideNav).addEvent("click", function () {
            $(this).retrieve("classInstance").shiftBar(-this.retrieve("classInstance").options.shiftAmount)
        });
        $(this.leftSlideNav).addEvent("mouseover", function () {
            $(this).setStyle("background", "url(" + this.retrieve("classInstance").options.handleLeftImageOver + ") no-repeat center center");
            $(this).setStyle("opacity","1");
        });
        $(this.leftSlideNav).addEvent("mouseout", function () {
            $(this).setStyle("background", "url(" + this.retrieve("classInstance").options.handleLeftImage + ") no-repeat center center");
            $(this).setStyle("opacity","0.5");
        });
        this.rightSlideNav.set({
            styles: {
                // left: this.options.width - this.options.handleWidth,
                left: 794,
                background: "url(" + this.options.handleRightImage + ") no-repeat center center",
                opacity: 0.5
            },
            id: "rightNav_" + this.options.name
        });
        $(this.rightSlideNav).store("classInstance", this);
        $(this.rightSlideNav).addEvent("click", function () {
            $(this).retrieve("classInstance").shiftBar(this.retrieve("classInstance").options.shiftAmount)
        });
        $(this.rightSlideNav).addEvent("mouseover", function () {
            $(this).setStyle("background", "url(" + this.retrieve("classInstance").options.handleRightImageOver + ") no-repeat center center");
            $(this).setStyle("opacity","1");
        });
        $(this.rightSlideNav).addEvent("mouseout", function () {
            $(this).setStyle("background", "url(" + this.retrieve("classInstance").options.handleRightImage + ") no-repeat center center");
            $(this).setStyle("opacity","0.5");
        });
        this.itemContainer = new Element("DIV", {
            styles: {
                width: this.options.width,
                height: this.options.height + (this.options.itemBorderWidth * 2),
                position: "absolute",
                left: "0",
                top: "0",
                "z-index": 1,
                overflow: "hidden"
            },
            id: "itemContainer_" + this.options.name
        });
        this.itemHolder = new Element("DIV", {
            styles: {
                width: "0",
                height: this.options.height + (this.options.itemBorderWidth * 2),
                position: "relative",
                top: "0",
                left: "0"
            },
            id: "itemHolder_" + this.options.name
        });
        this.itemContainer.appendChild(this.itemHolder);
        this.slideContainer.appendChild(this.leftSlideNav);
        this.slideContainer.appendChild(this.itemContainer);
        this.slideContainer.appendChild(this.rightSlideNav);
        if (this.options.handleBackground) {
            this.slideContainer.appendChild(this.leftSlideNavBacker);
            this.slideContainer.appendChild(this.rightSlideNavBacker)
        }
        $(this.options.targetDiv).appendChild(this.slideContainer);
        this.slideFx = new Fx.Morph(this.itemHolder, {
            duration: 500,
            link: "chain"
        });
        this.slideFx.options.transition = Fx.Transitions.Linear;
        this.slideFx.options.classInstance = this;
        this.slideFx.addEvent("complete", function () {
            this.options.classInstance.currentlyShifting = false
        })
    },
    addItem: function (C, G, A, B,overlayUrl) {
        this.itemArray[this.itemArray.length] = [];
        this.itemCount = this.itemArray.length;
        var F = this.itemArray[this.itemArray.length - 1];
        F.name = C;
        F.image = G;
        F.caption = A;
        F.url = B;
        this.currentDisplayWidth += this.options.itemWidth + this.options.itemPadding + (this.options.itemBorderWidth * 2);
        
		var newDiv = new Element("DIV",{
			styles:{
				width:this.options.itemWidth,
				height:this.options.itemHeight,
				marginLeft:this.options.itemPadding.toInt()/2,
				marginRight:this.options.itemPadding.toInt()/2,
				"float":"left",
				border:this.options.itemBorderWidth+"px solid " + this.options.itemBorderColour,
				cursor:"pointer",
				background:"url("+G + this.options.imageNameAppend +") center top no-repeat",
                "background-size": "100% 100%"
			}
		});
		
		var newImg = new Element("IMG",{
			src:overlayUrl,
			border:0,
			width:this.options.itemWidth,
			height:this.options.itemHeight
		});
		if (B!="") {
			var newLink=new Element("A",{
				href:B,
				target:"_blank"
			});
			newLink.appendChild(newImg);
			newDiv.appendChild(newLink);
		}
		$("itemHolder_" + this.options.name).setStyles({
            width: this.currentDisplayWidth
        });
		$("itemHolder_" + this.options.name).appendChild(newDiv)

		/*var E = new Element("IMG", {
            styles: {
                width: this.options.itemWidth,
                height: this.options.itemHeight,
                "margin-left": this.options.itemPadding.toInt() / 2,
                "margin-right": this.options.itemPadding.toInt() / 2,
                "float": "left",
                align: "absmiddle",
                border: this.options.itemBorderWidth + "px solid " + this.options.itemBorderColour,
                cursor: "pointer"
            },
            id: "item_" + this.options.name + "_" + (this.itemArray.length - 1),
            alt: C,
            title: C,
            src: G + this.options.imageNameAppend
        });
        $(E).addEvent("click", this.options.clickFunction.bind(this));
        $(E).store("url", B);
        $("itemHolder_" + this.options.name).setStyles({
            width: this.currentDisplayWidth
        });
        if (B != "") {
            var D = new Element("A", {
                href: B,
                target: "_blank"
            });
            D.appendChild(E);
            $("itemHolder_" + this.options.name).appendChild(D)
        } else {
            $("itemHolder_" + this.options.name).appendChild(E)
        }
        if (this.options.forceInitialItemClick) {
            if (this.itemCount == 1) {
                $(E).fireEvent("click", $("item_" + this.options.name + "_" + (this.itemArray.length - 1)))
            }
        }
        this.options.addFunction(E)*/
		this.options.addFunction(newImg);
    },
    addItems: function (D) {
		
        for (var C in D) {
            this.addItem(C, D[C][0], D[C][1], D[C][2],D[C][3])
        }
    },
    getItemData: function (A) {
        return this.itemArray[A]
    },
    shiftBar: function (C) {
        if (!this.currentlyShifting) {
            this.currentlyShifting = true;
            var D = $("itemHolder_" + this.options.name).getStyle("left").toInt();
            var F = false;
            var A = false,
                B = false;
            if (C > 0) {
                if (((this.itemCount - this.currentPosition) - this.availableVisible) <= C) {
                    var E = (this.itemCount - this.currentPosition) - this.availableVisible;
                    if (E > 0) {
                        C = E;
                        if (E <= this.options.shiftAmount) {
                            B = true
                        }
                    } else {
                        F = true;
                        B = true
                    }
                }
            } else {
                if ((this.currentPosition + C) <= 0) {
                    if (this.currentPosition !== 0) {
                        C = -this.currentPosition
                    } else {
                        F = true
                    }
                    A = true
                }
            }
            D -= (C * (this.options.itemWidth.toInt() + this.options.itemPadding.toInt() + (this.options.itemBorderWidth.toInt() * 2)));
            if (A) {
                this.leftSlideNav.setStyle("display", "none");
                if (this.options.handleBackground) {
                    this.leftSlideNavBacker.setStyle("display", "none")
                }
            } else {
                this.leftSlideNav.setStyle("display", "block");
                if (this.options.handleBackground) {
                    this.leftSlideNavBacker.setStyle("display", "block")
                }
            }
            if (B) {
                this.rightSlideNav.setStyle("display", "none");
                if (this.options.handleBackground) {
                    this.rightSlideNavBacker.setStyle("display", "none")
                }
            } else {
                this.rightSlideNav.setStyle("display", "block");
                if (this.options.handleBackground) {
                    this.rightSlideNavBacker.setStyle("display", "block")
                }
            }
            if (!F) {
                this.currentPosition += C;
                this.slideFx.start({
                    left: D
                })
            } else {
                this.currentlyShifting = false
            }
        }
    }
});