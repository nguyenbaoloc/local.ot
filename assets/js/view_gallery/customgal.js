dragStartX = null;
dragStartY = null;

function js_dustbin(dragable,dustbin,event) {
	imgID = dragable.id;
	// Destroy droppable and dragable
	Droppables.remove(imgID);
	if (window.execScript) window.execScript('drag_'+imgID+'.destroy()');
		else eval('drag_'+imgID).destroy();
		
	// Delete image
	makeRequest('processes/customgal_update.cfm?imgid='+imgID+'&cgid='+document.getElementById('customgal_id').value+'&action=remove','customgal_update');
	
	// Shuffle all the images along from draggable to the end
	starti = js_findRefPoint(dragable.id);
	if (starti < posToIdRef.length-1) {
		// Only move anything if it's not the last in the array
		if (starti < posToIdRef.length-2) {
			// Cycle if there's more than 1 item to move	
			for (i = posToIdRef.length-1; i > starti + 1; i--) {
				targetEle = document.getElementById(posToIdRef[i-1]);
				sourceEle = document.getElementById(posToIdRef[i]);
				// The element at -direction in the array needs to be moved to the pos_from position
				sourceEle.style.top = targetEle.style.top;
				sourceEle.style.left = targetEle.style.left;
			}
		}
		
		// The element at -direction in the array needs to be moved to the pos_from position
		document.getElementById(posToIdRef[starti+1]).style.top = dragStartY;
		document.getElementById(posToIdRef[starti+1]).style.left = dragStartX;
		// Unset drag start
		dragStartX = null;
		dragStartY = null;
		// Remove from the reference array
		posToIdRef.splice(starti,1);
	}
	
	
	
	// Remove div
	document.getElementById(imgID).parentNode.removeChild(document.getElementById(imgID));
}


function js_startDrag(dragable) {
	dragStartX = dragable.element.style.left;
	dragStartY = dragable.element.style.top;
}

function js_revert(dragable) {
	if (dragStartX !== null && dragStartY !== null) {
		dragable.style.top = dragStartY;
		dragable.style.left = dragStartX;
	}
	dragStartX = null;
	dragStartY = null;
}

function js_findRefPoint(id) {
	for (i =  0; i < posToIdRef.length; i++) {
		if (posToIdRef[i] == id) {
			return i;	
		}
	}
}

function cg_renameSet(customgal_id) {
	// This will allow a set to be renamed
		// Get elements with names in
			nameEleMain = document.getElementById('customgal_title_main');
			nameEleSide = document.getElementById('customgal_title_side');
			
		// Popup dialogue
			newName = window.prompt('Please enter a new name for this custom set',nameEleMain.innerHTML);
		
		// Send the new name off to update the table
			if (newName) {
				// Rename
					makeRequest('processes/customgal_rename.cfm?cgid='+document.getElementById('customgal_id').value+'&newname='+newName,'customgal_update');
					nameEleMain.innerHTML = newName;
					nameEleSide.innerHTML = newName;
			}
}

/* This js _swap will push the images along - needs to shift images on page, currnetly only does in DB
*/
function js_swap(dragable,dropable) {
	// Ajax request to swap images
	makeRequest('processes/customgal_update.cfm?imgid='+dragable.id+'&imgid2='+dropable.id+'&cgid='+document.getElementById('customgal_id').value+'&action=shift','customgal_update');
	// Get vars from the dropable before starting
	dropable_top = dropable.style.top;
	dropable_left = dropable.style.left;
	
	// Find the position in the array of both elements
	pos_from = js_findRefPoint(dragable.id);
	pos_to = js_findRefPoint(dropable.id);
	
	// Set a direction to be either positive or negative.  This relates to the direction of all other elements
	
	if (pos_from > pos_to) direction = 1;
	else direction = -1;
	
	// Now cycle from to until from-(direction*2) -> only do if the from-2 * direction is bigger than 1
	jumpSize = (pos_from-pos_to)*direction;
	if (jumpSize > 1) {
		for (i = 0; i < jumpSize; i++) {
			targetEle = document.getElementById(posToIdRef[pos_to+direction*i+direction]);
			sourceEle = document.getElementById(posToIdRef[pos_to+direction*i]);
			// The element at -direction in the array needs to be moved to the pos_from position
			sourceEle.style.top = targetEle.style.top;
			sourceEle.style.left = targetEle.style.left;
		}
	}
	
	// The element at -direction in the array needs to be moved to the pos_from position
	document.getElementById(posToIdRef[pos_from-direction]).style.top = dragStartY;
	document.getElementById(posToIdRef[pos_from-direction]).style.left = dragStartX;
	
	// Finally drop the dragable into place
	dragable.style.top = dropable_top;
	dragable.style.left = dropable_left;
	
	// And update the reference array
		// Pop out the from element
	posToIdRef.splice(pos_to,0,posToIdRef.splice(pos_from,1));
	
	// Unset drag start
	dragStartX = null;
	dragStartY = null;
}

/* Below js_swap will swap the position of the two images
function js_swap(dragable,dropable) {
	// Ajax request to swap images
	makeRequest('processes/customgal_update.cfm?imgid='+dragable.id+'&imgid2='+dropable.id+'&cgid='+document.getElementById('customgal_id').value+'&action=swap','customgal_update');
	// Get vars
	dropable_top = dropable.style.top;
	dropable_left = dropable.style.left;
	// Set vars
	dragable.style.top = dropable_top;
	dragable.style.left = dropable_left;
	dropable.style.top = dragStartY;
	dropable.style.left = dragStartX;
	// Unset drag start
	dragStartX = null;
	dragStartY = null;
}
*/

function js_toggleSelAll() {
	// Get the toggle href
	selAll = document.getElementById('selAll_1');
	str_Selected = 'Select all images';
	str_Deselected = 'Deselect all images';
	
	if (selAll.innerHTML == str_Deselected) {
		// If it was clicked to deselect, set the toggle value to false, etc
		selAll.innerHTML = str_Selected;
		toggle = false;
	} else {
		// Otherwise assume select all
		selAll.innerHTML = str_Deselected;
		toggle = true;
	}
	document.getElementById('selAll_2').innerHTML = document.getElementById('selAll_1').innerHTML;
	// Get the image canvas
	canvasEle = document.getElementById('imageCanvas');
	//canvasEle.childNodes[1].childNodes[1].onclick;
	
	// Now loop over it's child divs, to get their child a, and perform the onclick
	updateString = '';
	for (i=0; i<canvasEle.childNodes.length; i++) {
		// Check that it is a div
		if (canvasEle.childNodes[i].tagName == 'DIV') {
			curImage = canvasEle.childNodes[i].id;
			if(updateString.length > 0) updateString += ',';
			updateString += curImage;
			curCheckbox = document.getElementById('img_'+curImage);
			if (curCheckbox.checked !== toggle) js_toggleCheckBoxNoAjax(curImage)
			
		}
	}
	
	checkBox = document.getElementById('img_'+imgID);
	if (checkBox.checked == true) {
		// If it is true, then insert into table
		action='add';
	} else {
		// If it is false then delete from table
		action='remove';
	}
	
	makeRequest('/members/processes/customgal_update.cfm?imgid='+updateString+'&cgid='+cgID+'&action='+action,'customgal_update');
	return false;
}

function js_toggleCheckBoxNoAjax(imgID) {
	checkBox = document.getElementById('img_'+imgID);
	checkBox.checked = !checkBox.checked;
	if (checkBox.checked) document.getElementById(imgID).className = 'imgSelected';
	else document.getElementById(imgID).className = '';
}

function js_toggleCheckBox(imgID) {
	checkBox = document.getElementById('img_'+imgID);
	checkBox.checked = !checkBox.checked;
	js_updateCustomGal(imgID)
	if (checkBox.checked) document.getElementById(imgID).className = 'imgSelected';
	else document.getElementById(imgID).className = '';
}

function js_updateCustomGal(imgID) {
	
	// Get the value of the checkbox for this image
	cgID = document.getElementById('customgal_id_ref').value;
	checkBox = document.getElementById('img_'+imgID);
	if (checkBox.checked == true) {
		// If it is true, then insert into table
		action='add';
	} else {
		// If it is false then delete from table
		action='remove';
	}
	
	makeRequest('/members/processes/customgal_update.cfm?imgid='+imgID+'&cgid='+cgID+'&action='+action,'customgal_update');
}



function js_moveDustbin() {
	if (document.getElementById("dustbin")) document.getElementById("dustbin").style.marginTop = document.body.scrollTop;
}

window.onscroll = js_moveDustbin;