<cfoutput>
<cfinclude template="head.cfm">
<body>
    #view("common/header")#
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-12 col-xs-12 column">
                <div class="col-md-9 col-sm-12 right-side pull-right">
                    #body#
                </div>
                <div class="col-md-3 col-sm-12 left-side pull-left">
                    <h5 class="text-center desktop leftside-intro-text"><b>WELCOME TO THE MEMBERS AREA</b></h5>
                    #view('common/leftside')#
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    #view('common/footer')#
</body>

</html>

</cfoutput>