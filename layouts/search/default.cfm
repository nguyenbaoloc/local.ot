<cfoutput>
<cfset request.layout = false>
<cfinclude template="/layouts/head.cfm">
<body>
    #view("common/header")#
    <div class="container">
        <div class="row clearfix">
            #body#
        </div>
    </div>
    <!-- Footer -->
    #view('common/footer')#
</body>

</html>

</cfoutput>