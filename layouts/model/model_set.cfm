<cfoutput>
<cfset request.layout = false>
<cfinclude template="/layouts/head.cfm">
<body>
    #view("common/header")#
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-12 col-xs-12 column">
                <div class="col-md-9 col-sm-12 right-side pull-right">
                    #body#
                </div>
                <div class="col-md-3 col-sm-12 left-side pull-left">
                	#view('common/leftside_model_set')#
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    #view('common/footer')#
</body>
</html>
</cfoutput>