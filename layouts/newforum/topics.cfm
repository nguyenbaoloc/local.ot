<cfoutput>
<cfset request.layout = false>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>OnlyTease.com Members Area</title>
	<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/modern-business.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/jquery.bxslider.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/style.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/style-responsive.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/assets/css/style-forum.css">
	<link rel="stylesheet" href="/assets/css/style-forumTopic.css">

	<script src="/assets/js/jquery.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/twitter/jquery.waituntilexist.js"></script>
	<script src="/assets/js/jquery-migrate-1.2.1.min.js"></script>
	<script src="/assets/js/jquery.bxslider/jquery.bxslider.js"></script>
	<script src="/assets/js/jquery.bxslider/plugins/jquery.easing.1.3.js"></script>
	<script src="/assets/js/jquery.bxslider/plugins/jquery.fitvids.js"></script>
	<script type="text/javascript" language="JavaScript" src="/assets/js/banner/dropinslideshow.js"></script>
	<script type="text/javascript" language="JavaScript" src="/assets/js/jwplayer.js"></script>
	<script type="text/javascript" language="Javascript" src="/assets/js/banner/Mootools.js"></script>
	<script type="text/javascript" language="Javascript" src="/assets/js/banner/slider_1.js"></script>
	<script type="text/javascript" language="Javascript" src="/assets/js/view_gallery/customgal.js"></script>
	<script src="/assets/js/scripts.js"></script>
</head>
<body>
    #view("common/header")#
    <div class="container">
        #body#   
    </div>
    <!-- Footer -->
    #view('common/footer')#
</body>

</html>

</cfoutput>