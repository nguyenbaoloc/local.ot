component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}

	public any function getGalleryDetail (numeric pGalleryId, string pSerial, numeric pPrefId) {
        return QueryExecute("SELECT     galleries.gallery_description,
                                        galleries.directory_name,
                                        galleries.gallery_type,
                                        galleries.release_date,
                                        DATE_FORMAT( galleries.release_date, '%D %M %Y' ) as release_date_formarted,
                                        galleries.zipfile,
                                        galleries.zipfile_size,
                                        galleries.lrg_zipfile,
                                        galleries.lrg_zipfile_size,
                                        galleries.ultra_zipfile,
                                        galleries.ultra_zipfile_size,
                                        galleries.super_zipfile,
                                        galleries.super_zipfile_size,
                                        galleries.total_votes_cast_outfit,
                                        galleries.total_votes_cast_tease,
                                        galleries.average_outfit,
                                        galleries.average_model,
                                        galleries.average_tease,
                                        galleries.serial_code,
                                        galleries.image_thumb,
                                        galleries.image_standard,
                                        galleries.image_large,
                                        galleries.image_ultra,
                                        galleries.image_super,
                                        models.model_id,
                                        models.model_display_name,
                                        models.guest_link_url,
                                        models.guest_gallery_text,
                                        videos.video_total_time AS video_length,
                                        videos.video_directory_name,
                                        videos.video_hd_res_name,
                                        videos.video_hd_res_filesize,
                                        videos.video_high_res_name,
                                        videos.video_high_res_filesize,
                                        videos.video_medium_res_name,
                                        videos.video_medium_res_filesize,
                                        videos.video_low_res_name,
                                        videos.video_low_res_filesize,
                                        videos.video_ipod_res_name,
                                        videos.video_ipod_res_filesize,
                                        videos.video_mp4_hd_name,
                                        videos.video_mp4_hd_filesize,
                                        videos.video_mp4_sd_name,
                                        videos.video_mp4_sd_filesize,
                                        videos.video_webm_name,
                                        videos.video_webm_filesize,
                                        videos.video_format,
                                        videos.video_no_clips,
                                        videos.video_flash,
                                        videos.video_string,
                                        IF (galleries.v2_video_clip_id != 9999,galleries.v2_video_clip_id,0) AS video_clip,
                                        IF ((galleries.release_date <= NOW() AND galleries.release_date IS NOT NULL),1,
                                            IF (fast_tracks.ft_gallery_id = galleries.gallery_id,2,
                                                IF (permanent_fast_tracks.pft_gallery_id = galleries.gallery_id,3,
                                                    IF ((galleries.serial_code = :serial),0,-1)
                                                    )
                                                )
                                            ) AS set_type,
                                        IF ((galleries.release_date <= NOW() AND galleries.release_date IS NOT NULL),1,
                                            IF ((galleries.serial_code = :serial),1,
                                                IF ((   fast_tracks.ft_month = MONTH(NOW())
                                                    AND fast_tracks.ft_year = YEAR(NOW())),1,
                                                    IF ((permanent_fast_tracks.pft_gallery_id = galleries.gallery_id),1,0)
                                                    )
                                                )
                                            ) AS set_display
                            FROM        galleries
                            INNER JOIN  galleries_models
                            ON          galleries.gallery_id = galleries_models.gm_gallery_id
                            AND         galleries_models.gm_precedence = 1
                            INNER JOIN  models
                            ON          galleries_models.gm_model_id = models.model_id
                            LEFT JOIN   videos
                            ON          videos.video_id = galleries.video_clip_id
                            LEFT JOIN   fast_tracks
                            ON          fast_tracks.ft_pref_id = :prefId
                            AND         fast_tracks.ft_gallery_id = galleries.gallery_id
                            AND         fast_tracks.ft_month = MONTH(NOW())
                            AND         fast_tracks.ft_year = YEAR(NOW())
                            LEFT JOIN   permanent_fast_tracks
                            ON          permanent_fast_tracks.pft_pref_id = :prefId
                            AND         permanent_fast_tracks.pft_gallery_id = galleries.gallery_id
                            WHERE       galleries.gallery_id = :galId",
                            {
                                serial = {sqltype: "cf_sql_varchar", value: pSerial},
                                prefId = {sqltype: "cf_sql_integer", value: pPrefId},
                                galId  = {sqltype: "cf_sql_integer", value: pGalleryId}
                            }
                    );
    }

    public any function getBlocked_reporter() {
        return QueryExecute("SELECT *
                            FROM    preferences_myOT
                            WHERE   pref_id = '#session.pref_id#'");
    }

    public any function qry_insert(string newcustomgal_name) {
        QueryExecute("INSERT INTO customgal (pref_id,customgal_name,customgal_date_created)
                    VALUES (#session.pref_id#,?,NOW())",
                    [{sqltype: "cf_sql_varchar", value: newcustomgal_name}]
                    );
    }

    public any function getCustomGalBy_CustomName (string pCustomGalName) {
        return QueryExecute("SELECT customgal_name,customgal_id
                            FROM customgal
                            WHERE customgal_name = ? 
                            AND pref_id = ?",
                            [   {sqltype: "cf_sql_varchar", value: pCustomGalName},
                                {sqltype: "cf_sql_integer", value: session.pref_id}
                            ]
                        );
    }

    public any function getCustomGalBy_CustomId (numeric pCustomgal) {
        return QueryExecute("SELECT customgal_name, customgal_id
                            FROM customgal
                            WHERE customgal_id =  ?",
                            [   
                                {sqltype: "cf_sql_integer", value: pCustomgal}
                            ]
                        );
    }

    public any function getCustomGalBy_PrefId (numeric pPrefId) {
        return QueryExecute("SELECT customgal_id, customgal_name
                            FROM customgal
                            WHERE customgal.pref_id = ?
                            ORDER BY customgal_name ASC",
                            [   
                                {sqltype: "cf_sql_integer", value: session.pref_id}
                            ]
                        );
    }

    public any function getGalleryImages (boolean customgal, numeric customgal_id, numeric pgallery_id) {
    	return QueryExecute("SELECT  images.image_id,
									images.filename,
									images.image_width,
									images.image_height,
									galleries.gallery_type,
									galleries.release_date
									#customgal ? ',IF (cil.customgal_id IS NOT NULL,1,0) AS in_customgal' : ''#
							FROM images
							INNER JOIN galleries
							ON		images.gallery_id = galleries.gallery_id
							#customgal ? 'LEFT JOIN customgal_image_linking_table AS cil ON cil.image_id = images.image_id AND cil.customgal_id = :customgal_id ' : ''#
							WHERE	images.gallery_id 	= :gallery_id
							AND		galleries.show_future = 1
							GROUP BY images.image_id
							ORDER BY image_order ASC",
							{
								customgal_id: {sqltype: "cf_sql_integer", value: customgal_id},
								gallery_id: {sqltype: "cf_sql_integer", value: pgallery_id}
							}
							);
    }

    public any function qry_pfts (numeric pref_id, numeric gallery_id) {
    	return QueryExecute("SELECT  pft_pref_id
							FROM 	permanent_fast_tracks
							WHERE	pft_pref_id		= ?
							AND		pft_gallery_id	= ?",
							{
								{sqltype: "cf_sql_integer", value: pref_id},
								{sqltype: "cf_sql_integer", value: gallery_id}
							}
						);
    }

    public any function qry_tracking (string dir) {
    	return QueryExecute("SELECT  tracker_dateadded
							FROM	tracker_ref
							WHERE	tracker_folder_id = ?",
							[
								{sqltype: "cf_sql_varchar", value: dir}
							]
						);
    }

    public any function qryComments (numeric gallery_id) {
    	return QueryExecute("SELECT	galleries_comments.galleries_comment_id,
									galleries_comments.galleries_comment_text,
									galleries_comments.galleries_comment_added_by,
									galleries_comments.galleries_comment_date_added,
									preferences_myOT.forum_nickname
							FROM	galleries_comments
							INNER JOIN 	preferences_myOT ON preferences_myOT.pref_id = galleries_comments.galleries_comment_added_by
							WHERE	galleries_comments.gallery_id = ?
							AND		galleries_comments.galleries_comment_active = 1
							ORDER BY	galleries_comment_date_added DESC",
							[
								{sqltype: "cf_sql_integer", value: gallery_id}
							]
						);
    }

    public any function qry_keywords (numeric gallery_id) {
    	return QueryExecute("SELECT  gallery_keywords.gallery_keyword_id,
									gallery_keywords.gallery_keyword_description
							FROM	gallery_keywords
							INNER JOIN gallery_keyword_linking_table
							ON		gallery_keyword_linking_table.gallery_keyword_id	= gallery_keywords.gallery_keyword_id
							WHERE	gallery_keyword_linking_table.gallery_id			= ?
							ORDER BY gallery_keywords.gallery_keyword_description ASC",
							[
								{sqltype: "cf_sql_integer", value: gallery_id}
							]);
    }

    public any function qry_CheckPreferences (numeric gallery_id, string galleryType, numeric pref_id) {
    	return QueryExecute("SELECT  preferences_myOT.pref_id,
									my_favourites.my_favourites_keyid,
									my_votes.mv_pref_id, my_votes.mv_model, my_votes.mv_outfit, my_votes.mv_tease, mv_datevoted,
									preferences_myOT.voted_galleries
							FROM	preferences_myOT
							LEFT JOIN my_favourites
							ON		my_favourites.my_favourites_id 			= :galId
							AND		my_favourites.my_favourites_type 		= :galType
							AND		my_favourites.my_favourites_pref_id 	= preferences_myOT.pref_id
							LEFT JOIN my_votes
							ON		my_votes.mv_gallery_id					= :galId
							AND		my_votes.mv_pref_id						= preferences_myOT.pref_id
							WHERE	preferences_myOT.pref_id				= :prefId",
							{
								galId: {sqltype: "cf_sql_integer", value: gallery_id},
								galType: {sqltype: "cf_sql_varchar", value: galleryType},
								prefId: {sqltype: "cf_sql_integer", value: pref_id}
							});
    }

    public any function qry_Reminder_check (numeric gallery_id) {
        return QueryExecute("SELECT
                                mr_keyid
                            FROM
                                my_reminders
                            WHERE
                                mr_type = 'Gallery'
                                AND mr_pref_id = #session.pref_id#
                                AND mr_id = ?",
                            [
                                {sqltype:"cf_sql_integer", value:gallery_id}
                            ]
                            );
    }

    public any function ft_history (numeric gallery_id) {
        return QueryExecute("SELECT     DATE_FORMAT( ft_date_added, '%D %M %Y' ) as ft_date_added
                            FROM        fast_tracks
                            WHERE       ft_pref_id = #session.pref_id#
                            AND         ft_gallery_id = ?
                            ORDER BY    ft_date_added DESC",
                            [
                                {sqltype:"cf_sql_integer", value:gallery_id}
                            ]
                            );
    }

    public any function qry_pfts () {
        return QueryExecute("SELECT  (preferences_myOT.permanent_fasttracks_purchased - COUNT(permanent_fast_tracks.pft_gallery_id)) AS pft_remaining
                            FROM    preferences_myOT
                            LEFT JOIN permanent_fast_tracks
                            ON permanent_fast_tracks.pft_pref_id = preferences_myOT.pref_id
                            WHERE   preferences_myOT.pref_id = #session.pref_id#"
                            );
    }

    public any function qry_pref_view () {
        return QueryExecute("SELECT     pref_view
                            FROM        preferences_myOT
                            WHERE       pref_id = '#session.pref_id#'"
                            );
    }
}