component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}

	public any function get_thread (numeric pThread) {
		return QueryExecute("SELECT 	ForumThreads.Body,
										ForumThreads.ThreadID,
										ForumThreads.DateIn,
										ForumThreads.Author, 
										ForumThreads.login_username,
										ForumThreads.posted_on,
										ForumThreads.EditCount,
							            ForumThreads.MembersOnly,
										ForumThreads.LastEdit
								FROM ForumThreads
								WHERE Thread = ?
							    ORDER BY Parent, DateIn asc",
							    [
							    	{sqltype: "cf_sql_integer", value: pThread}
							    ],
							    {
							    	datasource: application.forum_datasource
							    });
	}

	public any function check_forum (numeric pForumID) {
		return QueryExecute("SELECT AllowHTML 
							FROM ForumList
							WHERE ForumID = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: pForumID}
							    ],
							    {
							    	datasource: application.forum_datasource
							    });
	}

	public any function get_thread_name (numeric pThreadID) {
		return QueryExecute("SELECT  IF(ForumThreads.Parent = 0, ForumThreads.ThreadID, ForumThreads.Parent ) AS ActualThread
							FROM	ForumThreads
							WHERE   ForumThreads.ThreadID = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: pThreadID}
							    ],
							    {
							    	datasource: application.forum_datasource
							    });
	}


	public any function post_topic (numeric pForumID, numeric pThread, numeric pParent, string pAuthor, string pSubject, string pEmail, string pReply, string pBody, string posted_on ) {
		return QueryExecute("INSERT INTO ForumThreads 
								(DateIn,ForumID,Thread,Parent,Author,LastPost,Subject,Email,Reply,Body,login_username,fp_id,posted_on)
							VALUES (#CreateODBCDateTime(Now())#,?,?,?,?, #CreateODBCDateTime(Now())#,?,?,?,?,trim(cgi.remote_user),session.pref_id,?)",
							    [
							    	{sqltype: "cf_sql_integer", value: pForumID},
							    	{sqltype: "cf_sql_integer", value: pThread},
							    	{sqltype: "cf_sql_integer", value: pParent},
							    	{sqltype: "cf_sql_varchar", value: pAuthor},
							    	{sqltype: "cf_sql_varchar", value: pSubject},
							    	{sqltype: "cf_sql_varchar", value: pEmail},
							    	{sqltype: "cf_sql_varchar", value: pReply},
							    	{sqltype: "cf_sql_varchar", value: pBody},
							    	{sqltype: "cf_sql_varchar", value: posted_on}
							    ],
							    {
							    	datasource: application.forum_datasource
							    });
	}


	public any function update_forum_1 (numeric pForumID) {
			return QueryExecute("SELECT ThreadID FROM ForumThreads
								WHERE ForumID = ? AND Parent = 0",
								    [
								    	{sqltype: "cf_sql_integer", value: pForumID}
								    ],
								    {
								    	datasource: application.forum_datasource
								    });
		}

	public any function update_forum_2 (numeric pForumID) {
		return QueryExecute("SELECT ThreadID FROM ForumThreads
							WHERE ForumID = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: pForumID}
							    ],
							    {
							    	datasource: application.forum_datasource
							    });
	}

	public any function update_forum_3 (numeric pForumID, numeric frum1_records, numeric frum2_records) {
		return QueryExecute("UPDATE ForumList
								Set NumOfTopics=?,
						            NumOfPosts=?,
						            LastPost=#CreateODBCDateTime("#Now()#")#
							WHERE ForumID = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: pForumID},
							    	{sqltype: "cf_sql_integer", value: frum1_records},
							    	{sqltype: "cf_sql_integer", value: frum2_records}
							    ],
							    {
							    	datasource: application.forum_datasource
							    });
	}


	public any function update_forum_4 (numeric pThread) {
		return QueryExecute("UPDATE ForumThreads
								Set LastPost=#CreateODBCDateTime("#Now()#")#
							WHERE Thread = ? AND Parent = 0",
							    [
							    	{sqltype: "cf_sql_integer", value: pThread}
							    ],
							    {
							    	datasource: application.forum_datasource
							    });
	}


	public any function mail_reply (numeric pForumID) {
		return QueryExecute("SELECT ForumName FROM ForumList
							WHERE ForumID = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: pForumID}
							    ],
							    {
							    	datasource: application.forum_datasource
							    });
	}

	public any function get_settings () {
		return QueryExecute("SELECT MailServer,BaseURL FROM ForumSettings",
						    [],
						    {
						    	datasource: application.forum_datasource
						    });
	}

	public any function get_forum (numeric pForumID) {
		return QueryExecute("SELECT ForumName FROM ForumList
							WHERE ForumID = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: pForumID}
							    ],
							    {
							    	datasource: application.forum_datasource
							    });
	}

	public any function update_total_all_posts_count () {
		return QueryExecute("UPDATE	ForumPreferences
								SET		total_all_posts_count = total_all_posts_count + 1
								WHERE	fp_id = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: session.pref_id}
							    ],
							    {
							    	datasource: application.dsn_name_forum
							    });
	}

	public any function qryCheckCount () {
		return QueryExecute("SELECT	fp_id, current_posts_count
							FROM	ForumPreferences
							WHERE	fp_id = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: session.pref_id}
							    ],
							    {
							    	datasource: application.dsn_name_forum
							    });
	}

	public any function ForumPreferences_1 () {
		return QueryExecute("UPDATE	ForumPreferences
								SET		fasttracks_allowed = fasttracks_allowed + 1,
										total_forum_posts_count = total_forum_posts_count + 1,
										current_posts_count = 0
								WHERE	fp_id = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: session.pref_id}
							    ],
							    {
							    	datasource: application.dsn_name_forum
							    });
	}

	public any function insert_ForumFasttracks () {
		return QueryExecute("INSERT INTO ForumFasttracks (ff_pref_id,ff_date)
								VALUES (?,NOW())",
							    [
							    	{sqltype: "cf_sql_integer", value: session.pref_id}
							    ],
							    {
							    	datasource: application.dsn_name_forum
							    });
	}

	public any function ForumPreferences_2 () {
		return QueryExecute("UPDATE	ForumPreferences
								SET		total_forum_posts_count = total_forum_posts_count + 1,
										current_posts_count = current_posts_count + 1
								WHERE	fp_id = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: session.pref_id}
							    ],
							    {
							    	datasource: application.dsn_name_forum
							    });
	}


	public any function qry_checkPreferences (numeric pThread) {
		return QueryExecute("SELECT  fp_id, forum_posts_banned, forum_post_allowed, forum_nickname, forum_email, dynamic_nick, forum_edit_all
							FROM	ForumPreferences
							WHERE	fp_username = ?",
							    [
							    	{sqltype: "cf_sql_varchar", value: trim(cgi.remote_user)}
							    ],
							    {
							    	datasource: application.dsn_name_forum
							    });
	}

	public any function get_message (numeric pThread) {
		return QueryExecute("SELECT * FROM ForumThreads
							WHERE ThreadID = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: pThread}
							    ],
							    {
							    	datasource: application.forum_datasource
							    });
	}

	public any function get_forum_2 (numeric pForumID) {
		return QueryExecute("SELECT 	ForumList.*, ForumImport.* 
							FROM 	ForumList
							LEFT JOIN ForumImport
							ON		ForumList.ImportID = ForumImport.f_id
							WHERE ForumList.ForumID = ?",
							    [
							    	{sqltype: "cf_sql_integer", value: pForumID}
							    ],
							    {
							    	datasource: application.forum_datasource
							    });
	}

}