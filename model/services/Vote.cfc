component output="false" displayname=""  {

	public function init(){
		return this;
	}

	public any function qry_user () {
		return QueryExecute (
				"SELECT	pref_username,
						forum_nickname
				FROM 	preferences_myOT
				WHERE	pref_id = ?",
				[
					{sqltype: "cf_sql_integer", value: session.pref_id}
				]
			);
	}

	public any function qry_content(numeric gallery_id) {
		return QueryExecute(
				"SELECT	average_model, average_outfit, average_tease, total_votes_cast_model, total_votes_cast_tease
					FROM	galleries
					WHERE	gallery_id = ?",
				[
					{sqltype: "cf_sql_integer", value: gallery_id}
				]
			);
	}
	public any function qry_votes(numeric gallery_id, numeric pref_id) {
		return QueryExecute("SELECT	mv_gallery_id
							FROM	my_votes
							WHERE	mv_gallery_id 	= ?
							AND		mv_pref_id		= ?",
							[	
								{sqltype: "cf_sql_integer", value: gallery_id},
								{sqltype: "cf_sql_integer", value: pref_id}
							]
							);
	}

	public any function qry_checkpreferences(numeric pref_id) {
		return QueryExecute("SELECT	preferences_myOT.voted_galleries
							FROM	preferences_myOT
							WHERE	preferences_myOT.pref_id = ?",
							[
								{sqltype: "cf_sql_integer", value: pref_id}
							]);
	}

	public any function insertVote(numeric gallery_id,numeric pref_id,numeric total_votes_model,numeric total_votes_outfit,numeric total_votes_tease) {
		QueryExecute("INSERT INTO my_votes (
											mv_gallery_id,
											mv_pref_id,
											mv_model,
											mv_outfit,
											mv_tease,
											mv_datevoted )
									VALUES	(
												?,?,?,?,?,#createODBCDatetime(now())#
											)",
					[
						{sqltype: "cf_sql_integer", value: gallery_id},
						{sqltype: "cf_sql_integer", value: pref_id},
						{sqltype: "cf_sql_integer", value: total_votes_model},
						{sqltype: "cf_sql_integer", value: total_votes_outfit},
						{sqltype: "cf_sql_integer", value: total_votes_tease}

					]
					);
	}

	public any function qry_curren_votes (numeric gallery_id,numeric pref_id) {
		return QueryExecute("SELECT	mv_model, mv_outfit, mv_tease
							FROM	my_votes
							WHERE 	mv_gallery_id = ?
							AND 	mv_pref_id = ?",
							[
								{sqltype: "cf_sql_integer", value: gallery_id},
								{sqltype: "cf_sql_integer", value: pref_id}
							]
							);
	}


	public any function updateVote(numeric gallery_id,numeric pref_id,numeric total_votes_model,numeric total_votes_outfit,numeric total_votes_tease) {
		QueryExecute("UPDATE	my_votes
						SET		mv_outfit = ?,
								mv_model = ?,
								mv_tease = ?,
								mv_datevoted = ?
						WHERE 	mv_gallery_id = ?
						AND 	mv_pref_id = ?",
					[
						{sqltype: "cf_sql_integer", value: total_votes_outfit},
						{sqltype: "cf_sql_integer", value: total_votes_model},
						{sqltype: "cf_sql_integer", value: total_votes_tease},
						{sqltype: "cf_sql_integer", value: gallery_id},
						{sqltype: "cf_sql_integer", value: pref_id}

					]
					);
	}

	public any function updateAverages (numeric gallery_id, required numeric old_model_value,required numeric old_outfit_value,required numeric old_tease_value, numeric cast_vote=1) {
		QueryExecute("UPDATE preferences_myOT
					SET last_voted = ?
					WHERE pref_id = ?
					LIMIT 1",
					[
						{sqltype: "cf_sql_timestamp", value:CreateODBCDateTime(Now())},
						{sqltype: "cf_sql_integer", value:session.pref_id}
					]
					);

		var current_votes = QueryExecute("SELECT	total_votes_outfit,
												total_votes_cast_outfit,
												total_votes_model,
												total_votes_cast_model,
												total_votes_tease,
												total_votes_cast_tease,
												model_id,
												second_model_id,
												third_model_id,
												fourth_model_id
										FROM	galleries
										WHERE	gallery_id = ?",
										[
											{sqltype: "cf_sql_integer", value:gallery_id}
										]);
		var new_total_votes_outfit = (current_votes.total_votes_outfit - old_outfit_value) + form.total_votes_outfit;
		var new_total_votes_cast_outfit = current_votes.total_votes_cast_outfit + cast_vote;
		var new_average_outfit = new_total_votes_outfit / new_total_votes_cast_outfit;
		var new_total_votes_model = (current_votes.total_votes_model - old_model_value) + form.total_votes_model;
		var new_total_votes_cast_model = current_votes.total_votes_cast_model + cast_vote;
		var new_average_model = new_total_votes_model / new_total_votes_cast_model;

		var new_total_votes_tease = (current_votes.total_votes_tease - old_tease_value) + form.total_votes_tease;

		var qry_teaseVoteCount = QueryExecute("SELECT	COUNT(*) AS voteCount
												FROM	my_votes
												WHERE	mv_gallery_id = ?
												AND		mv_tease != 0",
												[
													{sqltype: "cf_sql_integer", value:gallery_id}
												]);
		var qry_teaseVoteSum = QueryExecute("SELECT	SUM(mv_tease) AS voteSum
											FROM	my_votes
											WHERE	mv_gallery_id = ?",
											[
												{sqltype: "cf_sql_integer", value:gallery_id}
											]);

		var new_average_tease = qry_teaseVoteSum.voteSum / qry_teaseVoteCount.voteCount;
		var new_combined_average = (new_average_outfit + new_average_model + new_average_tease) / 3;


		QueryExecute("	UPDATE	galleries
						SET		total_votes_outfit = #new_total_votes_outfit#,
								total_votes_cast_outfit = #new_total_votes_cast_outfit#,
								total_votes_model = #new_total_votes_model#,
								total_votes_cast_model = #new_total_votes_cast_model#,
								total_votes_tease = #qry_teaseVoteSum.voteSum#,
								total_votes_cast_tease = #qry_teaseVoteCount.voteCount#,
								average_outfit = #new_average_outfit#,
								average_model = #new_average_model#,
								average_tease = #new_average_tease#,
								average_combined = #new_combined_average#
						WHERE	gallery_id = ?",
					[
						{sqltype: "cf_sql_integer", value:gallery_id}
					]);


		var model_list = "#current_votes.model_id#,#current_votes.second_model_id#,#current_votes.third_model_id#,#current_votes.fourth_model_id#";

		loop list="#model_list#" index="this_model" {
			var model_current_votes = QueryExecute("SELECT	model_total_vote,
															model_votes_cast
													FROM	models
													WHERE	model_id = ?",
													[
														{sqltype: "cf_sql_integer", value:this_model}
													]);
			if(model_current_votes.recordcount eq 1) {
				var model_new_total_vote = (val(model_current_votes.model_total_vote) - old_model_value) + form.total_votes_model;
				var model_new_votes_cast = val(model_current_votes.model_votes_cast) + cast_vote;
				var model_new_average_vote = model_new_total_vote / model_new_votes_cast;

				QueryExecute("UPDATE	models
								SET		model_total_vote	= #model_new_total_vote#,
										model_votes_cast	= #model_new_votes_cast#,
										model_average_vote	= #model_new_average_vote#
								WHERE	model_id			= ?",
								[
									{sqltype: "cf_sql_integer", value:this_model}
								]
							);
			}
		}
	}
}