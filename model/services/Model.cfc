component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}
	
    public query function getModelRealease (numeric nDays) {

		
    	var str_query = "
						SELECT  galleries.gallery_id,
				            galleries.tour_pic1_thumb,
				            galleries.tour_pic2_thumb,
				            galleries.tour_pic3_thumb,
				            galleries.updates_pic_thumb,
				            galleries.mailing_list_pic_thumb,
				            galleries.model_id,
				            DATE_FORMAT( galleries.release_date, '%D %M %Y' ) as release_date_formarted,
				            galleries.release_date,
				            galleries.gallery_type,
				            galleries.gallery_description,
				            galleries.zipfile,
				            galleries.zipfile_size,
				            galleries.lrg_zipfile,
				            galleries.lrg_zipfile_size,
				            galleries.ultra_zipfile,
				            galleries.ultra_zipfile_size,
				            galleries.super_zipfile,
				            galleries.super_zipfile_size,
				            galleries.total_votes_cast_outfit,
				            galleries.total_votes_cast_tease,
				            galleries.average_outfit,
				            galleries.average_tease,
				            galleries.average_model,
				            galleries.directory_name,
				            gallery_type.gallery_type_order,
				            models.model_display_name,
				            models.guest_link_url,
				            second_model.model_display_name     AS second_model_name,
				            galleries.second_model_id,
				            third_model.model_display_name      AS third_model_name,
				            galleries.third_model_id,
				            fourth_model.model_display_name     AS fourth_model_name,
				            galleries.fourth_model_id,
				            galleries.gallery_n_images          AS image_count,
				            videos.video_total_time             AS video_length,
				            videos.video_directory_name,
				            videos.video_hd_res_name,
				            videos.video_hd_res_filesize,
				            videos.video_high_res_name,
				            videos.video_high_res_filesize,
				            videos.video_medium_res_name,
				            videos.video_medium_res_filesize,
				            videos.video_low_res_name,
				            videos.video_low_res_filesize,
				            videos.video_format,
				            videos.video_no_clips,
				            videos.video_mp4_hd_name,
				            videos.video_mp4_hd_filesize,
				            videos.video_mp4_sd_name,
				            videos.video_mp4_sd_filesize,
				            videos.video_webm_name,
				            videos.video_webm_filesize,
				            IF (galleries.v2_video_clip_id != 9999,galleries.v2_video_clip_id,0) AS video_clip
				    FROM    galleries
				    INNER JOIN gallery_type
				    ON      gallery_type.gallery_type_id    = galleries.gallery_type
				    INNER JOIN models
				    ON      models.model_id         = galleries.model_id
				    LEFT JOIN models AS second_model
				    ON      second_model.model_id   = galleries.second_model_id
				    LEFT JOIN models AS third_model
				    ON      third_model.model_id    = galleries.third_model_id
				    LEFT JOIN models AS fourth_model
				    ON      fourth_model.model_id   = galleries.fourth_model_id
				    LEFT JOIN videos
				    ON      videos.video_id         = galleries.video_clip_id
				    WHERE   galleries.release_date 	= DATE_SUB(?,INTERVAL #nDays# DAY)
				    		AND 	galleries.release_date 	IS NOT NULL
							AND		(galleries.linked_release = 0 OR (galleries.linked_release = 1 AND galleries.v2_video_clip_id != 9999))
							AND 	galleries.show_future	= 1
				    GROUP BY galleries.gallery_id
				    ORDER BY release_date DESC,gallery_type.gallery_type_order DESC,gallery_type_order DESC
					";

		return QueryExecute(str_query,
									[{ sqltype: "cf_sql_date", value:now()}],
									{
										datasource:application.dsn_name,
										cachedwithin:application.queryTimeCache
									}
								);
    }

    public query function get_pfts(numeric pref_id) {
    	return QueryExecute(
								"SELECT
									preferences_myOT.permanent_fasttracks_purchased,
									SUM(permanent_fast_tracks.pft_cost) AS cost,
									(preferences_myOT.permanent_fasttracks_purchased - SUM(permanent_fast_tracks.pft_cost)) AS pft_remaining
								FROM
									preferences_myOT
								LEFT JOIN
									permanent_fast_tracks
									ON permanent_fast_tracks.pft_pref_id = preferences_myOT.pref_id
								WHERE preferences_myOT.pref_id 	= ?
								GROUP BY preferences_myOT.pref_id
								",
								[
									{sqltype:"CF_SQL_INTEGER", value: pref_id}
								],
								{
									datasource:application.dsn_name,
									cachedwithin:application.queryTimeCache
								}
							);
    }

    public query function getLetters() {
    	return QueryExecute("SELECT model_initial FROM models GROUP BY model_initial",
    					[],
    					{
    						datasource:application.dsn_name,
    						cachedwithin:application.queryTimeCache
    					}
    				);
    }

    public query function getLatestUpdates () {
    	return QueryExecute(
								"SELECT DISTINCT
									models.model_id,
									models.model_display_name,
									models.model_shortcut,
									models.model_n_videos			 		AS video_count,
									models.model_n_galleries				AS gallery_count,
									models.model_n_images					AS image_count,
									models.model_max_gallery 				AS latest_gallery
								FROM	models
								WHERE	models.model_show_members_area		= 1
								AND 	(models.guest_link_url IS NULL OR models.guest_link_url = '')
								ORDER BY latest_gallery DESC
								LIMIT 9",
								[],
								{
									datasource:application.dsn_name,
    								cachedwithin:application.queryTimeCache
								}
							);
    }

    public query function getModelsBySearch (string letter, string models_used, string condition1, string condition2, string search_text) {
    	return QueryExecute(
								sql:"SELECT  models.model_id,
										models.model_display_name,
										models.model_shortcut,
										models.model_n_videos			 		AS video_count,
										models.model_n_galleries				AS gallery_count,
										models.model_n_images					AS image_count
								FROM	models
								WHERE	models.model_show_members_area	= 1
								#condition1#
								#condition2#
								",
								params:{
									letter: {sqltype='CF_SQL_VARCHAR', value=letter},
									search_text:  {sqltype='CF_SQL_VARCHAR', value="%#search_text#%"},
									models_used: {sqltype='CF_SQL_VARCHAR', value=models_used}
								},
								datasource:{
									datasource:application.dsn_name,
    								cachedwithin:application.queryTimeCache
								}
							);
    }

    public string function getModelNamesByIndex(numeric index, query qry_data) {
    	<!--- create the string of model names --->
		var and_switch = FALSE;
		var model_names = "";
		var model_multiple = FALSE;
		if (qry_data.fourth_model_name[index] NEQ "") {
			model_names = " and <a href=""/index.cfm/model.model_set?model_id=#qry_data.fourth_model_id#"" title=""Click to view all sets of this model"">#qry_data.fourth_model_name[index]#</a>";
			and_switch = TRUE;
			model_multiple = TRUE;
		}
		if (qry_data.third_model_name[index] NEQ "") {
			if (and_switch) {
				model_names = ", <a href=""/index.cfm/model.model_set?model_id=#qry_data.third_model_id#"" title=""Click to view all sets of this model"">#qry_data.third_model_name[index]#</a>" & model_names;
			}
			else {
				model_names = " and <a href=""/index.cfm/model.model_set?model_id=#qry_data.third_model_id#"" title=""Click to view all sets of this model"">#qry_data.third_model_name[index]#</a>";
				and_switch = TRUE;
				model_multiple = TRUE;
			}
		}
		if (qry_data.second_model_name[index] NEQ "") {
			if (and_switch) {
				model_names = ", <a href=""/index.cfm/model.model_set?model_id=#qry_data.second_model_id#"" title=""Click to view all sets of this model"">#qry_data.second_model_name[index]#</a>" & model_names;
			}
			else {
				model_names = " and <a href=""/index.cfm/model.model_set?model_id=#qry_data.second_model_id#"" title=""Click to view all sets of this model"">#qry_data.second_model_name[index]#</a>";
				and_switch = TRUE;
				model_multiple = TRUE;
			}
		}
		model_names = "<a href=""/index.cfm/model.model_set?model_id=#qry_data.model_id[index]#"" title=""Click to view all sets of this model"">#qry_data.model_display_name[index]#</a>" & model_names;
		return model_names;
    }

    public string function getModelNames(query qry_data) {
    	<!--- create the string of model names --->
		var and_switch = FALSE;
		var model_names = "";
		var model_multiple = FALSE;
		if (qry_data.fourth_model_name NEQ "") {
			model_names = " and <a href=""/index.cfm/model.model_set?model_id=#qry_data.fourth_model_id#"" title=""Click to view all sets of this model"">#qry_data.fourth_model_name#</a>";
			and_switch = TRUE;
			model_multiple = TRUE;
		}
		if (qry_data.third_model_name NEQ "") {
			if (and_switch) {
				model_names = ", <a href=""/index.cfm/model.model_set?model_id=#qry_data.third_model_id#"" title=""Click to view all sets of this model"">#qry_data.third_model_name#</a>" & model_names;
			}
			else {
				model_names = " and <a href=""/index.cfm/model.model_set?model_id=#qry_data.third_model_id#"" title=""Click to view all sets of this model"">#qry_data.third_model_name#</a>";
				and_switch = TRUE;
				model_multiple = TRUE;
			}
		}
		if (qry_data.second_model_name NEQ "") {
			if (and_switch) {
				model_names = ", <a href=""/index.cfm/model.model_set?model_id=#qry_data.second_model_id#"" title=""Click to view all sets of this model"">#qry_data.second_model_name#</a>" & model_names;
			}
			else {
				model_names = " and <a href=""/index.cfm/model.model_set?model_id=#qry_data.second_model_id#"" title=""Click to view all sets of this model"">#qry_data.second_model_name#</a>";
				and_switch = TRUE;
				model_multiple = TRUE;
			}
		}
		model_names = "<a href=""/index.cfm/model.model_set?model_id=#qry_data.model_id#"" title=""Click to view all sets of this model"">#qry_data.model_display_name#</a>" & model_names;
		return model_names;
    }

    public query function getModelById(required numeric model_id) {
    	return  QueryExecute(
    								"SELECT
    									m.model_id,
										m.model_display_name,
										m.model_shortcut,
										m.model_bust,
										m.model_waist,
										m.model_hips,
										m.model_height,
										m.model_star_sign,
										m.model_random_fact,
										m.model_members_page,
										m.model_n_videos AS video_count,
										m.model_n_galleries AS gallery_count,
										m.model_n_images AS image_count,
										c.content_page_content,
										m.model_promo_active,
										m.model_promo_url,
										m.model_promo_image,
										m.model_promo_text

									FROM
										models m

									LEFT JOIN
										content c
										ON m.model_members_page = c.content_page_id

									WHERE
										m.model_id = ?
										AND model_show_members_area = 1

									GROUP BY
										m.model_id

									LIMIT
										1",
									[
										{sqltype="CF_SQL_INTEGER", value=model_id}
									],
									{
	    								cachedwithin:application.queryTimeCache
									}
    							);
    }

    public function qry_modelLinks (string str_model_shortcut, string str_model_display_name) {
    	return QueryExecute("SELECT		IF(models_links.ml_model_aka != ?, ml_model_aka, '') AS ml_model_name,
										models_links.ml_affiliate_url,
										ot_freesites_data.sponsor_sites.sponsor_site_name,
										ot_freesites_data.sponsor_sites.sponsor_site_img_name
							FROM		models_links
							INNER JOIN	ot_freesites_data.sponsor_sites
							ON			models_links.ml_affiliate_url = ot_freesites_data.sponsor_sites.sponsor_site_link_url
							AND			ot_freesites_data.sponsor_sites.sponsor_site_suspended = 0
							AND			ot_freesites_data.sponsor_sites.sponsor_site_sponsor_approved = 1
							AND			ot_freesites_data.sponsor_sites.sponsor_site_sponsor_suspended = 0
							WHERE		models_links.ml_model_shortcut = ?
							AND			models_links.ml_suspended = 0
							ORDER BY	RAND()",
							[
								{sqltype="CF_SQL_VARCHAR", value = str_model_display_name},
								{sqltype="CF_SQL_VARCHAR", value = str_model_shortcut}
							]);
    }
}