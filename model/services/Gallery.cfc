component accessors="true" output="false" displayname=""  {
	public function init(required any fw){
		variables.fw =fw;
		return this;
	}

	public query function getAvailableMonths(string sView) {

		var condition = "";
		if (sView EQ "pictures" OR sView EQ "videos") {
			condition = "AND galleries.gallery_type ";
			if (sView EQ "pictures") {
				condition &= " NOT "; 
			}
			condition &= " IN (#application.stc_set_types.str_videos#)";
		}

		return QueryExecute(
								"SELECT  gallery_id,
										MONTH(release_date) 	AS month,
										YEAR(release_date)	AS year
								FROM	galleries
								WHERE	release_date < ?
								#sView neq 'all' ? condition : ''#
								GROUP BY YEAR(release_date), MONTH(release_date)
								ORDER BY YEAR(release_date) DESC, MONTH(release_date) DESC",
								[
									{sqltype: "CF_SQL_DATE", value: now()}
								]
							);
	}

	public query function getTypeList() {
		return QueryExecute(
							"SELECT  gallery_type_id, 
									gallery_type_display_text,
									gallery_type_picture,
									gallery_type_video
							FROM 	gallery_type
							WHERE	gallery_type_deleted = 0
							ORDER BY  gallery_type_id ASC");
	}

	public array function getAvailableMonthsByYear(string sView, numeric year) {

		var condition = "";
		if (sView EQ "pictures" OR sView EQ "videos") {
			condition = "AND galleries.gallery_type ";
			if (sView EQ "pictures") {
				condition &= " NOT "; 
			}
			condition &= " IN (#application.stc_set_types.str_videos#)";
		}

		var qResult = QueryExecute(
								"SELECT  gallery_id,
										MONTH(release_date) 	AS month,
										YEAR(release_date)	AS year
								FROM	galleries
								WHERE	YEAR(release_date) = ?
										AND release_date < ?
								#sView neq 'all' ? condition : ''#
								GROUP BY YEAR(release_date), MONTH(release_date)
								ORDER BY YEAR(release_date) DESC, MONTH(release_date) ASC",
								[
									{sqltype: "CF_SQL_INTEGER", value: year},
									{sqltype: "CF_SQL_DATE", value: now()}
								]
							);
		var lstMonths = ValueList(qResult.month);
		var arrResult = [];
		for(item in qResult) {
			if(item.year eq year) {
				for(var i =1 ; i<=12; i++) {
					var stcResult = {};
				   	stcResult.month = i;
				   	stcResult.year = item.year;
					if(ListFind(lstMonths, i)) {
					   stcResult.disabled  = 0;
					   arrResult[i] = stcResult;
					}
					else {
						stcResult.disabled  = 1;
					   arrResult[i] = stcResult;
					}
				}
			}
		}
		return arrResult;
	}
}