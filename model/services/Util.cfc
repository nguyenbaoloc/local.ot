component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}
	
    public array function InitArrayOfMonth () {
    	var arr = [
    				{number:"1",name:"JAN"},
    				{number:"2",name:"FEB"},
    				{number:"3",name:"MAR"},
    				{number:"4",name:"APR"},
    				{number:"5",name:"MAY"},
    				{number:"6",name:"JUN"},
    				{number:"7",name:"JULY"},
    				{number:"8",name:"AUG"},
    				{number:"9",name:"SEP"},
    				{number:"10",name:"OCT"},
    				{number:"11",name:"NOV"},
    				{number:"12",name:"DEC"}
    			];
    	return arr;
    }

    <!--- XSS stripping function --->
	public string function udf_xssSafeString (required string str_formString) {
		 str_xssSafeString = arguments.str_formString;
		 str_xssSafeString = REReplaceNoCase(str_xssSafeString, "<script\b[^>]*>(.*?)</script>", "", "ALL");
		 str_xssSafeString = REReplaceNoCase(str_xssSafeString, "<embed\b[^>]*>(.*?)</embed>", "", "ALL");
		 str_xssSafeString = REReplaceNoCase(str_xssSafeString, "<object\b[^>]*>(.*?)</object>", "", "ALL");

		 str_xssSafeString = ReplaceNoCase(str_xssSafeString, "javascript:", "", "ALL");
		 str_xssSafeString = ReplaceNoCase(str_xssSafeString, "jscript:", "", "ALL");
		 str_xssSafeString = ReplaceNoCase(str_xssSafeString, "vbscript:", "", "ALL");
		 str_xssSafeString = ReplaceNoCase(str_xssSafeString, "ecmascript:", "", "ALL");
		 str_xssSafeString = ReplaceNoCase(str_xssSafeString, "actionscript:", "", "ALL");
		 str_xssSafeString = ReplaceNoCase(str_xssSafeString, "vbs:", "", "ALL");

		 str_xssSafeString = Trim(str_xssSafeString);
		 return str_xssSafeString;
	}

	<!--- function to return clean string --->
	public string function udf_cleanString (required string str_string, required boolean bol_html, required boolean bol_npcs) {
		var str_cleanString = Trim(arguments.str_string);
		var i = "";
		if(arguments.bol_html){
			str_cleanString = REReplaceNoCase(str_cleanString, "<[^>]*>", " ", "ALL");
		}
		if(arguments.bol_npcs){
			for(var i=1; i<=31; i++) {
				str_cleanString = REReplace(str_cleanString, Chr(i), " ", "ALL");
			}
			while(condition="#Find('  ', str_cleanString)#"){
				str_cleanString = Replace(str_cleanString, "  ", " ", "ALL");
			}
		}
		return Trim(str_cleanString);
	}

	<!--- function to output model name(s) - added by ST --->
	public string function udf_modelNames(required numeric int_gallery_id, boolean bol_linkModels=1) {
		var str_modelNames = "";
		qry_galleryModels = QueryExecute("SELECT		models.model_id,
														models.model_display_name
											FROM		galleries_models
											INNER JOIN	models
											ON			galleries_models.gm_model_id = models.model_id
											WHERE		galleries_models.gm_gallery_id = ?
											ORDER BY	galleries_models.gm_precedence
											",
											[
												{sqltype:"CF_SQL_INTEGER",value:arguments.int_gallery_id}
											],
											{
												datasource=application.dsn_name,
												cachedwithin= application.queryTimeCache
											}
										);
		for(var item in qry_galleryModels) {
			if(arguments.bol_linkModels){
				str_modelNames &= '<a href="/index.cfm/model.model_set?type=model&model_id=#item.model_id#">#item.model_display_name#</a>';
			}
			if(qry_galleryModels.RecordCount GT 1 AND qry_galleryModels.CurrentRow LT qry_galleryModels.RecordCount) {
				if(qry_galleryModels.CurrentRow EQ qry_galleryModels.RecordCount-1)
				{
					 str_modelNames &=' &amp; ';
				}
				else {
					str_modelNames &=', ' ;
				}
			}
		}
		
		return str_modelNames;
	}

	<!--- function to output VIP model name(s) - added by ST --->
	public string function udf_vipModelNames(required numeric int_gallery_id, boolean bol_linkModels=1) {
		var str_modelNames = "";
		qry_galleryModels = QueryExecute("SELECT		models.model_id,
														models.model_display_name
											FROM		vip_data.galleries_models AS galleries_models
											INNER JOIN	models
											ON			galleries_models.gm_model_id = models.model_id
											WHERE		galleries_models.gm_gallery_id = ?
											ORDER BY	galleries_models.gm_precedence
											",
											[
												{sqltype:"CF_SQL_INTEGER",value:arguments.int_gallery_id}
											],
											{
												datasource=application.dsn_name,
												cachedwithin= application.queryTimeCache
											}
										);

		for(var item in qry_galleryModels) {
			if(arguments.bol_linkModels) {
				str_modelNames &= '<a href="/index.cfm/model.model_set?type=model&model_id=#item.model_id#">#item.model_display_name#</a>';
			}
			if(qry_galleryModels.RecordCount GT 1 AND qry_galleryModels.CurrentRow LT qry_galleryModels.RecordCount)
			{
				if(qry_galleryModels.CurrentRow EQ qry_galleryModels.RecordCount-1){
					str_modelNames &=' &amp; ';
				}else
				{
					str_modelNames &=', ';
				}
			}
		}
		
		return str_modelNames;
	}

	public array function getTickerContent() {
		var qry_otcams_status = QueryExecute(
												"SELECT
													os.otc_model_name,
													cs.supplier_name,
													cs.supplier_url

												FROM
													otcams_status os

												INNER JOIN
													cam_supplier cs
													ON os.cam_supplier_id = cs.supplier_id

												WHERE
													os.otc_status='online'"
											);
		var todayDate = Now();
		var todayYear = DatePart("yyyy", todayDate);
		var todayMth = DatePart("m", todayDate);
		var todayDay = DatePart("d", todayDate);
		var newday = 0;
		var newMth = 0;
		var newYear = 0;

		var DaysinMth = DaysInMonth(now());

		if(todayDay + 7 gt DaysinMth) {

			daysLeftInMth = DaysinMth - todayDay;
			daystoadd = 7 - daysLeftInMth;
			newday = daystoadd;

			if(todayMth neq 12) {
				newMth = todayMth + 1;
				newYear = todayYear;
			}
			else {
				newMth = 1;
				 newYear = todayYear + 1;
			}

		}
		else {

			newday = todayDay + 7;
			newMth = todayMth;
			newYear = todayYear;

		}

		var qry_showtimes = QueryExecute(
											"SELECT
												os.ots_model_name,
												os.ots_day_of_week,
												os.ots_time,
												cs.supplier_name,
												cs.supplier_url

											FROM
												otcams_showtimes os

											INNER JOIN
												cam_supplier cs
												ON os.cam_supplier_id=cs.supplier_id

											WHERE
												os.ots_date BETWEEN ? AND ?
												AND os.ots_deleted = 0
												AND cs.supplier_active = 1

											ORDER BY
												os.ots_date",
											[
												{sqltype="cf_sql_date", value="#todayYear#/#todayMth#/#todayDay#"},
												{sqltype="cf_sql_date", value="#newYear#/#newMth#/#newday#"}
											]
										);
		var ticker_content = [];

		if (qry_otcams_status.recordCount > 0){
			for (i=1; i<=qry_otcams_status.recordCount; i++){
				ticker_content.add('[''<a href="#qry_otcams_status.supplier_url[i]#" target="_blank">#qry_otcams_status.otc_model_name[i]# is now online at #qry_otcams_status.supplier_name[i]#</a>'',1]');
			}
		}

		if (qry_showtimes.recordCount > 0){
			for (i=1; i<=qry_showtimes.recordCount; i++){
				when_day = (qry_showtimes.ots_day_of_week[i] == 'Today') ? '' : 'on ' & qry_showtimes.ots_day_of_week[i];
				when_time = (qry_showtimes.ots_time[i] == '') ? '' : 'at ' & timeFormat(qry_showtimes.ots_time[i],'HH:mm') & ' GMT';
				what_site = " on " &qry_showtimes.supplier_name[i];
				ticker_content.add('[''<a href="#qry_showtimes.supplier_url[i]#" target="_blank">#qry_showtimes.ots_model_name[i]# #when_day# #when_time# #what_site#</a>'',0]');
			}
		}
		return ticker_content;
	}

	public struct function pagingConfig(numeric totalRecords, numeric curPage, numeric disCount, string addedPath, string urlTemplate, string show_type="Images") {
		//paging
		var stc_config = {};
		stc_config.TOTALRECORDS=totalRecords;
		stc_config.CURRENTPAGE=curPage;
		stc_config.TEMPLATEURL="#urlTemplate#";
		stc_config.ADDEDPATH="#addedPath#";
		stc_config.DISPLAYCOUNT=#disCount#;
		stc_config.PAGEGROUP="5";

		var Start = (stc_config.CurrentPage - 1) * stc_config.DISPLAYCOUNT + 1;
		var End = stc_config.CurrentPage * stc_config.DisplayCount;
		End = IIf(End GT stc_config.TotalRecords, stc_config.TotalRecords, End);
		var display_start_row = IIf(End, Start, 0);

		stc_config.SHOW_TYPE = show_type;
		stc_config.START_ROW = display_start_row;
		stc_config.END_ROW = End;
		//end paging

		return stc_config;
	}
}