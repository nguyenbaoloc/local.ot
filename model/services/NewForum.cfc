component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}

	public any function user_details (string remote_user) {
		return QueryExecute("SELECT 	showpublicposts,
										forum_edit_all
							FROM 	ForumPreferences
							WHERE	fp_username = ?",
							[
								{sqltype:"CF_SQL_VARCHAR", value: remote_user}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function qry_forumstitle () {
		return QueryExecute("SELECT * FROM ForumSettings",
							[],
							{
								datasource: application.forum_datasource
							});
	}

	public any function get_forums (any showpublicposts, any forum_edit_all) {
		var condition = "";
		if (forum_edit_all == "0") {    
		    condition = "Where	ImportID <>  7
	    				 AND	ForumID <> 21";
			if (showpublicposts == "1") {  
				condition &= "AND	ImportID <>  6
		    				  AND	ImportID <>  8";
			}
		}
		return QueryExecute("SELECT 	ForumList.*, ForumImport.* 
							FROM 	ForumList
							LEFT JOIN ForumImport
							ON		ForumList.ImportID = ForumImport.f_id
							#condition#
							ORDER BY ImportID, ForumOrder",
							[],
							{
								datasource: application.forum_datasource
							});
	}

	public any function qry_fasttracks (any expiredDate, string remote_user) {
		return QueryExecute("SELECT  ForumFasttracks.ff_id
							FROM	ForumPreferences
							LEFT JOIN ForumFasttracks
							ON		ForumPreferences.fp_id = ForumFasttracks.ff_pref_id
							WHERE	ForumPreferences.fp_username	= ?
							AND		ForumFasttracks.ff_allocated 	= 0
							AND		ForumFasttracks.ff_date 		> #CreateODBCDateTime(expiredDate)#
							ORDER BY ForumFasttracks.ff_date ASC",
							[
								{sqltype:"CF_SQL_VARCHAR", value: remote_user}
							],
							{
								datasource: application.dsn_name_forum
							});
	}

	public any function qry_threads (any last_post_date, numeric ForumID, any showpublicposts) {
		return QueryExecute("SELECT  ThreadID
							FROM	ForumThreads
							WHERE	ForumID = ?
							AND		LastPost > #(last_post_date eq '') ? now() : createODBCDateTime(last_post_date)# 
		                    #(showpublicposts == 1) ? ' AND ForumThreads.MembersOnly = 1' : ''#",
		                    [
		                    	{sqltype: "cf_sql_integer", value: ForumID}
		                    ],
							{
								datasource: application.forum_datasource
							});
	}

	public any function Posts (numeric ForumID) {
		return QueryExecute("SELECT	ForumThreads.ForumID,
		                    		COUNT(postedThreads.ThreadID) As PostsCount
							FROM	ForumThreads
		                    LEFT JOIN ForumThreads AS postedThreads
							ON		postedThreads.Thread = ForumThreads.Thread
							WHERE	ForumThreads.Parent = 0
		                    AND		ForumThreads.ForumID = ?
		                    AND		postedThreads.Deleted = 0
		                    GROUP BY ForumThreads.ForumID",
		                    [
		                    	{sqltype: "cf_sql_integer", value: ForumID}
		                    ],
							{
								datasource: application.forum_datasource
							});
	}

	public any function Topics (numeric ForumID) {
		return QueryExecute("SELECT  ForumThreads.ForumID,
		                    		COUNT(postedThreads.ThreadID) As TopicCount
							FROM	ForumThreads
		                    LEFT JOIN ForumThreads AS postedThreads
							ON		postedThreads.ThreadID = ForumThreads.ThreadID
							WHERE	ForumThreads.Parent = 0
		                    AND		ForumThreads.ForumID = ?
		                    AND		postedThreads.Deleted = 0
		                    GROUP BY ForumThreads.ForumID",
		                    [
		                    	{sqltype: "cf_sql_integer", value: ForumID}
		                    ],
							{
								datasource: application.forum_datasource
							});
	}

	public any function LastPost1 (numeric ForumID) {
		return QueryExecute("SELECT  ForumThreads.DateIn
							FROM	ForumThreads
		                    LEFT JOIN ForumThreads AS postedThreads
							ON		postedThreads.ThreadID = ForumThreads.ThreadID
							WHERE	ForumThreads.ForumID = ?
		                    AND		postedThreads.Deleted = 0
		                   	ORDER BY ForumThreads.DateIn desc
		                   	limit 1",
		                    [
		                    	{sqltype: "cf_sql_integer", value: ForumID}
		                    ],
							{
								datasource: application.forum_datasource
							});
	}

	public any function qry_site (string tableName) {
		return QueryExecute("SELECT  user_name
							FROM	#tableName#
							WHERE	user_name = ?",
							[
								{sqltype: "CF_SQL_VARCHAR", value: cgi.remote_user}
							],
							{ datasource: "authentication"});
	}

	public any function qry_forum_lastupdate (any pref_id) {
		return QueryExecute("SELECT  forum_check_date, forum_last_access_date
							FROM 	ForumPreferences
							WHERE	fp_id = ?",
							[
								{sqltype: "CF_SQL_INTEGER", value: pref_id}
							],
							{ datasource: application.dsn_name_forum});
	}
	
	public any function qry_forum_update_lastupdate (numeric pref_id) {
		return QueryExecute("UPDATE  ForumPreferences
							SET		forum_last_access_date  = forum_check_date,
									forum_check_date		= #createODBCDateTime(now())#
							WHERE	fp_id	 				=  ?",
							[
								{sqltype: "CF_SQL_INTEGER", value: session.pref_id}
							],
							{ datasource: application.dsn_name_forum});
	}

	public any function get_forum_settings () {
		return QueryExecute("SELECT * FROM ForumSettings",
		                    [],
							{
								datasource: application.forum_datasource
							});
	}
}