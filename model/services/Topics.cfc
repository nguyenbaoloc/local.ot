component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}

	public any function get_forums() {
		return QueryExecute("SELECT ForumID,ForumName FROM ForumList",
							[],
							{
								datasource: application.forum_datasource
							});
	}

	public any function user_details(string remote_user) {
		return QueryExecute("SELECT 	showpublicposts
					        FROM 	ForumPreferences
					        WHERE	fp_username = ? ",
					        [
					        	{sqltype: "cf_sql_varchar", value: remote_user}
					        ],
							{
								datasource: application.forum_datasource
							});
	}

	public any function get_forum(numeric forumID) {
		return QueryExecute("SELECT 	ForumList.*, ForumImport.* 
							FROM 	ForumList
							LEFT JOIN ForumImport
							ON		ForumList.ImportID = ForumImport.f_id
							WHERE ForumList.ForumID = ? ",
							[
								{sqltype:"cf_sql_integer", value:forumID}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function get_topiccount(numeric pForumID, numeric pRange, any post_before) {
		return QueryExecute("SELECT  Count(ForumThreads.ThreadID) AS count
							FROM 	ForumThreads
							WHERE 	ForumThreads.ForumID = ?
							AND 	ForumThreads.Parent = 0
						    AND		ForumThreads.Deleted = 0
							#(pRange != 0) ? ' AND 	ForumThreads.LastPost > #CreateODBCDateTime("#post_before#")#' : ''# 
							ORDER by Sticky DESC, ForumThreads.LastPost desc",
							[
								{sqltype:"cf_sql_integer", value:pForumID}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function get_topics(numeric pForumID, numeric pRange, any post_before, numeric limitstart) {
		return QueryExecute("SELECT  	DISTINCT(ForumThreads.Thread),
										ForumThreads.Sticky,
										ForumThreads.ForumID,
										ForumThreads.ThreadID,
										ForumThreads.Subject,
										ForumThreads.LastPost,
										ForumThreads.PollQuestionID,
										ForumThreads.Author,
										ForumThreads.Views,
										ForumPreferences.fp_id,
										ForumPreferences.forum_nickname,
										ForumPreferences.fut_id,
										ForumPreferences.dynamic_nick,
						                ForumPreferences.showpublicposts,
										ForumUserTypes.fut_icon,
										COUNT(postedThreads.ThreadID) - 1 AS reply_count,
										SUM(IF(postedThreads.fp_id=#session.pref_id#,1,0)) AS user_posted
								FROM 	ForumThreads
								LEFT JOIN ForumThreads AS postedThreads
								ON		postedThreads.Thread = ForumThreads.Thread
								LEFT JOIN ForumPreferences
								ON		ForumThreads.fp_id = ForumPreferences.fp_id
								LEFT JOIN ForumUserTypes
								ON		ForumPreferences.fut_id = ForumUserTypes.fut_id
								WHERE 	ForumThreads.ForumID = ?
								AND 	ForumThreads.Parent = 0
						        AND		postedThreads.Deleted = 0
								#(pRange != 0) ? ' AND 	(ForumThreads.LastPost > #CreateODBCDateTime("#post_before#")# OR ForumThreads.Sticky = 1)' : '' #
							 	GROUP BY postedThreads.Thread
								ORDER by ForumThreads.Sticky DESC, ForumThreads.LastPost DESC
								LIMIT #limitstart#, #application.forum_threads_shown#",
							[
								{sqltype:"cf_sql_integer", value:pForumID}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function qry_fasttracks (any expiredDate, string remote_user) {
		return QueryExecute("SELECT  ForumFasttracks.ff_id
							FROM	ForumPreferences
							LEFT JOIN ForumFasttracks
							ON		ForumPreferences.fp_id = ForumFasttracks.ff_pref_id
							WHERE	ForumPreferences.fp_username	= ?
							AND		ForumFasttracks.ff_allocated 	= 0
							AND		ForumFasttracks.ff_date 		> #CreateODBCDateTime(expiredDate)#
							ORDER BY ForumFasttracks.ff_date ASC",
							[
								{sqltype:"CF_SQL_VARCHAR", value: remote_user}
							],
							{
								datasource: application.dsn_name_forum
							});
	}

	public any function LastPost1 (numeric pForumID, numeric pThreadId) {
		return QueryExecute("SELECT  	ForumThreads.DateIn,
	                    				ForumThreads.Parent
							FROM	ForumThreads
		                    LEFT JOIN ForumThreads AS postedThreads
							ON		postedThreads.ThreadID = ForumThreads.ThreadID
							WHERE	ForumThreads.ForumID = ?
		                    AND		ForumThreads.Parent = ?
		                    AND		postedThreads.Deleted = 0
		                    ORDER BY ForumThreads.DateIn DESC
		                    Limit 1",
		                    [
		                    	{sqltype: "cf_sql_integer", value: pForumID},
		                    	{sqltype: "cf_sql_integer", value: pThreadId}
		                    ],
							{
								datasource: application.forum_datasource
							});
	}

	public any function thread_query () {
		return QueryExecute("SELECT MAX(Thread) AS n FROM ForumThreads",
							[],
							{
								datasource: application.forum_datasource
							});
	}

	public any function check_forum (numeric ForumID) {
		return QueryExecute("SELECT AllowHTML FROM ForumList
							WHERE ForumID = ?",
							[
								{sqltype: "cf_sql_integer", value: ForumID}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function qry_CheckShortcut (string str_CheckShortcut) {
		return QueryExecute("SELECT  *
							FROM 	ForumThreads
							WHERE 	ForumThreads.Parent = 0
							AND	PUBLIC_SHORTCUT = ?",
							[
								{sqltype: "cf_sql_varchar", value: str_CheckShortcut}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function insertQuestion(string poll_question) {
		return QueryExecute("INSERT INTO		forum_poll_questions
							(Question)
							VALUES (?)",
							[
								{sqltype: "cf_sql_varchar", value: poll_question}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function insertAnswers(numeric questionID, string questionAnswer) {
		return QueryExecute("INSERT INTO		forum_poll_answers
							(QuestionID,Answer)
							VALUES (?,?)",
							[
								{sqltype: "cf_sql_integer", value: questionID},
								{sqltype: "cf_sql_varchar", value: questionAnswer}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function getQuestionID(string poll_question) {
		return QueryExecute("SELECT		QuestionID
							FROM		forum_poll_questions
							WHERE		Question = ?",
							[
								{sqltype: "cf_sql_varchar", value: poll_question}
							],
							{
								datasource: application.forum_datasource
							});
	}
	
	public any function post_topic (numeric forumID, 
									numeric thread, 
									string author, 
									string subject, 
									string email, 
									string reply, 
									string body, 
									string remote_user, 
									numeric pref_id,
									string posted_on, 
									any MembersOnly,
									string str_Shortcut, 
									numeric QuestionID,
									string poll_question) {
		return QueryExecute("INSERT INTO ForumThreads
								(DateIn,
								ForumID,
								Thread, 
								Author,
								LastPost,
								Subject,
								Email,
								Reply,
								Body,
								login_username,
								fp_id,posted_on,
								MembersOnly,
								PUBLIC_SHORTCUT,
								PollQuestionID)
							VALUES (#CreateODBCDateTime("#Now()#")#,
								?,
								?,
								?, 
								#CreateODBCDateTime("#Now()#")#,
								?,
								?,
								?,
								?,
								?,
								?,
								?,
								?,
								?,
								?)",
							[
								{sqltype: "cf_sql_integer", value: forumID},
								{sqltype: "cf_sql_integer", value: thread},
								{sqltype: "cf_sql_varchar", value: author},
								{sqltype: "cf_sql_varchar", value: subject},
								{sqltype: "cf_sql_varchar", value: email},
								{sqltype: "cf_sql_varchar", value: reply},
								{sqltype: "CF_SQL_LONGVARCHAR", value: body},
								{sqltype: "cf_sql_varchar", value: remote_user},
								{sqltype: "cf_sql_integer", value: pref_id},
								{sqltype: "cf_sql_varchar", value: posted_on},
								{sqltype: "cf_sql_tinyint", value: MembersOnly},
								{sqltype: "cf_sql_varchar", value: str_Shortcut},
								{sqltype: "cf_sql_integer", value: poll_question NEQ "" ? QuestionID : 0}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function update_forum_1(numeric ForumID) {
		return QueryExecute("SELECT ThreadID 
							FROM ForumThreads
							WHERE ForumID = ? AND Parent = 0",
							[
								{sqltype: "cf_sql_integer", value: ForumID}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function update_forum_2(numeric ForumID) {
		return QueryExecute("SELECT ThreadID 
							FROM ForumThreads
							WHERE ForumID = ?",
							[
								{sqltype: "cf_sql_integer", value: ForumID}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function update_forum_3(numeric forum_rd_1, numeric forum_rd_2, numeric ForumID) {
		return QueryExecute("UPDATE ForumList
							Set NumOfTopics=?,
								NumOfPosts=?,
								LastPost=#CreateODBCDateTime("#Now()#")#
							WHERE ForumID = ?",
							[
								{sqltype: "cf_sql_integer", value: forum_rd_1},
								{sqltype: "cf_sql_integer", value: forum_rd_2},
								{sqltype: "cf_sql_integer", value: ForumID}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function updateForumPreferences () {
		return QueryExecute("UPDATE	ForumPreferences
							SET		total_all_posts_count = total_all_posts_count + 1
							WHERE	fp_id = ?",
							[
								{sqltype: "cf_sql_integer", value: session.pref_id},
							],
							{
								datasource: application.dsn_name_forum
							});
	}

	public any function qryCheckCount() {
		return QueryExecute ("SELECT	fp_id, 
										current_posts_count
								FROM	ForumPreferences
								WHERE	fp_id = ?",
								[
									{sqltype: "cf_sql_integer", value: session.pref_id},
								],
								{
									datasource: application.dsn_name_forum
								});
	}

	public any function updateForumPreferences_2 () {
		return QueryExecute("UPDATE	ForumPreferences
							SET		fasttracks_allowed = fasttracks_allowed + 1,
									total_forum_posts_count = total_forum_posts_count + 1,
									current_posts_count = 0
							WHERE	fp_id = ?",
							[
								{sqltype: "cf_sql_integer", value: session.pref_id},
							],
							{
								datasource: application.dsn_name_forum
							});
	}

	public any function insertForumFasttracks () {
		return QueryExecute("INSERT INTO ForumFasttracks (ff_pref_id,ff_date)
								VALUES (?,NOW())",
							[
								{sqltype: "cf_sql_integer", value: session.pref_id},
							],
							{
								datasource: application.dsn_name_forum
							});
	}

	public any function updateForumPreferences_3 () {
		return QueryExecute("UPDATE	ForumPreferences
							SET		total_forum_posts_count = total_forum_posts_count + 1,
									current_posts_count = current_posts_count + 1
							WHERE	fp_id = ?",
							[
								{sqltype: "cf_sql_integer", value: session.pref_id},
							],
							{
								datasource: application.dsn_name_forum
							});
	}

	public any function qry_checkPreferences (string remote_user) {
		return QueryExecute("SELECT  fp_id, 
									forum_posts_banned, 
									forum_post_allowed, 
									forum_nickname, 
									forum_email, 
									dynamic_nick, 
									forum_edit_all
							FROM	ForumPreferences
							WHERE	fp_username = ?",
							[
								{sqltype: "cf_sql_varchar", value: remote_user},
							],
							{
								datasource: application.dsn_name_forum
							});
	}

	public any function paging(struct config) {

		var MaxPages = ceiling(config.TotalRecords / config.DisplayCount);

		var next = (config.CurrentPage + 1) * 1;
		var prev = (config.CurrentPage - 1) * 1;
		if(next >= MaxPages) {
			next = MaxPages;
		}
		if(prev <= 1) {
			prev = 1;
		}

		var PageStr = 	'<div class="block-text-topic">
							<div class="text-paging">#config.show_type# #config.start_row# to #config.end_row# of #config.TotalRecords#</div>
						</div>
						<div class="block-select-page">
							<ul class="list-unstyled pagination pagination-custom">
								<li class="text-pink text-pink-page">Page</li>';
		if(config.CurrentPage > 1) {
			PageStr &= '<li><a class="text-pink" href="#config.TEMPLATEURL##config.ADDEDPATH#&CurrentPage=#prev#"><i class="fa fa-angle-left"></i></a></li>';
		}
		PageStr &= 	'<li class="paddingLeft paddingRight"><select class="number-pages" onChange="if (options[selectedIndex].value) { location = options[selectedIndex].value }">';
		if (MaxPages GT 1) {
			for (var count="1"; count <="#MaxPages#"; count ++) {
				PageStr = "#PageStr# <option #Count IS config.CurrentPage ? 'selected="selected"' : ''# value='#config.TEMPLATEURL##config.ADDEDPATH#&CurrentPage=#Count#'>#Count#</option> ";
			}
			PageStr &= '</select></li>';
			if(config.CurrentPage < MaxPages) {
				PageStr &= '<li><a class="text-pink" href="#config.TEMPLATEURL##config.ADDEDPATH#&CurrentPage=#next#"><i class="fa fa-angle-right"></i></a></li>';
	    	}
	    	PageStr &= '</ul></div>';
		}
		else {
			PageStr = "";
		}
		return PageStr;
    }
}