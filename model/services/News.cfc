component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}
	
    public query function getLatestNews () {
    	qryNews = QueryExecute("
							        SELECT  item_sticky,
							                item_content,
							                item_optional_link,
							                item_link_target,
							                item_release_date
							        FROM    latest_news
							        WHERE   item_type = 1
							        AND     item_active = 1
							        AND     item_ot     = 1
							        ORDER   BY item_sticky DESC, item_release_date DESC
							        LIMIT   10",
							        {
							        	cachedwithin:application.queryTimeCache
							        }
							    );
    	return qryNews;
    }

}