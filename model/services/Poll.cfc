component accessors="true" output="false" displayname=""  {

	public query function getCurrentID () {
		return QueryExecute("
								SELECT 	model_questions.QuestionID
								FROM 	model_questions
								ORDER 	BY QuestionID DESC
								LIMIT 	1",
								{},
								{
									datasource=application.dsn_name,
    								cachedwithin:application.queryTimeCache
								}
							);
	}

	public query function getVoted(numeric questionID) {
		return QueryExecute("
								SELECT  mmp_date_added
								FROM	my_model_polls
								WHERE	mmp_pref_id		= 0
								AND		mmp_question_id	= :id",
								{
									id: {sqltype="CF_SQL_INTEGER", value=questionID}
								},
								{
									datasource:application.dsn_name,
    								cachedwithin:application.queryTimeCache
								}
							);
	}

	public query function getTotals (numeric questionID) {
		return QueryExecute("
								SELECT 	model_questions.Question, 
									SUM(model_answers.Votes) AS TotalVotes 
								FROM 	model_questions 
								INNER JOIN model_answers
								ON 		model_questions.QuestionID = model_answers.QuestionID 
								WHERE 	model_questions.QuestionID = :id
					    		GROUP BY model_questions.Question ",
					    		{
									id: {sqltype="CF_SQL_INTEGER", value=questionID}
								},
					    		{
					    			datasource:application.dsn_name,
    								cachedwithin:application.queryTimeCache
					    		}
				    		);
	}

	public query function getResults (numeric questionID) {
		return QueryExecute("
								SELECT 	model_answers.Answer, 
									model_answers.Votes 
								FROM 	model_answers 
								WHERE 	model_answers.QuestionID = :id 
								ORDER BY model_answers.AnswerID",
								{
									id: {sqltype="CF_SQL_INTEGER", value=questionID}
								},
								{
									datasource:application.dsn_name,
    								cachedwithin:application.queryTimeCache
								}
							);
	}

	public query function getQuestion (numeric questionID) {
		return QueryExecute("
								SELECT 	model_questions.QuestionID, 
										model_questions.Question, 
										model_answers.AnswerID, 
										model_answers.Answer 
								FROM 	model_questions 
								INNER JOIN model_answers 
								ON 		model_questions.QuestionID = model_answers.QuestionID 
								WHERE 	model_questions.QuestionID = :id
								ORDER 	BY model_answers.AnswerID ",
								{
									id: {sqltype="CF_SQL_INTEGER", value=questionID}
								},
								{
									datasource:application.dsn_name,
    								cachedwithin:application.queryTimeCache
								}
							);
	}
}