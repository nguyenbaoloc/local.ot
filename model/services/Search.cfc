component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}

	public any function qry_keywords() {
		return QueryExecute("SELECT gallery_keyword_id, 
									gallery_keyword_description 
							FROM gallery_keywords 
							WHERE gallery_keyword_description > '' 
							ORDER BY gallery_keyword_description ASC");
	}

	public any function qry_pfts () {
		return QueryExecute("SELECT	(preferences_myOT.permanent_fasttracks_purchased - COUNT(permanent_fast_tracks.pft_gallery_id)) AS pft_remaining
							    FROM	preferences_myOT
							    LEFT JOIN  permanent_fast_tracks
							    ON		permanent_fast_tracks.pft_pref_id 	= preferences_myOT.pref_id
							    WHERE	preferences_myOT.pref_id 			= #session.pref_id#
							    GROUP BY preferences_myOT.pref_id");
	}

	public any function qry_fts () {
		return QueryExecute("SELECT	(preferences_myOT.fasttracks_allowed - COUNT(fast_tracks.ft_gallery_id)) AS ft_remaining
						    FROM	preferences_myOT
						    LEFT JOIN  fast_tracks
						    ON		fast_tracks.ft_pref_id				= preferences_myOT.pref_id
						    AND		fast_tracks.ft_month 				= #month(now())#
						    AND		fast_tracks.ft_year 				= #year(now())#
						    WHERE	preferences_myOT.pref_id 			= #session.pref_id#
						    GROUP BY preferences_myOT.pref_id");
	}

	public any function qry_checkFilter_1 (numeric gallery_keyword_id, string gType) {
		var condition = "";
		if (gType EQ "f") {
			condition = " AND (g.release_date > NOW() OR g.release_date IS NULL)";
		}
        if (gType NEQ "a" AND url.gType NEQ "f") {
        	condition =  " AND g.gallery_type ";
        	if (gType NEQ "v") {
        		condition &= "NOT";
        	}
        	condition &= " IN (#application.video_set_types#)";
        }
		return QueryExecute("SELECT
		                        kwl.gallery_id,
		                        kwl.gallery_keyword_id
		                    FROM
		                        gallery_keyword_linking_table AS kwl
		                   	INNER JOIN
		                        gallery_keyword_linking_table AS linkCheck ON linkCheck.gallery_id = kwl.gallery_id
		                   	INNER JOIN
		                    	galleries AS g
		                        ON g.gallery_id = kwl.gallery_id
		                    WHERE
		                    	g.show_future = 1
								#condition#
		                    	AND ((g.release_date <= NOW() AND g.gallery_type = 7) OR g.gallery_type <> 7)
		                    	AND	(g.linked_release = 0 OR (g.linked_release = 1 AND g.v2_video_clip_id != 9999))
		                        AND linkCheck.gallery_keyword_id = ?
		                    ORDER BY
		                        kwl.gallery_id",

		                    [
    							{sqltype:"cf_sql_integer", value:gallery_keyword_id}
    						]
		                    );
	}
	
	public any function qry_galleries (numeric pOrder, string plist_galleries) {
		switch (pOrder) {
            case 2:
                sortOrder = " gallery_release ASC, galleries.release_date ASC, gallery_type.gallery_type_order DESC,galleries.gallery_id ASC";
            	break;
            case 3:
                sortOrder = " model_display_name ASC,gallery_type.gallery_type_order DESC,galleries.gallery_id DESC";
            	break;
            case 4:
                sortOrder = " model_display_name DESC,gallery_type.gallery_type_order DESC,galleries.gallery_id DESC";
            	break;
            case 5:
                sortOrder = " rand()";
            	break;
            default:
                sortOrder = " gallery_release ASC, galleries.release_date DESC,gallery_type.gallery_type_order DESC,galleries.gallery_id DESC";
            	break;
        }

        var stSql = "SELECT	galleries.gallery_id,
                  			IF (galleries.release_date <= NOW(),0,1) AS gallery_release
	                FROM	galleries
	                INNER JOIN gallery_type
	                ON		gallery_type.gallery_type_id	= galleries.gallery_type
	                INNER JOIN gallery_keyword_linking_table AS kwl
	                ON		kwl.gallery_id = galleries.gallery_id
	                INNER JOIN models
	                ON		models.model_id			= galleries.model_id
	                WHERE	galleries.gallery_id IN (#plist_galleries#)
					AND		models.model_show_members_area = 1
	                GROUP BY galleries.gallery_id
	                ORDER BY #sortOrder#";

		return QueryExecute (stSql);
	}

	public any function qry_gallery_data (string g_data_retrieve) {

		var stSql = "SELECT	galleries.gallery_id,
		                            galleries.tour_pic1_thumb,
		                            galleries.tour_pic2_thumb,
		                            galleries.tour_pic3_thumb,
		                            DATE_FORMAT(galleries.release_date,'%D %M %Y') AS release_date,
		                            DATE_FORMAT( galleries.release_date, '%W %D %M' ) as release_date_formarted_2,
		                            galleries.gallery_type,
		                            models.model_display_name,
		                            galleries.model_id,
		                            second_model.model_display_name 	AS second_model_name,
		                            galleries.second_model_id,
		                            third_model.model_display_name 		AS third_model_name,
		                            galleries.third_model_id,
		                            fourth_model.model_display_name 	AS fourth_model_name,
		                            galleries.fourth_model_id,
		                            IF (galleries.release_date <= NOW(),0,1) AS gallery_release,
		                            fast_tracks.ft_pref_id,
		                            fast_tracks.ft_month,
		                            fast_tracks.ft_year,
		                            permanent_fast_tracks.pft_pref_id,
		                            models.model_n_galleries			AS gallery_count,
		                            galleries.gallery_n_images			AS image_count,
		                            videos.video_total_time				AS video_length,
									videos.video_hd_res_name			AS hd_filename,
		                            IF (galleries.gallery_type IN (#application.video_set_types#),0,1) AS image_set
		                    FROM	galleries
		                    INNER JOIN gallery_type
		                    ON		gallery_type.gallery_type_id	= galleries.gallery_type
		                    INNER JOIN models
		                    ON		models.model_id			= galleries.model_id
		                    LEFT JOIN models AS second_model
		                    ON		second_model.model_id	= galleries.second_model_id
		                    LEFT JOIN models AS third_model
		                    ON		third_model.model_id	= galleries.third_model_id
		                    LEFT JOIN models AS fourth_model
		                    ON		fourth_model.model_id	= galleries.fourth_model_id
		                    LEFT JOIN fast_tracks
		                    ON		fast_tracks.ft_gallery_id 	= galleries.gallery_id
		                    AND		fast_tracks.ft_pref_id		= #session.pref_id#
		                    AND		fast_tracks.ft_month 		= #month(now())#
		                    AND		fast_tracks.ft_year 		= #year(now())#
		                    LEFT JOIN permanent_fast_tracks
		                    ON		permanent_fast_tracks.pft_gallery_id	= galleries.gallery_id
		                    AND		permanent_fast_tracks.pft_pref_id		= #session.pref_id#
		                    LEFT JOIN videos
		                    ON		videos.video_id			= galleries.video_clip_id
		                    WHERE galleries.gallery_id IN (#g_data_retrieve#)
		                    GROUP BY galleries.gallery_id";
		return QueryExecute(stSql);
	}

	public any function paging(struct config, string type) {

		var MaxPages = ceiling(config.TotalRecords / config.DisplayCount);
		var PageOffSet = int(config.PageGroup / 2);
		var FromPage = config.CurrentPage - 2;
		var ToPage = config.CurrentPage  + 2;

		if(FromPage <= 1) {
			FromPage = 1;
		}
		if(ToPage >= MaxPages) {
			ToPage = MaxPages;
		}
		if(ToPage - FromPage < 4) {
			if(MaxPages == ToPage) {
				FromPage = ToPage - 4;
				if(FromPage <= 1) {
					FromPage = 1;
				}
			}
			else {
				ToPage = FromPage + 4;
				if(ToPage >= MaxPages) {
					ToPage == MaxPages;
				}
			}
		}

		var next = (config.CurrentPage + 1) * 1;
		var prev = (config.CurrentPage - 1) * 1;
		if(next >= MaxPages) {
			next = MaxPages;
		}
		if(prev <= 1) {
			prev = 1;
		}

    	switch(type){
    		case 'dd':
    			var PageStr = "<span><label>#config.show_type# <strong>#config.start_row#</strong> to <strong>#config.end_row#</strong> of <strong>#config.TotalRecords#</strong></label></span><span class=""marginLeft5px""><label>PAGE</label></span>";
    			if(FromPage > 1) {
    				PageStr &= "<span class=""marginLeft5px""><A HREF="""" onclick=""filterUpdate(#prev#);return false;"" title=""Go to page #prev#""><i class=""fa fa-chevron-left""></i></A></span>";
    			}
    			PageStr &= "<span class=""marginLeft5px""><select id=""ddSet"" class=""filterSelect"">";
    		break;
    		case 'ul':
    			var PageStr = "<ul class=""pagination""><li><span>#config.show_type# <strong>#config.start_row#</strong> to <strong>#config.end_row#</strong> of <strong>#config.TotalRecords#</strong></span></li><li><span>PAGE</span></li>";
    		break;
    	}
		
		if (MaxPages GT 1) {
			switch(type){
	    		case 'dd':
	    			for (var count="#FromPage#"; count <="#MaxPages#"; count ++) {
						PageStr = "#PageStr# <option #Count IS config.CurrentPage ? 'selected="selected"' : ''# onclick=""filterUpdate(#Count#)"">#Count#</option> ";
					}
					PageStr &= "</select></span>";
					if(ToPage < MaxPages) {
						PageStr &= "<span><A HREF="""" onclick=""filterUpdate(#next#);return false;"" title=""Go to page #next#""><i class=""fa fa-chevron-right""></i></A></span>";

					}
	    		break;
	    		case 'ul':
	    			if(FromPage > 1) {
	    				PageStr = "#PageStr# <li><A HREF="""" onclick=""filterUpdate(#prev#);return false;"" title=""Go to page #prev#""><i class=""fa fa-chevron-left""></i></A></li> ";
	    				PageStr = "#PageStr# <li><A HREF="""" onclick=""filterUpdate(1);return false;"" title=""Go to page 1"">1</A></li><li><span>...</span></li> ";
	    			}
	    			for (var count="#FromPage#"; count <="#ToPage#"; count ++) {
						PageStr = "#PageStr# <li><A class=""#Count IS config.CurrentPage ? 'active' : ''#"" HREF="""" onclick=""filterUpdate(#Count#);return false;"" title=""Go to page #Count#""> #Count#</A></li> ";
					}
					if(ToPage < MaxPages) {
	    				PageStr = "#PageStr# <li><span>...</span></li><li><A HREF="""" onclick=""filterUpdate(#MaxPages#);return false;"" title=""Go to page #MaxPages#"">#MaxPages#</A></li>";
						PageStr &= "<li><A HREF="""" onclick=""filterUpdate(#next#);return false;"" title=""Go to page #next#""><i class=""fa fa-chevron-right""></i></A></li> ";
	    			}
					PageStr &= "</ul>";
	    		break;
	    	}
		}
		else {
			PageStr = "";
		}

		return PageStr;
    }

}