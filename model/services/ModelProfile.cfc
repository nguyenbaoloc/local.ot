component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}

	public any function qry_pfts () {
		return QueryExecute("SELECT	(preferences_myOT.permanent_fasttracks_purchased - COUNT(permanent_fast_tracks.pft_gallery_id)) AS pft_remaining
							FROM	preferences_myOT
							LEFT JOIN  permanent_fast_tracks
							ON		permanent_fast_tracks.pft_pref_id 	= preferences_myOT.pref_id
							WHERE	preferences_myOT.pref_id 			= ?
							GROUP BY preferences_myOT.pref_id",
							[
								{sqltype:"cf_sql_integer", value: session.pref_id}
							]);
	}

	public any function qry_fts () {
		return QueryExecute("SELECT	(preferences_myOT.fasttracks_allowed - COUNT(fast_tracks.ft_gallery_id)) AS ft_remaining
							FROM	preferences_myOT
							LEFT JOIN  fast_tracks
							ON		fast_tracks.ft_pref_id				= preferences_myOT.pref_id
							AND		fast_tracks.ft_month 				= #month(now())#
							AND		fast_tracks.ft_year 				= #year(now())#
							WHERE	preferences_myOT.pref_id 			= #session.pref_id#
							GROUP BY preferences_myOT.pref_id");
	}

	public any function qry_letters () {
		return QueryExecute("SELECT  model_initial
							FROM	models
							GROUP BY model_initial");
	}

	public any function qry_model (numeric model_id) {
		return QueryExecute("SELECT models.model_id,
									models.model_display_name,
									models.content_page_id,
									models.model_flash_intro,
									models.model_flash_profile,
									models.model_bust,
									models.model_waist,
									models.model_hips,
									models.model_shortcut,
									model_dob_active,
									models.model_height,
									models.model_shoe_size,
									models.model_dress_size,
									models.model_dob,
									models.model_star_sign,
									models.model_random_fact,
									models.model_members_page,
									models.model_public_tour_sets,
									models.model_n_videos			 		AS video_count,
									models.model_n_galleries				AS gallery_count,
									models.model_n_images					AS image_count,
									models.content_page_id,
									content.content_page_content
							FROM	models
							LEFT JOIN content ON models.model_members_page = content.content_page_id
							WHERE	models.model_id = ?
							GROUP BY models.model_id
							LIMIT   1",
							[
								{sqltype:"cf_sql_integer", value: model_id}
							]);
	}

	public any function qry_videoDetails (numeric model_flash_profile) {
		return QueryExecute("SELECT  	videos.video_directory_name,
										videos.video_hd_res_name,
										videos.video_string,
										galleries.gallery_id,
										galleries.directory_name
							FROM		videos
							LEFT JOIN 	galleries
							ON 			videos.video_id = galleries.video_clip_id
							WHERE		videos.video_directory_name	= ?",
							[
							 {sqltype:"cf_sql_integer", value: model_flash_profile}
							]);
	}

	public any function any_released (numeric model_id) {
		return QueryExecute("SELECT	release_date,
									model_id,
									video_clip_id
							FROM	galleries
							WHERE	model_id = ?
							AND		release_date IS NOT NULL
							AND		video_clip_id = 9999",
							[
								{sqltype: "cf_sql_integer", value:model_id}
							]);
	}
	
	public any function released_gal (numeric model_id) {
		return QueryExecute("SELECT 		release_date_day,
										release_date,
										tour_pic1_thumb,
										gallery_id,
										gallery_description,
										show_future,
										average_outfit,
										average_model,
										video_clip_id,
										model_id
							FROM		galleries
							WHERE		model_id = ?
							AND		video_clip_id = 9999
							AND 		show_future = 1
							ORDER BY	release_date DESC
							LIMIT 		1",
							[
								{sqltype:"cf_sql_integer", value: model_id }
							]);
	}		

	public any function qryComments (numeric model_id) {
		return QueryExecute("SELECT	model_comments.model_comment_id,
									model_comments.model_comment_text,
									model_comments.model_comment_added_by,
									model_comments.model_comment_date_added,
									preferences_myOT.forum_nickname
							FROM	model_comments,
									preferences_myOT
							WHERE	model_comments.model_id = ?
							AND		model_comments.model_comment_active = 1
							AND		model_comments.model_comment_added_by = preferences_myOT.pref_id
							AND		model_comments.model_comment_islink = 0
							ORDER BY	model_comment_date_added DESC",
							[
								{sqltype:"cf_sql_integer", value: model_id }
							]);
	}	

	public any function qryLinks (numeric model_id) {
		return QueryExecute("SELECT	model_comments.model_comment_id,
									model_comments.model_comment_text,
									model_comments.model_comment_added_by,
									model_comments.model_comment_date_added,
									preferences_myOT.forum_nickname
							FROM	model_comments,
									preferences_myOT
							WHERE	model_comments.model_id = ?
							AND		model_comments.model_comment_active = 1
							AND		model_comments.model_comment_added_by = preferences_myOT.pref_id
							AND		model_comments.model_comment_islink = 1
							ORDER BY	model_comment_date_added DESC",
							[
								{sqltype:"cf_sql_integer", value: model_id }
							]);
	}	
	
	public any function get_answers (numeric model_id) {
		return QueryExecute("SELECT 		answer_model_id,
										answer_question_id,
										answer_text
							FROM		model_info_answers
							WHERE		answer_model_id = ?
							AND			answer_question_id != 22
							AND			answer_question_id != 23",
							[
								{sqltype: "cf_sql_integer", value: model_id}
							]);
	}
		
	public any function facebook (numeric model_id) {
		return QueryExecute("SELECT 		mia.answer_model_id,
											mia.answer_question_id,
											mia.answer_text,
											miq.question_id,
											miq.question_text
								FROM		model_info_answers as mia
								LEFT JOIN	model_info_questions as miq
								ON			answer_question_id = question_id
								WHERE		answer_model_id = ?
								AND			mia.answer_question_id = 23",
							[
								{sqltype: "cf_sql_integer", value: model_id}
							]);
	}

	public any function twitter (numeric model_id) {
		return QueryExecute("SELECT 		mia.answer_model_id,
											mia.answer_question_id,
											mia.answer_text,
											miq.question_id,
											miq.question_text
								FROM		model_info_answers as mia
								LEFT JOIN	model_info_questions as miq
								ON			answer_question_id = question_id
								WHERE		answer_model_id = ?
								AND			mia.answer_question_id = 22",
							[
								{sqltype: "cf_sql_integer", value: model_id}
							]);
	}

	public any function top_gallery (numeric model_id) {
		return QueryExecute("SELECT 		average_combined,
											tour_pic1_thumb,
											gallery_id,
											video_clip_id,
											model_id
								FROM		galleries
								WHERE		model_id = ?
								AND			video_clip_id = 9999
								ORDER BY	average_combined DESC
								LIMIT 		1",
							[
								{sqltype: "cf_sql_integer", value: model_id}
							]);
	}

	public any function qry_favourite(numeric model_id, numeric pref_id, string type) {
		return QueryExecute("SELECT	my_favourites_keyid
							FROM	my_favourites
							WHERE	my_favourites_id 		= ?
							AND		my_favourites_pref_id	= ?
							AND		my_favourites_type		= ?",
							[
								{sqltype:"cf_sql_integer", value: model_id},
								{sqltype:"cf_sql_integer", value: pref_id},
								{sqltype:"cf_sql_varchar", value: type}
							]);
	}
	
	public any function get_questions(numeric answer_question_id) {
		return QueryExecute("SELECT		question_id,
										question_text,
										question_active
							FROM		model_info_questions
							WHERE		question_id = ?",
							[
								{sqltype: "cf_sql_integer", value: answer_question_id}
							]);
	}
}