component output="false" displayname=""  {

	public function init(){
		return this;
	}

	public function qry_galleryimagesbygalId (numeric gallery_id) {
		return QueryExecute("SELECT  image_id
							FROM	images
							WHERE	images.gallery_id = ?
							ORDER BY image_order ASC"
							,
							[
								{sqltype: "cf_sql_integer", value: gallery_id}
							]
							);
	}


	public function qry_galleryimages (numeric customgal_id) {
		return QueryExecute("SELECT  DISTINCT cil.image_id
							FROM	customgal AS cg
							INNER JOIN customgal_image_linking_table AS cil
							ON		cil.customgal_id = cg.customgal_id
							WHERE	cg.customgal_id = ?
							AND		cg.pref_id = ?
							ORDER BY cil.cil_order ASC"
							,
							{
								{sqltype: "cf_sql_integer", value: customgal_id},
								{sqltype: "cf_sql_varchar", value: session.pref_id}
							}
							);
	}

	public function qry_setDetails (numeric gallery_id, string stSerial) {
		return QueryExecute("SELECT 	IF (
										IF (DATE(galleries.release_date) <= DATE(NOW()) AND galleries.release_date IS NOT NULL,1,0) OR
										IF (fast_tracks.ft_gallery_id != '',1,0) OR
										IF (permanent_fast_tracks.pft_gallery_id != '',1,0) OR
										IF (galleries.serial_code = ?,1,0)
									,1,0) AS set_release
							FROM	galleries
							LEFT JOIN fast_tracks
							ON		fast_tracks.ft_gallery_id = galleries.gallery_id
							AND		fast_tracks.ft_pref_id = ?
							AND		fast_tracks.ft_month = MONTH(NOW())
							AND		fast_tracks.ft_year = YEAR(NOW())
							LEFT JOIN permanent_fast_tracks
							ON		permanent_fast_tracks.pft_gallery_id = galleries.gallery_id
							AND		permanent_fast_tracks.pft_pref_id = ?
							WHERE	galleries.gallery_id = ?"
							,
							[
								{sqltype: "cf_sql_varchar", value: stSerial},
								{sqltype: "cf_sql_varchar", value: session.pref_id},
								{sqltype: "cf_sql_varchar", value: session.pref_id},
								{sqltype: "cf_sql_integer", value: gallery_id}
							]
							);
	}

	public function qry_imageDetails (numeric image_id) {
		return QueryExecute("SELECT  	galleries.gallery_id,
										gallery_type,
										galleries.directory_name,
										galleries.zipfile,
										galleries.lrg_zipfile,
										galleries.ultra_zipfile,
										galleries.super_zipfile,
										galleries.image_thumb,
										galleries.image_standard,
										galleries.image_large,
										galleries.image_ultra,
										galleries.image_super,
										images.filename,
										models.model_id,
										models.model_display_name,
										models.guest_link_url
							FROM		galleries
							INNER JOIN	images
							ON			galleries.gallery_id = images.gallery_id
							INNER JOIN	models
							ON			galleries.model_id = models.model_id
							WHERE		images.image_id = ?",
							[
								{sqltype: "cf_sql_integer", value: image_id}
							]
							);
	}
}