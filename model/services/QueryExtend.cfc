component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}



    public query function get_pft_remaining(numeric pref_id) {
    	return QueryExecute("
    							SELECT	(preferences_myOT.permanent_fasttracks_purchased - COUNT(permanent_fast_tracks.pft_gallery_id)) AS pft_remaining
								FROM	preferences_myOT
								LEFT JOIN  permanent_fast_tracks
								ON		permanent_fast_tracks.pft_pref_id 	= preferences_myOT.pref_id
								WHERE	preferences_myOT.pref_id 			= ?
								GROUP BY preferences_myOT.pref_id
    						",
    						[
    							{sqltype:"cf_sql_integer", value:pref_id}
    						]);
    }

    public query function get_ft_remaining(numeric pref_id) {
    	return QueryExecute("
    							SELECT	(preferences_myOT.fasttracks_allowed - COUNT(fast_tracks.ft_gallery_id)) AS ft_remaining
								FROM	preferences_myOT
								LEFT JOIN  fast_tracks
								ON		fast_tracks.ft_pref_id				= preferences_myOT.pref_id
								AND		fast_tracks.ft_month 				= #month(now())#
								AND		fast_tracks.ft_year 				= #year(now())#
								WHERE	preferences_myOT.pref_id 			= ?
								GROUP BY preferences_myOT.pref_id
    						",
    						[
    							{sqltype:"cf_sql_integer", value:pref_id}
    						]);
    }

    public any function getGuestGallery(numeric gallery_id) {
    	return QueryExecute("
    							SELECT		models.model_id,
											models.model_display_name,
											models.model_link,
											models.guest_link_url
								FROM		galleries_models
								INNER JOIN	models
								ON			galleries_models.gm_model_id = models.model_id
								WHERE		galleries_models.gm_gallery_id = ?
    						",
    						[
    							{sqltype:"cf_sql_integer", value:gallery_id}
    						]);
    }

    public any function getFT_history(numeric gallery_id) {
    	return QueryExecute("
    							SELECT		ft_date_added,
    										DATE_FORMAT( ft_date_added, '%D %M %Y' ) as ft_date_added_formarted
								FROM		fast_tracks
								WHERE		ft_pref_id = ?
								AND			ft_gallery_id = ?
								ORDER BY 	ft_date_added DESC
    						",
    						[
    							{sqltype:"cf_sql_integer", value:session.pref_id},
    							{sqltype:"cf_sql_integer", value:gallery_id}
    						]);
    }
    
    public any function getVipData(date dDat_release) {
        return QueryExecute(
                            "SELECT     galleries.gallery_id,
                                        galleries.tour_pic1_thumb,
                                        galleries.tour_pic2_thumb,
                                        galleries.tour_pic3_thumb,
                                        galleries.updates_pic_thumb,
                                        galleries.mailing_list_pic_thumb,
                                        galleries.model_id,
                                        galleries.release_date,
                                        galleries.gallery_type,
                                        galleries.gallery_description,
                                        galleries.zipfile,
                                        galleries.zipfile_size,
                                        galleries.lrg_zipfile,
                                        galleries.lrg_zipfile_size,
                                        galleries.ultra_zipfile,
                                        galleries.ultra_zipfile_size,
                                        galleries.total_votes_cast_outfit,
                                        galleries.total_votes_cast_tease,
                                        galleries.average_outfit,
                                        galleries.average_tease,
                                        galleries.average_model,
                                        galleries.directory_name,
                                        models.model_display_name,
                                        second_model.model_display_name     AS second_model_name,
                                        galleries.second_model_id,
                                        third_model.model_display_name      AS third_model_name,
                                        galleries.third_model_id,
                                        fourth_model.model_display_name     AS fourth_model_name,
                                        galleries.fourth_model_id,
                                        galleries.gallery_n_images          AS image_count,
                                        videos.video_total_time             AS video_length,
                                        videos.video_directory_name,
                                        videos.video_hd_res_name,
                                        videos.video_hd_res_filesize,
                                        videos.video_high_res_name,
                                        videos.video_high_res_filesize,
                                        videos.video_medium_res_name,
                                        videos.video_medium_res_filesize,
                                        videos.video_low_res_name,
                                        videos.video_low_res_filesize,
                                        videos.video_format,
                                        videos.video_no_clips,
                                        IF (galleries.video_clip_id != 9999,1,0) AS video_clip
                                FROM    galleries
                                LEFT JOIN ot2004_data.models
                                ON      models.model_id         = galleries.model_id
                                LEFT JOIN ot2004_data.models AS second_model
                                ON      second_model.model_id   = galleries.second_model_id
                                LEFT JOIN ot2004_data.models AS third_model
                                ON      third_model.model_id    = galleries.third_model_id
                                LEFT JOIN ot2004_data.models AS fourth_model
                                ON      fourth_model.model_id   = galleries.fourth_model_id
                                LEFT JOIN videos
                                ON      videos.video_id         = galleries.video_clip_id
                                LEFT JOIN gallery_type
                                ON      gallery_type.gallery_type_id    = galleries.gallery_type
                                WHERE       galleries.release_date = ?",
                                [{ 
                                    sqltype:"cf_sql_date"
                                    ,value: dDat_release
                                }],
                                {
                                    datasource: "vip_data"
                                }
                            );
    }

    public any function checkPreferencesBy_PrefId (any pref_id) {
        return QueryExecute("SELECT  pref_imagesize,
                                    pref_thumbs,
                                    pref_view,
                                    pref_window,
                                    pref_modellist,
                                    pref_sets,
                                    pref_ss_autoresize,
                                    archive_display,
                                    download_server,
                                    widget_display,
                                    suggested_sites,                
                                    cdn_option
                            FROM    preferences_myOT
                            WHERE   pref_id = ?",
                            [
                                {sqltype: "cf_sql_integer", value: pref_id}
                            ],
                            {
                                maxrows: 1
                            });
    }

    public any function qry_defaults_forum () {
        return QueryExecute("SELECT ip,
                                    rootdir,
                                    dsn_name
                            FROM    admin_options",
                            [],
                            {   
                                datasource: "ot_fullforum"
                            });
    }
}