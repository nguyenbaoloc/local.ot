component accessors="true" output="false" displayname=""  singleton="true"  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}
	
 	public query function getFortcomingShoots () {
	    var qry_forthcoming_shoots = QueryExecute(
	    											"
	    												SELECT      forthcoming_shoots.fs_link_request,
												                    MONTH(forthcoming_shoots.fs_shoot_date) AS fs_shoot_date_month,
												                    YEAR(forthcoming_shoots.fs_shoot_date) AS fs_shoot_date_year,
												                    models.model_id,
												                    models.model_display_name
												        FROM        forthcoming_shoots
												        INNER JOIN  models
												        ON          forthcoming_shoots.fs_model_id = models.model_id
												        WHERE       forthcoming_shoots.fs_shoot_date <= ?
												        GROUP BY    forthcoming_shoots.fs_id
												        ORDER BY    fs_shoot_date_year,
												                    fs_shoot_date_month,
												                    models.model_display_name
												        LIMIT 4
	    											",
	    											[
	    												{sqltype="CF_SQL_DATE", value=now()}
	    											],
	    											{
    													cachedwithin:application.queryTimeCache
	    											}
	    										);
		return qry_forthcoming_shoots;
    }

    public query function getUpcomingRelease () {
    	<!--- generate the latest updates page --->
		var coming_soon_sets = 9;
		
		var qry_ComingSoon = QueryExecute("SELECT  galleries.gallery_id,
						            galleries.tour_pic1_thumb,
						            galleries.tour_pic2_thumb,
						            galleries.model_id,
						            galleries.second_model_id,
						            galleries.release_date,
				            		DATE_FORMAT( galleries.release_date, '%W %D %M' ) as release_date_formarted,
				            		DATE_FORMAT( galleries.release_date, '%D %M %Y' ) as release_date_formarted_2,
						            galleries.gallery_type,
						            models.model_display_name,
						            second_model.model_display_name     AS second_model_name,
						            third_model.model_display_name      AS third_model_name,
						            fourth_model.model_display_name     AS fourth_model_name,
						            galleries.gallery_n_images          AS image_count,
						            videos.video_total_time             AS video_length,
						            IF (galleries.v2_video_clip_id != 9999,galleries.v2_video_clip_id,0) AS video_clip
						    FROM    galleries
						    INNER JOIN gallery_type
						    ON      gallery_type.gallery_type_id    = galleries.gallery_type
						    LEFT JOIN models
						    ON      models.model_id         = galleries.model_id
						    LEFT JOIN models AS second_model
						    ON      second_model.model_id   = galleries.second_model_id
						    LEFT JOIN models AS third_model
						    ON      third_model.model_id    = galleries.third_model_id
						    LEFT JOIN models AS fourth_model
						    ON      fourth_model.model_id   = galleries.fourth_model_id
						    LEFT JOIN videos
						    ON      videos.video_id         = galleries.video_clip_id
						    WHERE galleries.release_date  > ?
						    AND     galleries.release_date  IS NOT NULL
						    AND     galleries.show_future       = 1
						    AND     (galleries.linked_release = 0 OR (galleries.linked_release = 1 AND galleries.v2_video_clip_id != 9999))
						    GROUP BY galleries.gallery_id
						    ORDER BY galleries.release_date ASC,gallery_type.gallery_type_order DESC,gallery_type.gallery_type_order DESC
						    LIMIT   0,#coming_soon_sets#",
						    [
								{sqltype="CF_SQL_DATE", value=now()}
							],
						    {
						    	cachedwithin:application.queryTimeCache
						    }
						);
		return qry_ComingSoon;
    }

    public query function getGalleryByModelId(required numeric model_id, string gType) {
    	var condition1 = "";
    	var condition2 = "";
    	if(gType eq "f") {
    		condition1 = " AND (galleries.release_date > NOW() OR galleries.release_date IS NULL)";
    		condition2 = " AND (vip_galleries.release_date > NOW() OR vip_galleries.release_date IS NULL)";
    	}
    	else if (gType NEQ "a"){
    		if(gType neq "v") {
    			condition1 = " AND galleries.gallery_type NOT IN (#application.video_set_types#)";
    			condition2 = " AND vip_galleries.release_date <= NOW()
							   AND vip_galleries.gallery_type NOT IN (#application.video_set_types#)";
    		}
    		else {
    			condition1 = " AND galleries.gallery_type IN (#application.video_set_types#)";
    			condition2 = " AND vip_galleries.release_date <= NOW()
							   AND vip_galleries.gallery_type IN (#application.video_set_types#)";
    		}
    	}

    	return QueryExecute("
				(
					SELECT		0 AS 'vip_set',
								galleries.gallery_id,
								galleries.tour_pic1_thumb,
								galleries.tour_pic2_thumb,
								galleries.tour_pic3_thumb,
								galleries.updates_pic_thumb,
								galleries.mailing_list_pic_thumb,
								galleries.serial_code,
								galleries.release_date,
								DATE_FORMAT( galleries.release_date, '%D %M %Y' ) as release_date_formarted,
								galleries.gallery_type,
								galleries.gallery_description,
								galleries.zipfile,
								galleries.zipfile_size,
								galleries.lrg_zipfile,
								galleries.lrg_zipfile_size,
								galleries.ultra_zipfile,
								galleries.ultra_zipfile_size,
								galleries.super_zipfile,
								galleries.super_zipfile_size,
								galleries.total_votes_cast_outfit,
								galleries.total_votes_cast_tease,
								galleries.average_outfit,
								galleries.average_model,
								galleries.average_tease,
								galleries.directory_name,
								gallery_type.gallery_type_order,
								galleries.gallery_n_images AS image_count,
								videos.video_total_time AS video_length,
								videos.video_directory_name,
								videos.video_hd_res_name,
								videos.video_hd_res_filesize,
								videos.video_high_res_name,
								videos.video_high_res_filesize,
								videos.video_medium_res_name,
								videos.video_medium_res_filesize,
								videos.video_low_res_name,
								videos.video_low_res_filesize,
								videos.video_format,
								videos.video_no_clips,
								videos.video_mp4_hd_name,
								videos.video_mp4_hd_filesize,
								videos.video_mp4_sd_name,
								videos.video_mp4_sd_filesize,
								videos.video_webm_name,
								videos.video_webm_filesize,
								IF (galleries.v2_video_clip_id != 9999,galleries.v2_video_clip_id,0) AS video_clip,
								IF (galleries.release_date <= NOW(),0,1) AS gallery_release,
								fast_tracks.ft_pref_id,
								fast_tracks.ft_month,
								fast_tracks.ft_year,
								permanent_fast_tracks.pft_pref_id,
								IF (galleries.PFT_cost_override = 1,galleries.PFT_cost,gt.gallery_type_PFT_cost) AS PFT_cost
					FROM		galleries
					INNER JOIN	gallery_type
					ON			gallery_type.gallery_type_id = galleries.gallery_type
					INNER JOIN	galleries_models
					ON			galleries.gallery_id = galleries_models.gm_gallery_id
					LEFT JOIN	videos
					ON			videos.video_id = galleries.video_clip_id
					LEFT JOIN	fast_tracks
					ON			fast_tracks.ft_gallery_id = galleries.gallery_id
					AND			fast_tracks.ft_pref_id = ?
					AND			fast_tracks.ft_month = MONTH(NOW())
					AND			fast_tracks.ft_year = YEAR(NOW())
					LEFT JOIN	permanent_fast_tracks
					ON			permanent_fast_tracks.pft_gallery_id = galleries.gallery_id
					AND			permanent_fast_tracks.pft_pref_id = ?
					LEFT JOIN	gallery_type AS gt
					ON			galleries.gallery_type = gt.gallery_type_id
					WHERE		galleries_models.gm_model_id = ?
					AND			galleries.show_future = 1
					AND			galleries.linked_release = galleries.special_type
					#condition1#
				)
				UNION
				(
					SELECT		1 AS 'vip_set',
								vip_galleries.gallery_id,
								vip_galleries.tour_pic1_thumb,
								vip_galleries.tour_pic2_thumb,
								vip_galleries.tour_pic3_thumb,
								vip_galleries.updates_pic_thumb,
								vip_galleries.mailing_list_pic_thumb,
								vip_galleries.serial_code,
								vip_galleries.release_date,
								DATE_FORMAT( vip_galleries.release_date, '%D %M %Y' ) as release_date_formarted,
								vip_galleries.gallery_type,
								vip_galleries.gallery_description,
								vip_galleries.zipfile,
								vip_galleries.zipfile_size,
								vip_galleries.lrg_zipfile,
								vip_galleries.lrg_zipfile_size,
								vip_galleries.ultra_zipfile,
								vip_galleries.ultra_zipfile_size,
								vip_galleries.super_zipfile,
								vip_galleries.super_zipfile_size,
								vip_galleries.total_votes_cast_outfit,
								vip_galleries.total_votes_cast_tease,
								vip_galleries.average_outfit,
								vip_galleries.average_tease,
								vip_galleries.average_model,
								vip_galleries.directory_name,
								vip_gallery_type.gallery_type_order,
								vip_galleries.gallery_n_images		AS image_count,
								videos.video_total_time				AS video_length,
								videos.video_directory_name,
								videos.video_hd_res_name,
								videos.video_hd_res_filesize,
								videos.video_high_res_name,
								videos.video_high_res_filesize,
								videos.video_medium_res_name,
								videos.video_medium_res_filesize,
								videos.video_low_res_name,
								videos.video_low_res_filesize,
								videos.video_format,
								videos.video_no_clips,
								videos.video_mp4_hd_name,
								videos.video_mp4_hd_filesize,
								videos.video_mp4_sd_name,
								videos.video_mp4_sd_filesize,
								videos.video_webm_name,
								videos.video_webm_filesize,
								IF (vip_galleries.video_clip_id != 9999, vip_galleries.video_clip_id,0) AS video_clip,
								IF (vip_galleries.release_date <= NOW(),0,1) AS gallery_release,
								fast_tracks.ft_pref_id,
								fast_tracks.ft_month,
								fast_tracks.ft_year,
								permanent_fast_tracks.pft_pref_id,
								vip_galleries.PFT_cost_override AS PFT_cost
					FROM		vip_data.gallery_type AS vip_gallery_type
					INNER JOIN	vip_data.galleries AS vip_galleries
					ON			vip_gallery_type.gallery_type_id = vip_galleries.gallery_type
					INNER JOIN	vip_data.galleries_models AS galleries_models
					ON			vip_galleries.gallery_id = galleries_models.gm_gallery_id
					LEFT JOIN	videos
					ON			videos.video_id = vip_galleries.video_clip_id
					LEFT JOIN	fast_tracks
					ON			fast_tracks.ft_gallery_id = vip_galleries.gallery_id
					AND			fast_tracks.ft_pref_id = #session.pref_id#
					AND			fast_tracks.ft_month = MONTH(NOW())
					AND			fast_tracks.ft_year = YEAR(NOW())
					LEFT JOIN	permanent_fast_tracks
					ON			permanent_fast_tracks.pft_gallery_id = vip_galleries.gallery_id
					AND			permanent_fast_tracks.pft_pref_id = #session.pref_id#
					WHERE		galleries_models.gm_model_id = ?
					AND			vip_galleries.show_future = 1
					AND			vip_galleries.linked_release = vip_galleries.special_type
					AND			vip_galleries.release_date <= NOW()
					#condition2#
				)
				ORDER BY	gallery_release ASC,
							release_date DESC,
							vip_set ASC,
							gallery_type_order DESC
			",
			[
				{ sqltype= "cf_sql_integer", value= session.pref_id},
				{ sqltype= "cf_sql_integer", value= session.pref_id},
				{ sqltype= "cf_sql_integer", value= model_id},
				{ sqltype= "cf_sql_integer", value= model_id},
			],
			{
				cachedwithin:application.queryTimeCache
			}
		);
    }

    public query function getModelForModelSet(numeric model_id) {
    	return QueryExecute(
								"SELECT	model_shortcut,
										model_display_name
								FROM	models
								WHERE	model_id = ?",
								[
									{sqltype:"CF_SQL_INTEGER", value:model_id}
								]
    						);
    }

    public query function getGalsForModelSet (string sModelCheck, string sDatasource) {
    	return QueryExecute (
    							"SELECT		models.model_id,
											galleries.gallery_id,
											galleries.tour_pic1_thumb,
											galleries.tour_pic2_thumb,
											galleries.tour_pic3_thumb,
											galleries.gallery_type,
											IF(galleries.release_date IS NULL, 1, 0) AS fasttrack
								FROM		models
								INNER JOIN	galleries
								ON			models.model_id = galleries.model_id
								LEFT JOIN	videos
								ON			galleries.video_clip_id = videos.video_id
								WHERE		models.model_shortcut = ?
								AND			show_future = 1
								#sDatasource eq 'ot2004_data' ? 'AND show_public = 1' : ''#
								ORDER BY	galleries.gallery_order,
											galleries.gallery_id DESC",
								[
									{sqltype:"CF_SQL_VARCHAR", value:sModelCheck}
								],
								{
									datasource:sDatasource
								}
				    		);
    }

    public query function getPicsForModelSet (string sModelCheck, string sDatasource) {

    	return QueryExecute(
    							"SELECT		galleries.gallery_n_images
								FROM		models
								INNER JOIN	galleries
								ON			models.model_id = galleries.model_id
								WHERE		models.model_shortcut = ?
								AND			galleries.gallery_type IN (#application.stc_set_types.str_pictures#)
								AND			(
												galleries.linked_release_id IS NULL
												OR
												(
													galleries.linked_release_id IS NOT NULL
													AND
													galleries.special_type = 1
												)
											)
								AND			show_future = 1
								#sDatasource eq 'ot2004_data' ? 'AND show_public = 1' : ''#",
								[
									{sqltype:"CF_SQL_VARCHAR", value:sModelCheck}
								],
								{
									datasource:sDatasource
								}
    						);
    }

    public query function getVideosForModelSet (string sModelCheck, string sDatasource) {

    	return QueryExecute(
    							"SELECT		COUNT(galleries.video_clip_id) AS int_videoCount
								FROM		models
								INNER JOIN	galleries
								ON			models.model_id = galleries.model_id
								WHERE		models.model_shortcut = ?
								AND			(
												galleries.gallery_type IN (#application.stc_set_types.str_videos#)
												OR
												(
													galleries.gallery_type IN (#application.stc_set_types.str_pictures#)
													AND
													galleries.linked_release_id IS NOT NULL
													AND
													galleries.special_type = 0
												)
											)
								AND			show_future = 1
								#sDatasource eq 'ot2004_data' ? 'AND show_public = 1' : ''#
								GROUP BY	models.model_id",
								[
									{sqltype:"CF_SQL_VARCHAR", value:sModelCheck}
								],
								{
									datasource:sDatasource
								}
    						);
    }

    public query function getGalleries(string sView, numeric nMonth, numeric nYear) {
    	var condition = "";
		if (sView EQ "pictures" OR sView EQ "videos") {
			condition = "AND galleries.gallery_type ";
			if (sView EQ "pictures") {
				condition &= " NOT "; 
			}
			condition &= " IN (#application.stc_set_types.str_videos#)";
		}
    	return QueryExecute(
    							"SELECT		galleries.gallery_id,
											galleries.tour_pic1_thumb,
											galleries.tour_pic2_thumb,
											galleries.tour_pic3_thumb,
											galleries.updates_pic_thumb,
											galleries.mailing_list_pic_thumb,
											galleries.model_id,
											galleries.release_date,
											DATE_FORMAT( galleries.release_date, '%D %M %Y' ) as release_date_formarted,
											galleries.gallery_type,
											galleries.gallery_description,
											galleries.zipfile,
											galleries.zipfile_size,
											galleries.lrg_zipfile,
											galleries.lrg_zipfile_size,
											galleries.ultra_zipfile,
											galleries.ultra_zipfile_size,
											galleries.super_zipfile,
											galleries.super_zipfile_size,
											galleries.total_votes_cast_outfit,
											galleries.total_votes_cast_tease,
											galleries.average_outfit,
											galleries.average_tease,
											galleries.average_model,
											galleries.directory_name,
											gallery_type.gallery_type_order,
											galleries.gallery_n_images AS image_count,
											videos.video_total_time AS video_length,
											videos.video_directory_name,
											videos.video_hd_res_name,
											videos.video_hd_res_filesize,
											videos.video_high_res_name,
											videos.video_high_res_filesize,
											videos.video_medium_res_name,
											videos.video_medium_res_filesize,
											videos.video_low_res_name,
											videos.video_low_res_filesize,
											videos.video_format,
											videos.video_no_clips,
											videos.video_mp4_hd_name,
											videos.video_mp4_hd_filesize,
											videos.video_mp4_sd_name,
											videos.video_mp4_sd_filesize,
											videos.video_webm_name,
											videos.video_webm_filesize,
											gallery_type.gallery_type_order,
											IF (galleries.v2_video_clip_id != 9999,galleries.v2_video_clip_id,0) AS video_clip
								FROM		models
								INNER JOIN	galleries_models
								ON			models.model_id = galleries_models.gm_model_id
								AND			galleries_models.gm_precedence = 1
								INNER JOIN	galleries
								ON			galleries_models.gm_gallery_id = galleries.gallery_id
								INNER JOIN	gallery_type
								ON			gallery_type.gallery_type_id = galleries.gallery_type
								LEFT JOIN	videos
								ON			videos.video_id = galleries.video_clip_id
								WHERE		MONTH(galleries.release_date) = ?
								AND			YEAR(galleries.release_date) = ?
								AND			galleries.release_date <= NOW()
								#sView neq 'all' ? condition : ''#
								AND 		galleries.release_date IS NOT NULL
								AND			(galleries.linked_release = 0 OR (galleries.linked_release = 1 AND galleries.v2_video_clip_id != 9999))
								AND			galleries.type = 'Exclusive'
								AND			galleries.show_future = 1
								GROUP BY	galleries.gallery_id
								ORDER BY	release_date DESC,
											gallery_type.gallery_type_order DESC,
											gallery_type_order DESC",
								[
									{sqltype: "CF_SQL_INTEGER", value: nMonth},
									{sqltype: "CF_SQL_INTEGER", value: nYear}
								]
							);
    }

    public query function checkPreferences (string remote_user) {
    	return QueryExecute("
								SELECT  pref_id,
										shown_new_member_page
								FROM	preferences_myOT
								WHERE	pref_username = ?
								ORDER BY	pref_id	DESC",
								[remote_user],
								{
									datasource=application.dsn_name, 
									maxrows="1"
								}
							);
    }

    public query function getMaxPrefId(string remote_user) {
		return QueryExecute(sql:"SELECT 	Max(pref_id) as new_prefid
											FROM	preferences_myOT
											WHERE	pref_username = :remote_user
											",
							params:
							{
								remote_user:
								{
									value=remote_user,
									CFSQLType='cf_sql_varchar'
								}
							},
							datasource:application.dsn_name
							);
    }

    public query function checkVipMember(string sPrefId, date dJoinDate) {
    	return QueryExecute(
								"SELECT 		join_date
								FROM		preferences_myOT
								WHERE		pref_id = ?
								AND 		join_date < ?",
								[
									{sqltype="cf_sql_varchar", value=sPrefId},
									{sqltype="cf_sql_date", value=dJoinDate}
								]
							);
    }

    public query function authUser (string remote_user) {
    	return QueryExecute(
								"SELECT	IF(COUNT(user_name) = 0, 0, 1) AS bol_member
								FROM	oas_users
								WHERE	user_name = ?",
								[
									{sqltype:"CF_SQL_VARCHAR", value:remote_user}
								],
								{
									datasource:"authentication"
							});
    }

    public query function getModelLinks(string str_model_display_name, string str_model_shortcut) {
    	return QueryExecute(
								"SELECT		IF(models_links.ml_model_aka != '#str_model_display_name#', ml_model_aka, '') AS ml_model_name,
											models_links.ml_affiliate_url,
											ot_freesites_data.sponsor_sites.sponsor_site_name,
											ot_freesites_data.sponsor_sites.sponsor_site_img_name
								FROM		models_links
								INNER JOIN	ot_freesites_data.sponsor_sites
								ON			models_links.ml_affiliate_url = ot_freesites_data.sponsor_sites.sponsor_site_link_url
								AND			ot_freesites_data.sponsor_sites.sponsor_site_suspended = 0
								AND			ot_freesites_data.sponsor_sites.sponsor_site_sponsor_approved = 1
								AND			ot_freesites_data.sponsor_sites.sponsor_site_sponsor_suspended = 0
								WHERE		models_links.ml_model_shortcut = ?
								AND			models_links.ml_suspended = 0
								ORDER BY	RAND()",
								[
									{sqltype:"cf_sql_varchar", value:str_model_shortcut}
								]
							);
    }


    public struct function getComingSoonSet(numeric updatesSortOrder, numeric viewFT) {
    	var whereclause = "";
    	var orderby		= "";
    	if (viewFT EQ 0 ) {
    		whereclause = "AND	galleries.release_date	> #createODBCDate(now())# 
						   AND 	galleries.release_date 	IS NOT NULL";
    	}
    	else {
    		whereclause = "AND 	(galleries.release_date	> #createODBCDate(now())# OR galleries.release_date IS NULL OR galleries.release_date = '')
							AND 	galleries.gallery_type <> 7
							AND		galleries.gallery_n_images > 0";
    	}

    	if (viewFT EQ 0) {
    		orderby = "galleries.release_date ASC";
    	}
		else {
			switch(updatesSortOrder){
				case 0:
					orderby = "galleries.gallery_id DESC";
					break;
				case 1:
					orderby = "galleries.gallery_id ASC";
					break;
				case 2:
					orderby = "models.model_display_name ASC";
					break;
				case 3:
					orderby = "models.model_display_name DESC";
					break;
				default:
					orderby = "galleries.gallery_id DESC";
					break;
			}
		}
		var strQuery = "SELECT  galleries.gallery_id,
									galleries.tour_pic1_thumb,
									galleries.tour_pic2_thumb,
									galleries.tour_pic3_thumb,
									galleries.updates_pic_thumb,
									galleries.mailing_list_pic_thumb,
									galleries.model_id,
									galleries.release_date,
									DATE_FORMAT( galleries.release_date, '%D %M %Y' ) as release_date_formarted,
									galleries.gallery_type,
									galleries.gallery_description,
									galleries.zipfile,
									galleries.zipfile_size,
									galleries.lrg_zipfile,
									galleries.lrg_zipfile_size,
									galleries.ultra_zipfile,
									galleries.ultra_zipfile_size,
									galleries.total_votes_cast_outfit,
									galleries.average_outfit,
									galleries.average_model,
									galleries.directory_name,
									models.model_display_name,
									second_model.model_display_name 	AS second_model_name,
									galleries.second_model_id,
									third_model.model_display_name 		AS third_model_name,
									galleries.third_model_id,
									fourth_model.model_display_name 	AS fourth_model_name,
									galleries.fourth_model_id,
									galleries.gallery_n_images			AS image_count,
									videos.video_total_time				AS video_length,
									videos.video_directory_name,
									videos.video_hd_res_name,
									videos.video_hd_res_filesize,
									videos.video_high_res_name,
									videos.video_high_res_filesize,
									videos.video_medium_res_name,
									videos.video_medium_res_filesize,
									videos.video_low_res_name,
									videos.video_low_res_filesize,
									videos.video_format,
									videos.video_no_clips,
									videos.video_mp4_hd_name,
									IF (galleries.v2_video_clip_id != 9999,galleries.v2_video_clip_id,0) AS video_clip,
									fast_tracks.ft_pref_id,
									fast_tracks.ft_month,
									fast_tracks.ft_year,
									permanent_fast_tracks.pft_pref_id
							FROM	galleries
							INNER JOIN gallery_type
							ON		gallery_type.gallery_type_id	= galleries.gallery_type
							INNER JOIN models
							ON		models.model_id			= galleries.model_id
							LEFT JOIN models AS second_model
							ON		second_model.model_id	= galleries.second_model_id
							LEFT JOIN models AS third_model
							ON		third_model.model_id	= galleries.third_model_id
							LEFT JOIN models AS fourth_model
							ON		fourth_model.model_id	= galleries.fourth_model_id
							LEFT JOIN videos
							ON		videos.video_id			= galleries.video_clip_id
							LEFT JOIN fast_tracks
							ON		fast_tracks.ft_gallery_id 	= galleries.gallery_id
							AND		fast_tracks.ft_pref_id		= #session.pref_id#
							LEFT JOIN permanent_fast_tracks
							ON		permanent_fast_tracks.pft_gallery_id	= galleries.gallery_id
							AND		permanent_fast_tracks.pft_pref_id		= #session.pref_id#
							WHERE	galleries.show_future		= 1
							AND		(galleries.linked_release = 0 OR (galleries.linked_release = 1 AND galleries.v2_video_clip_id != 9999))
							#whereclause#
							GROUP BY galleries.gallery_id
							ORDER BY
							#orderby#,gallery_type.gallery_type_order DESC";
    	var qry = QueryExecute(strQuery);

    	var rsStruct = {};
    	rsStruct.strQuery = strQuery;
    	rsStruct.rsQuery = qry;

    	return rsStruct;
		
    }

    public query function getComingSoonModelLists(string strQuery) {

    	var strQry = "SELECT DISTINCT model_display_name, model_id
							FROM (#strQuery#) a
							ORDER BY model_display_name ASC";
    	return QueryExecute(strQry);
    }

    public query function getComingSoonSetByModelName(string strQuery, string updatesFilterModelName) {

    	var strQry = "SELECT  gallery_id,
									tour_pic1_thumb,
									tour_pic2_thumb,
									tour_pic3_thumb,
									updates_pic_thumb,
									mailing_list_pic_thumb,
									model_id,
									release_date,
									DATE_FORMAT( release_date, '%D %M %Y' ) as release_date_formarted,
									gallery_type,
									gallery_description,
									zipfile,
									zipfile_size,
									lrg_zipfile,
									lrg_zipfile_size,
									ultra_zipfile,
									ultra_zipfile_size,
									total_votes_cast_outfit,
									average_outfit,
									average_model,
									directory_name,
									model_display_name,
									second_model_name,
									second_model_id,
									third_model_name,
									third_model_id,
									fourth_model_name,
									fourth_model_id,
									image_count,
									video_length,
									video_directory_name,
									video_hd_res_name,
									video_mp4_hd_name,
									video_hd_res_filesize,
									video_high_res_name,
									video_high_res_filesize,
									video_medium_res_name,
									video_medium_res_filesize,
									video_low_res_name,
									video_low_res_filesize,
									video_format,
									video_no_clips,
									video_clip,
									ft_pref_id,
									ft_month,
									ft_year,
									pft_pref_id
							FROM (#strQuery#) a
							WHERE a.model_id = :mID
									OR a.second_model_id = :smID
									OR a.third_model_id = :tmID
									OR a.fourth_model_id = :fmID";
    	return QueryExecute(strQry,
							{
								mID:updatesFilterModelName,
								smID:updatesFilterModelName,
								tmID:updatesFilterModelName,
								fmID:updatesFilterModelName
							});
    }
}