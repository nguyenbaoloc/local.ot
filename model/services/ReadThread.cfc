component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}

	public any function get_topic (numeric pThreadID) {
		return QueryExecute("SELECT 	ForumThreads.Subject,
										ForumThreads.Author,
										ForumThreads.DateIn,
										DATE_FORMAT( ForumThreads.DateIn, '%D %M %Y' ) as DateIn_formarted,
										ForumThreads.ForumID,
										ForumThreads.Thread,
										ForumThreads.ThreadID,
										ForumThreads.thread_suspended,
										ForumThreads.PollQuestionID,
										ForumPreferences.fp_id,
										ForumPreferences.forum_nickname,
										ForumPreferences.fut_id,
										ForumPreferences.dynamic_nick,
										ForumPreferences.title,
										ForumPreferences.avatar,
										ForumPreferences.age,
										ForumPreferences.location,
										ForumPreferences.total_all_posts_count,
										ForumUserTypes.fut_icon,
										forum_poll_questions.Question,
										forum_poll_questions.QuestionID,
										IF(DATE(forum_poll_questions.Question_close_date) < DATE(NOW()), 0, 1) AS Question_status
								FROM 	ForumThreads
								LEFT JOIN ForumPreferences
								ON		ForumThreads.fp_id = ForumPreferences.fp_id
								LEFT JOIN ForumUserTypes
								ON		ForumPreferences.fut_id = ForumUserTypes.fut_id
								LEFT JOIN	forum_poll_questions
								ON		forum_poll_questions.QuestionID = ForumThreads.PollQuestionID
								WHERE 	ThreadID = ?
								AND		Parent = 0
								AND		ForumThreads.Deleted = 0",
								[{sqltype: "cf_sql_integer", value: pThreadID}],
								{
									datasource: application.forum_datasource
								});
	}

	public any function insert_forum_poll_votes (numeric pPollQuestionID, numeric pradioPoll) {
		return QueryExecute("INSERT IGNORE INTO forum_poll_votes (
								fpv_user_id,
								fpv_question_id,
								fpv_answer_id
							)
							VALUES (
								#session.pref_id#,
								?,
								?
							)",
							[
								{sqltype: "cf_sql_integer", value: pPollQuestionID},
								{sqltype: "cf_sql_integer", value: pradioPoll}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function getPollAnswers (numeric pQuestionID) {
		return QueryExecute("SELECT		Answer,
										AnswerID,
										Votes
							FROM		forum_poll_answers
							WHERE		QuestionID = ?
							ORDER BY	AnswerID ASC",
							[
								{sqltype: "cf_sql_integer", value: pQuestionID}
							],
							{
								datasource: application.forum_datasource
							});
	}
	
	public any function get_thread (numeric Thread) {
		return QueryExecute("SELECT 	ForumThreads.Body,
										ForumThreads.ThreadID,
										ForumThreads.DateIn,
										DATE_FORMAT( ForumThreads.DateIn, '%D %M %Y' ) as DateIn_formarted,
										ForumThreads.Author,
										ForumThreads.login_username,
										ForumThreads.posted_on,
										ForumThreads.EditCount,
										DATE_FORMAT( ForumThreads.LastEdit, '%D %M %Y' ) as LastEdit_formarted,
										ForumThreads.LastEdit,
										ForumThreads.MembersOnly,
										ForumThreads.Parent,
										ForumPreferences.fp_id,
										ForumPreferences.forum_nickname,
										ForumPreferences.fut_id,
										ForumPreferences.dynamic_nick,
										ForumPreferences.title,
										ForumPreferences.avatar,
										ForumPreferences.age,
										ForumPreferences.location,
										ForumPreferences.forum_email,
										ForumPreferences.total_all_posts_count,
										ForumPreferences.fp_first_login_date,
										DATE_FORMAT( ForumPreferences.fp_first_login_date, '%D %M %Y' ) as fp_first_login_date_formarted,
										ForumUserTypes.fut_icon
								FROM ForumThreads
								LEFT JOIN ForumPreferences
								ON		ForumThreads.fp_id = ForumPreferences.fp_id
								LEFT JOIN ForumUserTypes
								ON		ForumPreferences.fut_id = ForumUserTypes.fut_id
								WHERE Thread = ?
								AND		ForumThreads.Deleted = 0
								ORDER BY Parent, DateIn asc",
							[
								{sqltype: "cf_sql_integer", value: Thread}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function update_view_1 (numeric pThread) {
		return QueryExecute("SELECT ThreadID,Views FROM ForumThreads WHERE Thread = ?",
							[
								{sqltype: "cf_sql_integer", value: pThread}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function update_view_2 (any pViews, numeric pThreadID) {
		return QueryExecute("UPDATE ForumThreads
							Set Views = ?
							WHERE ThreadID =  ?",
							[
								{sqltype: "cf_sql_integer", value: pThreadID},
								{sqltype: "cf_sql_integer", value: pViews}
							],
							{
								datasource: application.forum_datasource
							});
	}

	public any function qry_forumeditall () {
		return QueryExecute("SELECT  forum_edit_all, dynamic_nick, fp_id
							FROM	ForumPreferences
							WHERE	fp_username = ?",
							[
								{sqltype: "cf_sql_varchar", value: trim(cgi.remote_user)}
							],
							{
								datasource: application.dsn_name_forum
							});
	}


}