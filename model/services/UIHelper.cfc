component accessors="true" output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}
	
	public any function displayVideo (query qry_sets, string is_bts="video", string bts_id="0", string strServerVideoLink="") {
		var op_hd_res = "";
		var op_high_res = "";
		var op_med_res = "";
		var op_low_res = "";
		var op_mp4_sd = "";
		var op_mp4_hd = "";

		if (is_bts EQ "bts") {
        	rc.vidData = QueryExecute(
	        							" SELECT
							                    videos.video_total_time             AS video_length,
							                    videos.video_directory_name,
							                    videos.video_hd_res_name,
							                    videos.video_hd_res_filesize,
							                    videos.video_high_res_name,
							                    videos.video_high_res_filesize,
							                    videos.video_medium_res_name,
							                    videos.video_medium_res_filesize,
							                    videos.video_low_res_name,
							                    videos.video_low_res_filesize,
							                    videos.video_format,
							                    videos.video_no_clips,
							                    videos.video_mp4_hd_name,
							                    videos.video_mp4_hd_filesize,
							                    videos.video_mp4_sd_name,
							                    videos.video_mp4_sd_filesize,
							                    videos.video_webm_name,
							                    videos.video_webm_filesize
							            FROM    galleries
							            INNER JOIN videos
							            ON      videos.video_id = galleries.video_clip_id
							            WHERE   galleries.gallery_id = :bts_id
							            LIMIT 1",
							            {
							            	bts_id: {sqltype="CF_SQL_INTEGER",value=bts_id}
							            },
							            {
							            	datasource:application.dsn_name,
							            	cachedwithin:application.queryTimeCache
							            }
	        						);
 		}  
   		else {
        	rc.vidData = QueryExecute(
        								"SELECT
							                    video_length,
							                    video_directory_name,
							                    video_hd_res_name,
							                    video_hd_res_filesize,
							                    video_high_res_name,
							                    video_high_res_filesize,
							                    video_medium_res_name,
							                    video_medium_res_filesize,
							                    video_low_res_name,
							                    video_low_res_filesize,
							                    video_format,
							                    video_no_clips,
							                    video_mp4_hd_name,
							                    video_mp4_hd_filesize,
							                    video_mp4_sd_name,
							                    video_mp4_sd_filesize,
							                    video_webm_name,
							                    video_webm_filesize
							            FROM    qry_sets
							            WHERE gallery_id = :bts_id",
							            {
							            	bts_id: {sqltype="CF_SQL_INTEGER",value=bts_id}
							            },
							            {
							            	dbtype:"query",
							            	cachedwithin:application.queryTimeCache
							            }
        							);
    	}
		return rc.vidData;
    }

    public any function displayVideoModelSets (query qry_sets, string is_bts="video", string bts_id="0", string strServerVideoLink="") {
		var op_hd_res = "";
		var op_high_res = "";
		var op_med_res = "";
		var op_low_res = "";
		var op_mp4_hd = "";
		var op_mp4_sd = "";

		if (is_bts EQ "bts") {
        	rc.vidData = QueryExecute(
	        							" SELECT
							                    videos.video_total_time             AS video_length,
							                    videos.video_directory_name,
							                    videos.video_hd_res_name,
							                    videos.video_hd_res_filesize,
							                    videos.video_high_res_name,
							                    videos.video_high_res_filesize,
							                    videos.video_medium_res_name,
							                    videos.video_medium_res_filesize,
							                    videos.video_low_res_name,
							                    videos.video_low_res_filesize,
							                    videos.video_format,
							                    videos.video_no_clips,
							                    videos.video_mp4_hd_name,
							                    videos.video_mp4_hd_filesize,
							                    videos.video_mp4_sd_name,
							                    videos.video_mp4_sd_filesize,
							                    videos.video_webm_name,
							                    videos.video_webm_filesize
							            FROM    galleries
							            INNER JOIN videos
							            ON      videos.video_id = galleries.video_clip_id
							            WHERE   galleries.gallery_id = :bts_id
							            LIMIT 1",
							            {
							            	bts_id: {sqltype="CF_SQL_INTEGER",value=bts_id}
							            },
							            {
							            	datasource:application.dsn_name,
							            	cachedwithin:application.queryTimeCache
							            }
	        						);
 		}  
   		else {
        	rc.vidData = QueryExecute(
        								"SELECT
							                    video_length,
							                    video_directory_name,
							                    video_hd_res_name,
							                    video_hd_res_filesize,
							                    video_high_res_name,
							                    video_high_res_filesize,
							                    video_medium_res_name,
							                    video_medium_res_filesize,
							                    video_low_res_name,
							                    video_low_res_filesize,
							                    video_format,
							                    video_no_clips,
							                    video_mp4_hd_name,
							                    video_mp4_hd_filesize,
							                    video_mp4_sd_name,
							                    video_mp4_sd_filesize,
							                    video_webm_name,
							                    video_webm_filesize
							            FROM    qry_sets
							            WHERE gallery_id = :bts_id",
							            {
							            	bts_id: {sqltype="CF_SQL_INTEGER",value=bts_id}
							            },
							            {
							            	dbtype:"query",
							            	cachedwithin:application.queryTimeCache
							            }
        							);
    	}                   
		
		return rc.vidData;
    }

    public struct function udf_dailyStrips(date dat_release="",string uui_release="",boolean allsites_member=1, boolean allsites_memberdirect=1, boolean tease_memberpage=1) {

	    <!--- set a result of struct --->
	    var stc_result = {};

	    <!--- create array to store other sites set data for this release day --->
	    var arr_otherDaysSets = [];
	    for(var i=1;i<=ArrayLen(application.arr_othersites); i++) {
	    	if(application.arr_othersites[i].str_datasource IS NOT application.dsn_name) {
	    		var qry_otherSitesSets = QueryExecute(
	    												"SELECT      galleries.tour_pic1_thumb,
										                            galleries.tour_pic2_thumb,
										                            galleries.tour_pic3_thumb,
										                            galleries.model_id,
										                            galleries.gallery_type
										                FROM        galleries
										                INNER JOIN  gallery_type
										                ON          gallery_type.gallery_type_id = galleries.gallery_type
										                LEFT JOIN   models
										                ON          models.model_id = galleries.model_id
										                LEFT JOIN   models AS second_model
										                ON          second_model.model_id = galleries.second_model_id
										                LEFT JOIN   models AS third_model
										                ON          third_model.model_id = galleries.third_model_id
										                LEFT JOIN   models AS fourth_model
										                ON          fourth_model.model_id = galleries.fourth_model_id
										                LEFT JOIN   videos
										                ON          videos.video_id = galleries.video_clip_id
										                WHERE       galleries.show_future = 1
										                AND     	galleries.release_date = ?
										                AND         (galleries.linked_release = 0 OR (galleries.linked_release = 1 AND galleries.video_clip_id != 9999))
										                GROUP BY    galleries.gallery_id
										                ORDER BY    galleries.release_date desc,
										                            gallery_type.gallery_type_order DESC",
										                [
										                	{sqltype:"CF_SQL_DATE", value:dat_release}
										                ],
										                {
										                	datasource:application.arr_othersites[i].str_datasource,
										                	cachedwithin:application.queryTimeCache
										                }
										    		);
	            if(qry_otherSitesSets.RecordCount > 0) {
	            	for(var item in qry_otherSitesSets) {
	            		var stc_modelSet = {};
                     	stc_modelSet.str_member_url = "#application.arr_otherSites[i].str_siteUrlMembers#/model.default?model_id=#item.model_id#";
                     	stc_modelSet.int_model_id = item.model_id;
                     	stc_modelSet.str_standsforsite = application.arr_otherSites[i].str_standsforsite;
                     	stc_modelSet.str_site = application.arr_otherSites[i].str_siteName;
                     	stc_modelSet.str_tour_url = application.arr_otherSites[i].str_siteUrlTour;
                     	stc_modelSet.str_logo = application.arr_otherSites[i].str_siteLogo;
                     	stc_modelSet.str_thumb = application.arr_otherSites[i].str_siteThumb;
                     	stc_modelSet.str_colour = application.arr_otherSites[i].str_siteColour;
                     	stc_modelSet.arr_tourImages = ArrayNew(1);
                     	stc_modelSet.arr_tourImages[1] = "#application.arr_otherSites[i].str_picsPath#/#item.tour_pic1_thumb#";
                     	stc_modelSet.arr_tourImages[2] = "#application.arr_otherSites[i].str_picsPath#/#item.tour_pic2_thumb#";
                     	stc_modelSet.arr_tourImages[3] = "#application.arr_otherSites[i].str_picsPath#/#item.tour_pic3_thumb#";
                     	stc_modelSet.arr_tourImages[5] = "/views/images/spacer.gif";
                     	if(allsites_member EQ 1) {
	                        stc_modelSet.arr_tourImages[4] = "#application.arr_otherSites[i].str_siteUrlMembers#/index.cfm/members.view_gallery?gallery_id=#item.gallery_id#";
	                    }
	                    else if (allsites_memberdirect EQ 1) {
	                        stc_modelSet.arr_tourImages[4] = "#application.arr_otherSites[i].str_siteUrlMembers#/index.cfm/members.view_gallery?gallery_id=#item.gallery_id#";
	                   	}
	                   	else if (tease_memberpage EQ 1) {
	                        stc_modelSet.arr_tourImages[4] = "http://members.onlytease.com/members/ccbill/upgrade2OAS.cfm";
	                    }
	                    else {
	                        stc_modelSet.arr_tourImages[4] = "http://members.onlytease.com/members/ccbill/upgrade2OAS.cfm";
	                    }
	                    stc_modelSet.arr_tourImages[6] = item.gallery_type;
	                    ArrayAppend(arr_otherDaysSets, stc_modelSet);
	            	}
	            }
	        }
	    }
	    <!--- create list of images and their urls, etc for the slider script --->
	    if(ArrayLen(arr_otherDaysSets) NEQ 0) {
	        var imageList_OO = "";
	        var imageList_OS = "";
	        var imageList_OSS = "";
	        var arr_OO = [];
	        var arr_OS = [];
	        var arr_OSS = [];
	        var imageCounter = 0;
	        var lastsite = "";
	        for(var l=1; l<=ArrayLen(arr_otherDaysSets); l++) {
	            if(ArrayLen(arr_otherDaysSets[l].arr_tourImages) GT 1) {
	                var setUrl = arr_otherDaysSets[l].arr_tourImages[4];
	                imageCounter = IncrementValue(imageCounter);
	                
	                if(lastsite NEQ "#arr_otherDaysSets[l].str_standsforsite#") {
	                    var strTemp = {};
	                    strTemp.img = arr_otherDaysSets[l].str_thumb;
	                    strTemp.link = arr_otherDaysSets[l].arr_tourImages[5];

	                    //add logo image first
	                    switch(arr_otherDaysSets[l].str_standsforsite){
	                    	case "oo":
	                    		arrayAppend(arr_OO,{link:setUrl, image: arr_otherDaysSets[l].str_thumb, gallery_type: 1});
	                    	break;

	                    	case "os":
	                    		arrayAppend(arr_OS,{link:setUrl, image: arr_otherDaysSets[l].str_thumb, gallery_type: 1});
	                    	break;

	                    	case "oss":
	                    		arrayAppend(arr_OSS,{link:setUrl, image: arr_otherDaysSets[l].str_thumb, gallery_type: 1});
	                    	break;
	                    }
	                }

	               	//add sample images
	                for(var j=1; j<=3; j++) {
	                	imageCounter = IncrementValue(imageCounter);
	                	switch(arr_otherDaysSets[l].str_standsforsite){
	                    	case "oo":
	                    		arrayAppend(arr_OO,{link:setUrl, image: arr_otherDaysSets[l].arr_tourImages[j], gallery_type: arr_otherDaysSets[l].arr_tourImages[6]});
	                    	break;

	                    	case "os":
	                    		arrayAppend(arr_OS,{link:setUrl, image: arr_otherDaysSets[l].arr_tourImages[j], gallery_type: arr_otherDaysSets[l].arr_tourImages[6]});
	                    	break;

	                    	case "oss":
	                    		arrayAppend(arr_OSS,{link:setUrl, image: arr_otherDaysSets[l].arr_tourImages[j], gallery_type: arr_otherDaysSets[l].arr_tourImages[6]});
	                    	break;
	                    }
	                }
	                lastsite = "#arr_otherDaysSets[l].str_standsforsite#";
	            }
	        }
	        stc_result.arr_OO = arr_OO;
	        stc_result.arr_OS = arr_OS;
	        stc_result.arr_OSS = arr_OSS;
	    }
	    stc_result.arr_otherDaysSets = arr_otherDaysSets;
	    stc_result.str_releaseDate = DateFormat(arguments.dat_release, "yyyymmdd");
	    stc_result.uui_release = uui_release;
	    stc_result.dat_release = dat_release;
	    return stc_result;
    }

    public struct function udf_Vipsets(required date dat_release, string uui_release="#CreateUUID()#", string sView="all") {
    	var stc_result = {};

    	var bonus_condition = "";
    	if(sView EQ "pictures") {
    		bonus_condition = " AND galleries.video_clip_id = 9999 ";
    	}
		if(sView EQ "videos") {
			bonus_condition = " AND galleries.video_clip_id != 9999 ";
		}

    	var vipUpdates = QueryExecute(
    									"SELECT  galleries.gallery_id,
							                    galleries.tour_pic1_thumb,
							                    galleries.tour_pic2_thumb,
							                    galleries.tour_pic3_thumb,
							                    galleries.updates_pic_thumb,
							                    galleries.mailing_list_pic_thumb,
							                    galleries.model_id,
							                    galleries.release_date,
							                    galleries.gallery_type,
							                    galleries.gallery_description,
							                    galleries.zipfile,
							                    galleries.zipfile_size,
							                    galleries.lrg_zipfile,
							                    galleries.lrg_zipfile_size,
							                    galleries.ultra_zipfile,
							                    galleries.ultra_zipfile_size,
							                    galleries.total_votes_cast_outfit,
							                    galleries.total_votes_cast_tease,
							                    galleries.average_outfit,
							                    galleries.average_tease,
							                    galleries.average_model,
							                    galleries.directory_name,
							                    models.model_display_name,
							                    second_model.model_display_name     AS second_model_name,
							                    galleries.second_model_id,
							                    third_model.model_display_name      AS third_model_name,
							                    galleries.third_model_id,
							                    fourth_model.model_display_name     AS fourth_model_name,
							                    galleries.fourth_model_id,
							                    galleries.gallery_n_images          AS image_count,
							                    videos.video_total_time             AS video_length,
							                    videos.video_directory_name,
							                    videos.video_hd_res_name,
							                    videos.video_hd_res_filesize,
							                    videos.video_high_res_name,
							                    videos.video_high_res_filesize,
							                    videos.video_medium_res_name,
							                    videos.video_medium_res_filesize,
							                    videos.video_low_res_name,
							                    videos.video_low_res_filesize,
							                    videos.video_format,
							                    videos.video_no_clips,
							                    IF (galleries.video_clip_id != 9999,1,0) AS video_clip
							            FROM    galleries
							            LEFT JOIN ot2004_data.models
							            ON      models.model_id         = galleries.model_id
							            LEFT JOIN ot2004_data.models AS second_model
							            ON      second_model.model_id   = galleries.second_model_id
							            LEFT JOIN ot2004_data.models AS third_model
							            ON      third_model.model_id    = galleries.third_model_id
							            LEFT JOIN ot2004_data.models AS fourth_model
							            ON      fourth_model.model_id   = galleries.fourth_model_id
							            LEFT JOIN videos
							            ON      videos.video_id         = galleries.video_clip_id
							            LEFT JOIN gallery_type
							            ON      gallery_type.gallery_type_id    = galleries.gallery_type
							            WHERE       galleries.release_date = ? 
							            #sView neq 'all' ? bonus_condition : ''#
							            ",
							            [
						                	{sqltype:"CF_SQL_DATE", value:dat_release}
						                ],
							            {
							            	datasource:"vip_data",
							            	cachedwithin:application.queryTimeCache
							        	}
							    	);
		var image_set = true;
		if(ListFind(application.video_set_types, vipUpdates.gallery_type)) {
            image_set = false;
        }

        <!--- create the string of model names --->
        var and_switch = false;
        var model_names = "";
        var model_multiple = false;
        if(vipUpdates.fourth_model_name NEQ "") {
            model_names = " and <a href=""#application.vip_location#/model.default?model_id=#vipUpdates.fourth_model_id#"" title=""Click to view all sets of this model"">#vipUpdates.fourth_model_name#</a>";
            and_switch = true;
            model_multiple = true;
        }
        if(vipUpdates.third_model_name NEQ "") {
            if(and_switch) {
                model_names = ", <a href=""#application.vip_location#/model.default?model_id=#vipUpdates.third_model_id#"" title=""Click to view all sets of this model"">#vipUpdates.third_model_name#</a>" & model_names;
            }
            else {
                model_names = " and <a href=""#application.vip_location#/model.default?model_id=#vipUpdates.third_model_id#"" title=""Click to view all sets of this model"">#vipUpdates.third_model_name#</a>";
                and_switch = true;
                model_multiple = true;
            }
        }
        if(vipUpdates.second_model_name NEQ "") {
            if(and_switch) {
                model_names = ", <a href=""#application.vip_location#/model.default?model_id=#vipUpdates.second_model_id#"" title=""Click to view all sets of this model"">#vipUpdates.second_model_name#</a>" & model_names;
            }
            else {
                model_names = " and <a href=""#application.vip_location#/model.default?model_id=#vipUpdates.second_model_id#"" title=""Click to view all sets of this model"">#vipUpdates.second_model_name#</a>";
                and_switch = true;
                model_multiple = true;
            }
        }
        model_names = "<a href=""#application.vip_location#/model.default?model_id=#vipUpdates.model_id#"" title=""Click to view all sets of this model"" target=""_new"">#vipUpdates.model_display_name#</a>" & model_names;

        stc_result.str_releaseDate = DateFormat(arguments.dat_release, "yyyymmdd");
        stc_result.vipUpdates = vipUpdates;
        stc_result.model_names = model_names;
        stc_result.image_set = image_set;
        stc_result.dat_release = dat_release;
        stc_result.uui_release = uui_release;

        return stc_result;
    }

    public any function set_TextOfGalleryType (numeric gallery_type) {
    	
		var bonus_set = '';
        switch(gallery_type){
         	case 2:
        	 	bonus_set = '<span class="bonusSet">Bonus Set!</span>';
        	 	break;
            case 3:
        	 	bonus_set = '<span class="momSet">Model of the Month!</span>';
        	 	break;
            case 5:
        	 	bonus_set = '<span class="bonusSet">Bonus Video Set!</span>';
        	 	break;
            case 6:
        	 	bonus_set = '<span class="momSet">Model of the Month Video!</span>';
        	 	break;
            case 7:
        	 	bonus_set = '<span class="bonusSet">Prestige Tease Set</span>';
        	 	break;
            case 10:
        	 	bonus_set = '<span class="bonusSet">Model of the Year Bonus</span>';
        	 	break;
            case 11:
        	 	bonus_set = '<span class="bonusSet">Newcomer of the Year Bonus</span>';
        	 	break;
            case 12:
        	 	bonus_set = '<span class="bonusSet">Model of Year Video!</span>';
        	 	break;
            case 13:
        	 	bonus_set = '<span class="bonusSet">Newcomer of Year Video!</span>';
        	 	break;
            case 14:
        	 	bonus_set = '<span class="bonusSet">Interview  Video</span>';
        	 	break;
            case 15:
        	 	bonus_set = '<span class="bonusSet">Guest Editor Bonus</span>';
        	 	break;
            case 16:
        	 	bonus_set = '<span class="bonusSet">Guest Editor Bonus Video</span>';
        	 	break;
            case 17:
        	 	bonus_set = '<span class="bonusSet">Tease @ Home Photo Set</span>';
        	 	break;
            case 18:
        	 	bonus_set = '<span class="bonusSet">Tease @ Home Video Set</span>';
        	 	break;
            case 19:
        	 	bonus_set = '<span class="bonusSet">Behind the Scenes Photos</span>';
        	 	break;
            case 20:
        	 	bonus_set = '<span class="bonusSet">Behind the Scenes Videos</span>';
        	 	break;
         }

        return bonus_set;
    }

    public any function paging(struct config, string type) {

		var MaxPages = ceiling(config.TotalRecords / config.DisplayCount);
		var PageOffSet = int(config.PageGroup / 2);
		var FromPage = config.CurrentPage - 2;
		var ToPage = config.CurrentPage  + 2;

		if(FromPage <= 1) {
			FromPage = 1;
		}
		if(ToPage >= MaxPages) {
			ToPage = MaxPages;
		}
		if(ToPage - FromPage < 4) {
			if(MaxPages == ToPage) {
				FromPage = ToPage - 4;
				if(FromPage <= 1) {
					FromPage = 1;
				}
			}
			else {
				ToPage = FromPage + 4;
				if(ToPage >= MaxPages) {
					ToPage == MaxPages;
				}
			}
		}

		var next = (config.CurrentPage + 1) * 1;
		var prev = (config.CurrentPage - 1) * 1;
		if(next >= MaxPages) {
			next = MaxPages;
		}
		if(prev <= 1) {
			prev = 1;
		}

    	switch(type){
    		case 'dd':
    			var PageStr = "<span><label>PAGE</label></span><span class=""marginLeft5px""><select id=""ddSet"" class=""filterSelect"">";
    		break;
    		case 'ul':
    			var PageStr = "<ul class=""pagination""><li><span>#config.show_type# <strong>#config.start_row#</strong> to <strong>#config.end_row#</strong> of <strong>#config.TotalRecords#</strong></span></li><li><span>PAGE</span></li>";
    		break;
    	}
		
		if (MaxPages GT 1) {
			switch(type){
	    		case 'dd':
	    			for (var count="#FromPage#"; count <="#MaxPages#"; count ++) {
						PageStr = "#PageStr# <option #Count IS config.CurrentPage ? 'selected' : ''# value=""#config.TemplateURL#?CurrentPage=#Count##config.AddedPath#"">#Count#</option> ";
					}
					PageStr &= "</select></span>";
	    		break;
	    		case 'ul':
	    			if(FromPage > 1) {
	    				PageStr = "#PageStr# <li><A HREF=""#config.TemplateURL#?CurrentPage=#prev##config.AddedPath#"" title=""Go to page #prev#""><i class=""fa fa-chevron-left""></i></A></li> ";
	    				PageStr = "#PageStr# <li><A HREF=""#config.TemplateURL#?CurrentPage=1#config.AddedPath#"" title=""Go to page 1"">1</A></li><li><span>...</span></li> ";
	    			}
	    			for (var count="#FromPage#"; count <="#ToPage#"; count ++) {
						PageStr = "#PageStr# <li><A class=""#Count IS config.CurrentPage ? 'active' : ''#"" HREF=""#config.TemplateURL#?CurrentPage=#Count##config.AddedPath#"" title=""Go to page #Count#""> #Count#</A></li> ";
					}
					if(ToPage < MaxPages) {
	    				PageStr = "#PageStr# <li><span>...</span></li><li><A HREF=""#config.TemplateURL#?CurrentPage=#MaxPages##config.AddedPath#"" title=""Go to page #MaxPages#"">#MaxPages#</A></li>";
						PageStr &= "<li><A HREF=""#config.TemplateURL#?CurrentPage=#next##config.AddedPath#"" title=""Go to page #next#""><i class=""fa fa-chevron-right""></i></A></li> ";
	    			}
					PageStr &= "</ul>";
	    		break;
	    	}
		}
		else {
			PageStr = "";
		}

		return PageStr;
    }

  //   public any function paging(struct config, string type) {

		// var MaxPages = ceiling(config.TotalRecords / config.DisplayCount);
		// var PageOffSet = int(config.PageGroup / 2);
		// var FromPage = config.CurrentPage - 2;
		// var ToPage = config.CurrentPage  + 2;

		// if(FromPage <= 1) {
		// 	FromPage = 1;
		// }
		// if(ToPage >= MaxPages) {
		// 	ToPage = MaxPages;
		// }
		// if(ToPage - FromPage < 4) {
		// 	if(MaxPages == ToPage) {
		// 		FromPage = ToPage - 4;
		// 		if(FromPage <= 1) {
		// 			FromPage = 1;
		// 		}
		// 	}
		// 	else {
		// 		ToPage = FromPage + 4;
		// 		if(ToPage >= MaxPages) {
		// 			ToPage == MaxPages;
		// 		}
		// 	}
		// }

		// var next = (config.CurrentPage + 1) * 1;
		// var prev = (config.CurrentPage - 1) * 1;
		// if(next >= MaxPages) {
		// 	next = MaxPages;
		// }
		// if(prev <= 1) {
		// 	prev = 1;
		// }

  //   	switch(type){
  //   		case 'dd':
  //   			var PageStr = "<span><label>#config.show_type# <strong>#config.start_row#</strong> to <strong>#config.end_row#</strong> of <strong>#config.TotalRecords#</strong></label></span><span class=""marginLeft5px""><label>PAGE</label></span>";
  //   			if(FromPage > 1) {
  //   				PageStr &= "<span class=""marginLeft5px""><A HREF="""" onclick=""filterUpdate(#prev#);return false;"" title=""Go to page #prev#""><i class=""fa fa-chevron-left""></i></A></span>";
  //   			}
  //   			PageStr &= "<span class=""marginLeft5px""><select id=""ddSet"" class=""filterSelect"">";
  //   		break;
  //   		case 'ul':
  //   			var PageStr = "<ul class=""pagination""><li><span>#config.show_type# <strong>#config.start_row#</strong> to <strong>#config.end_row#</strong> of <strong>#config.TotalRecords#</strong></span></li><li><span>PAGE</span></li>";
  //   		break;
  //   	}
		
		// if (MaxPages GT 1) {
		// 	switch(type){
	 //    		case 'dd':
	 //    			for (var count="#FromPage#"; count <="#MaxPages#"; count ++) {
		// 				PageStr = "#PageStr# <option #Count IS config.CurrentPage ? 'selected="selected"' : ''# onclick=""filterUpdate(#Count#)"">#Count#</option> ";
		// 			}
		// 			PageStr &= "</select></span>";
		// 			if(ToPage < MaxPages) {
		// 				PageStr &= "<span><A HREF="""" onclick=""filterUpdate(#next#);return false;"" title=""Go to page #next#""><i class=""fa fa-chevron-right""></i></A></span>";

		// 			}
	 //    		break;
	 //    		case 'ul':
	 //    			if(FromPage > 1) {
	 //    				PageStr = "#PageStr# <li><A HREF="""" onclick=""filterUpdate(#prev#);return false;"" title=""Go to page #prev#""><i class=""fa fa-chevron-left""></i></A></li> ";
	 //    				PageStr = "#PageStr# <li><A HREF="""" onclick=""filterUpdate(1);return false;"" title=""Go to page 1"">1</A></li><li><span>...</span></li> ";
	 //    			}
	 //    			for (var count="#FromPage#"; count <="#ToPage#"; count ++) {
		// 				PageStr = "#PageStr# <li><A class=""#Count IS config.CurrentPage ? 'active' : ''#"" HREF="""" onclick=""filterUpdate(#Count#);return false;"" title=""Go to page #Count#""> #Count#</A></li> ";
		// 			}
		// 			if(ToPage < MaxPages) {
	 //    				PageStr = "#PageStr# <li><span>...</span></li><li><A HREF="""" onclick=""filterUpdate(#MaxPages#);return false;"" title=""Go to page #MaxPages#"">#MaxPages#</A></li>";
		// 				PageStr &= "<li><A HREF="""" onclick=""filterUpdate(#next#);return false;"" title=""Go to page #next#""><i class=""fa fa-chevron-right""></i></A></li> ";
	 //    			}
		// 			PageStr &= "</ul>";
	 //    		break;
	 //    	}
		// }
		// else {
		// 	PageStr = "";
		// }

		// return PageStr;
  //   }

    public struct function setDailyStripForModelSet(array aOtherModelSets) {
    	var stc_result = {};
    	if(ArrayLen(aOtherModelSets) NEQ 0) {
	        var arr_OO = [];
	        var arr_OS = [];
	        var arr_OSS = [];
	        var logo_OO = {};
	        var logo_OS = {};
	        var logo_OSS = {};
	        for(var l=1; l<=ArrayLen(aOtherModelSets); l++) {
	        	//add logo image first
        		switch(aOtherModelSets[l].str_standsforsite){
                	case "oo":
                		logo_OO.link = aOtherModelSets[l].str_tour_url;
                		logo_OO.image = aOtherModelSets[l].str_logo; 
                		logo_OO.gallery_type = 1;
                	break;

                	case "os":
                		logo_OS.link = aOtherModelSets[l].str_tour_url;
                		logo_OS.image = aOtherModelSets[l].str_logo; 
                		logo_OS.gallery_type = 1;
                	break;

                	case "oss":
                		logo_OSS.link = aOtherModelSets[l].str_tour_url;
                		logo_OSS.image = aOtherModelSets[l].str_logo; 
                		logo_OSS.gallery_type = 1;
                	break;
                }

                //add sample images
	            if(ArrayLen(aOtherModelSets[l].arr_tourImages) GT 1) {
	                var setUrl = aOtherModelSets[l].arr_tourImages[4];
	               	for(var i=1;i<=arraylen(aOtherModelSets[l].arr_tourImages);i++) {
	               		for(var j=1;j<=3;j++) {
	               			if(arrayLen(aOtherModelSets[l].arr_tourImages[i]) > 0) {
	               				switch(aOtherModelSets[l].str_standsforsite){
			                    	case "oo":
			                    		arrayAppend(arr_OO,{link:aOtherModelSets[l].arr_tourImages[i][4], image: aOtherModelSets[l].arr_tourImages[i][j], gallery_type: aOtherModelSets[l].arr_tourImages[i][6]});
			                    	break;

			                    	case "os":
			                    		arrayAppend(arr_OS,{link:aOtherModelSets[l].arr_tourImages[i][4], image: aOtherModelSets[l].arr_tourImages[i][j], gallery_type: aOtherModelSets[l].arr_tourImages[i][6]});
			                    	break;

			                    	case "oss":
			                    		arrayAppend(arr_OSS,{link:aOtherModelSets[l].arr_tourImages[i][4], image: aOtherModelSets[l].arr_tourImages[i][j], gallery_type: aOtherModelSets[l].arr_tourImages[i][6]});
			                    	break;
			                    }
	               			}
		                }
	               	}
	            }
	        }
	        stc_result.arr_OO = arr_OO;
	        stc_result.arr_OS = arr_OS;
	        stc_result.arr_OSS = arr_OSS;
	        stc_result.logo_OO = logo_OO;
	        stc_result.logo_OS = logo_OS;
	        stc_result.logo_OSS = logo_OSS;
	    }

	    return stc_result;
    }
}