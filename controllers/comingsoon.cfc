component accessors="true" output="false" displayname=""  {

	property QueryService;
	property UtilService;
	property UIHelperService;
	property ModelService;
	property QueryExtendService;
	public function init(){
		return this;
	}

	public function default (struct rc) {

    	param name="rc.display_random" default="0";
		param name="URL.viewFT" default=0;
		param name="URL.updatesSortOrder" default=0;
		param name="URL.updatesFilterModelName" default="all";
		param name="URL.CurrentPage" default=1;
		param name="rc.allow_fasttrack" default="FALSE";
		param name="rc.allow_permfasttrack" default="FALSE";
		param name="rc.display_start_row" default="1";

		rc.modelService = variables.ModelService;
		rc.UIHelperService = variables.UIHelperService;
		var comingset = variables.QueryService.getComingSoonSet(URL.updatesSortOrder,URL.viewFT);
		rc.qry_sets = comingset.rsQuery;
		rc.qry_ModelList = variables.QueryService.getComingSoonModelLists(comingset.strQuery);
		if(updatesFilterModelName neq "all") {
			rc.qry_sets = variables.QueryService.getComingSoonSetByModelName(comingset.strQuery,updatesFilterModelName);
		}

		rc.number_days_display = application.number_coming_soon;

		rc.qry_pfts = variables.QueryExtendService.get_pft_remaining(session.pref_id);
		if (rc.qry_pfts.pft_remaining GT 0) {
			rc.allow_permfasttrack="TRUE";
		}

		rc.qry_fts = variables.QueryExtendService.get_ft_remaining(session.pref_id);
		if (rc.qry_fts.ft_remaining GT 0) {
			rc.allow_fasttrack="TRUE";
		}


		if (rc.qry_sets.RecordCount GT 0) {

			var addedpath = "&updatesFilterModelName=#updatesFilterModelName#&viewFT=#viewFT#&updatesSortOrder=#updatesSortOrder#";

			rc.stc_config = variables.UtilService.pagingConfig(rc.qry_sets.RecordCount,CurrentPage,10,addedpath,"#SCRIPT_NAME#/comingsoon.default");
		}
	}
}