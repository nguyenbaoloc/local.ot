component accessors="true" output="false" displayname=""  {

	property UtilService;
	property ModelService;
	property QueryService;
	property QueryExtendService;
	property UIHelperService;
	property ViewGalleryService;
	property ViewImagesService;
	property VoteService;
	property ModelProfileService;

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}

	public function default (struct rc) {
	  
	 	param name="rc.display_random" default="0";
		param name="URL.type" default="model";

		// params for type = model
		param name="rc.search"   default="FALSE";
		param name="rc.search_text" default="";
		param name="rc.letter"   default="";
		param name="rc.CurrentPage" default="1";
		param name="rc.gType" default="a";

		//setup page's variables
	 	rc.QueryService = variables.QueryService;

		//for latest_news MODEL_ID
		rc.arr_month = variables.UtilService.InitArrayOfMonth();

		<!--- get content page for page --->
		
		
		rc.qry_letters = variables.ModelService.getLetters();
		rc.letter_list = ValueList(rc.qry_letters.model_initial);
		rc.searchby = "";
		rc.orderby = "";
		rc.models_used = ""
		if (rc.letter == "" AND NOT rc.search) {
			rc.qry_latest_updates = variables.ModelService.getLatestUpdates();
			rc.models_used = ValueList(rc.qry_latest_updates.model_id,',');
			rc.searchby = " AND  models.model_id NOT IN (#rc.models_used#)  AND  (models.guest_link_url IS NULL OR models.guest_link_url = '')";
			rc.orderby = "ORDER BY RAND()				LIMIT 9";
		}
		else {
			if (rc.search) {
				rc.searchby = " AND models.model_display_name LIKE :search_text";
			}
			else {
				rc.searchby = " AND  models.model_initial = :letter"; 
			}
			rc.orderby = " ORDER BY models.model_display_name ASC";
		}
		rc.qry_models = variables.ModelService.getModelsBySearch(rc.letter,rc.models_used,rc.searchby,rc.orderby,rc.search_text);

		if (rc.search){
			rc.new_added_path = "&type=model&search=#rc.search#&search_text=#rc.search_text#";
		}
		else {
			rc.new_added_path = "&type=model&letter=#rc.letter#";
		}
	}

	public function model_set(struct rc) {

		param name="URL.model_id" default="0";
		param name="URL.gType" default="a";
		param name="URL.CurrentPage" default="1";
		param name="URL.gallery_id" default="0";
		param name="rc.allow_fasttrack" default="FALSE";
		param name="rc.allow_permfasttrack" default="FALSE";

		param name="rc.bol_allsites_member" default="0";
		param name="rc.bol_allsites_memberdirect" default="0";
		param name="rc.bol_tease_memberpage" default="0";
		param name="rc.display_start_row" default="1";

		rc.qry_pfts = variables.ModelService.get_pfts(session.pref_id);

		if (rc.qry_pfts.pft_remaining > 0 OR (rc.qry_pfts.cost == "" && rc.qry_pfts.permanent_fasttracks_purchased > 0)){
			rc.allow_permfasttrack="TRUE";
		}
		rc.allow_fasttrack="TRUE";

		if (rc.bol_allsites_member EQ 1) {
		    rc.strServerZipLink = application.file_locat_url_zip;
		    rc.strServerVideoLink = application.file_locat_url;
		}
		else if (rc.bol_allsites_memberdirect EQ 1) {
		    rc.strServerZipLink = application.direct_file_locat_url_zip;
		    rc.strServerVideoLink = application.direct_file_locat_url;
		}
		else if (rc.bol_tease_memberpage EQ 1) {
		    rc.strServerZipLink = application.direct_file_locat_url_zip;
		    rc.strServerVideoLink = application.direct_file_locat_url;
		}
		else {
		    rc.strServerZipLink = application.file_locat_url_zip;
		    rc.strServerVideoLink = application.file_locat_url;
		}


		rc.ModelService = variables.ModelService;
		rc.UtilService = variables.UtilService;
		rc.UIHelperService = variables.UIHelperService;
		rc.QueryService = variables.QueryService;
		rc.QueryExtendService = variables.QueryExtendService;

		rc.qrymodel = variables.ModelService.getModelById(URL.model_id);
		rc.qry_modelLinks = rc.QueryService.getModelLinks(rc.qrymodel.model_display_name,rc.qrymodel.model_shortcut);
		
		if(URL.model_id == 0) {
			location url="/index.cfm/model.default";
		}
		else {
			if(rc.qrymodel.RecordCount > 0) {
				rc.qrygalleries = rc.QueryService.getGalleryByModelId(URL.model_id, URL.gType);
				
				rc.qry_modelData = rc.QueryService.getModelForModelSet(URL.model_id);

				var model_check_string = rc.qry_modelData.model_shortcut;

				<!--- set array to store gallery data for this model on other OT sites --->
				rc.arr_otherModelSets = ArrayNew(1);
				loop from="1" to=ArrayLen(application.arr_otherSites) index="iIndex" {
					var site =  application.arr_otherSites[iIndex];
					if (site.bol_display EQ 1) {
						
						rc.qry_gals = rc.QueryService.getGalsForModelSet(model_check_string,site.str_datasource);
						rc.qry_pics = rc.QueryService.getPicsForModelSet(model_check_string,site.str_datasource);
						rc.qry_vids = rc.QueryService.getVideosForModelSet(model_check_string,site.str_datasource);
						
						if (rc.qry_gals.RecordCount NEQ 0 ) {
							stc_modelSet = StructNew();
							stc_modelSet.int_model_id = rc.qry_gals.model_id;
							stc_modelSet.str_standsforsite = site.str_standsforsite;
							stc_modelSet.str_site = site.str_siteName;
							stc_modelSet.str_tour_url = site.str_siteUrlTour;
							switch (session.bol_allSitesMember) {
								case 1:
									stc_modelSet.str_member_url = "#site.str_siteUrlMembers#/members/search.cfm?type=model&model_id=#rc.qry_gals.model_id#";
									break;
								default:
									stc_modelSet.str_member_url = "#site.str_siteUrlTour##model_check_string#";
									break;
							}
							stc_modelSet.str_overlay = site.str_siteOverlay;
							stc_modelSet.str_logo = site.str_siteLogo;
							stc_modelSet.str_colour = site.str_siteColour;
							stc_modelSet.int_sets = rc.qry_pics.RecordCount;
							stc_modelSet.int_vids = Iif(rc.qry_vids.RecordCount EQ 0, 0, rc.qry_vids.int_videoCount);
							stc_modelSet.int_pics = 0;
							loop query="rc.qry_pics" {
								stc_modelSet.int_pics = stc_modelSet.int_pics + rc.qry_pics.gallery_n_images;
							}
							stc_modelSet.arr_tourImages = ArrayNew(2);
							loop query="rc.qry_gals" {
								stc_modelSet.arr_tourImages[rc.qry_gals.CurrentRow][1] = "#site.str_picsPath#/#rc.qry_gals.tour_pic1_thumb#";
								stc_modelSet.arr_tourImages[rc.qry_gals.CurrentRow][2] = "#site.str_picsPath#/#rc.qry_gals.tour_pic2_thumb#";
								stc_modelSet.arr_tourImages[rc.qry_gals.CurrentRow][3] = "#site.str_picsPath#/#rc.qry_gals.tour_pic3_thumb#";
								stc_modelSet.arr_tourImages[rc.qry_gals.CurrentRow][4] = "#site.str_siteUrlMembers#/members/view_gallery.cfm?gallery_id=#rc.qry_gals.gallery_id#";
								<!--- James, this is the overlay image to be used --->
								if (rc.qry_gals.fasttrack) {
									stc_modelSet.arr_tourImages[rc.qry_gals.CurrentRow][5] = "#stc_modelSet.str_overlay#";
								}
								else {
									stc_modelSet.arr_tourImages[rc.qry_gals.CurrentRow][5] = "/members/images/spacer.gif";
								}
								stc_modelSet.arr_tourImages[rc.qry_gals.CurrentRow][6] = rc.qry_gals.gallery_type;
							}
							ArrayAppend(rc.arr_otherModelSets, stc_modelSet);
						}
					}
				}

				<!--- if OnlyAllSites, remove first 6 OT sets --->
				if (application.ApplicationName eq "onlyallsite_application") {
					loop from="1" to="#ArrayLen(rc.arr_otherModelSets)#" index="int_site_index" {
						if (rc.arr_otherModelSets[int_site_index].str_site eq "Only Tease") {
							loop from="1" to="6" index="oas_remove" {
								try {
									ArrayDeleteAt(rc.arr_otherModelSets[int_site_index].arr_tourImages, 1);
								}
								catch(type variables) {
								}
							}
						}
					}
				}

				<!--- calculate whether enough sets exist to display --->
				rc.bol_displaySets = 0;
				loop from="1" to="#ArrayLen(rc.arr_otherModelSets)#" index="int_site_index" {
					if (ArrayLen(rc.arr_otherModelSets[int_site_index].arr_tourImages) GTE 1) {
						rc.bol_displaySets = 1;
					}
				}

				rc.paging_config = variables.UtilService.pagingConfig(rc.qrygalleries.RecordCount,CurrentPage,12,"&model_id=#URL.model_id#&gType=#URL.gType#","#SCRIPT_NAME#/model.model_set");
				rc.display_start_row = rc.paging_config.START_ROW;
			}
		}
	}

	public function view_gallery(struct rc) {
		param name="URL.CurrentPage" default="1";
		param name="URL.image_standard" default=false;
		param name="URL.view_image" default=false;
		param name="rc.flag" default="1";
		param name="rc.customgal" default=false;
		param name="rc.customgal_id" default=0;
		param name="rc.customgal_name" default="";
		param name="rc.serial" default="";
		param name="rc.topvar" default="187";
		param name="rc.display_start_row" default="1";

		param name="rc.cs_use" default=false;
		param name="rc.cs_max_directory" default="1750";
		param name="rc.cs_url" default="http://cdn.content.onlytease.com";

		param name="rc.suspend_view" default=false;
		param name="rc.guest_gallery_size" default="0";

		param name="rc.bol_allsites_member" default="0";
		param name="rc.bol_allsites_memberdirect" default="0";
		param name="rc.bol_tease_memberpage" default="0";


		param name="rc.allow_fasttrack" default="FALSE";
		param name="rc.allow_permfasttrack" default="FALSE";

		rc.UIHelperService = variables.UIHelperService;
		if (rc.bol_allsites_member EQ 1) {
		    rc.strServerZipLink = application.file_locat_url_zip;
		    rc.strServerVideoLink = application.file_locat_url;
		}
		else if (rc.bol_allsites_memberdirect EQ 1) {
		    rc.strServerZipLink = application.direct_file_locat_url_zip;
		    rc.strServerVideoLink = application.direct_file_locat_url;
		}
		else if (rc.bol_tease_memberpage EQ 1) {
		    rc.strServerZipLink = application.direct_file_locat_url_zip;
		    rc.strServerVideoLink = application.direct_file_locat_url;
		}
		else {
		    rc.strServerZipLink = application.file_locat_url_zip;
		    rc.strServerVideoLink = application.file_locat_url;
		}

		if (isDefined("url.report")) {
			rc.report = "1";
		}
		else {
			rc.report = "0";
		}

		rc.blocked_reporter = variables.ViewGalleryService.getBlocked_reporter();

		if (isDefined("url.newCustGal") AND  isDefined("form.newcustomgal_name") AND form.newcustomgal_name NEQ "") {
			form.newcustomgal_name = udf_xssSafeString(form.newcustomgal_name);
			var qry_insert = variables.ViewGalleryService.qry_insert(form.newcustomgal_name);

			rc.qry_customgal = variables.ViewGalleryService.getCustomGalBy_CustomName(form.newcustomgal_name);
			rc.customgal = true;
			rc.customgal_name = rc.qry_customgal.customgal_name;
			rc.customgal_id = rc.qry_customgal.customgal_id;
			location url="/index.cfm/model.view_gallery?gallery_id=#URL.gallery_id#&CurrentPage=#URL.CurrentPage#&customgal=#rc.customgal_id#" addtoken="false";
		}
		else if (isDefined("url.customgal") AND url.customgal GT 0) {
			rc.qry_customgal = variables.ViewGalleryService.getCustomGalBy_CustomId(url.customgal);
			if (qry_customgal.RecordCount GT 0) {
				rc.customgal_name = rc.qry_customgal.customgal_name;
				rc.customgal_id = rc.qry_customgal.customgal_id;
				rc.customgal = true;
			}
			else {
				rc.customgal = false;
				url.customgal = "";
			}
		}

		rc.qry_customgal = variables.ViewGalleryService.getCustomGalBy_PrefId(session.pref_id);
		
		if( isDefined("URL.gallery_id")) {
			rc.qry_galleryimages = variables.ViewGalleryService.getGalleryImages(rc.customgal, rc.customgal_id, URL.gallery_id);
			if (rc.qry_galleryimages.gallery_type EQ 7 && rc.qry_galleryimages.release_date EQ "") {
				rc.suspend_view = TRUE;


				rc.qry_pfts = variables.ViewGalleryService.qry_pfts(session.pref_id, URL.gallery_id);
				if (rc.qry_pfts.recordcount GT 0) {
					rc.suspend_view = FALSE;
				}
			}

			if (rc.qry_galleryimages.recordcount > 0 && NOT rc.suspend_view) {
				rc.qry_galleryDetails = variables.ViewGalleryService.getGalleryDetail(URL.gallery_id, rc.serial, session.pref_id);
				rc.qrymodel = variables.ModelService.getModelById(rc.qry_galleryDetails.model_id);
				rc.qry_modelLinks = variables.QueryService.getModelLinks(rc.qrymodel.model_display_name,rc.qrymodel.model_shortcut);
				if (rc.qry_galleryDetails.set_display EQ 1) {
					rc.display_set = TRUE;
				}
				else {
					rc.display_set = FALSE;
				}

				rc.qry_tracking = variables.ViewGalleryService.qry_tracking(rc.qry_galleryDetails.directory_name);

				if (rc.qry_tracking.recordcount GT 0) {
					rc.tracked_set = TRUE;
				}
				else {
					rc.tracked_set = FALSE;
				}

				rc.qryComments = variables.ViewGalleryService.qryComments(URL.gallery_id);

				if (REFind("p",rc.qry_galleryDetails.directory_name) GT 0) {
					rc.sets_locat = "sets";
				}
				else if (REReplace(rc.qry_galleryDetails.directory_name,"[^\d]*","","all") GT 5000) {
					rc.sets_locat = "sets2";
				}
				else {
					rc.sets_locat = "sets";
				}

				rc.thumb_image_replace = rc.qry_galleryDetails.image_thumb;
				rc.large_image_replace = rc.qry_galleryDetails.image_standard;
				rc.ultra_image_replace = rc.qry_galleryDetails.image_standard;
				rc.super_image_replace = rc.qry_galleryDetails.image_standard;
				rc.ultra_image_dir = "images";
				rc.large_image_dir = "images";
				rc.super_image_dir = "images";
				if (rc.qry_galleryDetails.super_zipfile NEQ "") {
					rc.super_image_dir = "images4";
					rc.ultra_image_dir = "images3";
					rc.large_image_dir = "images2";
					rc.large_image_replace = rc.qry_galleryDetails.image_large;
					rc.ultra_image_replace = rc.qry_galleryDetails.image_ultra;
					rc.super_image_replace = rc.qry_galleryDetails.image_super;
				}
				else if (rc.qry_galleryDetails.ultra_zipfile NEQ "") {
					rc.super_image_dir = "images3";
					rc.ultra_image_dir = "images3";
					rc.large_image_dir = "images2";
					rc.large_image_replace = rc.qry_galleryDetails.image_large;
					rc.ultra_image_replace = rc.qry_galleryDetails.image_ultra;
					rc.super_image_replace = rc.qry_galleryDetails.image_ultra;
				}
				else if (rc.qry_galleryDetails.lrg_zipfile NEQ "") {
					rc.super_image_dir = "images2";
					rc.ultra_image_dir = "images2";
					rc.large_image_dir = "images2";
					rc.large_image_replace = rc.qry_galleryDetails.image_large;
					rc.ultra_image_replace = rc.qry_galleryDetails.image_large;
					rc.super_image_replace = rc.qry_galleryDetails.image_large;
				}

				switch(session.preferences.pref_imagesize){
					case 3:
						rc.image_dir = rc.super_image_dir;
						rc.image_replace = rc.super_image_replace;
						break;
					case 2:
						rc.image_dir = rc.ultra_image_dir;
						rc.image_replace = rc.ultra_image_replace;
						break;
					case 1:
						rc.image_dir = rc.large_image_dir;
						rc.image_replace = rc.large_image_replace;
						break;
				
					default:
						rc.image_dir = "images";
						rc.image_replace = rc.qry_galleryDetails.image_standard;
						break;
				}

				if (rc.qry_galleryDetails.gallery_type EQ 21) {
					if (session.preferences.pref_imagesize NEQ 0) {
						rc.guest_gallery_size = 0;
					}
				}
				else {
					rc.guest_gallery_size = session.preferences.pref_imagesize;
				}

				switch (rc.guest_gallery_size) {
					case 3:
						rc.image_dir = super_image_dir;
						rc.image_replace = super_image_replace;
						break;
					case 2:
						rc.image_dir = ultra_image_dir;
						rc.image_replace = ultra_image_replace;
						break;
					case 1:
						rc.image_dir = large_image_dir;
						rc.image_replace = large_image_replace;
						break;
					default:
						rc.image_dir = "images";
						rc.image_replace = rc.qry_galleryDetails.image_standard;
						break;
				}

				rc.ipadVideo = "#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_ipod_res_name#.m4v";

				<!--- create the string of model names --->
				rc.model_names = variables.UtilService.udf_modelNames(URL.gallery_id);

				rc.qry_keywords = variables.ViewGalleryService.qry_keywords(URL.gallery_id);
				rc.qry_CheckPreferences = variables.ViewGalleryService.qry_CheckPreferences(URL.gallery_id, "Gallery", session.pref_id);
				if (rc.qry_CheckPreferences.my_favourites_keyid NEQ "") {
					rc.favourite_gallery = TRUE;
				}
				else {
					rc.favourite_gallery = FALSE;
				}
				rc.voted_old = false;
				rc.voted_new = false;
				rc.not_voted = false;
				rc.model_vote = "";
				rc.outfit_vote = "";
				rc.tease_vote = "";
				if (rc.qry_CheckPreferences.mv_pref_id eq "") {
					if (listFind(rc.qry_CheckPreferences.voted_galleries,URL.gallery_id,",")) {
						rc.voted_old = true;
					}
					else {
						rc.not_voted = true;
					}
				}
				else {
					rc.voted_new = true;
					<!--- Votes before this date were inserted the wrong way round in vote.cfm, so this is a hack to get around that--->
					if (dateCompare(rc.qry_CheckPreferences.mv_datevoted, createDate(2008, 07, 30)) lt 0) {
						rc.outfit_vote = rc.qry_CheckPreferences.mv_model;
						rc.model_vote = rc.qry_CheckPreferences.mv_outfit;
						rc.tease_vote = rc.qry_CheckPreferences.mv_tease;
					}
					else {
						rc.model_vote = rc.qry_CheckPreferences.mv_model;
						rc.outfit_vote = rc.qry_CheckPreferences.mv_outfit;
						rc.tease_vote = rc.qry_CheckPreferences.mv_tease;
					}
				}

				
				rc.qry_Reminder_check = variables.ViewGalleryService.qry_Reminder_check(URL.gallery_id);

				var models_used = rc.qry_galleryDetails.model_id;
				var searchby = " AND  models.model_id NOT IN (#models_used#)  AND  (models.guest_link_url IS NULL OR models.guest_link_url = '')";
				var orderby = "ORDER BY RAND() LIMIT 1";
				rc.qry_modelRandom = variables.ModelService.getModelsBySearch("", models_used, searchby, orderby, "");

				rc.qry_pref_view = variables.ViewGalleryService.qry_pref_view();
				
				if (rc.qry_galleryDetails.gallery_type EQ 21) {
					if (rc.qry_pref_view.pref_view EQ 1) {
						session.preferences.pref_view = 0;
					}
				}
				else {
					session.preferences.pref_view = rc.qry_pref_view.pref_view;
				}
				if (session.preferences.pref_view) {
					if ((rc.cs_use AND rc.qry_galleryDetails.directory_name LTE cs_max_directory AND rc.qry_galleryDetails.directory_name DOES NOT CONTAIN "a")) {
						rc.view_url = "#rc.cs_url#/members/#rc.sets_locat#/#rc.qry_galleryDetails.directory_name#/#rc.image_dir#/";
					}
					else {
						rc.view_url = "http://members.onlytease.com/members/#rc.sets_locat#/#rc.qry_galleryDetails.directory_name#/#rc.image_dir#/";
					}
				}
				else {
					if (rc.serial NEQ "") {
						rc.view_url = "/index.cfm/model.view_gallery?gallery_id=#gallery_id#&serial=#rc.serial#&image_id=";
					}
					else {
						rc.view_url = "/index.cfm/model.view_gallery?gallery_id=#URL.gallery_id#&image_id=";
					}
				}

				switch (rc.qry_galleryDetails.set_type) {
					case -1:
						if (rc.qry_galleryDetails.gallery_type NEQ "7") {
								rc.qry_pfts = variables.ViewGalleryService.qry_pfts();
								if (rc.qry_pfts.pft_remaining GT 0) {
									rc.allow_permfasttrack="TRUE";
								}
								rc.allow_fasttrack="TRUE";
							}
							break;
					default:
					break;
				}

				<!--- create page through --->
				if (rc.serial NEQ "") {
					rc.new_added_path = "&gallery_id=#URL.gallery_id#&serial=#rc.serial#";
				}
				else {
					rc.new_added_path = "&gallery_id=#URL.gallery_id#";
				}
				if (rc.customgal) {
					rc.new_added_path = rc.new_added_path & "&customgal=" & rc.customgal_id;
				}


				rc.paging_config = variables.UtilService.pagingConfig(rc.qry_galleryimages.RecordCount,CurrentPage,24,rc.new_added_path,"#SCRIPT_NAME#/model.view_gallery");
				rc.display_start_row = rc.paging_config.START_ROW;

				if(URL.view_image eq true) {
					if (IsDefined("url.customgal_id")) {
						var gID = url.customgal_id;
						var gVar = 'customgal_id';
					}
					else {
						var gID = url.gallery_id;
						var gVar = 'gallery_id';
					}
					rc.qry_galleryimages = variables.ViewImagesService.qry_galleryimagesbygalId(URL.gallery_id);
					rc.image_list = ValueList(rc.qry_galleryimages.image_id);

					if(NOT IsDefined("url.image_id")) {
						URL.image_id = ListGetAt(rc.image_list,URL.CurrentPage);
					}

					if (rc.qry_galleryimages.recordcount GT 0 AND ListContains(rc.image_list, URL.image_id) GT 0) {

						rc.qry_imageDetails = variables.ViewImagesService.qry_imageDetails(URL.image_id);

						<!---set the sets_locat variable to find sets in sets2 directory if the set directory_number is greater than 5000--->
						rc.prestige = REFind("p", rc.qry_imageDetails.directory_name);
						if (rc.prestige EQ true) {
							rc.sets_locat = "sets";
						}
						else if (rc.qry_imageDetails.directory_name GT 5000) {
							rc.sets_locat = "sets2";
						}
						else {
							rc.sets_locat = "sets";
						}
						<!---end of setting sets_locat --->

						<!--- workout the largest available image size and set the directory to use --->
						rc.standard_image_dir = "images";
						rc.thumb_image_replace = rc.qry_imageDetails.image_thumb;
						rc.large_image_replace = rc.qry_imageDetails.image_standard;
						rc.ultra_image_replace = rc.qry_imageDetails.image_standard;
						rc.super_image_replace = rc.qry_imageDetails.image_standard;
						rc.standard_image_replace = rc.qry_imageDetails.image_standard;
						rc.ultra_image_dir = "images";
						rc.large_image_dir = "images";
						rc.super_image_dir = "images";
						if (rc.qry_imageDetails.super_zipfile NEQ "") {
							rc.super_image_dir = "images4";
							rc.ultra_image_dir = "images3";
							rc.large_image_dir = "images2";
							rc.large_image_replace = rc.qry_imageDetails.image_large;
							rc.ultra_image_replace = rc.qry_imageDetails.image_ultra;
							rc.super_image_replace = rc.qry_imageDetails.image_super;
						}
						else if (rc.qry_imageDetails.ultra_zipfile NEQ "") {
							rc.super_image_dir = "images3";
							rc.ultra_image_dir = "images3";
							rc.large_image_dir = "images2";
							rc.large_image_replace = rc.qry_imageDetails.image_large;
							rc.ultra_image_replace = rc.qry_imageDetails.image_ultra;
							rc.super_image_replace = rc.qry_imageDetails.image_ultra;
						}
						else if (rc.qry_imageDetails.lrg_zipfile NEQ "") {
							rc.super_image_dir = "images2";
							rc.ultra_image_dir = "images2";
							rc.large_image_dir = "images2";
							rc.large_image_replace = rc.qry_imageDetails.image_large;
							rc.ultra_image_replace = rc.qry_imageDetails.image_large;
							rc.super_image_replace = rc.qry_imageDetails.image_large;
						}

						if (rc.qry_imageDetails.gallery_type EQ 21) {
							if (session.preferences.pref_imagesize NEQ 0) {
								rc.guest_gallery_size = 0;
							}
						}
						else {
							rc.guest_gallery_size = session.preferences.pref_imagesize;
						}

						switch (rc.guest_gallery_size) {
							case 3:
								rc.image_dir = rc.super_image_dir;
								rc.image_replace = rc.super_image_replace;
								break;
							case 2:
								rc.image_dir = rc.ultra_image_dir;
								rc.image_replace = rc.ultra_image_replace;
								break;
							case 1:
								rc.image_dir = rc.large_image_dir;
								rc.image_replace = rc.large_image_replace;
								break;
							default:
								rc.image_dir = rc.standard_image_dir;
								rc.image_replace = rc.standard_image_replace;
								break;
						}

						if(rc.qry_imageDetails.gallery_type NEQ 21 AND rc.qry_imageDetails.lrg_zipfile NEQ "") {
							rc.sets_locat = "sets2";
							rc.image_dir = rc.large_image_dir;
							rc.image_replace = rc.large_image_replace; 
						}

						<!--- setup the first, previous, next and last links --->
						rc.current_position = ListContains(rc.image_list, URL.image_id);
						rc.CurrentPage = ((rc.current_position - 1)\session.preferences.pref_thumbs) + 1;

						if (rc.serial NEQ "") {
							rc.standard_url = "/index.cfm/model.view_images?#gVar#=#gID#&serial=#rc.serial#&image_id=";
							rc.normal_url = "/index.cfm/model.view_images?#gVar#=#gID#&serial=#rc.serial#&image_dir=images&image_id=";
							rc.large_url = "/index.cfm/model.view_images?#gVar#=#gID#&serial=#rc.serial#&image_dir=images2&image_id=";
							rc.ultra_url = "/index.cfm/model.view_images?#gVar#=#gID#&serial=#rc.serial#&image_dir=images3&image_id=";
							rc.super_url = "/index.cfm/model.view_images?#gVar#=#gID#&serial=#rc.serial#&image_dir=images4&image_id=";
							if (gVar EQ 'customgal_id') {
								rc.gallery_url  = "/index.cfm/model.view_customgal?gallery_id=#gID#&serial=#rc.serial#&CurrentPage=#rc.CurrentPage#";
							}
							else {
								rc.gallery_url  = "/index.cfm/model.view_gallery?gallery_id=#gID#&serial=#rc.serial#&CurrentPage=#rc.CurrentPage#";
							}
						}
						else {
							rc.standard_url = "/index.cfm/model.view_images?#gVar#=#gID#&image_id=";
							rc.normal_url = "/index.cfm/model.view_images?#gVar#=#gID#&image_dir=images&image_id=";
							rc.large_url = "/index.cfm/model.view_images?#gVar#=#gID#&image_dir=images2&image_id=";
							rc.ultra_url = "/index.cfm/model.view_images?#gVar#=#gID#&image_dir=images3&image_id=";
							rc.super_url = "/index.cfm/model.view_images?#gVar#=#gID#&image_dir=images4&image_id=";
							if (gVar EQ 'customgal_id') {
								rc.gallery_url  = "/index.cfm/model.view_customgal?gallery_id=#gID#&CurrentPage=#rc.CurrentPage#";
							}
							else {
								rc.gallery_url  = "/index.cfm/model.view_gallery?gallery_id=#gID#&CurrentPage=#rc.CurrentPage#";
							}
						}
						rc.current_image = "#ListGetAt(rc.image_list,(rc.current_position))#";
						rc.new_added_path = "&gallery_id=#URL.gallery_id#&view_image=true";
						rc.paging_config = variables.UtilService.pagingConfig(rc.qry_galleryimages.RecordCount,rc.current_position,1,rc.new_added_path,"#SCRIPT_NAME#/model.view_gallery");
						rc.display_start_row = rc.paging_config.START_ROW;
					}
				}
			}
		}

	}

	public function view_images (struct rc) {
		param name="rc.cs_use" default="false";
		param name="rc.cs_max_directory" default="1750";
		param name="rc.cs_url" default="http://cdn.content.onlytease.com";
		param name="rc.serial" default="";
		param name="guest_gallery_size" default="0";

		if (IsDefined("url.customgal_id")) {
			var gID = url.customgal_id;
			var gVar = 'customgal_id';

			rc.qry_galleryimages = variables.ViewImagesService.qry_galleryimages(url.customgal_id);
		}
		else {
			var gID = url.gallery_id;
			var gVar = 'gallery_id';

			rc.qry_setDetails = variables.ViewImagesService.qry_setDetails(URL.gallery_id, rc.serial);
			
			if (rc.qry_setDetails.set_release) {
				rc.qry_galleryimages = variables.ViewImagesService.qry_galleryimagesbygalId(URL.gallery_id);
			}
			else {
				rc.qry_galleryimages = QueryNew("image_id");
			}
		}

		rc.image_list = ValueList(rc.qry_galleryimages.image_id);


		if (rc.qry_galleryimages.recordcount GT 0 AND ListContains(rc.image_list, URL.image_id) GT 0) {

			rc.qry_imageDetails = variables.ViewImagesService.qry_imageDetails(URL.image_id);

			<!---set the sets_locat variable to find sets in sets2 directory if the set directory_number is greater than 5000--->
			rc.prestige = REFind("p",#rc.qry_imageDetails.directory_name# );
			if (rc.prestige EQ true) {
				rc.sets_locat = "sets";
			}
			else if (rc.qry_imageDetails.directory_name GT 5000) {
				rc.sets_locat = "sets2";
			}
			else {
				rc.sets_locat = "sets";
			}
			<!---end of setting sets_locat --->

			<!--- workout the largest available image size and set the directory to use --->
			rc.standard_image_dir = "images";
			rc.thumb_image_replace = rc.qry_imageDetails.image_thumb;
			rc.large_image_replace = rc.qry_imageDetails.image_standard;
			rc.ultra_image_replace = rc.qry_imageDetails.image_standard;
			rc.super_image_replace = rc.qry_imageDetails.image_standard;
			rc.standard_image_replace = rc.qry_imageDetails.image_standard;
			rc.ultra_image_dir = "images";
			rc.large_image_dir = "images";
			rc.super_image_dir = "images";
			if (rc.qry_imageDetails.super_zipfile NEQ "") {
				rc.super_image_dir = "images4";
				rc.ultra_image_dir = "images3";
				rc.large_image_dir = "images2";
				rc.large_image_replace = rc.qry_imageDetails.image_large;
				rc.ultra_image_replace = rc.qry_imageDetails.image_ultra;
				rc.super_image_replace = rc.qry_imageDetails.image_super;
			}
			else if (rc.qry_imageDetails.ultra_zipfile NEQ "") {
				rc.super_image_dir = "images3";
				rc.ultra_image_dir = "images3";
				rc.large_image_dir = "images2";
				rc.large_image_replace = rc.qry_imageDetails.image_large;
				rc.ultra_image_replace = rc.qry_imageDetails.image_ultra;
				rc.super_image_replace = rc.qry_imageDetails.image_ultra;
			}
			else if (rc.qry_imageDetails.lrg_zipfile NEQ "") {
				rc.super_image_dir = "images2";
				rc.ultra_image_dir = "images2";
				rc.large_image_dir = "images2";
				rc.large_image_replace = rc.qry_imageDetails.image_large;
				rc.ultra_image_replace = rc.qry_imageDetails.image_large;
				rc.super_image_replace = rc.qry_imageDetails.image_large;
			}

			if (rc.qry_imageDetails.gallery_type EQ 21) {
				if (session.preferences.pref_imagesize NEQ 0) {
					rc.guest_gallery_size = 0;
				}
			}
			else {
				rc.guest_gallery_size = session.preferences.pref_imagesize;
			}

			switch (rc.guest_gallery_size) {
				case 3:
					rc.image_dir = rc.super_image_dir;
					rc.image_replace = rc.super_image_replace;
					break;
				case 2:
					rc.image_dir = rc.ultra_image_dir;
					rc.image_replace = rc.ultra_image_replace;
					break;
				case 1:
					rc.image_dir = rc.large_image_dir;
					rc.image_replace = rc.large_image_replace;
					break;
				default:
					rc.image_dir = rc.standard_image_dir;
					rc.image_replace = rc.standard_image_replace;
					break;
			}

			<!--- setup the first, previous, next and last links --->
			rc.current_position = ListContains(rc.image_list, URL.image_id);
			rc.CurrentPage = ((rc.current_position - 1)\session.preferences.pref_thumbs) + 1;

			if (rc.serial NEQ "") {
				rc.standard_url = "/index.cfm/model.view_images?#gVar#=#gID#&serial=#rc.serial#&image_id=";
				rc.normal_url = "/index.cfm/model.view_images?#gVar#=#gID#&serial=#rc.serial#&image_dir=images&image_id=";
				rc.large_url = "/index.cfm/model.view_images?#gVar#=#gID#&serial=#rc.serial#&image_dir=images2&image_id=";
				rc.ultra_url = "/index.cfm/model.view_images?#gVar#=#gID#&serial=#rc.serial#&image_dir=images3&image_id=";
				rc.super_url = "/index.cfm/model.view_images?#gVar#=#gID#&serial=#rc.serial#&image_dir=images4&image_id=";
				if (gVar EQ 'customgal_id') {
					rc.gallery_url  = "/index.cfm/model.view_customgal?gallery_id=#gID#&serial=#rc.serial#&CurrentPage=#rc.CurrentPage#";
				}
				else {
					rc.gallery_url  = "/index.cfm/model.view_gallery?gallery_id=#gID#&serial=#rc.serial#&CurrentPage=#rc.CurrentPage#";
				}
			}
			else {
				rc.standard_url = "/index.cfm/model.view_images?#gVar#=#gID#&image_id=";
				rc.normal_url = "/index.cfm/model.view_images?#gVar#=#gID#&image_dir=images&image_id=";
				rc.large_url = "/index.cfm/model.view_images?#gVar#=#gID#&image_dir=images2&image_id=";
				rc.ultra_url = "/index.cfm/model.view_images?#gVar#=#gID#&image_dir=images3&image_id=";
				rc.super_url = "/index.cfm/model.view_images?#gVar#=#gID#&image_dir=images4&image_id=";
				if (gVar EQ 'customgal_id') {
					rc.gallery_url  = "/index.cfm/model.view_customgal?gallery_id=#gID#&CurrentPage=#rc.CurrentPage#";
				}
				else {
					rc.gallery_url  = "/index.cfm/model.view_gallery?gallery_id=#gID#&CurrentPage=#rc.CurrentPage#";
				}
			}

			if (rc.current_position GT 1) {
				rc.first_link = "#rc.standard_url##ListGetAt(rc.image_list,1)#&image_dir=#URL.image_dir#";
			}
			else {
				rc.first_link = "";
			}

			if (rc.current_position GT 1) {
				rc.previous_link = "#rc.standard_url##ListGetAt(rc.image_list,(rc.current_position-1))#&image_dir=#URL.image_dir#";
			}
			else {
				rc.previous_link = "";
			}

			if (rc.current_position LT ListLen(rc.image_list)) {
				rc.next_link = "#rc.standard_url##ListGetAt(rc.image_list,(rc.current_position+1))#&image_dir=#URL.image_dir#";
			}
			else {
				rc.next_link = "";
			}

			if (rc.current_position LT ListLen(rc.image_list)) {
				rc.last_link = "#rc.standard_url##ListGetAt(rc.image_list,ListLen(rc.image_list))#&image_dir=#URL.image_dir#";
			}
			else {
				rc.last_link = "";
			}

			rc.current_image = "#ListGetAt(rc.image_list,(rc.current_position))#";
		}
	}

	public function vote(struct rc) {
		<!--- if vote form is empty, redirect home --->
		if (StructIsEmpty(form)) {
			location addtoken="no" url="/index.cfm/members.default";
		}

		<!--- process form --->
		if (not (rc.total_votes_outfit eq 0 or rc.total_votes_model eq 0 or rc.total_votes_tease eq 0)) {
			var qry_votes = variables.VoteService.qry_votes(rc.gallery_id, session.pref_id);
			if(qry_votes.recordcount EQ 0) {
				var qry_checkpreferences = variables.VoteService.qry_CheckPreferences(session.pref_id);

				if (listFind(qry_CheckPreferences.voted_galleries, gallery_id,",")) {
					var old_vote = true;
				}
				else {
					var old_vote = false;
				}
				if (not old_vote) {
					variables.VoteService.insertVote(rc.gallery_id, session.pref_id, rc.total_votes_model, rc.total_votes_outfit, rc.total_votes_tease);
					variables.VoteService.updateAverages(rc.gallery_id,0,0,0,1);
				}
			}
			else {
				var qry_current_votes = variables.VoteService.qry_curren_votes(rc.gallery_id,session.pref_id);

				variables.VoteService.updateVote(rc.gallery_id, session.pref_id, rc.total_votes_model, rc.total_votes_outfit, rc.total_votes_tease);

				variables.VoteService.updateAverages(rc.gallery_id,qry_current_votes.mv_model,qry_current_votes.mv_outfit,qry_current_votes.mv_tease,0);
			}
		}
		else {
			<!--- haven't selected a value, kick back to gallery page which will display vote results --->
			location url="/index.cfm/members/view_gallery?gallery_id=#rc.gallery_id#&CurrentPage=#rc.CurrentPage#&voteError=incomplete##voteform" addtoken="No";
		}

		rc.qry_user = variables.VoteService.qry_user();
		rc.qry_content = variables.VoteService.qry_content(rc.gallery_id);
	}

	public function model_profile(struct rc) {
		param name="type" default="model";
		param name="rc.allow_fasttrack" default="FALSE";
		param name="rc.allow_permfasttrack" default="FALSE";
		param name="search" 		default="FALSE";
		param name="search_text" default="";
		param name="letter" 		default="";
		param name="url.CurrentPage" default="1";
		param name="url.url.model_id" 	default="0";
		param name="url.gType" default="a";
		param name="URL.gallery_id" default="0";

		rc.modelProfileService = variables.ModelProfileService;
		var qry_pfts = variables.ModelProfileService.qry_pfts();
		if (qry_pfts.pft_remaining GT 0) {
			rc.allow_permfasttrack="TRUE";
		}

		var qry_fts = variables.ModelProfileService.qry_fts();
		if (qry_fts.ft_remaining GT 0) {
			rc.allow_fasttrack="TRUE";
		}

		var qry_letters = variables.ModelProfileService.qry_letters();
		var letter_list = ValueList(qry_letters.model_initial);

		if (Not IsNumeric(trim(url.model_id))) {
			url.model_id = 0;
		}

		if(url.model_id > 0) {
			rc.qry_model = variables.ModelProfileService.qry_model(url.model_id);

			if(rc.qry_model.model_flash_profile != "") {
				rc.qry_videoDetails = variables.ModelProfileService.qry_videoDetails(rc.qry_model.model_flash_profile);
			}
			else {
				rc.any_released = variables.ModelProfileService.any_released(url.model_id);
				rc.released_gal = variables.ModelProfileService.released_gal(url.model_id);
			}

			rc.get_answers = variables.ModelProfileService.get_answers(url.model_id);
			rc.facebook    = variables.ModelProfileService.facebook(url.model_id);
			rc.twitter     = variables.ModelProfileService.twitter(url.model_id);
			rc.top_gallery = variables.ModelProfileService.top_gallery(url.model_id);

			if(rc.qry_model.recordcount > 0) {
				rc.qryComments = variables.ModelProfileService.qryComments(url.model_id);
				rc.qryLinks	   = variables.ModelProfileService.qryLinks(url.model_id);

				var qry_favourite = variables.ModelProfileService.qry_favourite(url.model_id,session.pref_id,"Model");
				if (qry_favourite.recordcount GT 0) {
					rc.favourite_gallery = TRUE;
				}
				else {
					rc.favourite_gallery = FALSE;
				}

				rc.answer_counter = 0;
				loop query="rc.get_answers" {
					if (rc.get_answers.answer_text NEQ "") {
						rc.answer_counter = 1;
					}
				}
			}
		}
	}
}	