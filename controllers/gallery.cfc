component accessors="true" output="false" displayname=""  {

	property GalleryService;
	property QueryService;
	property UtilService;
	property UIHelperService;
	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public function default (struct rc) {
		param name="URL.view" default="all";
		param name="rc.pMonth" default="#month(now())#";
		param name="rc.pYear" default="#year(now())#";

		param name="rc.bol_allsites_member" default="0";
		param name="rc.bol_allsites_memberdirect" default="0";
		param name="rc.bol_tease_memberpage" default="0";


		if (rc.bol_allsites_member EQ 1) {
		    rc.strServerZipLink = application.file_locat_url_zip;
		    rc.strServerVideoLink = application.file_locat_url;
		}
		else if (rc.bol_allsites_memberdirect EQ 1) {
		    rc.strServerZipLink = application.direct_file_locat_url_zip;
		    rc.strServerVideoLink = application.direct_file_locat_url;
		}
		else if (rc.bol_tease_memberpage EQ 1) {
		    rc.strServerZipLink = application.direct_file_locat_url_zip;
		    rc.strServerVideoLink = application.direct_file_locat_url;
		}
		else {
		    rc.strServerZipLink = application.file_locat_url_zip;
		    rc.strServerVideoLink = application.file_locat_url;
		}

		rc.UtilService = variables.UtilService;
		rc.UIHelperService = variables.UIHelperService;
		rc.qry_available_months = variables.GalleryService.getAvailableMonths(URL.view);
		rc.arrAvailableMonths = variables.GalleryService.getAvailableMonthsByYear(URL.view,rc.pYear);
		rc.qry_archive = variables.QueryService.getGalleries(URL.view,rc.pMonth,rc.pYear);
	}

	public any function getAvailableMonthsByYear(string View, numeric year) {
		variables.fw.renderData("Json",variables.GalleryService.getAvailableMonthsByYear(View,year));
	}
}