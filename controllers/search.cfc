component accessors="true" output="false" displayname=""  {

	property SearchService;
	property UtilService;
	property UtilService;
	property UIHelperService;
	public function init(required any fw){
		variables.fw = fw;
		return this;
	}

	public function default (struct rc) {
		param name="keyword_id" default="0";
		param name="CurrentPage" default="1";

		rc.kw_filter_1 = variables.SearchService.qry_keywords();

		rc.kw_array = "";
		rc.kw_ref = "";
		loop query="rc.kw_filter_1" {
			rc.kw_ref = rc.kw_ref & "keyword_ref[#gallery_keyword_id#]='#gallery_keyword_description#';";

			if (rc.kw_array GT "") {
				rc.kw_array = rc.kw_array & ",";
			}
			rc.kw_array = rc.kw_array & "#gallery_keyword_id#";
		}
		rc.kw_array = "new Array(#rc.kw_array#)";
	}

	public any function search_update (struct rc) {
		<!--- If url.gType is set to "load" then load up default and then load up the session if there is one --->
		if (isDefined("url.gType") AND url.gType EQ "load") {
			url.gType = "a";
			if (isDefined("session.storedSearch")) {
				for(var curIndex in session.storedSearch) {
					url[curIndex] = session.storedSearch[curIndex];
				}
		   	}
		}

		<!--- Inputs --->
		param name="url.order" default="1";
		param name="url.CurrentPage" default="1";
		param name="url.gType" default="a";
		param name="editID" default="128";
		param name="allow_fasttrack" default="FALSE";
		param name="allow_permfasttrack" default="FALSE";
		param name="keyword_id" default="0";		

		var prefix_filter = "FILTER_";
		var filter_xml = "";
		var gallery_xml = "";

		<!--- Set stored session variables --->
		session.storedSearch = {};
		loop collection="#url#" item="curIndex" {
			session.storedSearch[curIndex] = url[curIndex];
		}


		var qry_pfts = variables.SearchService.qry_pfts();
		if(qry_pfts.pft_remaining GT 0) {
			allow_permfasttrack="TRUE";
		}

		var qry_fts = variables.SearchService.qry_fts();
		if (qry_fts.ft_remaining GT 0) {
			allow_fasttrack="TRUE";
		}

		<!--- STARTING SEARCH --->

		<!--- Find filters submitted --->
		var arr_filters = [];
		loop collection="#url#" item="curIndex" {
			<!--- check key for "filter" --->
		    if (FindNoCase(prefix_filter,curIndex,1)) {
		    	var curFilterNumber = Right(curIndex,(Len(curIndex)-Len(prefix_filter)));
		        <!--- If it's an integer add to array --->
		        if (IsNumeric(curFilterNumber)) {
		        	arr_filters[curFilterNumber] = url[curIndex];
		        }
		    }
		}

		var list_filters = "";
		var list_filters_raw = ArrayToList(arr_filters,",");

		loop list="#list_filters_raw#" index="actual_filter" {
			list_filters = ListAppend(list_filters, actual_filter,",");
		}

		<!--- Loop over the filters, populating xml --->
		var listIndex = 0;
		var list_tmp = "";
		savecontent variable="filter_xml" {
			loop list="#list_filters#" index="curFilter" {
				if (Len(list_tmp) EQ 0 AND listIndex GT 0) {
					break;
				}
		        listIndex = listIndex + 1;

		        var sec = (listIndex EQ 1 OR ListFind(list_tmp,curFilter,",")) ? curFilter : '';
		        writeOutput('<filter number="#listIndex#" selected="#sec#">');

				if (listIndex GT 1) {
	                var stc_tmp = Duplicate(Evaluate("stc_checkFilter_#listIndex-1#"));
	                list_tmp = ListSort(list_tmp, "numeric", "asc", ",");
	    			var previous = "";
	                loop list="#list_tmp#" index="curKW" delimiters="," {
	    				if (curKW NEQ previous) {
	                        writeOutput("<opt>#curKW#</opt>");
							previous = curKW;
	                    }
	                }

	                list_tmp = "";

	                loop collection="#stc_tmp#" item="filteringIndex" {
	                    if (NOT ListFind(stc_tmp[filteringIndex],ListGetAt(list_filters,listIndex,","),",")) {
	                        <!--- Gallery doest have this filter so remove --->
	                        tmp = StructDelete(stc_tmp,filteringIndex);
	                    }
	                    else {
	                        <!--- Remove this keyword from this structure item --->
	                        stc_tmp[filteringIndex] = ListDeleteAt(stc_tmp[filteringIndex],ListFind(stc_tmp[filteringIndex],ListGetAt(list_filters,listIndex,","),","),",");
	                        <!--- Add this item to the list for next filter --->
	                        list_tmp = ListAppend(list_tmp,stc_tmp[filteringIndex]);
	                    }
	                }
	                tmp = SetVariable("stc_checkFilter_#listIndex#",Duplicate(stc_tmp));
	            }
			    else {
			    	var qry_checkFilter_1 = variables.SearchService.qry_checkFilter_1(ListGetAt(list_filters,1,','));
	                <!--- Dump it into an array I guess --->

					var stc_checkFilter_1 = {};
    				list_tmp = "";
                  	loop query="qry_checkFilter_1" group="gallery_id" {
                       	kwl_list="";
                       	loop{
                           if (kwl_list GT "") {
	                           kwl_list = kwl_list & ",";
	                       	}
                  	 		kwl_list = kwl_list & gallery_keyword_id;
                  	 	}
                   
                       	stc_checkFilter_1[gallery_id] = kwl_list;
                       	<!--- Add this item to the list for next filter --->
                      	list_tmp = ListAppend(list_tmp,stc_checkFilter_1[gallery_id]);
                   	}
			    }

			    writeOutput("</filter>");
			}
		    <!--- Add last fresh filter if there are options --->
			if (Len(list_tmp) GT 0 OR listIndex EQ 0) {
		        list_tmp = ListSort(list_tmp, "numeric", "asc", ",");

		        writeOutput('<filter number="#listIndex+1#" selected="">');
		        var previous = "";
	           	loop list="#list_tmp#" index="curKW" delimiters="," {
	                if (curKW NEQ previous) {
	                	writeOutput("<opt>#curKW#</opt>");
	                    previous = curKW;
	                }
	            }
	            writeOutput("</filter>");
		    }
		}
		<!--- END OF SEARCH - GALLERIES TO RETRIEVE WILL BE IN stc_checkFilter_#listIndex# --->
		<!--- Go from listIndex down to filter 1 --->

		if (isDefined("stc_checkFilter_1") AND ListLen(list_filters) GT 0) {
			loop from="#listIndex#" to="1" step="-1" index="checkMe" {
		    	if (isDefined("stc_checkFilter_#checkMe#")) {
		        	var testStruct = Evaluate("stc_checkFilter_#checkMe#");
		            if (StructCount(testStruct) GT 0) {
						list_galleries = StructKeyList(testStruct,",");
		            	break;
		            }
		        }
		    }

			if (isDefined("list_galleries") AND list_galleries GT "") {

		        var result_stc_location = "session.search.resultset";
		        var results_exist = TRUE;

		        <!--- Go through to see if the result is saved --->
		        if (NOT isDefined("session.search.resultset")) {
		            results_exist = FALSE;
		        }
		        else {
		            <!--- Confirm filters are the same, else break --->
		            if (NOT StructKeyExists(Evaluate(result_stc_location),"filters") OR ListSort(list_filters,"numeric","asc",",") NEQ ListSort(Evaluate(result_stc_location).filters,"numeric","asc",",")) {
		                results_exist = FALSE;
		            }

		            <!--- Confirm search type is the same else break --->
		            if (results_exist EQ FALSE OR NOT StructKeyExists(Evaluate(result_stc_location),"search_order") OR Evaluate("#result_stc_location#.search_order") NEQ url.order) {
		                results_exist = FALSE;
		            }
		            if (results_exist EQ FALSE OR NOT StructKeyExists(Evaluate(result_stc_location),"gType") OR Evaluate("#result_stc_location#.gType") NEQ gType) {
		                results_exist = FALSE;
		            }
		        }

		        <!--- Now only do this if the results don't exist, otherwise grab results --->
		        if (NOT results_exist) {
		            <!--- Clear old results --->
		            if (isDefined("session.search.resultset")) {
		                var temp = StructDelete(session.search, "resultset");
		            }
		            session.search.resultset = {};

		            <!--- Get stuff --->
		        	<!---<cfdump var="#list_galleries#"><hr />--->
		            <!--- Select all matching gallery id's --->
		            var qry_galleries = variables.SearchService.qry_galleries(url.order, list_galleries);

		       		gal_results = ValueList(qry_galleries.gallery_id,',');

		            temp = Evaluate("#result_stc_location#.filters = ""#list_filters#""") ;
		            temp = Evaluate("#result_stc_location#.gType = ""#gType#""") ;
		            temp = Evaluate("#result_stc_location#.search_order = 	#url.order#") ;
		            temp = Evaluate("#result_stc_location#.data = gal_results") ;
		     	}
		     	else {
		         	gal_results = Evaluate("#result_stc_location#.data");
		      	}
		        <!--- Now got #gal_results, an ordered list, do something with it --->

				if (ListLen(gal_results) GT 0) {
					var pagethru_limit = session.preferences.pref_thumbs;
					if (pagethru_limit EQ 9999) {
						pagethru_limit = 64;
					}

					rc.paging_config = variables.UtilService.pagingConfig(ListLen(gal_results),url.CurrentPage,pagethru_limit,"","#SCRIPT_NAME#/search","SETS");
					rc.display_start_row = rc.paging_config.START_ROW - 1;
					
		            var resultset="";
		            var g_data_retrieve="";
		            loop from="#rc.paging_config.START_ROW#" to="#rc.paging_config.END_ROW#" index="i" {
		                c_result = ListGetAt(gal_results,i,",");
		                resultset = ListAppend(resultset,c_result);
	                    g_data_retrieve = ListAppend(g_data_retrieve,c_result);
		            }

		            var final_results = {}
		            if (ListLen(g_data_retrieve) GT 0) {
	            		<!--- Only get more data from the db if it's not already stored in the session --->
	            		var qry_gallery_data = variables.SearchService.qry_gallery_data(g_data_retrieve);
		                <!--- Store gallery info --->
		                loop query="qry_gallery_data" {
	                        final_results[gallery_id] = "";
	                        loop list="#qry_gallery_data.ColumnList#" index="i" {
	                         	final_results[gallery_id] = final_results[gallery_id] & "<#i#>" & Evaluate("#i#") & "</#i#>";
	                        }
		                }
		          	}

		            <!--- Build xml for galleries --->
		            savecontent variable="gallery_xml" {
			            var atts = "";
			            if (isDefined('resultset') AND ListLen(resultset,",") GT 0) {
			            	var len_gal_resaults = ListLen(gal_results,",");
			            	atts = "
					        results_page='#pagethru_limit#'
					        results_number='#ListLen(resultset)#'
					        pref_id='#session.pref_id#'
					        gallery_type='#url.gType#'
					        gallery_sort='#url.order#'
					        allow_fasttrack='#allow_fasttrack#'
					        allow_permfasttrack='#allow_permfasttrack#'
					        total_results='#len_gal_resaults#'";
						}
			            writeOutput("
				            <results
								current_page='#CurrentPage#'
								#atts#
						    >
						");
						


						if (isDefined('resultset') AND ListLen(resultset,",") GT 0) {
							loop list="#resultset#" index="c_result" {
								writeOutput("<g>#final_results[c_result]#</g>");
				        	}		
						}
						writeOutput("</results>");
					}
				}
		    }
		}
		
		savecontent variable="paging" {
				writeOutput("<paging_content_desk>");
				if(structKeyExists(rc,"paging_config")) {
					writeOutput("#getHtmlElementsPaging(rc.paging_config,"ul")#");
				}	
				writeOutput("</paging_content_desk>");
				writeOutput("<paging_content_mobi>");
				if(structKeyExists(rc,"paging_config")) {
					writeOutput("#getHtmlElementsPaging(rc.paging_config,"dd")#");
				}	
				writeOutput("</paging_content_mobi>");
		}
		
		savecontent variable="results"{
			writeOutput("	#filter_xml# 
							#gallery_xml#
							#paging# 
						");
		}

		xml variable="xml_Pages" casesensitive="no" {
			writeOutput("<searchRequest>
							#results#
						</searchRequest>");
		}
		
		variables.fw.renderData("xml", ToString(xml_Pages));
	}

	private any function getHtmlElementsPaging(struct config, string typeofpaging) {
		return variables.SearchService.paging(config,typeofpaging);
	}
}