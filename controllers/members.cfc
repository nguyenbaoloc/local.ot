component accessors="true" output="false" displayname=""  {

	property PollService;
	property UIHelperService;
	property UtilService;
	property NewsService;
	property ModelService;
	property GalleryService;
	property QueryService;
	property QueryExtendService;
	public function init(){
		return this;
	}

	public function default(struct rc) {

		param name="rc.bol_allsites_member" default="0";
		param name="rc.bol_allsites_memberdirect" default="0";
		param name="rc.bol_tease_memberpage" default="0";
    	param name="rc.display_random" default="0";

    	rc.UIHelperService = variables.UIHelperService;
    	rc.QueryService = variables.QueryService;
    	rc.QueryExtendService = variables.QueryExtendService;
    	rc.modelService = variables.ModelService;

		var str_membersPath = ExpandPath("/views/members");

		if (rc.bol_allsites_member EQ 1) {
		    rc.strServerZipLink = application.file_locat_url_zip;
		    rc.strServerVideoLink = application.file_locat_url;
		}
		else if (rc.bol_allsites_memberdirect EQ 1) {
		    rc.strServerZipLink = application.direct_file_locat_url_zip;
		    rc.strServerVideoLink = application.direct_file_locat_url;
		}
		else if (rc.bol_tease_memberpage EQ 1) {
		    rc.strServerZipLink = application.direct_file_locat_url_zip;
		    rc.strServerVideoLink = application.direct_file_locat_url;
		}
		else {
		    rc.strServerZipLink = application.file_locat_url_zip;
		    rc.strServerVideoLink = application.file_locat_url;
		}

		//show view_model_poll
		rc.view_model_poll = view_model_poll();

		// for today release
		rc.qry_sets = variables.ModelService.getModelRealease(0);  

		// for yesterday release
		rc.qry_yesterday = variables.ModelService.getModelRealease(1);					    	

		// for previous release 1
		rc.qry_previous1 = variables.ModelService.getModelRealease(2);		
					    
		// for previous release 2
		rc.qry_previous2 = variables.ModelService.getModelRealease(3);					    

		rc.qry_ComingSoon = variables.QueryService.getUpcomingRelease();

	}

	public struct function view_model_poll() {
		var rs = {};
		rs.qry_CurrentID = variables.PollService.getCurrentID();

		if(rs.qry_CurrentID.recordcount gt 0)
		{
			rs.qry_voted = variables.PollService.getVoted(rs.qry_CurrentID.QuestionID);
			if(rs.qry_voted.recordcount GT 0)
			{
				rs.Totals = variables.PollService.getTotals(rs.qry_CurrentID.QuestionID);
				rs.Results = variables.PollService.getResults(rs.qry_CurrentID.QuestionID)
			}
			else
			{
				rs.Question = variables.PollService.getQuestion(rs.qry_CurrentID.QuestionID);
			}
		}

		return rs;
	}
}