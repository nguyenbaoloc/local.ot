component accessors="true" output="false" displayname=""  {

	property NewForumService;
	property TopicsService;
	property UtilService;
	property ReadThreadService;
	property PostReplyService;
	public function init(required any fw){
		variables.fw = fw;
		return this;
	}

	public function default (struct rc) {
		rc.NewForumService = variables.NewForumService;
		rc.user_details    = rc.NewForumService.user_details(trim(cgi.remote_user));
		rc.get_forums	   = rc.NewForumService.get_forums(rc.user_details.showpublicposts, rc.user_details.forum_edit_all);
		rc.qry_fasttracks  = rc.NewForumService.qry_fasttracks(DateAdd("d",-5,now()), trim(cgi.remote_user));

	}

	public function topics (struct rc) {
		param name="form.Range" Default = 30;
		param name="url.Range" Default = form.Range;
		param name="url.CurrentPage" default=1;
		param name="url.ForumID" default=0;

		rc.TopicsService = variables.TopicsService;
		rc.get_forums    = rc.TopicsService.get_forums();
		rc.user_details  = rc.TopicsService.user_details(trim(cgi.remote_user));
		rc.get_forum 	 = rc.TopicsService.get_forum(url.ForumID);

		var post_before = "";
		if (url.Range neq 0) {
			var todays_date = "#CreateDate("#Year("#Now()#")#","#Month("#Now()#")#","#Day("#Now()#")#")#";
			var post_date   = "#CreateTimeSpan("-#url.Range#","0","0","0")#";
			post_before     = "#todays_date#" + "#post_date#";
		}

		rc.get_topiccount = rc.TopicsService.get_topiccount(url.ForumID, url.Range, post_before);

		if (rc.get_topiccount.count > 0) {
			rc.paging_config = variables.UtilService.pagingConfig(rc.get_topiccount.count,url.CurrentPage,application.forum_threads_shown,"?ForumID=#url.ForumID#&Range=#url.Range#","#SCRIPT_NAME#/newforum.topics","Topics");
			rc.get_topics = rc.TopicsService.get_topics(url.ForumID, url.Range, post_before, rc.paging_config.START_ROW - 1);
		}
		else {
			rc.get_topics = StructNew();
			rc.get_topics.RecordCount = 0;
		}

		rc.qry_fasttracks = rc.TopicsService.qry_fasttracks(DateAdd("d",-5,now()), trim(cgi.remote_user));

		rc.paging_config = variables.UtilService.pagingConfig(rc.get_topiccount.count,url.CurrentPage,application.forum_threads_shown,"?ForumID=#url.ForumID#&Range=#url.Range#","#SCRIPT_NAME#/newforum.topics","TOPICS");
	}

	public function post_topic (struct rc) {

		
		param name="rc.int_perRow" default="1";
		param name="rc.int_answers" default="8";

		rc.TopicsService = variables.TopicsService;
		rc.minimum_for_FT = 250;
		rc.bolImageError = false;
		rc.bolInlineImageError = false;

		if (isDefined("url.clear")) {
			<!--- Clear cookies to remember author --->
			cookie name = "forum_author" value = "" expires = "now";
			cookie name = "forum_email" value = "" expires = "now";
		}

		<!--- Forum cookie --->
		if (isDefined("Cookie.forum_author") AND isDefined("Cookie.forum_email")) {
			param name="form.Author" default="#Cookie.forum_author#";
			param name="form.Email" default="#Cookie.forum_email#";
		}
		else {
			param name="form.Author" default="";
			param name="form.Email" default="";
		}

		param name="rc.Button" default="";
		param name="rc.DoublePost" default="0";

		param name="form.poll_question" default="";
		param name="form.poll_answer_1" default="";
		param name="form.poll_answer_2" default="";
		param name="form.poll_answer_3" default="";
		param name="form.poll_answer_4" default="";
		param name="form.poll_answer_5" default="";
		param name="form.poll_answer_6" default="";
		param name="form.poll_answer_7" default="";
		param name="form.poll_answer_8" default="";
		param name="rc.pollError" default="0";
		param name="rc.emptyCount" default="0";
		param name="form.body" default="";
		param name="form.Subject" default="";
		param name="rc.Reply" default="0";

		loop from="1" to="8" index="rg" {
			if ( evaluate('form.poll_answer_#rg#') EQ "") {
				rc.emptyCount = rc.emptycount + 1;
			}
		}

		if (form.poll_question NEQ "" AND rc.emptyCount GTE 7) {
			rc.button = "Preview Topic";
			rc.pollError = 1;
		}

		if(rc.Button == "Submit Topic") {

			rc.thread_query = rc.TopicsService.thread_query();

			if(form.Subject EQ "") {
				form.Subject = "[No Subject]";
			}

			if(rc.thread_query.n gt 0) {
				rc.Thread = rc.thread_query.n+1;
			}
			else {
					rc.Thread = 1;
			}

			rc.check_forum = rc.TopicsService.check_forum(url.ForumID);

			if(rc.check_forum.AllowHTML == "N") {
				form.Author = REReplaceNoCase("#form.Author#", "<[^>]*>", "", "all");
				form.Email = REReplaceNoCase("#form.Email#", "<[^>]*>", "", "all");
				form.Subject = REReplaceNoCase("#form.Subject#", "<[^>]*>", "", "all");
				form.Body = REReplaceNoCase("#form.Body#", "<[^>]*>", "", "all");
			}

			form.Body = trim(form.Body);
			form.Body = ReplaceNoCase(form.Body, Chr(10), "<br>", "ALL");
			form.Body = ReplaceNoCase(form.Body, Chr(9), "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "ALL");
			form.Body = ReplaceNoCase(form.Body, "  ", "&nbsp;&nbsp;", "ALL");

			if(cgi.server_name CONTAINS "onlycarla") {
				rc.posted_on = "Only Carla";
			}
			else if (cgi.server_name CONTAINS "only-opaques") {
				rc.posted_on = "Only Opaques";
			}
			else if (cgi.server_name CONTAINS "only-secretaries") {
				rc.posted_on = "Only Secretaries";
			}
			else if (cgi.server_name CONTAINS "onlysilkandsatin") {
				rc.posted_on = "Only Silk and Satin";
			}
			else if (cgi.server_name CONTAINS "onlymelanie") {
				rc.posted_on = "Only Melanie";
			}
			else {
				rc.posted_on = "Only Tease";
			}

			param default="0" name="MembersOnly";

			rc.str_Shortcut = form.Subject;
			rc.str_Shortcut = REReplace(rc.str_Shortcut, "[^a-zA-Z0-9\_]", " ", "ALL");
			rc.str_Shortcut = Replace(rc.str_Shortcut," ","-","ALL");
			rc.str_Shortcut = REReplace(rc.str_Shortcut, "-+", "-", "ALL");
			if(len(rc.str_Shortcut) EQ 1) {
				rc.str_Shortcut = "1";
			}
			if(Right(rc.str_Shortcut, 1) EQ "-") {
				rc.str_Shortcut = Left(rc.str_Shortcut, Len(rc.str_Shortcut)-1);
			}

			if(Left(rc.str_Shortcut, 1) EQ "-") {
				rc.str_Shortcut = Right(rc.str_Shortcut, Len(rc.str_Shortcut)-1);
			}
		 	rc.str_Shortcut =  LCase(rc.str_Shortcut);

			rc.str_Shortcut = uniqueShortcut(rc.str_Shortcut);

			if(form.Body DOES NOT CONTAIN "members/sets" AND form.Body DOES NOT CONTAIN "data:image") {

				<!--- add poll data to tables --->
				// rc.insertQuestion = rc.TopicsService.insertQuestion(poll_question);

				rc.getQuestionID = rc.TopicsService.getQuestionID(poll_question);

				// loop from="1" to="8" index="rg" {
				// 	if(evaluate('poll_answer_#rg#') NEQ "") {
				// 		// rc.insertAnswers = rc.TopicsService.insertAnswers(rc.getQuestionID.QuestionID, evaluate('poll_answer_#rg#'));
				// 	}
				// }

				// rc.post_topic = rc.TopicsService.post_topic(CreateODBCDateTime("#Now()#"),
				// 											ForumID,
				// 											Thread,
				// 											form.Author, 
				// 											CreateODBCDateTime("#Now()#"),
				// 											form.Subject,
				// 											form.Email,
				// 											#rc.Reply == 1 ? 'Y' : 'N'#,
				// 											form.Body,
				// 											trim(cgi.remote_user),
				// 											session.pref_id,
				// 											posted_on,
				// 											MembersOnly,
				// 											str_Shortcut,
				// 											rc.getQuestionID.QuestionID,
				// 											form.poll_question
				// 											);

				if(rc.DoublePost EQ 1) {
					<!--- link up members and public forums --->
					if(url.ForumID EQ 1) {
						rc.DoubleForumid = 16;
					}
					else if (url.ForumID EQ 3) {
						rc.DoubleForumid = 20;
					}
					else if (url.ForumID EQ 11) {
						rc.DoubleForumid = 21;
				   	}
				   	else if (url.ForumID EQ 5) {
						rc.DoubleForumid = 18;
				   	}

				   	rc.thread_query = rc.TopicsService.thread_query();

					if(form.Subject EQ "") {
						form.Subject = "[No Subject]";
					}

					if(rc.thread_query.n gt 0) {
						rc.Thread = rc.thread_query.n+1;
					}
					else {
						rc.Thread = 1;
					}

					rc.str_Shortcut = form.Subject;
					rc.str_Shortcut = REReplace(str_Shortcut, "[^a-zA-Z0-9\_]", " ", "ALL");
					rc.str_Shortcut = Replace(str_Shortcut," ","-","ALL");
					rc.str_Shortcut = REReplace(str_Shortcut, "-+", "-", "ALL");
					if(len(str_Shortcut) EQ 1) {
						rc.str_Shortcut = "1";
					}
					if(Right(str_Shortcut, 1) EQ "-") {
						rc.str_Shortcut = Left(str_Shortcut, Len(str_Shortcut)-1);
					}

					if(Left(str_Shortcut, 1) EQ "-") {
						rc.str_Shortcut = Right(str_Shortcut, Len(str_Shortcut)-1);
					}
				 	rc.str_Shortcut =  LCase(str_Shortcut);
					rc.str_Shortcut = uniqueShortcut(str_Shortcut);

					// rc.post_topic = rc.TopicsService.post_topic(CreateODBCDateTime("#Now()#"),
					// 											rc.DoubleForumid,
					// 											Thread,
					// 											form.Author, 
					// 											CreateODBCDateTime("#Now()#"),
					// 											form.Subject,
					// 											form.Email,
					// 											#rc.Reply == 1 ? 'Y' : 'N'#,
					// 											form.Body,
					// 											trim(cgi.remote_user),
					// 											session.pref_id,
					// 											posted_on,
					// 											MembersOnly,
					// 											str_Shortcut,
					// 											rc.getQuestionID.QuestionID,
					// 											form.poll_question
					// 											);
				}

				//update forums
				// rc.update_forum_1 = rc.TopicsService.update_forum_1(url.ForumID);
				// rc.update_forum_2 = rc.TopicsService.update_forum_2(url.ForumID);
				// rc.update_forum_3 = rc.TopicsService.update_forum_3(rc.update_forum_1.RecordCount,rc.update_forum_2.RecordCount,url.ForumID);
				// rc.TopicsService.updateForumPreferences();

				if(Len(form.Body) GT rc.minimum_for_FT) {
					<!--- now check and update the count fasttracks --->
						rc.qryCheckCount = rc.TopicsService.qryCheckCount();

						if(rc.qryCheckCount.recordcount EQ 1) {
							<!--- if current_posts_count is 9, means we can then give them new FT
							and alert the user --->
							if(rc.qryCheckCount.current_posts_count EQ 9) {

								// rc.TopicsService.updateForumPreferences_2();
								// rc.TopicsService.insertForumFasttracks();
								location url="http://members.onlytease.com/members/new_forum/ft_notice.cfm?new=true&ForumID=#ForumID#" addtoken="No";
							}
						}
						// else {
						// 		// rc.TopicsService.updateForumPreferences_3();
						// }
				}
				location url="/index.cfm/newforum.topics?ForumID=#url.ForumID#" addtoken="No";
			}
			else {
				if(form.Body CONTAINS "members/sets") {
					rc.bolImageError = true;
				}
				if(form.Body CONTAINS "data:image") {
					rc.bolInlineImageError = true;
				}
			}
		}

		if (cgi.remote_user NEQ "" OR (isDefined("session.pref_id") AND session.pref_id GT 0)) {
			rc.qry_checkPreferences = rc.TopicsService.qry_checkPreferences(trim(cgi.remote_user));
			
			if (rc.qry_checkPreferences.forum_posts_banned EQ 1) {
				location url="/index.cfm/newforum/forum_posts_banned" addtoken="no";
			}
			else if (rc.qry_checkPreferences.forum_post_allowed EQ 0) {
				<!--- username found and they have not been banned from posting --->
				location url="http://members.onlytease.com/preferences/preferences.cfm?new=true" addtoken="no";
			}
		}

		rc.get_forum = rc.TopicsService.get_forum(url.ForumID);
		rc.NewForumService = variables.NewForumService;
		rc.user_details    = rc.NewForumService.user_details(trim(cgi.remote_user));
		rc.get_forums	   = rc.NewForumService.get_forums(rc.user_details.showpublicposts, rc.user_details.forum_edit_all);
		rc.qry_fasttracks = rc.TopicsService.qry_fasttracks(DateAdd("d",-5,now()), trim(cgi.remote_user));

	}

	public function read_thread (struct rc) {
		param name="CurrentPage" default="1";
		param name="session.preferences.forum_display" default="30";
		rc.ReadThreadService = variables.ReadThreadService;
		rc.NewForumService = variables.NewForumService;
		rc.TopicsService   = variables.TopicsService;
		rc.user_details    = rc.NewForumService.user_details(trim(cgi.remote_user));
		rc.get_forums	   = rc.NewForumService.get_forums(rc.user_details.showpublicposts, rc.user_details.forum_edit_all);
		rc.qry_fasttracks  = rc.NewForumService.qry_fasttracks(DateAdd("d",-5,now()), trim(cgi.remote_user));

		if (url.CurrentPage EQ "0") {
			url.CurrentPage = "1";
		}

		rc.get_topic = rc.ReadThreadService.get_topic(url.ThreadID);
		<!--- process form submission --->
		if (NOT StructIsEmpty(form)) {
			// rc.ReadThreadService.insert_forum_poll_votes(rc.get_topic.PollQuestionID, form.radioPoll);
			location addtoken="no" url="read_thread.cfm?CurrentPage=#url.CurrentPage#&ForumID=#url.ForumID#&ThreadID=#url.ThreadID#&Thread=#url.Thread#";
		}

		if (rc.get_topic.PollQuestionID NEQ 0) {
			rc.getPollAnswers = rc.ReadThreadService.getPollAnswers(rc.get_topic.QuestionID);
		}

		rc.get_thread = rc.ReadThreadService.get_thread(url.Thread);

		rc.paging_config = variables.UtilService.pagingConfig(rc.get_thread.RecordCount,url.CurrentPage,session.preferences.forum_display,"?ForumID=#url.ForumID#&ThreadID=#url.ThreadID#&Thread=#url.Thread#","#SCRIPT_NAME#/newforum.read_thread","POSTS");

		rc.get_forum = variables.TopicsService.get_forum(url.ForumID);

		// rc.update_view_1 = rc.ReadThreadService.update_view_1(url.Thread);
		// rc.update_view_2 = rc.ReadThreadService.update_view_2(rc.update_view_1.Vỉews, rc.update_view_1.ThreadID);

		rc.qry_forumeditall = rc.ReadThreadService.qry_forumeditall();

		if (rc.qry_forumeditall.forum_edit_all EQ 1 ) {
			rc.forum_edit_all = TRUE;
		}
		else {
			rc.forum_edit_all = FALSE;
		}
	}

	public function post_reply (struct rc) {

		param name="form.Author" default="";
		param name="form.Email" default="";
		param name="rc.Button" default="";
		param name="Reply" default="0";
		param name="form.body" default="";

		rc.bolImageError = false;
		rc.bolInlineImageError = false;

		if ( isDefined("url.clear") ) {
			<!--- Clear cookies to remember author --->
			cookie name = "forum_author" value = "" expires = "now";
			cookie name = "forum_email" value = "" expires = "now";
		}

		rc.PostReplyService = variables.PostReplyService;
		rc.minimum_for_FT = 250;

		rc.get_thread = rc.PostReplyService.get_thread(url.Thread);

		rc.CurrentPage = url.CurrentPage;
		rc.tempCurrentPage = rc.CurrentPage;
		rc.CurrentPage = (rc.CurrentPage / 30);
		rc.LastPage = (rc.get_thread.recordcount / 30);
		rc.LastPage = Ceiling(rc.LastPage);
		rc.CurrentPage = Ceiling(rc.CurrentPage);

		if (rc.Button == "Submit Reply") {
			if (form.Body DOES NOT CONTAIN "members/sets" AND form.Body DOES NOT CONTAIN "data:image") {
				rc.check_forum = rc.PostReplyService.check_forum(url.ForumID);
				if ( rc.check_forum.AllowHTML is "N") {
					form.Author = REReplaceNoCase("#form.Author#", "<[^>]*>", "", "all");
					form.Email = REReplaceNoCase("#form.Email#", "<[^>]*>", "", "all");
					form.Subject = REReplaceNoCase("#form.Subject#", "<[^>]*>", "", "all");
					form.Body = REReplaceNoCase("#form.Body#", "<[^>]*>", "", "all");
				}
				form.Body = trim(form.Body);
				form.Body = ReplaceNoCase(form.Body, Chr(10), "<br>", "ALL");
				form.Body = ReplaceNoCase(form.Body, Chr(9), "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "ALL");
				form.Body = ReplaceNoCase(form.Body, "  ", "&nbsp;&nbsp;", "ALL");

				rc.qry_name = rc.PostReplyService.get_thread_name(url.ThreadID);

				if (cgi.server_name CONTAINS "onlycarla") {
					rc.posted_on = "Only Carla";
				}
				else if (cgi.server_name CONTAINS "only-opaques") {
					rc.posted_on = "Only Opaques";
				}
				else if (cgi.server_name CONTAINS "only-secretaries") {
					rc.posted_on = "Only Secretaries";
				}
				else if (cgi.server_name CONTAINS "onlysilkandsatin") {
					rc.posted_on = "Only Silk and Satin";
				}
				else if (cgi.server_name CONTAINS "onlymelanie") {
					rc.posted_on = "Only Melanie";
				}
				else {
					rc.posted_on = "Only Tease";
				}

				// rc.post_topic = rc.PostReplyService.post_topic(url.ForumID, url.Thread, rc.qry_name.ActualThread, form.Author, form.Subject, form.Email, (rc.Reply == 0) ? 'N' : 'Y', form.Body, rc.posted_on);
				// rc.update_forum_1 = rc.PostReplyService.update_forum_1(url.ForumID);
				// rc.update_forum_2 = rc.PostReplyService.update_forum_2(url.ForumID);
				// rc.update_forum_3 = rc.PostReplyService.update_forum_3(url.ForumID,rc.update_forum_1.RecordCount,rc.update_forum_2.RecordCount);
				// rc.update_forum_4 = rc.PostReplyService.update_forum_4(url.Thread);

				var mail_reply = rc.PostReplyService.mail_reply(url.ForumID);

				if (mail_reply.Reply == "Y") {
				
					var get_forum = rc.PostReplyService.get_forum(url.ForumID);
					var get_settings = rc.PostReplyService.get_settings();

					var mailer = new Mail();

					var content = "The following was sent to you in response
					to your post on #AppName#.
					==========================================================
					#DateFormat("#Now()#", "dddd, mmmm d, yyyy")# @ #TimeFormat("#Now()#", 'h:mm tt')#
					==========================================================
					#form.Author# has posted a reply to your topic. To see this 
					thread, use the following address:

					#get_settings.BaseURL##appilcation.AppPath#read_thread.cfm?ForumID=#url.ForumID#&CurrentPage=#rc.LastPage#&ThreadID=#mail_reply.ThreadID#&Thread=#url.Thread#

					==========================================================";

					savecontent variable="mail_content"  append="false" { WriteOutput(content); };
	 				mailer.setTo(RTrim(mail_reply.Email)); 
			        mailer.setFrom("forum@onlytease.com"); 
			        mailer.setsubject("#get_forum.ForumName# - #RTrim(form.Subject)#"); 
			        mailer.setType("html"); 
			        mailer.addPart( type="html", charset="utf-8", body=warning);
			        mailer.send(); 
				}

				// var update_total_all_posts_count = rc.PostReplyService.update_total_all_posts_count();
				if (Len(form.Body) GT rc.minimum_for_FT) {
					<!--- now check and update the count fasttracks --->
					var qryCheckCount = rc.PostReplyService.qryCheckCount();

					if (qryCheckCount.recordcount EQ 1) {
						<!--- if current_posts_count is 9, means we can then give them new FT
						and alert the user --->
						if (qryCheckCount.current_posts_count GTE 9) {

							// var ForumPreferences_1 = rc.PostReplyService.ForumPreferences_1();
							// var insert_ForumFasttracks = rc.PostReplyService.insert_ForumFasttracks();
							location url="http://members.onlytease.com/members/new_forum/ft_notice.cfm?new=true&ForumID=#url.ForumID#&CurrentPage=#rc.LastPage#&ThreadID=#rc.qry_name.ActualThread#&Thread=#url.Thread#" addtoken="No";
						}
						// else {
						// 	<!--- just update the counts for current and total posts --->
						// 	var ForumPreferences_2 = rc.PostReplyService.ForumPreferences_2();
						// }
					}
				}
				location url="/index.cfm/newforum.read_thread?ForumID=#url.ForumID#&CurrentPage=#rc.LastPage#&ThreadID=#rc.qry_name.ActualThread#&Thread=#url.Thread###end" addtoken="No";
			}
			else {
				if (form.Body CONTAINS "members/sets") {
					rc.bolImageError = true;
				}
				if (form.Body CONTAINS "data:image") {
					rc.bolInlineImageError = true;
				}
			}
		}

		if (rc.Button == "Cancel Reply") {
			rc.qry_name = rc.PostReplyService.get_thread_name(url.ThreadID);

			location url="/index.cfm/newforum.read_thread?ForumID=#ForumID#&CurrentPage=#rc.tempCurrentPage#&ThreadID=#rc.qry_name.ActualThread#&Thread=#url.Thread###end" addtoken="No";
		}

		if (isDefined("session.pref_id")) {
			<!--- registered a username --->
			<!--- MY OT preferences check and initial setup --->
			<!--- check for this user in database --->
			rc.qry_checkPreferences = rc.PostReplyService.qry_checkPreferences();
			
			if ((rc.qry_checkPreferences.recordcount GT 0) AND (rc.qry_checkPreferences.forum_posts_banned EQ 1)) {
				location url="/index.cfm/newforum/forum_posts_banned" addtoken="no";
			}
			else if (rc.qry_checkPreferences.forum_post_allowed EQ 0) {
				<!--- username found and they have not been banned from posting --->
				location url="http://members.onlytease.com/members/new_forum/preferences.cfm?new=true" addtoken="no";
			}
		}

		rc.get_message = rc.PostReplyService.get_message(url.ThreadID);
		rc.get_forum   = rc.PostReplyService.get_forum_2(url.ForumID);

		param name="rc.Subject" default="#rc.get_message.Subject#";

		if (Left(rc.get_message.Subject,4) neq "RE: ") {
			rc.Subject = "RE: " & rc.get_message.Subject;
		}

		rc.qry_fasttracks = variables.TopicsService.qry_fasttracks(DateAdd("d",-5,now()), trim(cgi.remote_user));
	}

	private string function uniqueShortcut (string str_Shortcut) {
		var int_Count = 1;
		var str_CheckShortcut = str_Shortcut;

		<!--- look for the shortcut in the shortcuts table --->
		var qry_CheckShortcut = variables.TopicsService.qry_CheckShortcut(str_CheckShortcut);

		while (qry_CheckShortcut.recordcount gt 0) {
			int_Count = int_Count + 1;
			str_CheckShortcut = "#str_Shortcut#-#int_Count#";
			qry_CheckShortcut = variables.TopicsService.qry_CheckShortcut(str_CheckShortcut);
		}

		return str_CheckShortcut;
	}
}