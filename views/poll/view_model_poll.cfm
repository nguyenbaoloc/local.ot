<!--- get the id of the latest poll --->
<cfif data.qry_CurrentID.recordcount gt 0>
<!--- check voting status on current poll. --->
	<cfif data.qry_voted.recordcount GT 0>
		<!--- Query the question and the total number of votes for all the answers --->
		<!--- Query the answers and the number of votes for each one. --->
		<cfoutput query="data.Totals"> 
      		<div align="center"><strong>Current Poll:<br>#Question#</strong></div>
       	</cfoutput> 
      	<table width="175" border="0" align="center" cellpadding="2" cellspacing="0">
			<cfoutput query="data.Results"> 
	        	<!--- Set the percent of total votes for each answer. --->
		        <cfset Percent=Round((Votes / data.Totals.TotalVotes) * 100)>
		        <tr> 
		         	<td><div align="left">#Answer#</div></td>
		        </tr>
		        <tr> 
					<td><div align="left"><img align="middle" src="/images/poll-graph.gif" width="#Percent#%" height="5"></div></td>
		        </tr>
		        <tr> 
		          	<td><div align="right"><em> - #Percent#% of votes</em></div></td>
		        </tr>
	      	</cfoutput>
		</table>
	<cfelse>
		<form action="/index.cfm/polls/model_poll_results" method="post" name="Poll">
				<cfoutput query="data.Question" group="QuestionID"> 
					<!--- Pass the question id as a hidden variable. --->
					<input type="hidden" name="QuestionID" value="#data.qry_CurrentID.QuestionID#">
					<cfoutput> 
					<article style="padding-bottom: 30px;">
	                    <p style="margin:0px;">
	                    	<label class="model_name">
	                    		<label class="lblname">NAME:&nbsp;</label>
                    			<!--- #HTMLParse(Answer).XmlRoot.XmlChildren[1].XmlChildren[1].XmlChildren[1].XmlText# --->
	                    		<input type="radio" name="AnswerID" value="#AnswerID#" style="float:right;">
	                    	</label>
	                    </p>
	                    <P>
	                    	<!--- <a href="/index.cfm#replace(HTMLParse(Answer).XmlRoot.XmlChildren[1].XmlChildren[1].XmlChildren[1].XmlAttributes.href,'.cfm','')#"> --->
	                    	<a>
	                    		<!--- #HTMLParse(Answer).XmlRoot.XmlChildren[1].XmlChildren[1].XmlChildren[1].XmlChildren[1]# --->
		                    </a>
		                </P>
	                </article>
					</cfoutput>
				</cfoutput> 
				<input name="Submit" type="submit" class="btn btn-success btn_modelofmonth" value="Submit Vote">
		</form>
	</cfif>
<cfelse>
	no poll to display	
</cfif>	