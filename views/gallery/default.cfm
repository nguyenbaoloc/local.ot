<cfoutput>
	<div class="row clearfix archive-filter">
		<div class="col-md-7 col-sm-7 left">
			<label class="desktop" style="font-weight: normal;">GALLERY </label>
		</div>
		<div class="col-md-5 col-sm-5 right">
			<form id="fViewGallery" class="form-inline filterForm" method="post">
				<div class="form-group">
					<label>Gallery Filter:</label>
					<select id="ddViewGallery" class="filterSelect">
						<option value="all" <cfif URL.view EQ "all">selected</cfif>>View All</option>
						<option value="pictures" <cfif URL.view EQ "pictures">selected</cfif>>Picture sets only</option>
						<option value="videos" <cfif URL.view EQ "videos">selected</cfif>>Video sets only</option>
					</select>
				</div>
				<input type="Hidden" name="view" value="#URL.view#">
			    <input type="Hidden" id="pMonth" name="pMonth" value="#rc.pMonth#">
				<input type="Hidden" id="pYear" name="pYear" value="#rc.pYear#"> 
				<input type="Hidden" id="Now-Year" value="#year(now())#"> 
			</form>
		</div>
	</div>

	<cfif rc.qry_archive.recordcount GT 0>
		<cfset current_date = "">
		<cfset current_date2 = "">
		<cfloop query="rc.qry_archive">
			<cfif current_date neq rc.qry_archive.release_date_formarted>
				<div class="row clearfix becky-image-sets">
					<div class="right">
						#rc.qry_archive.release_date_formarted#	
					</div>
				</div>
				<cfset current_date2 = rc.qry_archive.release_date_formarted>
			</cfif>
			#view('gallery/releasedset', {data: rc.qry_archive})#

			<cfif current_date2 neq rc.qry_archive.release_date_formarted[CurrentRow + 1]>
				<cfset dataVip = rc.UIHelperService.udf_Vipsets(dat_release:rc.qry_archive.release_date,sView:URL.view)>
				<cfif dataVip.vipUpdates.recordcount gt 0>
					<div class="row bg-vipContent" style="margin-top:10px;">
                        <div class="row custom-min-height">
                        	<cfif ListFind(application.stc_set_types.str_videos, dataVip.vipUpdates.gallery_type)>
						        <cfset image_set = FALSE>
						    <cfelse>
						        <cfset image_set = TRUE>
						    </cfif>
	            			#view('common/image_vip_set', {image_set: image_set, data: dataVip.vipUpdates})#
	            		</div>
	            	</div>
	            	<div class="row clearfix download-zone download-vip">
	            		#view('common/row_vip_info', {model_names:dataVip.model_names, data: dataVip.vipUpdates})#
	            	</div>
				</cfif>
			</cfif>
			<cfset current_date = rc.qry_archive.release_date_formarted>
		</cfloop>
	<cfelse>
		- no galleries to show -
	</cfif>
</cfoutput>

