<cfoutput>
<cfset video_info = "">
<div class="row">
    <div class="col-md-12 col-sm-12">
        <cfif ListFind(application.stc_set_types.str_videos, data.gallery_type)>
            <cfset image_set = FALSE>
        <cfelse>
            <cfset image_set = TRUE>
        </cfif>
        <div class="row" style="margin-top:10px;">
            <div class="row custom-min-height">
                #view('common/images_set', {image_set: image_set, data: data})#
            </div>
        </div>
        <div class="contentSpacer"></div>
        <div class="<cfif image_set>row clearfix download-zone<cfelse>row clearfix view-video-zone-pink</cfif>">
            <cfif data.gallery_type NEQ 21>
                <cfif ListFind(application.video_set_types,data.gallery_type)>
                     <cfset video_info = '<p>
                                             <label class="model-name">#rc.UtilService.udf_modelNames(data.gallery_id)#</label><span class="border-left"><i class="fa fa-film"></i> Video length: #data.video_length# </span><span class="border-left desktop">#rc.UIHelperService.set_TextOfGalleryType(data.gallery_type)#</span>
                                         </p>'/>       
                <cfelse>
                    <cfset release_date = #data.release_date#>
                    <cfset from_date = #createODBCDate(DateAdd("d",-30,now()))#>

                    <cfif data.video_clip>
                        <br/><img src="/img/icon_XL_Purple.png" align="absmiddle" alt=""> <label class="xl-size size-text">Plus behind the scenes video available!</label>
                    </cfif>
                </cfif>
            </cfif>
            <cfif data.gallery_type EQ 21>
                <cfset #data.model_display_name# = Trim(ReplaceNoCase(model_display_name, " ", "-", "All"))>
                <br><a href="#data.guest_link_url#" target="_new" alt="Click Here To Visit #data.model_display_name#" title="Click Here To Visit #data.model_display_name#"><img src="/images/guest_galleries/#data.model_display_name#_cover_#data.model_id#.jpg" border="0" width="260"></a>
            <cfelse>
                #view('common/row_info_pink', {data: data, model_names: rc.UtilService.udf_modelNames(data.gallery_id), UIHelperService: rc.UIHelperService, video_info:video_info,strServerZipLink: rc.strServerZipLink,strServerVideoLink: rc.strServerVideoLink })#
            </cfif>
        </div>
    </div>
</div>
</cfoutput>