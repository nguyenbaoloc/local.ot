<cfoutput>
	<cfset data = rc.QueryExtendService.getVipData(pDate_release)>
	<cfif data.recordcount gt 0 >
		<!--- create the string of model names --->
		<cfset and_switch = FALSE>
		<cfset model_names = "">
		<cfset model_multiple = FALSE>
		<cfif data.fourth_model_name NEQ "">
			<cfset model_names = " and <a href=""#application.vip_location#/members/search.cfm?type=model&model_id=#data.fourth_model_id#"" title=""Click to view all sets of this model"">#data.fourth_model_name#</a>">
			<cfset and_switch = TRUE>
			<cfset model_multiple = TRUE>
		</cfif>
		<cfif data.third_model_name NEQ "">
			<cfif and_switch>
				<cfset model_names = ", <a href=""#application.vip_location#/members/search.cfm?type=model&model_id=#data.third_model_id#"" title=""Click to view all sets of this model"">#data.third_model_name#</a>" & model_names>
			<cfelse>
				<cfset model_names = " and <a href=""#application.vip_location#/members/search.cfm?type=model&model_id=#data.third_model_id#"" title=""Click to view all sets of this model"">#data.third_model_name#</a>">
				<cfset and_switch = TRUE>
				<cfset model_multiple = TRUE>
			</cfif>
		</cfif>
		<cfif data.second_model_name NEQ "">
			<cfif and_switch>
				<cfset model_names = ", <a href=""#application.vip_location#/members/search.cfm?type=model&model_id=#data.second_model_id#"" title=""Click to view all sets of this model"">#data.second_model_name#</a>" & model_names>
			<cfelse>
				<cfset model_names = " and <a href=""#application.vip_location#/members/search.cfm?type=model&model_id=#data.second_model_id#"" title=""Click to view all sets of this model"">#data.second_model_name#</a>">
				<cfset and_switch = TRUE>
				<cfset model_multiple = TRUE>
			</cfif>
		</cfif>

		<cfset model_names = "<a href=""#application.vip_location#/members/search.cfm?type=model&model_id=#data.model_id#"" title=""Click to view all sets of this model"" target=""_new"">#data.model_display_name#</a>" & model_names>

		<div class="row marginTop">
	        <div class="col-md-12 col-sm-12">
	            <cfif ListFind(application.video_set_types, data.gallery_type)>
	                <cfset image_set = FALSE>
	            <cfelse>
	                <cfset image_set = TRUE>
	            </cfif>
	            <div class="row bg-vipContent marginTop">
	                <div class="row custom-min-height">
						<cfif NOT image_set>
						    <cfif data.video_hd_res_name NEQ "">
						        <div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic">
									<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
										<img width="260" class="img-responsive" src="#application.vip_location#/images/tour_images/#replace(data.tour_pic3_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
										<div class="opacity-vid-pic">
						                	<p>
						                		<i class="fa fa-film"></i>
						                	</p>	
						                	<label>Video</label>
						                </div>
									</a>
								</div>
							</cfif>
							<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic'#">
								<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
									<img src="#application.vip_location#/images/mailing_list/#replace(data.mailing_list_pic_thumb,'tn','tn260')#" class="img-responsive" border="0" alt="Click here to view this video" title="Click here to view this video">
								</a>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic desktop">
								<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
									<img src="#application.vip_location#/images/updates/tn260#data.updates_pic_thumb#" class="img-responsive" border="0" alt="Click here to view this video" title="Click here to view this video">
								</a>
							</div>
						</cfif>
						<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic'#">
							<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
								<img width="260" class="img-responsive" src="#application.vip_location#/images/tour_images/#replace(data.tour_pic1_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
							</a>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic'#">
							<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
								<img width="260" class="img-responsive" src="#application.vip_location#/images/tour_images/#replace(data.tour_pic2_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
							</a>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic desktop'#">
							<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
								<img width="260" class="img-responsive" src="#application.vip_location#/images/tour_images/#replace(data.tour_pic3_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
							</a>
						</div>
	                </div>
	            </div>
	            <div class="row clearfix download-zone download-vip marginTop">
					<div class="row model-info-pink model-info-yellow">
					    <div class="row col-md-9 col-sm-9 row-info-pink row-vip-no-border">
					        <p>
					            <label class="model-name model-name-vip">
					                #model_names#
					            </label> 
					            <span class="border-left border-left-vip">
					            	<cfif image_set>
					            		<i class="fa fa-camera"></i> #NumberFormat(data.image_count,"9,999")#
					            	<cfelse>
					            		<i class="fa fa-film"></i> Video length: #data.video_length#
					            	</cfif>
					            </span>
					        </p>
					        <p>
					        	<img src="#application.vip_location#/images/vip-include-logo.jpg" width="194" height="37">
					        </p>
					    </div>
					    <div class="col-md-3 col-sm-3 btn-view-area btn-view-vip">
							<a class="btn btn-success btn-pink-view no-border btn-vipContent" href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
								View this <cfif image_set>Set<cfelse>Video</cfif>
							</a>
					    </div>
					</div>
	            </div>
	        </div>
	    </div>
	</cfif>
</cfoutput>