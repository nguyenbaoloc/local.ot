<cfoutput>
<cfif findNoCase('Android', cgi.http_user_agent,1) or findNoCase('iPhone', cgi.http_user_agent,1)>
	<cfset mobile = true/>
<cfelse>
	<cfset mobile = false/>
</cfif>
<div class="row clearfix today-release upcoming-release">
    <div class="col-md-4 col-sm-4 left"> 
        UPCOMING RELEASES
    </div>
    <div class="col-md-4 col-sm-4 right" id="upcoming_date"> 
        
    </div>
    <div class="col-md-4 col-sm-4 upcoming-landscape">
        <img class="float-img" src="/assets/img/img_comingsoon.png">
    </div>
</div>
<div class="row">
	<cfset Total_Records = rc.qry_ComingSoon.RecordCount>
	<cfif Total_Records gt 0 >
    	<div class="carousel slide upcomming" id="carousel-upcomingrelease">
    	<ul class="carousel-indicators indicators-carousel background-none">
        	<!--- convert to array to get release date --->
        	<cfset arr_date=arraynew(2)> 
			<!--- Populate the array row by row ---> 
			<cfloop query="rc.qry_ComingSoon"> 
			    <cfset arr_date[CurrentRow][1] = #rc.qry_ComingSoon.release_date_formarted_2#> 
			</cfloop>
        	<cfset counter = 1>
        	<cfset number_count = 1>

        	<cfif mobile>
        		<cfloop  index="Counter" from="1" to="#Total_Records gt 3 ? 3 : Total_Records#">
        			<cfif counter == 1>
	                    <li class="upcom-li active" data-slide-to="#number_count-1#" data-target="##carousel-upcomingrelease" order="#number_count#" r-date="#arr_date[Counter][1]#">#number_count#
	                	</li>
	                </cfif>
	                <cfif counter neq 3>
	                    <li data-slide-to="#number_count#" data-target="##carousel-upcomingrelease" order="#number_count+1#" r-date="#arr_date[Counter][1]#" class="upcom-li">#number_count+1#
	                	</li>
	                    <cfset number_count += 1>
	                </cfif>
                	<cfset counter += 1>
        		</cfloop>
        	<cfelse>
        		<cfloop index="Counter" from="1" to="#Total_Records#"> 
				    <cfif counter == 1>
	                    <li class="upcom-li active" data-slide-to="#number_count-1#" data-target="##carousel-upcomingrelease" order="#number_count#" r-date="#arr_date[Counter][1]#">#number_count#
	                	</li>
	                </cfif>
	            	<cfif counter%3 eq 0 && counter neq Total_Records>
	                    <li data-slide-to="#number_count#" data-target="##carousel-upcomingrelease" order="#number_count+1#" r-date="#arr_date[Counter][1]#" class="upcom-li">
	                    	#number_count+1#
	                	</li>
	                    <cfset number_count += 1>
	                </cfif>
	                <cfset counter += 1>
				</cfloop>
        	</cfif>
        </ul>
        <div class="carousel-inner">

        	<cfset counter_first_mobile = 1>
        	<cfset counter_second_mobile = 2>
        	<cfset Total_Records = (mobile eq true) ? 3 : Total_Records/>
        	<cfloop  index="Counter" from="1" to="#Total_Records#">
        		<cfif counter == 1>
                    <div class="item active">
    					<div class="row">
                </cfif>
				<cfif ListFind(application.video_set_types, rc.qry_ComingSoon.gallery_type[counter])>
					<cfset csimage_set = FALSE>
				<cfelse>
					<cfset csimage_set = TRUE>
				</cfif>
				
             	<div class="col-md-4 col-sm-12 col-xs-12 release-pic upcoming-col">
             		<div class="bg-block-upcom">
						<cfif rc.qry_ComingSoon.image_count[counter] eq 100>
							<div class="contentSpacer"></div><div class="bonusSet size-text"><strong>Bonus Set!</strong></div>
						</cfif>

						<cfif rc.qry_ComingSoon.image_count[counter] eq 69>
							<div class="contentSpacer"></div><div class="momSet size-text"><strong>Model of the Month!</strong></div>
						</cfif>

						<cfswitch expression="#rc.qry_ComingSoon.gallery_type[counter]#">
							<cfcase value=application.video_set_types>

							   <cfif rc.qry_ComingSoon.video_hd_res_name[counter] NEQ "" OR rc.qry_ComingSoon.video_mp4_hd_name[counter] NEQ "">
								<div style="margin:12px 0px 11px 0px;">
									<a href="/index.cfm/members/view_gallery?gallery_id=#qry_sets.gallery_id#">
										<img src="/images/video_icon_searchhd.gif" style="margin-top:35px; margin-bottom:15px;" border="0" width="145" height="82" alt="Click here to preview this video" title="Click here to preview this video">
									</a>
								</div>
								<cfelse>
									<div style="margin:12px 0px 11px 0px;">
										<a href="/index.cfm/members/view_gallery?gallery_id=#qry_sets.gallery_id#">
											<img src="/images/video_icon_search.gif" border="0" width="145" height="104" alt="Click here to preview this video" title="Click here to preview this video">
										</a>
									</div>
								</cfif>
							</cfcase>
						</cfswitch>


						<div class="row custom-min-height-upcomming no-margin-568 #!findNoCase('ipad', cgi.http_user_agent,1) ? 'desktop' : ''#">
							<div class="col-lg-12 col-md-12 col-xs-12">
								<a href="/index.cfm/members/view_gallery?gallery_id=#rc.qry_ComingSoon.gallery_id[counter]#" alt="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>" title="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>">
									<img src="/images/v2/tour_images/#replace(rc.qry_ComingSoon.tour_pic1_thumb[counter],'tn','tn260')#" width="260px" class="img-responsive"/>
								</a>
								<cfif ListFind(application.video_set_types,rc.qry_ComingSoon.gallery_type[counter])>
									<a href="/index.cfm/members/view_gallery?gallery_id=#rc.qry_ComingSoon.gallery_id[counter]#" alt="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>" title="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>" style="margin-top:15px;">
										<img src="/images/v2/tour_images/#replace(rc.qry_ComingSoon.tour_pic2_thumb[counter],'tn','tn260')#" width="260px" class="img-responsive"/>
									</a>
								</cfif>
							</div>
						</div>
						<cfif rc.qry_ComingSoon.video_clip[counter]>
							<br/><img class="desktop" src="/images/video_icon_sml.gif" align="absmiddle" width="22" height="21" alt=""> <strong>Plus behind the scenes<br>video available!</strong>
						</cfif>
						<div class="caption #!findNoCase('ipad', cgi.http_user_agent,1) ? 'desktop' : ''#" style="margin-left: 15px; margin-right: 15px;">
	                        <p class="upcoming-date-text #!findNoCase('ipad', cgi.http_user_agent,1) ? 'desktop' : ''#">#rc.qry_ComingSoon.release_date_formarted[counter]#</p>
	                        <p class="text-blue upcoming-text border-left-parent">
	                            <label class="model-name">#rc.modelService.getModelNamesByIndex(counter,rc.qry_ComingSoon)#</label>
	                            <cfif ListFind(application.video_set_types,rc.qry_ComingSoon.gallery_type[counter])>
			                          <span class="border-left-blue"><i class="fa fa-film"></i> #rc.qry_ComingSoon.video_length[counter]#</span>
								<cfelse>
									  <span class="border-left-blue"><i class="fa fa-camera"></i> #rc.qry_ComingSoon.image_count[counter]#</span>
								</cfif>
	                        </p>
	                        <p class="text-center">
	                            <a class="btn btn-success btn-blue-upcoming no-border" href="/index.cfm/members/view_gallery?gallery_id=#rc.qry_ComingSoon.gallery_id[counter]#" title="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>">PREVIEW <cfif csimage_set>SET<cfelse>VIDEO</cfif></a>
	                        </p>
	                    </div>



						<div class="mobile">

							<div class="col-xs-6 custom-min-height-upcomming no-margin-568">
								
								<cfif ListFind(application.video_set_types,rc.qry_ComingSoon.gallery_type[counter_first_mobile])>
									<div>
										<a href="/index.cfm/members/view_gallery?gallery_id=#rc.qry_ComingSoon.gallery_id[counter_first_mobile]#" alt="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>" title="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>">
											<img src="/images/v2/tour_images/#replace(rc.qry_ComingSoon.tour_pic1_thumb[counter_first_mobile],'tn','tn260')#" class="img-responsive"/>
										</a>
										<a href="/index.cfm/members/view_gallery?gallery_id=#rc.qry_ComingSoon.gallery_id[counter_second_mobile]#" alt="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>" title="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>" style="margin-top:5px;">
											<img src="/images/v2/tour_images/#replace(rc.qry_ComingSoon.tour_pic2_thumb[counter_second_mobile],'tn','tn260')#" class="img-responsive"/>
										</a>
									</div>
									<cfelse>
										<div>
											<a href="/index.cfm/members/view_gallery?gallery_id=#rc.qry_ComingSoon.gallery_id[counter_first_mobile]#" alt="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>" title="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>">
												<img src="/images/v2/tour_images/#replace(rc.qry_ComingSoon.tour_pic1_thumb[counter_first_mobile],'tn','tn260')#" width="260px" class="img-responsive"/>
											</a>
										</div>
								</cfif>
								<cfif rc.qry_ComingSoon.video_clip[counter_first_mobile]>
									<br/><img src="/images/video_icon_sml.gif" align="absmiddle" width="22" height="21" alt=""> <strong>Plus behind the scenes<br>video available!</strong>
								</cfif>
								<div class="caption0" style="">
			                        <p class="upcoming-date-text desktop">#rc.qry_ComingSoon.release_date_formarted[counter_first_mobile]#</p>
			                        <p class="text-blue upcoming-text border-left-parent">
			                            <label class="model-name">#rc.modelService.getModelNamesByIndex(counter_first_mobile,rc.qry_ComingSoon)#</label>
			                            <cfif ListFind(application.video_set_types,rc.qry_ComingSoon.gallery_type[counter_first_mobile])>
					                          <span class="border-left-blue"><i class="fa fa-film"></i> #rc.qry_ComingSoon.video_length[counter_first_mobile]#</span>
										<cfelse>
											  <span class="border-left-blue"><i class="fa fa-camera"></i> #rc.qry_ComingSoon.image_count[counter_first_mobile]#</span>
										</cfif>
			                        </p>
			                        <p class="text-center">
			                            <a class="btn btn-success btn-blue-upcoming no-border" href="/index.cfm/members/view_gallery?gallery_id=#rc.qry_ComingSoon.gallery_id[counter_first_mobile]#" title="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>">PREVIEW <cfif csimage_set>SET<cfelse>VIDEO</cfif></a>
			                        </p>
			                    </div>
							</div>

							<div class="col-xs-6 custom-min-height-upcomming no-margin-568">
								<cfif ListFind(application.video_set_types,rc.qry_ComingSoon.gallery_type[counter_second_mobile])>
									<div>
										<a href="/index.cfm/members/view_gallery?gallery_id=#rc.qry_ComingSoon.gallery_id[counter_second_mobile]#" alt="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>" title="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>">
											<img src="/images/v2/tour_images/#replace(rc.qry_ComingSoon.tour_pic1_thumb[counter_second_mobile],'tn','tn260')#" class="img-responsive"/>
										</a>
										<a href="/index.cfm/members/view_gallery?gallery_id=#rc.qry_ComingSoon.gallery_id[counter_second_mobile]#" alt="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>" title="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>" style="margin-top:5px;">
											<img src="/images/v2/tour_images/#replace(rc.qry_ComingSoon.tour_pic2_thumb[counter_second_mobile],'tn','tn260')#" class="img-responsive"/>
										</a>
									</div>
								<cfelse>
										<div>
											<a href="/index.cfm/members/view_gallery?gallery_id=#rc.qry_ComingSoon.gallery_id[counter_second_mobile]#" alt="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>" title="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>">
												<img src="/images/v2/tour_images/#replace(rc.qry_ComingSoon.tour_pic1_thumb[counter_second_mobile],'tn','tn260')#" width="260px" class="img-responsive"/>
											</a>
										</div>
								</cfif>
								<cfif rc.qry_ComingSoon.video_clip[counter_second_mobile]>
									<br/><img src="/images/video_icon_sml.gif" align="absmiddle" width="22" height="21" alt=""> <strong>Plus behind the scenes<br>video available!</strong>
								</cfif>
								<div class="caption" style="">
			                        <p class="upcoming-date-text desktop">#rc.qry_ComingSoon.release_date_formarted[counter_second_mobile]#</p>
			                        <p class="text-blue upcoming-text border-left-parent">
			                            <label class="model-name">#rc.modelService.getModelNamesByIndex(counter_second_mobile,rc.qry_ComingSoon)#</label>
			                            <cfif ListFind(application.video_set_types,rc.qry_ComingSoon.gallery_type[counter_second_mobile])>
					                          <span class="border-left-blue"><i class="fa fa-film"></i> #rc.qry_ComingSoon.video_length[counter_second_mobile]#</span>
										<cfelse>
											  <span class="border-left-blue"><i class="fa fa-camera"></i> #rc.qry_ComingSoon.image_count[counter_second_mobile]#</span>
										</cfif>
			                        </p>
			                        <p class="text-center">
			                            <a class="btn btn-success btn-blue-upcoming no-border" href="/index.cfm/members/view_gallery?gallery_id=#rc.qry_ComingSoon.gallery_id[counter_second_mobile]#" title="Click here to preview this <cfif csimage_set>set<cfelse>video</cfif>">PREVIEW <cfif csimage_set>SET<cfelse>VIDEO</cfif></a>
			                        </p>
			                    </div>
							</div>

						</div>
                    </div>
                </div>
                <cfif mobile>
            	 	<cfif counter neq Total_Records>
            	 		<cfset counter_first_mobile += 2>
						<cfset counter_second_mobile += 2>
	                    </div>
	                    </div>
                		<div class="item">
        					<div class="row">
	                </cfif>
	            <cfelse>
	            	<cfif counter%3 eq 0 && counter neq Total_Records>
	                    </div>
	                    </div>
                		<div class="item">
        					<div class="row">
	                </cfif>
                </cfif>
                <cfif counter eq Total_Records>
                    </div>
                    </div>
                </cfif>
                <cfset counter += 1>
        	</cfloop>
        </div>
        <div class="fullwidth-carousel">
            <a class="left carousel-control upcom-prev" href="##carousel-upcomingrelease" data-slide="prev">
                <span class="fa fa-angle-left fa-2x fa-border background-white color-black border-white"></span>
            </a>
            <a class="right carousel-control upcom-next" href="##carousel-upcomingrelease" data-slide="next">
                <span class="fa fa-angle-right fa-2x fa-border background-white color-black border-white"></span>
            </a>
        </div>
    	</div>
    <cfelse>
    	No Data to show !
	</cfif>
</div>
</cfoutput>