<cfoutput>
    <cfset qry_sets = rc.qry_sets/>
    <cfparam name="number_days_display" default="4">
    <cfset current_date = "">
    <cfset day_count = 1>
    <cfset next_gallery_count = 1>
    <cfset video_info = "">

    <div class="row clearfix today-release">
        <div class="col-md-9 col-sm-6 left"> 
            TODAYS RELEASES
        </div>
        <div class="col-md-3 col-sm-6 right">#qry_sets["release_date_formarted"][1]#</div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <cfif qry_sets.recordcount gt 0>
                <cfoutput query="qry_sets">
                    <cfif ((day_count LTE number_days_display) OR (current_date EQ qry_sets.release_date))>

                        <cfif ListFind(application.video_set_types, qry_sets.gallery_type)>
                            <cfset image_set = FALSE>
                        <cfelse>
                            <cfset image_set = TRUE>
                        </cfif>

                        <cfif current_date NEQ qry_sets.release_date>
                            <cfif day_count NEQ 1>
                                    <cfset data = #rc.UIHelperService.udf_dailyStrips(qry_sets.release_date[CurrentRow-1],createUUID(), rc.bol_allsites_member,rc.bol_allsites_memberdirect,rc.bol_tease_memberpage)#>
                                    #view('common/udf_dailyStrips', {data: data, image_set:image_set, showvipset: false})#
                            </cfif>
                            <cfset day_count = day_count + 1>
                            <cfset current_date = qry_sets.release_date>
                        </cfif>
                       
                        <div class="row marginTop">
                            <div class="row custom-min-height">
                                #view('common/images_set', {image_set: image_set, data: qry_sets})#
                            </div>
                        </div>
                        <div class="contentSpacer"></div>
                        <div class="<cfif image_set>row clearfix download-zone<cfelse>row clearfix view-video-zone-pink</cfif>">
                            <cfif qry_sets.gallery_type NEQ 21>
                                <cfif ListFind(application.video_set_types,qry_sets.gallery_type)>
                                     <cfset video_info = '<p>
                                                            <label class="model-name">#rc.modelService.getModelNames(qry_sets)#</label>
                                                            <span class="border-left">
                                                                <i class="fa fa-film"></i> Video length: #qry_sets.video_length# 
                                                            </span>
                                                            <span class="border-left desktop">#rc.UIHelperService.set_TextOfGalleryType(qry_sets.gallery_type)#
                                                            </span>
                                                         </p>'/>       
                                <cfelse>
                                    <cfset release_date = #qry_sets.release_date#>
                                    <cfset from_date = #createODBCDate(DateAdd("d",-30,now()))#>

                                    <cfif qry_sets.video_clip>
                                        <br/><img src="/img/icon_XL_Purple.png" align="absmiddle" alt=""> <label class="xl-size size-text">Plus behind the scenes video available!</label>
                                    </cfif>
                                </cfif>
                            </cfif>
                            <cfif qry_sets.gallery_type EQ 21>
                                <cfset #qry_sets.model_display_name# = Trim(ReplaceNoCase(model_display_name, " ", "-", "All"))>
                                <br>
                                <a href="#qry_sets.guest_link_url#" target="_new" alt="Click Here To Visit #qry_sets.model_display_name#" title="Click Here To Visit #qry_sets.model_display_name#">
                                    <img src="/images/guest_galleries/#qry_sets.model_display_name#_cover_#qry_sets.model_id#.jpg" border="0" width="260">
                                </a>
                            <cfelse>
                                #view('common/row_info_pink', {data: qry_sets, model_names: rc.modelService.getModelNames(qry_sets), UIHelperService: rc.UIHelperService, video_info:video_info,strServerZipLink: rc.strServerZipLink,strServerVideoLink: rc.strServerVideoLink })#
                            </cfif>
                        </div>
                        <cfset next_gallery_count = next_gallery_count + 1>
                    </cfif>
                </cfoutput>
            <cfelse>
                No Data to show !
            </cfif>
        </div>
    </div>
    <cfif qry_sets.recordcount gt 0>
        #view('members/homepages/viprelease',{pDate_release: qry_sets.release_date[next_gallery_count-1]})#
        <cfset data = rc.UIHelperService.udf_dailyStrips(qry_sets.release_date[next_gallery_count-1],createUUID(), rc.bol_allsites_member,rc.bol_allsites_memberdirect,rc.bol_tease_memberpage)>
        #view('common/udf_dailyStrips', {data: data, image_set:image_set, showvipset: false})#
    </cfif>
</cfoutput>