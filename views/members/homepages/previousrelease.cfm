<cfoutput>
<cfset qry_sets = data/>
<cfparam name="number_days_display"  default="4">
<cfset current_date = "">
<cfset day_count = 1>
<cfset next_gallery_count = 1>

<cfset id_carousel = createUUID()/>
<div class="row clearfix today-release yesterday-release">
    <div class="col-md-9 col-sm-8 left"> 
        #title#
    </div>
    <div class="col-md-3 col-sm-4 right">#qry_sets["release_date_formarted"][1]#</div>
</div>
<div class="row">
    <cfif  qry_sets.recordcount gt 0>
        <div class="carousel slide previousre" id="carousel-#id_carousel#">
            <ul class="carousel-indicators indicators-carousel background-none">
                <cfset rows = qry_sets.RecordCount>
                <cfset flag = 1>
                <cfset number_count = 1>
                <cfloop from="1" to="#rows#" index="number_index">
                    <li class="#(flag == 1) ? 'active' : ''#" data-slide-to="#flag-1#" data-target="##carousel-#id_carousel#">#flag#
                    </li>
                    <cfset flag += 1>
                </cfloop>
            </ul>
            <div class="carousel-inner">
                <cfset flag = 1>
                <cfoutput query="qry_sets">
                    <cfif flag eq 1>
                        <div class="item active">
                    </cfif>
                    <cfif ((day_count LTE number_days_display) OR (current_date EQ qry_sets.release_date))>
                        <cfif ListFind(application.video_set_types, qry_sets.gallery_type)>
                            <cfset image_set = FALSE>
                        <cfelse>
                            <cfset image_set = TRUE>

                        <cfif current_date NEQ qry_sets.release_date>
                            <cfif day_count NEQ 1>
                                    <cfset data = rc.UIHelperService.udf_dailyStrips(qry_sets.release_date[CurrentRow-1],createUUID(), rc.bol_allsites_member,rc.bol_allsites_memberdirect,rc.bol_tease_memberpage)>
                                    #view('common/udf_dailyStrips', {data: data, showvipset: true})#
                            </cfif>
                            <cfset day_count = day_count + 1>
                            <cfset current_date = qry_sets.release_date>
                        </cfif>
                    </cfif>
                    
                    <div class="no-margin-568" style="margin-top:10px;">
                        <div class="row custom-min-height">
                            #view('common/images_set', {image_set: image_set, data: qry_sets})#                            
                        </div>
                    </div>
                    <div class="<cfif image_set>row clearfix download-zone download-zone-blue<cfelse>row clearfix view-video-zone-blue</cfif>" style="margin-left:auto;">
                        <cfif qry_sets.gallery_type NEQ 21>
                            <cfif ListFind(application.video_set_types,qry_sets.gallery_type)>
                                <cfset video_info = '<p>
                                        <label class="model-name">#rc.modelService.getModelNames(qry_sets)#</label><span class="border-blue"><i class="fa fa-film"></i> Video length: #qry_sets.video_length#</span><span class="border-blue"></span>
                                    </p>'/>
                            <cfelse>
                                <cfset release_date = #qry_sets.release_date#>
                                <cfset from_date = #createODBCDate(DateAdd("d",-30,now()))#>

                                <cfif qry_sets.video_clip>
                                    <br/><img src="/img/icon_XL_Blue.png" align="absmiddle" alt=""> <label class="xl-size">Plus behind the scenes video available!</label>
                                </cfif>
                            </cfif>
                        </cfif>
                        <cfif qry_sets.gallery_type EQ 21>
                            <cfset #qry_sets.model_display_name# = Trim(ReplaceNoCase(model_display_name, " ", "-", "All"))>
                            <br>
                            <a href="#qry_sets.guest_link_url#" target="_new" alt="Click Here To Visit #qry_sets.model_display_name#" title="Click Here To Visit #qry_sets.model_display_name#">
                                <img src="/images/guest_galleries/#qry_sets.model_display_name#_cover_#qry_sets.model_id#.jpg" border="0" width="260" height="120">
                            </a>
                        <cfelse>
                            #view('common/row_info_blue', {data: qry_sets, model_names: rc.modelService.getModelNames(qry_sets), UIHelperService: rc.UIHelperService, video_info:video_info,strServerZipLink: rc.strServerZipLink,strServerVideoLink: rc.strServerVideoLink })#
                        </cfif>
                    </div>
                    <cfset next_gallery_count = next_gallery_count + 1>
                    </cfif>
                    <!--- close and then open div item --->
                    <cfif flag neq qry_sets.recordcount>
                        </div>
                        <div class="item">
                    </cfif>
                    <!--- close last div item --->
                    <cfif flag eq qry_sets.recordcount>
                            </div>
                            <div class="fullwidth-carousel">
                                <a class="left carousel-control" href="##carousel-#id_carousel#" data-slide="prev">
                                    <span class="fa fa-angle-left fa-2x fa-border background-white color-black border-white"></span>
                                </a>
                                <a class="right carousel-control" href="##carousel-#id_carousel#" data-slide="next">
                                    <span class="fa fa-angle-right fa-2x fa-border background-white color-black border-white"></span>
                                </a>
                            </div>
                    </cfif>
                    <cfset flag += 1>
                </cfoutput>
            </div>
        </div>
    <cfelse>
        No Data to show
    </cfif>
</div>
    <cfif  qry_sets.recordcount gt 0>
        <cfset data = rc.UIHelperService.udf_dailyStrips(qry_sets.release_date[next_gallery_count-1],createUUID(), rc.bol_allsites_member,rc.bol_allsites_memberdirect,rc.bol_tease_memberpage)>
        #view('common/udf_dailyStrips', {data: data, showvipset: true})#
    </cfif>
</cfoutput>