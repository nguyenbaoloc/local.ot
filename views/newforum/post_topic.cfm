<cfoutput>
	<div class="block-note">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<ul class="list-unstyled pull-right left-note">
				<li>
					<a class="text-link-blue" href="http://members.onlytease.com/members/new_forum/preferences.cfm">
						Forum Preferences
					</a>
				</li>
				<cfif (rc.qry_fasttracks.recordcount GT 0 AND NOT (session.website EQ "oc_users" OR session.website EQ "om_users"))>
					<li>
						<a href="http://members.onlytease.com/members/new_forum/ft_notice.cfm">
							You have Fasttracks to Allocate !
						</a>
					</li>
				</cfif>
			</ul>
		</div>
	</div>
	<div class="modulForm-newTopic">
		<h3 class="text-blue title-blue">New topic in '#rc.get_forum.ForumName#'</h3>	
		<div class="col-md-8 col-sm-8 col-xs-8">
			<form action="/index.cfm/newforum.post_topic?ForumID=#url.ForumID#" method="post" onsubmit="return notEmpty(document.getElementById('Subject'), 'Please enter a Subject for this topic')">
				<input type="Hidden" name="Author" value="#rc.qry_checkPreferences.forum_nickname#">
				<input type="Hidden" name="Email" value="#rc.qry_checkPreferences.forum_email#">
			  	<div class="form-group">
			    	<label>Posting as</label>
			    	<p>
			    		#rc.qry_checkPreferences.forum_nickname# (#rc.qry_checkPreferences.forum_email#)
			    	</p>
			  	</div>
			  	<div class="form-group">
				    <label for="exampleInputEmail1">Subject</label>
				    <input type="input" class="form-control" placeholder="" name="Subject" id="Subject" value="#form.Subject#">
			  	</div>
			  	<div class="form-group">
				    <label for="exampleInputEmail1">Comment</label>
				    <cfparam name="Body" default="">
				    <textarea id="post_topic_comment" class="form-control" rows="3" name="Body">#form.Body#</textarea>
			  	</div>
			  	<cfif rc.qry_checkPreferences.forum_edit_all EQ 1>
					<cfif url.ForumID EQ 1 OR url.ForumID EQ 3 OR url.ForumID EQ 11 OR url.ForumID EQ 5>
			  			<div class="form-group">
						    <label for="exampleInputEmail1">Do you want this post to go on the public forum?</label>
						    <div class="radio">
							    <label class="radio-inline">
								    <input type="radio" name="DoublePost" value="1"  <cfif url.ForumID EQ 3 OR url.ForumID EQ 5>checked="Yes"</cfif>>Yes
							  	</label>
							  	<label class="radio-inline">
								    <input type="radio" name="DoublePost" value="0" <cfif url.ForumID EQ 1 OR url.ForumID EQ 11>checked="Yes"</cfif> >No
							  	</label>
							</div>
			  			</div>
			  		</cfif>
			  	</cfif>
			  	<div class="form-group">
				    <label for="exampleInputEmail1">Would you like to recieve replies to this topic at #rc.qry_checkPreferences.forum_email# ?</label>
				    <div class="radio">
					    <label class="radio-inline">
						    <input type="radio"  name="Reply" value="1" <cfif rc.Reply eq 1> checked</cfif> >Yes
					  	</label>
					  	<label class="radio-inline">
						    <input type="radio"  name="Reply" value="0" <cfif rc.Reply eq 0> checked</cfif> >No
					  	</label>
					</div>
			  	</div>
			  	<div class="form-inline pull-right">
			  		<div class="form-group">
				  		<input type="submit" name="Button" value="Preview Topic" class="btn btn-success btn-blue-view no-border btn-forum">
						<input type="submit" name="Button" value="Submit Topic" class="btn btn-success btn-blue-view no-border btn-forum">
						<input type="Button" name="Button" value="Cancel Topic" class="btn btn-success btn-blue-view no-border btn-forum" onclick="window.location='/index.cfm/newforum.topics?ForumID=#url.ForumID#';">
				  	</div>
			  	</div>			
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4">
			<div class="textNewTopic-pink">Add a poll?</div>
				<cfif rc.pollError EQ 1>
					<div class="form-group">
						<label for="exampleInputEmail1">
							Please make sure you have filled in at least two 'poll options' and a 'poll question'.<br>
							If you do not wish to make a poll, please remove any information from the poll fields.
						</label>
				  	</div>	
				</cfif>
				<div class="form-group">
					<cfparam name="poll_question" default="">
				    <label for="exampleInputEmail1">Poll question</label>
				    <input type="input" class="form-control" placeholder="" name="poll_question" value="#form.poll_question#">
			  	</div>	
			  	<div class="form-group post-topic-poll-options-container">
				    <label for="exampleInputEmail1">Poll options</label>
					<cfloop from="1" to="#rc.int_answers#" index="rg">
						<cfparam name="form.poll_answer_#rg#" default="">
						<input type="text" class="post-topic-poll-options form-control marginTop <cfif evaluate("form.poll_answer_#rg#") EQ "" and rg neq 1>hide</cfif>" placeholder="" name="poll_answer_#rg#" value="#evaluate("form.poll_answer_#rg#")#">
					</cfloop>
			  	</div>
			  	<div class="form-group pull-right">
				    <a class="btn btn-success btn-pink-view no-border" href="" id="post_topic_addmore">+ Add Another Option</a>
			  	</div>	
			</form>	
		</div>
	</div>
</cfoutput>