<cfoutput>
	<div class="row block-note">
		<div class="col-md-8 col-sm-8 col-xs-8 pull-left">
			<p class="note">
				<Strong>NOTE:</strong> You can earn 1 extra FastTrack for every 10 forum posts made. 
				<a class="text-link-blue" href="http://members.onlytease.com/members/new_forum/help.cfm?type=forumFT">More info here</a>
			</p>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4">
			<ul class="list-unstyled pull-right left-note">
				<li>
					<cfif #rc.get_forums.RecordCount# gt 0>
						#rc.get_forums.RecordCount#
					<cfelse>
						0
					</cfif> Forums Available
				</li>
				<li>
					<a class="text-link-blue" href="http://members.onlytease.com/members/new_forum/preferences.cfm">
						Forum Preferences
					</a>
				</li>
				<cfif (rc.qry_fasttracks.recordcount GT 0 AND NOT (session.website EQ "oc_users" OR session.website EQ "om_users"))>
					<li>
						<a href="http://members.onlytease.com/members/new_forum/ft_notice.cfm">
							You have Fasttracks to Allocate !
						</a>
					</li>
				</cfif>
			</ul>
		</div>
	</div>
	<div class="block-mainForum">
		<cfif rc.get_forums.RecordCount neq 0>
			<cfset forum_grouping = 0>
			<cfloop query="rc.get_forums">
				<cfif rc.get_forums.ImportID NEQ forum_grouping>
					<cfset forum_grouping = rc.get_forums.ImportID>
					<cfif forum_grouping GT 1>
								</tbody>
							</table>
						</div>	
					</cfif>
					<div class="table-responsive">
						<table id="generalTable" class="table table-striped allTable generalTable">
							<thead>
					            <tr>
					                <th class="textBold-pink col-md-7">#rc.get_forums.f_title#</th>
					                <th class="col-md-2 text-center">Topics</th>
					                <th class="col-md-2 text-center">Posts</th>
					                <th class="col-md-1 text-center">Last Post</th>
					            </tr>
					        </thead>	
					        <tbody>
				</cfif>
				<cfset qry_threads = rc.NewForumService.qry_threads(application.last_post_date, rc.get_forums.ForumID,rc.user_details.showpublicposts)>
				<cfif qry_threads.recordcount GT 0>
					<cfset new_posts = TRUE>
				<cfelse>
					<cfset new_posts = FALSE>
				</cfif>

				<cfset Posts = rc.NewForumService.Posts(rc.get_forums.ForumID)>

				<cfset Topics = rc.NewForumService.Topics(rc.get_forums.ForumID)>

				<cfset LastPost1 = rc.NewForumService.LastPost1(rc.get_forums.ForumID)>

				<tr>
	                <td class="col-md-7">
	                	<div class="box-td">
	                		<h5>
	                			<a class="textBold-blue" href="/index.cfm/newforum.topics?ForumID=#rc.get_forums.ForumID#">
	                				#rc.get_forums.ForumName#
	                				<cfif new_posts>
	                					 <i class="fa fa-calendar-o"></i>
									</cfif>
	                			</a>
	                			<cfif rc.get_forums.ImportID EQ '6' OR rc.get_forums.ImportID EQ '7' OR rc.get_forums.ImportID EQ '8' >
                           	 		<button type="button" class="btn btn-primary label-publicForum">PUBLIC FORUM</button>
                                </cfif>
	                		</h5>
	                		<p>#rc.get_forums.ForumDescription#</p>
	                	</div>
	                </td>
	                <td class="col-md-2 text-center">
	                	<div class="box-td">
	                		<div class="text-td-blue">#NumberFormat(Topics.TopicCount,",")#</div>
	                	</div>	
	                </td>
	                <td class="col-md-2 text-center">
	                	<div class="box-td">
	                		<div class="text-td-blue">#NumberFormat(Posts.PostsCount,",")#</div>
	                	</div>	
	                </td>
	                <td class="col-md-1 text-center">
	                	<div class="box-td">
	                		<div class="text-td-blue">
	                			<cfif LastPost1.recordcount NEQ 0>
	                				#DateFormat("#LastPost1.DateIn#", "mmmm d, yyyy")# @ #TimeFormat("#LastPost1.DateIn#", 'h:mm tt')#
	                			</cfif>
	                		</div>
	                	</div>	
	                </td>
	            </tr>
			</cfloop>
					</tbody>
				</table>
			</div>	
		<cfelse>
			<div class="table-responsive">
				<table id="generalTable" class="table table-striped allTable generalTable">
					<tr>
						<td>There are currently no Forums available.</td>
					</tr>
				</table>
			</div>
		</cfif>
	</div>
	<div class="block-indicates">
		<div class="box-indicates">
			<span class="text-link-blue"><i class="fa fa-calendar-o"></i></span>Indicates new forum topics or replies posted since #DateFormat(application.last_post_date,"mmm d, yyyy")# #TimeFormat(application.last_post_date,"hh:mm:ss tt")#
		</div>
	</div>
</cfoutput>