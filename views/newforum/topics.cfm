<cfoutput>
	<div class="row block-note desktop">
		<div class="col-md-8 col-sm-8 col-xs-8 pull-left">
			<p class="note">
				<Strong>NOTE:</strong> You can earn 1 extra FastTrack for every 10 forum posts made. <a class="text-link-blue" href="http://members.onlytease.com/members/new_forum/help.cfm?type=forumFT">More info here</a>
			</p>
			<cfif url.ForumID EQ 5>
				<p>
					<a href="/index.cfm/comingsoon">
						Click here
					</a>
					<strong> to view ALL Forthcoming Shoots</strong>
				</p>
			</cfif>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4">
			<ul class="list-unstyled left-note pull-right">
				<li>#rc.get_topiccount.count# Active Topics</li>
				<cfif url.ForumID EQ 5>
					<li>
						<a href="http://members.onlytease.com/members/forthcoming_shoots.cfm"><strong>Click here</a> to view ALL Forthcoming Shoots</strong>
					</li>
				</cfif>
				<li><a class="text-link-blue" href="http://members.onlytease.com/members/new_forum/preferences.cfm">Forum Preferences</a></li>
				<cfif (rc.qry_fasttracks.recordcount GT 0 AND NOT (session.website EQ "oc_users" OR session.website EQ "om_users"))>
					<li>
						<a href="http://members.onlytease.com/members/new_forum/ft_notice.cfm">
							You have Fasttracks to Allocate !
						</a>
					</li>
				</cfif>
			</ul>
		</div>
	</div>

	<div class="row block-addTopic-paging">
		<div class="col-md-3 col-sm-3 col-xs-3 pull-left parent-left-note">
			<ul class="list-unstyled left-note">
				<li><a class="text-link-blue" href="/index.cfm/newforum.post_topic?ForumID=#url.ForumID#">New Topic</a></li>
				<li><a class="text-link-blue last-child" href="">Search</a></li>
			</ul>
		</div>
		<div class="col-md-9 col-sm-9 col-xs-9 pull-right">
			<div class="pull-right block-paging ">
				<div class="block-time-day">
					<select name="range" class="time-pastDay" onChange="if (options[selectedIndex].value) { location = options[selectedIndex].value }"> 
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=1&CurrentPage=#url.CurrentPage#"<cfif Range eq 1> selected</cfif>>past day
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=2&CurrentPage=#url.CurrentPage#"<cfif Range eq 2> selected</cfif>>past 2 days
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=7&CurrentPage=#url.CurrentPage#"<cfif Range eq 7> selected</cfif>>past 7 days
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=14&CurrentPage=#url.CurrentPage#"<cfif Range eq 14> selected</cfif>>past 14 days
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=30&CurrentPage=#url.CurrentPage#"<cfif Range eq 30> selected</cfif>>past 30 days
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=60&CurrentPage=#url.CurrentPage#"<cfif Range eq 60> selected</cfif>>past 60 days
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=180&CurrentPage=#url.CurrentPage#"<cfif Range eq 180> selected</cfif>>past 6 months
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=365&CurrentPage=#url.CurrentPage#"<cfif Range eq 365> selected</cfif>>past year
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=0&CurrentPage=#url.CurrentPage#"<cfif Range eq 0> selected</cfif>>Everything
					</select>
				</div>
				<!--- paging --->
				#rc.TopicsService.paging(rc.paging_config)#
				<!--- end paging --->
			</div>
		</div>
	</div>

	<div class="row block-topicForum">
		<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
			<table id="GeneralChatTable" class="table table-striped allTable GeneralChatTable">
				<thead>
		            <tr>
		                <th class="textBold-blue col-md-8">#rc.get_forum.ForumName#</th>
		                <th class="col-md-1 text-center">Author</th>
		                <th class="col-md-1 text-center">Replies</th>
		                <th class="col-md-1 text-center">Views</th>
		                <th class="col-md-1 text-center">Last Post</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<cfif rc.get_topics.RecordCount NEQ 0>
						<cfloop query="rc.get_topics">
							<!--- check to see if the user has posted in this forum. --->
							<cfif rc.get_topics.user_posted GT 0>
								<cfset posted = TRUE>
							<cfelse>
								<cfset posted = FALSE>
							</cfif>
							<cfif rc.get_topics.Sticky EQ 1>
								<cfset sticky = TRUE>
							<cfelse>	
								<cfset sticky = FALSE>
							</cfif>

							<cfset LastPost1 = rc.TopicsService.LastPost1(url.ForumID, rc.get_topics.ThreadID)>
							<tr>
								<cfset total_post_count = rc.get_topics.reply_count + 1>
				                <td class="col-md-8">
				                	<div class="box-td">
				                		<div class="text-title">
				                			<cfset label_count1 = 1>
		                                    <cfset position_count = 1>
											<cfloop condition="position_count LTE total_post_count">
													<cfset label_count1 = label_count1 + 1>
													<cfset position_count = position_count + 30>
											</cfloop>
		                                    
		                                    <cfset label_count1 = label_count1 - 1>
		                                 	<cfif total_post_count GT 30>
		                                		<a href="/index.cfm/newforum.read_thread?CurrentPage=#label_count1#&ForumID=#rc.get_topics.ForumID#&ThreadID=#rc.get_topics.ThreadID#&Thread=#rc.get_topics.Thread#">
		                                			#rc.get_topics.Subject#
		                                		</a>
		                                		<cfif rc.get_topics.LastPost GT application.last_post_date><i class="fa fa-calendar-o"></i></cfif>
												<cfif sticky><i class="fa fa-thumb-tack"></i></cfif>
	                                			<cfif posted><i class="fa fa-check-square"></i></cfif>
												<cfif rc.get_topics.PollQuestionID NEQ 0 AND rc.get_topics.PollQuestionID NEQ "">
													<i class="fa fa-list"></i>
												</cfif>
		                                		<cfset label_count1 = label_count1 + 1>
			                                <cfelse>
												<a href="/index.cfm/newforum.read_thread?CurrentPage=1&ForumID=#rc.get_topics.ForumID#&ThreadID=#rc.get_topics.ThreadID#&Thread=#rc.get_topics.Thread#">
													#rc.get_topics.Subject#

												</a>
												<cfif rc.get_topics.LastPost GT application.last_post_date><i class="fa fa-calendar-o"></i></cfif>
												<cfif sticky><i class="fa fa-thumb-tack"></i></cfif>
	                                			<cfif posted><i class="fa fa-check-square"></i></cfif>
												<cfif rc.get_topics.PollQuestionID NEQ 0 AND rc.get_topics.PollQuestionID NEQ "">
													<i class="fa fa-list"></i>
												</cfif>
											</cfif>

		                                	<cfif total_post_count GT 30>
												<cfset position_count = 1>
												<cfset label_count = 1>
												<ul class="list-unstyled pagination pagination--defaut-custom">
		                							<li><span>Jump to page:</span></li>
													<cfloop condition="position_count LTE total_post_count">
														<li>
															<a href="/index.cfm/newforum.read_thread?CurrentPage=#label_count#&ForumID=#rc.get_topics.ForumID#&ThreadID=#rc.get_topics.ThreadID#&Thread=#rc.get_topics.Thread#" title="view page #label_count# of this thread">
																#label_count#
															</a>
														</li>
														<cfset label_count = label_count + 1>
														<cfset position_count = position_count + 30>
													</cfloop>
												</ul>
											</cfif>
		                                    <cfset label_count1 = label_count1 - 1>
			                			</div>
				                	</div>
				                </td>
				                <td class="col-md-1 text-center">
				                	<div class="box-td">
				                		<div>
				                			<cfif rc.get_topics.dynamic_nick EQ 0 OR rc.get_topics.dynamic_nick EQ "">
												#rc.get_topics.Author#
											<cfelse>
												<cfif rc.get_topics.fut_icon NEQ "">
													<span class="icon-pink"><i class="fa fa-user-plus"></i></span>
												</cfif>
												#rc.get_topics.forum_nickname#
											</cfif>
				                		</div>
				                	</div>	
				                </td>
				                <td class="col-md-1 text-center">
				                	<div class="box-td">
				                		<div class="">#NumberFormat(rc.get_topics.reply_count,",")#</div>
				                	</div>	
				                </td>
				                <td class="col-md-1 text-center">
				                	<div class="box-td">
				                		<div class="">#NumberFormat(rc.get_topics.Views,",")#</div>
				                	</div>	
				                </td>
				                <td class="col-md-1 text-center">
				                	<div class="box-td">
				                		<div class="">
				                			<cfif LastPost1.recordcount EQ 0>
				                				#DateFormat("#rc.get_topics.LastPost#", "mmm d, yyyy")# @ #TimeFormat("#rc.get_topics.LastPost#", 'h:mm tt')#
			                				<cfelse>
			                					#DateFormat("#LastPost1.DateIn#", "mmm d, yyyy")# @ #TimeFormat("#LastPost1.DateIn#", 'h:mm tt')#
			                				</cfif>
				                		</div>
				                	</div>	
				                </td>
				            </tr>
						</cfloop>
					<cfelse>
						<tr>
							<td class="col-md-12 text-center">
			                	<div class="box-td">
			                		There are currently no Topics available
			                	</div>	
			                </td>
						</tr>
					</cfif>
		        </tbody>
			</table>
		</div>	
	</div>

	<div class="row block-addTopic-paging">
		<div class="col-md-3 col-sm-3 col-xs-3 pull-left parent-left-note">
			<ul class="list-unstyled left-note">
				<li><a class="text-link-blue" href="/index.cfm/newforum.post_topic?ForumID=#url.ForumID#">Add New Topic</a></li>
				<li><a class="text-link-blue last-child" href="">Search</a></li>
			</ul>
		</div>
		<div class="col-md-9 col-sm-9 col-xs-9 pull-right">
			<div class="pull-right block-paging ">
				<div class="block-time-day">
					<select name="range" class="time-pastDay" onChange="if (options[selectedIndex].value) { location = options[selectedIndex].value }"> 
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=1&CurrentPage=#url.CurrentPage#"<cfif Range eq 1> selected</cfif>>past day
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=2&CurrentPage=#url.CurrentPage#"<cfif Range eq 2> selected</cfif>>past 2 days
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=7&CurrentPage=#url.CurrentPage#"<cfif Range eq 7> selected</cfif>>past 7 days
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=14&CurrentPage=#url.CurrentPage#"<cfif Range eq 14> selected</cfif>>past 14 days
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=30&CurrentPage=#url.CurrentPage#"<cfif Range eq 30> selected</cfif>>past 30 days
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=60&CurrentPage=#url.CurrentPage#"<cfif Range eq 60> selected</cfif>>past 60 days
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=180&CurrentPage=#url.CurrentPage#"<cfif Range eq 180> selected</cfif>>past 6 months
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=365&CurrentPage=#url.CurrentPage#"<cfif Range eq 365> selected</cfif>>past year
						<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#&Range=0&CurrentPage=#url.CurrentPage#"<cfif Range eq 0> selected</cfif>>Everything
					</select>
				</div>
				<!--- paging --->
				#rc.TopicsService.paging(rc.paging_config)#
				<!--- end paging --->
			</div>
		</div>
	</div>

	<div class="row block-note-ft desktop">
		<div class="col-md-6 col-sm-6 col-xs-6 pull-left">
			<p><i class="fa fa-calendar-o"></i>Indicates new forum topics or replies posted since 29/07/2015 03:41 PM</p>
			<p><i class="fa fa-check-square"></i>Indicates you have posted in this forum topic.</p>
			<p><i class="fa fa-list"></i>Indicates this topic includes a poll.</p>
			<p><i class="fa fa-thumb-tack"></i>Indicates that this topic has been pinned.</p>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<div class="pull-right block-see-other">
				<form>
					<select name="sites" class="see-other-forum" onChange="if (options[selectedIndex].value) { location = options[selectedIndex].value }">
						<option value="">See Another Forum...</option>
						<cfloop query="rc.get_forums">
							<option value="/index.cfm/newforum.topics?ForumID=#url.ForumID#">#rc.get_forums.ForumName#</option>
						</cfloop>
					</select>
				</form>
			</div>
		</div>
	</div>
</cfoutput>
