<cfoutput>
	<div class="row">
		<div class="col-md-12 col-xs-12">
			Sorry, at present we have stopped you posting new posts or replies to the forum.
			<br><br>
			If you would like to be able to post again, please email <a href="mailto:support@OnlyTease.com">support@OnlyTease.com</a>
			and provide us with your login username.<br><br>
			Many thanks<br><br>
			<strong>Support Dept<br>
			Onlytease.com</strong>
		</div>
	</div>
</cfoutput>