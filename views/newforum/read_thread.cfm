<cfoutput>
	<div class="row block-note">
		<div class="col-md-4 col-sm-4 col-xs-4 pull-left parent-left-note" id="top">
			<ul class="list-unstyled left-note">
				<li><a class="text-link-blue" href="/index.cfm/newforum.post_topic?ForumID=#url.ForumID#">Add New Topic</a></li>
				<li><a class="text-link-blue last-child" href="">Search #DateFormat(now(), "short")# </a></li>
			</ul>
		</div>
		<div class="col-md-8 col-sm-8 col-xs-8">
			<ul class="list-unstyled left-note pull-right">
				<li>
					<cfif #rc.get_forums.RecordCount# gt 0>
						#rc.get_forums.RecordCount#
					<cfelse>
						0
					</cfif> Forums Available
				</li>
				<li>
					<a class="text-link-blue" href="http://members.onlytease.com/members/new_forum/preferences.cfm">
						Forum Preferences
					</a>
				</li>
				<cfif (rc.qry_fasttracks.recordcount GT 0 AND NOT (session.website EQ "oc_users" OR session.website EQ "om_users"))>
					<li>
						<a href="http://members.onlytease.com/members/new_forum/ft_notice.cfm">
							You have Fasttracks to Allocate !
						</a>
					</li>
				</cfif>
			</ul>
		</div>
	</div>
	<div class="row block-addTopic-paging">
		<div class="col-md-12 col-sm-12 col-xs-12 pull-right">
			<div class="pull-right block-paging ">
				#rc.TopicsService.paging(rc.paging_config)#
			</div>
		</div>
	</div>
	<div class="row block-titleForm">			
	    <div class="textBold-blue col-md-9">#rc.get_topic.Subject#
	    	<cfif rc.get_forum.ImportID EQ '6' OR rc.get_forum.ImportID EQ '7' OR rc.get_forum.ImportID EQ '8' >
				<br><span  class="pull-left">This is a Public Thread</span>
			</cfif>
		</div>
	    <div class="col-md-3 text-right text-create">
	    	Created: #rc.get_topic.DateIn_formarted# @ #TimeFormat(rc.get_topic.DateIn, "h:mm tt")# by 
	    	<cfif rc.get_topic.dynamic_nick EQ 0 OR rc.get_topic.dynamic_nick EQ "">
				#rc.get_topic.Author#
			<cfelse>
				<cfif rc.get_topic.fut_icon NEQ ""><span class="icon-pink"><i class="fa fa-user-plus"></i></span></cfif> 
				#rc.get_topic.forum_nickname#
			</cfif>
	    </div>
	</div>	
	<div class="row block-viewTopicTable">
		<div class="table-responsive">
			<table id="viewTopicTable" class="table table-striped allTable viewTopicTable">
		        <tbody>
		        	<cfif rc.get_thread.recordcount EQ 0>
		        		<tr>
							<td class="col-md-12 text-center">
			                	<div class="box-td">
			                		There are currently no Topics available
			                	</div>	
			                </td>
						</tr>
		        	<cfelse>
		        		<cfif rc.get_topic.thread_suspended EQ 1>
							<cfset suspended = TRUE>
						<cfelse>
							<cfset suspended = FALSE>
						</cfif>
						<cfloop query="rc.get_thread" startRow="#rc.paging_config.START_ROW#" endRow="#rc.paging_config.END_ROW#">
							<tr>
				            	<td class="col-left">
				            		<div class="left-table">
				            			<cfif rc.get_thread.dynamic_nick EQ 0 OR rc.get_thread.dynamic_nick EQ "">
				            				<div class="form-group">
					            				<label class="lable-name">#rc.get_thread.Author#</label>
					            			</div>
										<cfelse>
											<div class="form-group">
												<cfif rc.get_thread.avatar NEQ "">
					            					<p class="pic-avatar"><a href=""><img src="http://forums.onlytease.com/avatars/#rc.get_thread.avatar#"></a></p>
					            				</cfif>
					            				<label class="lable-name">
					            					<cfif rc.get_topic.fut_icon NEQ ""><span class="icon-pink"><i class="fa fa-user-plus"></i></span></cfif> 
													#rc.get_topic.forum_nickname#
												</label>
					            			</div>
											<cfif rc.qry_forumeditall.dynamic_nick EQ 1>
						            			<cfif rc.get_thread.title NEQ "">
						            				<div class="form-group">
							            				<label class="lable-name center">rc.get_thread.title</label>
							            			</div>
						            			</cfif>
						            			<cfif rc.get_thread.location NEQ "">
							            			<div class="form-group info-group">
							            				<p class="location">
							            					<label class="label-location">Location:</label>
							            					<span>#rc.get_thread.location#</span>
														</p>
							            			</div>
						            			</cfif>
						            			<cfif rc.get_thread.age NEQ "">
							            			<div class="form-group info-group">
							            				<p class="info">
							            					<label class="label-location">Age:</label> 
							            					<span>#rc.get_thread.age#</span>
														</p>
							            			</div>
							            		</cfif>
							            		<cfif rc.get_thread.total_all_posts_count GT 0>
							            			<div class="form-group info-group">
							            				<p class="info">
							            					<label class="label-location">No. of posts:</label> 
							            					<span>#rc.get_thread.total_all_posts_count#</span>
														</p>
							            			</div>
						            			</cfif>
						            			<cfif rc.get_thread.fp_first_login_date NEQ "">
													<cfset first_login_date = rc.get_thread.fp_first_login_date_formarted>
													<cfif first_login_date NEQ "01/01/2008">
								            			<div class="form-group info-group">
								            				<p class="info">
								            					<label class="label-location">Member Since:</label> 
								            					<span>#rc.get_thread.fp_first_login_date_formarted#</span>
															</p>
								            			</div>
							            			</cfif>
						            			</cfif>
											</cfif>
											<cfif rc.forum_edit_all>
												<div class="form-group info-group">
						            				<p class="info">
						            					<label class="label-location">Username:</label> 
						            					<span>#rc.get_thread.login_username#</span>
													</p>
						            			</div>
											</cfif>

											<div class="form-group info-group">
					            				<p class="info">
					            					<label class="label-location">Posted on:</label> 
					            					<span>#rc.get_thread.posted_on#</span>
												</p>
					            			</div>
											
											<cfif (rc.forum_edit_all AND rc.get_thread.posted_on EQ "Public Forum")>
												<div class="form-group info-group">
						            				<p class="info">
						            					<label class="label-location">Email:</label> 
						            					<span><a href="mailto:#rc.get_thread.forum_email#">#rc.get_thread.forum_email#</a></span>
													</p>
						            			</div>
											</cfif>
										</cfif>
				            		</div>
				            	</td>
				            	<td class="col-right">
				            		<div class="row right-table">
				            			<div class="col-md-6 col-sm-6 col-xs-6">
				            				<p class="pull-left paddingLeft">#rc.get_thread.DateIn_formarted# @ #TimeFormat("#rc.get_thread.DateIn#", 'h:mm tt')#</p>
				            			</div>
				            			<div class="col-md-6 col-sm-6 col-xs-6">
				            				<ul class="list-unstyled pull-right action-topic">
				            					<cfif suspended>
													<li><strong>Topic Ended</strong></li>
												<cfelse>
													<cfif rc.forum_edit_all>
					            						<li><a href="#application.AppPath#admin.cfm?ForumID=#url.ForumID#&ThreadID=#rc.get_thread.ThreadID#&Thread=#url.Thread#&Parent=#rc.get_thread.Parent#&admin_flag=1">Delete Thread</a></li>
						            					<li><a href="#application.AppPath#admin.cfm?ForumID=#url.ForumID#&ThreadID=#rc.get_thread.ThreadID#&Thread=#url.Thread#&Parent=#rc.get_thread.Parent#&admin_flag=2">Delete Post</a></li>
						            					<cfif rc.get_thread.Parent EQ "0">
						            						<cfif url.ForumID EQ 1 OR url.ForumID EQ 3 OR url.ForumID EQ 11 OR url.ForumID EQ 5>
														 		<li><a href="#application.AppPath#admin.cfm?ForumID=#url.ForumID#&ThreadID=#rc.get_thread.ThreadID#&Thread=#url.Thread#&Parent=#rc.get_thread.Parent#&admin_flag=3">Copy to public forums</a></li>

																&nbsp;&nbsp;|&nbsp;&nbsp;
														 	</cfif>
															 <li><a href="#application.AppPath#admin.cfm?ForumID=#url.ForumID#&ThreadID=#rc.get_thread.ThreadID#&Thread=#url.Thread#&Parent=#rc.get_thread.Parent#&admin_flag=5">Move Thread</a></li>
						            					</cfif>
					            					</cfif>
					            					<cfif (rc.get_thread.login_username eq cgi.remote_user OR rc.forum_edit_all)>
					            						<li><a href="http://members.onlytase.com/members/new_forum/edit_thread.cfm?ForumID=#url.ForumID#&ThreadID=#rc.get_thread.ThreadID#&Thread=#url.Thread#">Edit</a></li>
					            					</cfif>
					            					<li><a href="http://members.onlytase.com/members/new_forum/post_reply.cfm?ForumID=#url.ForumID#&CurrentPage=#rc.paging_config.END_ROW#&ThreadID=#url.ThreadID#&Thread=#url.Thread#">Reply</a></li>
					            				</cfif>
					            				<li><a href="javascript:printTopic('http://members.onlytase.com/members/new_forum/print_topic.cfm?ForumID=#url.ForumID#&ThreadID=#url.ThreadID#&Thread=#url.Thread#','print','640','400');">Print</a></li>
				            					<li><a href="##top">Top</a></li>
				            				</ul>
				            			</div>
				            		</div>
				            		<div class="content-detail">
				            			#rc.get_thread.Body#
				            			<br><br>
				            			<cfif rc.get_thread.EditCount GT 0>
											Lasted edited #rc.get_thread.LastEdit_formarted# @ #TimeFormat(rc.get_thread.LastEdit,"h:mm tt")#. Edited #rc.get_thread.EditCount# time<cfif rc.get_thread.EditCount GT 1>s</cfif>
										</cfif>
				            		</div>
				            	</td>
				            </tr>
						</cfloop>
		        	</cfif>
		        </tbody>
			</table>
		</div>
	</div>
</cfoutput>