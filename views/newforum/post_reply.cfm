<cfoutput>
	<div class="block-note">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<ul class="list-unstyled pull-right left-note">
				<li>
					<a class="text-link-blue" href="http://members.onlytease.com/members/new_forum/preferences.cfm">
						Forum Preferences
					</a>
				</li>
				<cfif (rc.qry_fasttracks.recordcount GT 0 AND NOT (session.website EQ "oc_users" OR session.website EQ "om_users"))>
					<li>
						<a href="http://members.onlytease.com/members/new_forum/ft_notice.cfm">
							You have Fasttracks to Allocate !
						</a>
					</li>
				</cfif>
			</ul>
		</div>
	</div>
	<div class="modulForm-newTopic">
		<h3 class="text-blue title-blue">Reply to topic '#rc.get_forum.ForumName#'</h3>	
		<div class="col-md-12 col-sm-12 col-xs-12">
			<cfif #rc.Button# is "Preview Reply">
				<cfif get_forum.AllowHTML is "N">
					<cfset preview_body = #REReplaceNoCase("#form.Body#", "<[^>]*>", "", "all")#>
				<cfelse>
					<cfset preview_body = #trim(form.Body)#>
					<cfset preview_body = #ReplaceNoCase(preview_body, Chr(10), "<br>", "ALL")#>
					<cfset preview_body = #ReplaceNoCase(preview_body, Chr(9), "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "ALL")#>
					<cfset preview_body = #ReplaceNoCase(preview_body, "  ", "&nbsp;&nbsp;", "ALL")#>
				</cfif>
				<h3 class="text-blue title-blue">New Topic Preview</h3>
				<div class="form-group">
				    <label for="exampleInputEmail1">Subject</label>
				    <div>#rc.Subject#</div>
			  	</div>
			  	<div class="form-group">
				    <label for="exampleInputEmail1">#rc.qry_checkPreferences.forum_nickname#</label>
				    <div>#DateFormat(now(), "mmmm d, yyyy")# @ #TimeFormat(now(), 'h:mm tt')#</div>
			  	</div>
			</cfif>
			<cfif rc.bolImageError>
				<div class="form-group">
				    <label for="exampleInputEmail1">
				    	Your post contains references to files/images within the members area of one of our sites - this can cause security and log in issues for other members.<br>
						You must remove these references to the files/images before you will be allowed to submit your post.
					</label>
			  	</div>
			</cfif>
			<cfif rc.bolInlineImageError>
				<div class="form-group">
				    <label for="exampleInputEmail1">
				    	You appear to be trying to post an image using a method that is not allowed.  You must remove this image before you will be able to post your reply.<br/><br/>
						You should only be using a free image hosting solution and then linking to that image using the &quot;insert an image&quot; tool in the editor toolbar.
					</label>
			  	</div>
			</cfif>
			<form action="/index.cfm/newforum.post_reply?ForumID=#url.ForumID#&CurrentPage=#url.CurrentPage#&ThreadID=#url.ThreadID#&Thread=#url.Thread#" method="post" onsubmit="return formCheck()">
				<input type="Hidden" name="Author" value="#rc.qry_checkPreferences.forum_nickname#">
				<input type="Hidden" name="Email" value="#rc.qry_checkPreferences.forum_email#">
				<input type="Hidden" name="Subject" value="#RTrim(rc.Subject)#">
				<cfparam name="Body" default="">

				<div class="form-group">
				    <label for="exampleInputEmail1">Posting as</label>
			    	<p>
			    		#rc.qry_checkPreferences.forum_nickname# (#rc.qry_checkPreferences.forum_email#)
			    	</p>
			  	</div>
			  	<div class="form-group">
				    <label for="exampleInputEmail1">Subject</label>
				    <p>
			    		#rc.Subject#
			    	</p>
			  	</div>
			  	<div class="form-group">
				    <label for="exampleInputEmail1">Comment</label>
				    <cfparam name="Body" default="">
				    <textarea id="reply_topic_comment" class="form-control" rows="3" name="Body">#form.Body#</textarea>
			  	</div>
			  	<div class="form-inline pull-right">
			  		<div class="form-group">
				  		<input type="submit" name="Button" value="Preview Topic" class="btn btn-success btn-blue-view no-border btn-forum">
						<input type="submit" name="Button" value="Submit Topic" class="btn btn-success btn-blue-view no-border btn-forum">
						<input type="Button" name="Button" value="Cancel Topic" class="btn btn-success btn-blue-view no-border btn-forum" onclick="window.location='/index.cfm/newforum.topics?ForumID=#url.ForumID#';">
				  	</div>
			  	</div>	
			</form>
		</div>
	</div>
</cfoutput>