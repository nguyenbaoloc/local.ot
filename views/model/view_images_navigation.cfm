<cfoutput>
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-6 setsizeButton">
			<div class="col-md-3 col-sm-3 col-xs-3 blockpic right">
				<cfif rc.last_link NEQ "">
					<a href="#rc.last_link#" class="lastlink">
						<img src="/assets/img/lastlink.png" class="img-responsive"/>
					</a>
				<cfelse>
					<img src="/assets/img/lastlink-grey.png" class="img-responsive"/>
				</cfif>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3 blockpic right">
				<cfif rc.next_link NEQ "">
					<a href="#rc.next_link#" class="nextlink">
						<img src="/assets/img/nextlink.png" class="img-responsive"/>
					</a>
				<cfelse>
					<img src="/assets/img/nextlink-grey.png" class="img-responsive"/>
				</cfif>	
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3 blockpic right">
				<cfif rc.previous_link NEQ "">
					<a href="#rc.previous_link#" class="prevlink">
						<img src="/assets/img/prevlink.png" class="img-responsive"/>
					</a>
				<cfelse>
					<img src="/assets/img/prevlink-grey.png" class="img-responsive"/>
				</cfif>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3 blockpic right">
				<cfif rc.first_link NEQ "">
					<a href="#rc.first_link#" class="firstlink">
						<img src="/assets/img/firstlink.png" class="img-responsive"/>
					</a>
				<cfelse>
					<img src="/assets/img/firstlink-grey.png" class="img-responsive"/>
				</cfif>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6 setsizeButton left">
			<div class="col-md-3 col-sm-3 col-xs-3 blockpic">
				<cfif rc.qry_imageDetails.gallery_type NEQ 21 AND rc.qry_imageDetails.zipfile NEQ "">
					<a href="#rc.normal_url##rc.current_image#">
						<img src="/assets/img/normal.png" class="img-responsive"/>
					</a>
				<cfelse>
					<img src="/assets/img/normal-grey.png" class="img-responsive"/>
				</cfif>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3 blockpic">
				<cfif rc.qry_imageDetails.gallery_type NEQ 21 AND rc.qry_imageDetails.lrg_zipfile NEQ "">
					<a href="#rc.large_url##rc.current_image#">
						<img src="/assets/img/large.png" class="img-responsive"/>
					</a>
				<cfelse>
					<img src="/assets/img/large-grey.png" class="img-responsive" />
				</cfif>
			</div> 
			<div class="col-md-3 col-sm-3 col-xs-3 blockpic">
				<cfif rc.qry_imageDetails.gallery_type NEQ 21 AND rc.qry_imageDetails.ultra_zipfile NEQ "">
					<a href="#rc.ultra_url##rc.current_image#">
						<img src="/assets/img/ultra.png" class="img-responsive"/>
					</a>
				<cfelse>
					<img src="/assets/img/ultra-grey.png" class="img-responsive" />
				</cfif>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3 blockpic">
				<cfif rc.qry_imageDetails.gallery_type NEQ 21 AND rc.qry_imageDetails.super_zipfile NEQ "">
					<a href="#rc.super_url##rc.current_image#">
						<img src="/assets/img/super.png" class="img-responsive"/>
					</a>
				<cfelse>
					<img src="/assets/img/super-grey.png" class="img-responsive" />
				</cfif>
			</div>
		</div>
	</div>
</cfoutput>