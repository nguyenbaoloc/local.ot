<cfoutput>
<cfset video_info = "">
<div class="row">
    <div class="col-md-12 col-sm-12">
        <cfif ListFind(application.video_set_types, data.gallery_type)>
            <cfset image_set = FALSE>
        <cfelse>
            <cfset image_set = TRUE>
        </cfif>
       
        <div class="row bg-vipContent marginTop">
            <div class="row custom-min-height">
                #view('common/images_vip_set', {image_set: image_set, data: data})#
            </div>
        </div>
        <div class="row clearfix download-zone download-vip">
            <cfif data.gallery_type NEQ 21>
                <cfif NOT image_set>
                     <cfset video_info = '<p>
                                             <label class="model-name">#rc.UtilService.udf_vipModelNames(data.gallery_id)#</label><span class="border-left"><i class="fa fa-film"></i> Video length: #data.video_length# </span><span class="border-left desktop">#rc.UIHelperService.set_TextOfGalleryType(data.gallery_type)#</span>
                                         </p>'/>       
                <cfelse>
                    <cfset release_date = #data.release_date#>
                    <cfset from_date = #createODBCDate(DateAdd("d",-30,now()))#>

                    <cfif data.video_clip>
                        <br/><img src="/img/icon_XL_Purple.png" align="absmiddle" alt=""> <label class="xl-size size-text">Plus behind the scenes video available!</label>
                    </cfif>
                </cfif>
            </cfif>
            <cfif data.gallery_type EQ 21>
                <cfset #data.model_display_name# = Trim(ReplaceNoCase(model_display_name, " ", "-", "All"))>
                <br><a href="#data.guest_link_url#" target="_new" alt="Click Here To Visit #data.model_display_name#" title="Click Here To Visit #data.model_display_name#"><img src="/images/guest_galleries/#data.model_display_name#_cover_#data.model_id#.jpg" border="0" width="260"></a>
            <cfelse>
                #view('common/row_vip_info', {data: data, model_names: rc.UtilService.udf_vipModelNames(data.gallery_id)})#
            </cfif>
        </div>
    </div>
</div>
</cfoutput>