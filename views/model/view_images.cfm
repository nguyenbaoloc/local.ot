<cfoutput>
	<cfinclude template="view_images_navigation.cfm">

	<div class="row picSize-view-images">
		<cfif IsDefined("url.image_dir")>
			<cfset image_dir = url.image_dir>
		</cfif>

		<cfif rc.qry_imageDetails.gallery_id GT application.gallery_change>

			<cfif image_dir EQ 'images'>
				<cfset rc.image_replace = rc.standard_image_replace>
			<cfelseif image_dir EQ 'images2'>
				<cfset rc.image_replace = rc.large_image_replace>
			<cfelseif image_dir EQ 'images3'>
				<cfset rc.image_replace = rc.ultra_image_replace>
			<cfelseif image_dir EQ 'images4'>
				<cfset rc.image_replace = rc.super_image_replace>
			</cfif>

			<cfif rc.qry_imageDetails.gallery_type EQ 21>
				<a href="#rc.qry_imageDetails.guest_link_url#" target="_new">
					<img src="http://members.onlytease.com/members/#rc.sets_locat#/#rc.qry_imageDetails.directory_name#/#image_dir#/#replaceNoCase(rc.qry_imageDetails.filename,"tn#rc.thumb_image_replace#","#rc.image_replace#")#">
				</a>
			<cfelse>
				<img src="http://members.onlytease.com/members/#rc.sets_locat#/#rc.qry_imageDetails.directory_name#/#image_dir#/#replaceNoCase(rc.qry_imageDetails.filename,"tn#rc.thumb_image_replace#","#rc.image_replace#")#">
			</cfif>
		<cfelse>
			<img src="<cfif (cs_use AND rc.qry_imageDetails.directory_name LTE cs_max_directory AND rc.qry_imageDetails.directory_name DOES NOT CONTAIN "a")>#cs_url#</cfif>/members/#rc.sets_locat#/#rc.qry_imageDetails.directory_name#/#image_dir#/#replaceNoCase(rc.qry_imageDetails.filename,"tn","")#">

		</cfif>
	</div>

	<cfinclude template="view_images_navigation.cfm">
</cfoutput>