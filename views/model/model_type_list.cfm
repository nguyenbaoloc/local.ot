<cfoutput>
<div class="block_lastUpdates">
	<cfif color eq "pink">
		<div class="row clearfix today-release model-search mobile">
			<div class="col-md-9 col-sm-9 left model-search">OT MODELS</div>
			<div class="col-md-3 col-sm-3 right"></div>
		</div>
		<div class="row mobile">
			<form action="/index.cfm/model" method="get" class="search-form">
				<input type="hidden" name="type" value="model">
				<input type="Hidden" name="search" value="TRUE">
				<input type="text" name="search_text" class="search-models" place>
				<button type="submit" name="submit" value="Search" class=""><i class="fa fa-search"></i></button>
			</form>	
			<form action="" class="form-search-letter">
				<button type="button" name="submit" value="Search" class="navbar-toggle collapsed btn-search-letter" data-toggle="collapse" data-target="##box-search-letter" aria-expanded="false" aria-controls="box-search-letter">Search model by first letter<i class="fa fa-chevron-down"></i></button>
				<div id="box-search-letter" class="box-search-alphabet navbar-collapse collapse">
					<p class="">
						<cfloop from="65" to="73" index="this_letter">
							<cfif ListContains(rc.letter_list, "#Chr(this_letter)#")>
								<span><a href="/index.cfm/model?letter=#Chr(this_letter)#">#Chr(this_letter)#</a></span>
							<cfelse>
								<span class="letter-model">#Chr(this_letter)#</span>
							</cfif>
						</cfloop>
					</p>
					<p class="">
						<cfloop from="74" to="82" index="this_letter">
							<cfif ListContains(rc.letter_list, "#Chr(this_letter)#")>
								<span><a href="/index.cfm/model?letter=#Chr(this_letter)#">#Chr(this_letter)#</a></span>
							<cfelse>
								<span class="letter-model">#Chr(this_letter)#</span>
							</cfif>
						</cfloop>
					</p>
					<p class="">
						<cfloop from="83" to="90" index="this_letter">
							<cfif ListContains(rc.letter_list, "#Chr(this_letter)#")>
								<span><a href="/index.cfm/model?letter=#Chr(this_letter)#">#Chr(this_letter)#</a></span>
							<cfelse>
								<span class="letter-model">#Chr(this_letter)#</span>
							</cfif>
						</cfloop>
					</p>
				</div>
			</form>
		</div>
	</cfif>
	<div class="row clearfix today-release #color eq 'blue' ? 'yesterday-release' : ''#">
		<div class="col-md-9 col-sm-9 left"><label class="desktop" style="font-weight: normal;">OT MODELS - </label>#title#</div>
		<div class="col-md-3 col-sm-3 right"></div>
	</div>
	<div class="row">
		<div class="row custom-min-height">
			<cfset counter = 1/>
			<cfloop query="#data#" item="item">
				<cfset counter += 1>
				<div class="col-md-4 col-sm-4 col-xs-6 release-pic release-pic-models #counter gt data.recordCount? '#!findNoCase('ipad', cgi.http_user_agent,1) ? 'desktop' : ''#':''#">
					<div class="width-img">
						<a href="/index.cfm/model.model_set?model_id=#data.model_id#">
							<img width="260px" class="img-responsive" 
							src="http://members.onlytease.com/tour/images/models/260x390#replace(data.model_shortcut," ","-","ALL")#_cover_#data.model_id#.jpg" title="#data.model_display_name#" >

						</a>
					</div>	
					<div class="caption caption-updates">
						<p class="text-name model-border-left-#color#"><a class="text-#color#" href="/index.cfm/model.model_set?model_id=#data.model_id#">#data.model_display_name#</a></p>
						<p class="text-#color# #color#-info-model desktop">
							<span class="border-left-#color#-model"><i class="fa fa-list-ul"></i>#NumberFormat(data.gallery_count,"9,999")#</span>
							<span class="border-left-#color#-model"><i class="fa fa-camera"></i>#NumberFormat(data.image_count,"9,999")#</span>
							<span class="border-left-#color#-model"><i class="fa fa-video-camera"></i>#NumberFormat(data.video_count,"9,999")#</span>	
						</p>
						<p class="text-center">
							<a class="btn #color eq 'blue' ? 'btn-blue-model' : 'btn-pink-model'# no-border" href="/index.cfm/model.model_set?model_id=#data.model_id#">VIEW MODEL</a>	
						</p>
					</div>
				</div>
			</cfloop>
		</div>
	</div>
	<div class="bg-white"></div>
</div>
</cfoutput>