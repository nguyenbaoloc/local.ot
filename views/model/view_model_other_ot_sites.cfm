<cfoutput>
	<cfset arr_length = ArrayLen(rc.arr_otherModelSets)>
	<cfif rc.bol_displaySets and arr_length NEQ 0>
		<cfset stData = rc.UIHelperService.setDailyStripForModelSet(rc.arr_otherModelSets)>
		<div class="row clearfix daily-strip">
	        <div class="col-md-12 column" style="padding: 0;">
	            <div class="tabbable">
	            	<ul class="nav nav-tabs daily-strip-tap-menu desktop daily-tabmenu-desktop">
	                    <cfset list_nametab = ""/>
	                    <cfloop from="1" to="#arr_length#" index="int_site_index">
	                        <cfset nametab = rc.arr_otherModelSets[int_site_index].str_standsforsite>
	                        <li class="col-md-#int(12/arr_length)# #nametab# tab-action" style="padding:0px;">
	                            <a href="##tab-#nametab#" data-toggle="tab" class="#(int_site_index eq 1) ? 'tab-active' : ''#">#rc.arr_otherModelSets[int_site_index].STR_SITE#</a>
	                        </li>
	                        <cfif nametab neq "OO">
	                            <cfset list_nametab &= "#nametab#,"/>
	                        </cfif>
	                    </cfloop>
	                </ul>
	                <ul class="nav nav-tabs daily-strip-tap-menu mobile daily-tabmenu-ipad">
	                    <cfloop from="1" to="#arr_length#" index="int_site_index">
	                        <cfset nametab = rc.arr_otherModelSets[int_site_index].str_standsforsite>
	                        <li class="col-xs-#int(12/arr_length)# #nametab# tab-action" style="padding:0px;">
	                            <a href="##tab-#nametab#" data-toggle="tab" class="#(int_site_index eq 1) ? 'tab-active' : ''#">#nametab#</a>
	                        </li>
	                    </cfloop>
	                </ul>

	                <div class="tab-content dailystrip-content">
	                    <cfloop from="1" to="#arr_length#" index="int_site_index">
	                        <cfset str_standsforsite = rc.arr_otherModelSets[int_site_index].str_standsforsite/>
	                        <div class="tab-pane #(int_site_index == 1) ? 'active first-tab-pane' : ''#" id='tab-#str_standsforsite#'>
	                            <div class="row">
	                                <div class="col-md-12 col-md-offset-2 dailystrip-children-row"><div class="tab-release-date">SETS OF #rc.qry_modelData.model_display_name# ON OUR OTHER SITES</div></div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-12 col-md-offset-2 bxwapper-model dailystrip-children-row">
	                                        <ul class="set-#str_standsforsite# #(int_site_index == 1) ? 'first-tab-content' : ''#">
	                                            <cfswitch expression="#rc.arr_otherModelSets[int_site_index].str_standsforsite#">
	                                                <cfcase value="OO">
	                                                	<li>
                                                        	<a href="#stData.logo_OO.link#">
                                                        		<img alt="" src="#replace(stData.logo_OO.image,'tn','tn120')#" class="dailystrip-sample-image">
                                                        	</a>
                                                    	</li>
	                                                    <cfloop from="1" to="#arraylen(stData.arr_OO)#" index="item">
	                                                    	<li>
	                                                        	<a href="#stData.arr_OO[item].link#">
	                                                        		<img alt="" src="#replace(stData.arr_OO[item].image,'tn','tn120')#" class="dailystrip-sample-image">
	                                                        		<cfif ListFind(application.video_set_types, stData.arr_OO[item].gallery_type)>
	                                                        			<div class="box-pic-height">
	                                                        				<p class="box-pic-icon">
	                                                        					<i class="fa fa-film"></i>
	                                                        				</p>
	                                                        				<p class="box-pic-text">Video</p>
	                                                        			</div>
	                                                        		</cfif>
	                                                        	</a>
	                                                    	</li>
	                                                    	<cfif item % 3 eq 0 and item neq arraylen(stData.arr_OO)>
	                                                    		<li>
		                                                        	<a href="#stData.logo_OO.link#">
		                                                        		<img alt="" src="#replace(stData.logo_OO.image,'tn','tn120')#" class="dailystrip-sample-image">
		                                                        	</a>
		                                                    	</li>
	                                                    	</cfif>
	                                                    </cfloop>
	                                                </cfcase>
	                                                <cfcase value="OS">
	                                                	<li>
                                                        	<a href="#stData.logo_OS.link#">
                                                        		<img alt="" src="#replace(stData.logo_OS.image,'tn','tn120')#" class="dailystrip-sample-image">
                                                        	</a>
                                                    	</li>
	                                                    <cfloop from="1" to="#arraylen(stData.arr_OS)#" index="item">
	                                                       <li>
		                                                       	<a href="#stData.arr_OS[item].link#">
		                                                       		<img alt="" src="#replace(stData.arr_OS[item].image,'tn','tn120')#" class="dailystrip-sample-image">
		                                                       		<cfif ListFind(application.video_set_types, stData.arr_OS[item].gallery_type)>
		                                                       			<div class="box-pic-height">
		                                                       				<p class="box-pic-icon">
		                                                       					<i class="fa fa-film"></i>
		                                                       				</p>
		                                                       				<p class="box-pic-text">Video</p>
		                                                       			</div>
		                                                       		</cfif>
		                                                       	</a>
		                                                   	</li>
		                                                   	<cfif item % 3 eq 0 and item neq arraylen(stData.arr_OS)>
	                                                    		<li>
		                                                        	<a href="#stData.logo_OS.link#">
		                                                        		<img alt="" src="#replace(stData.logo_OS.image,'tn','tn120')#" class="dailystrip-sample-image">
		                                                        	</a>
		                                                    	</li>
	                                                    	</cfif>
	                                                    </cfloop>
	                                                </cfcase>
	                                                <cfcase value="OSS">
	                                                	<li>
                                                        	<a href="#stData.logo_OSS.link#">
                                                        		<img alt="" src="#replace(stData.logo_OSS.image,'tn','tn120')#" class="dailystrip-sample-image">
                                                        	</a>
                                                    	</li>
	                                                    <cfloop from="1" to="#arraylen(stData.arr_OSS)#" index="item">
	                                                        <li>
	                                                        	<a href="#stData.arr_OSS[item].link#">
	                                                        		<img alt="" src="#replace(stData.arr_OSS[item].image,'tn','tn120')#" class="dailystrip-sample-image">
	                                                        		<cfif ListFind(application.video_set_types, stData.arr_OSS[item].gallery_type)>
	                                                        			<div class="box-pic-height">
		                                                       				<p class="box-pic-icon">
		                                                       					<i class="fa fa-film"></i>
		                                                       				</p>
		                                                       				<p class="box-pic-text">Video</p>
		                                                       			</div>
	                                                        		</cfif>
	                                                        	</a>
	                                                    	</li>
	                                                    	<cfif item % 3 eq 0 and item neq arraylen(stData.arr_OSS)>
	                                                    		<li>
		                                                        	<a href="#stData.logo_OSS.link#">
		                                                        		<img alt="" src="#replace(stData.logo_OSS.image,'tn','tn120')#" class="dailystrip-sample-image">
		                                                        	</a>
		                                                    	</li>
	                                                    	</cfif>
	                                                    </cfloop>
	                                                </cfcase>
	                                            </cfswitch>
	                                        </ul>
	                                </div>
	                            </div>
	                        </div>
	                        <!--- generate slider script --->
	                        <cfif str_standsforsite neq "OO">
	                            <script type="text/javascript">
	                                jQuery(document).ready(function($) {
	                                    var maxslides = 4;
	                                    if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	                                        maxslides = 2;
	                                    }
	                                    $('a[href="##tab-#str_standsforsite#"]').one('shown.bs.tab', function (e) {
	                                        $('.set-#str_standsforsite#').bxSlider({
	                                            infiniteLoop: false,
	                                            hideControlOnEnd: true,
	                                            pager: false,
	                                            minSlides: 1,
	                                            maxSlides: maxslides,
	                                            slideWidth: 125,
	                                            slideMargin: 10
	                                        });
	                                    });

	                                    $('a[href="##tab-OS"]').css("color","##662E91");
	                                    $('a[href="##tab-OSS"]').css("color","##9CBA62");
	                                    $('.set-OS > li').each(function() {
                        	                $(this).css("border","1px solid ##662E91");
	                                    });
	                                    $('.set-OSS > li').each(function() {
                        	                $(this).css("border","1px solid ##9CBA62");
	                                    });
	                                });
	                            </script>
	                        </cfif>
	                    </cfloop>
	                </div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
	        jQuery(document).ready(function($) {

	            var maxslides = 4;
	            if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	                maxslides = 2;
	            }

	            $('.first-tab-content').bxSlider({
	                infiniteLoop: false,
	                hideControlOnEnd: true,
	                pager: false,
	                minSlides: 1,
	                maxSlides: maxslides,
	                slideWidth: 125,
	                slideMargin: 10
	            });

	            $('##tab-OS').attr("color","##662E91");
                $('##tab-OSS').attr("color","##9CBA62");
	            $('##tab-OO').attr("color","##EE008E");
	            $('.set-OO > li').each(function() {
	                $(this).css("border","1px solid ##EE008E");
                });
	            $('a[href="##tab-OO"]').css("color","##EE008E");
             	$('.tab-content').css("border-bottom-color",$('.first-tab-pane').attr("color"));
	        });
	    </script>
	</cfif>
</cfoutput>

