<cfoutput>
	<div class="mainContainer">	
		<cfset color = "pink">
		<cfif isDefined("rc.qry_latest_updates") AND rc.qry_latest_updates.RecordCount GT 0>
			<cfset title = "LATEST UPDATES">
			#view('model/model_type_list',{data:rc.qry_latest_updates, title:"#title#", color:"#color#"})#
		</cfif>	
		<cfif rc.search>
			<cfset title = "SEARCH RESULTS FOR : #search_text#">
		<cfelseif rc.letter NEQ "">
			<cfset title = "NAMES STARTING WITH #rc.letter#">
		<cfelse>
			<cfset title = "RANDOM SELECTION">
			<cfset color = "blue">
		</cfif>		
		#view('model/model_type_list',{data:rc.qry_models, title:"#title#", color:"#color#"})#		 
	</div>
</cfoutput>