<cfoutput>
	<table width="600" cellpadding="0" cellspacing="0" border="0" align="center">
		<tr>
			<td align="center">
				<br><br>
				<cfif isDefined("session.pref_id")>
					<strong>THANK YOU
					<cfif rc.qry_user.forum_nickname NEQ ""><cfoutput>#UCase(rc.qry_user.forum_nickname)#</cfoutput>
					<cfelse><cfoutput>#UCase(rc.qry_user.pref_username)#</cfoutput>
					</cfif>
					</strong><br><br>

					Your vote has been registered and recorded.<br><br>
					You will only be able to vote once per gallery, but we do ask you to vote on as many galleries as possible, as it helps
					us decide what models and outfits members like and dislike. You can change your vote for this gallery at any time.
					<br><br>
				<cfelse>
					<strong>THANK YOU</strong><br><br>
					Your vote has been registered. You will only be able to vote once per
					gallery, but we do ask you to vote on as many galleries as possible, as it helps
					us decide what models and outfits members like and dislike. You can change your vote for this gallery at any time.
					<br><br>
				</cfif>

				<cfif rc.qry_content.total_votes_cast_model GTE number_minimum_votes>
					<strong><u>Voting results for this Set</u></strong><br><br>
					<cfoutput>
						<strong>Model:</strong>&nbsp;#LSNumberFormat(rc.qry_content.average_model, "____.__")#
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<strong>Outfit:</strong>&nbsp;#LSNumberFormat(rc.qry_content.average_outfit, "____.__")#
						<cfif rc.qry_content.total_votes_cast_tease GT number_minimum_votes>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<strong>Tease Factor:</strong>&nbsp;#LSNumberFormat(rc.qry_content.average_tease, "____.__")#
						</cfif>
					</cfoutput>
				<cfelse>
					<br>This sets voting results will be shown once a minimum number of votes have been registered.
				</cfif>
				<br><br>
				<div class="VoteViewSet">
					<div class="VoteViewSetButton">
						<a href="<cfoutput>/members/view_gallery.cfm?gallery_id=#rc.gallery_id#&CurrentPage=#rc.CurrentPage#</cfoutput>">Back to Gallery</a>
					</div>
				</div>
				<br><br><br><br><br><br>
			</td>
		</tr>
	</table>
</cfoutput>