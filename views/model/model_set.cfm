<cfoutput>
	<cfif rc.qrymodel.recordcount gt 0>
		<cfif rc.qrygalleries.recordcount gt 0>
			<div class="row clearfix becky-sets">
				<div class="col-md-3 col-sm-3 col-xs-7 left">
					#rc.qrymodel.model_display_name#'S SET<cfif rc.qrygalleries.RecordCount GT 1>S</cfif>
				</div>
				<div class="col-md-9 col-sm-9 right desktop">
			    	#rc.UIHelperService.paging(rc.paging_config,'ul')#
				</div>
				<div class="col-md-3 col-sm-6 col-xs-5 right mobile" style="padding-right: 0px;">
					#rc.UIHelperService.paging(rc.paging_config,'dd')#
				</div>
			</div>

			<form class="form-inline filterForm">
				<input type="Hidden" value="#model_id#" id="hdModelId">
				<div class="form-group form-select-gallery mobile">
					<label class="col-xs-6">Model Filter:</label>
					<select id="galTypeSelect" class="col-xs-6 filterSelect">
						<option value="a" <cfif gType EQ "a">selected</cfif>>ALL SETS</option>
						<option value="v" <cfif gType EQ "v">selected</cfif>>VIDEO SET SONLY</option>
						<option value="f" <cfif gType EQ "f">selected</cfif>>FAST TRACK SETS ONLY</option>
						<option value="p" <cfif gType EQ "p">selected</cfif>>PICTURE SETS ONLY</option>
					</select>
				</div>
			</form>

			<div class="row clearfix becky-radio-group desktop">
				<div class="form-radio">
			      	<div class="radio">
			      		<label>
			      			<input type="radio" name="galTypeRadio" onClick="mSearchFilter('#model_id#','a')"<cfif gType EQ "a"> checked</cfif>> ALL SETS
			      		</label>
				    </div>   
				</div>
				<div class="form-radio">
			      	<div class="radio">
			      		<label>
			      			<input type="radio" name="galTypeRadio" onClick="mSearchFilter('#model_id#','v')"<cfif gType EQ "v"> checked</cfif>> VIDEO SET SONLY
			      		</label>
				    </div>   
				</div>
				<div class="form-radio">
			      	<div class="radio">
			      		<label>
			      			<input type="radio" name="galTypeRadio" onClick="mSearchFilter('#model_id#','f')"<cfif gType EQ "f"> checked</cfif>> FAST TRACK SETS ONLY
			      		</label>
				    </div>   
				</div>
				<div class="form-radio">
			      	<div class="radio">
			      		<label>
			      			<input type="radio" name="galTypeRadio" onClick="mSearchFilter('#model_id#','p')"<cfif gType EQ "p"> checked</cfif>> PICTURE SETS ONLY
			      		</label>
				    </div>   
				</div>
			</div>

			<cfloop query="rc.qrygalleries" startrow="#rc.display_start_row#" maxrows="12">
				<cfif rc.qrygalleries.gallery_release EQ 0>
					<cfif NOT rc.qrygalleries.vip_set>
						<div class="row clearfix becky-image-sets">
							<div class="right">
								#rc.qrygalleries.release_date_formarted#	
							</div>
						</div>
						#view('model/releasedset', {data: rc.qrygalleries})#
					<cfelse>
						<cfif NOT IsDefined("current_date_vip") OR rc.qrygalleries.release_date NEQ current_date_vip>
							<div class="row clearfix becky-image-sets">
								<div class="right">
									#rc.qrygalleries.release_date_formarted#	
								</div>
							</div>
							<cfset current_date_vip = rc.qrygalleries.release_date>
						</cfif>
						#view('model/releasedvipset', {data: rc.qrygalleries})#
					</cfif>
				<cfelse>
					<cfif NOT isDefined("check_showingForthcoming") OR check_showingForthcoming EQ 0>
						<div class="row clearfix becky-sets">
							<div class="col-md-9 col-sm-6 col-xs-7 left">
								Forthcoming sets:
							</div>
							<div class="col-md-3 col-sm-6 right desktop">
							</div>
						</div>
						<cfset check_showingForthcoming = 1>
					</cfif>
					#view('model/forthcomingset', {data: rc.qrygalleries})#
				</cfif>
			</cfloop>

			<div class="bg-white"></div>

			<div class="row clearfix becky-sets becky-sets-video">
				<div class="col-md-12 col-sm-12 right desktop">
			    	#rc.UIHelperService.paging(rc.paging_config,'ul')#
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 right mobile" style="padding-right: 0px;">
					#rc.UIHelperService.paging(rc.paging_config,'dd')#
				</div>
			</div>
			#view('model/view_model_other_ot_sites', {model_name: rc.qrymodel.model_display_name})#
		<cfelse>
			<div class="row clearfix becky-sets">
				<div class="col-md-12 col-sm-12 left">
					<cfif rc.qrygalleries.RecordCount GT 0>
						Model does not exist
					<cfelse>
						This Model currently has no sets
					</cfif>
				</div>
			</div>
			<div class="row clearfix becky-sets">
				<cfif rc.qrygalleries.RecordCount GT 0>
					<div class="nonResultsContent"><strong>- This model does not exist, please select try again -</strong></div>
				<cfelse>
					<div class="nonResultsContent"><strong>- This model currently does not have any sets -</strong></div>
				</cfif>
			</div>
		</cfif>
	</cfif>
</cfoutput>