<cfoutput>
		
	<cfif ListFind(application.stc_set_types.str_videos,rc.qry_galleryDetails.gallery_type)>
		<cfset video_image = true>
	<cfelse>
		<cfset video_image = false>
	</cfif>

	<cfif ListFind(application.stc_set_types.str_videos,rc.qry_galleryDetails.gallery_type)>
		<cfif rc.display_set>
			<div class="row clearfix becky-sets">
				<div class="col-md-12 col-sm-12 col-xs-12 left">
					Available Video Options
				</div>
			</div>
			<div class="row galleryVideo">
				<p class="marginTop">To download the file to your local drive, right click the link of the resolution you wish to down, and select Save File As then save the file to your local drive.
				</p>	
				<p>You can also stream the video in your web browser by clicking on one of the streaming options to the right hand side.</p>
				<p>If you are having trouble viewing the videos, please first view the <a href="http://members.onlytease.com/members/help.cfm?type=video" class="videoFAQS">Video FAQS</a></p>
				<cfswitch expression="#rc.qry_galleryDetails.video_mp4_hd_name#">
					<cfcase value="">
						<div class="col-sm-12 col-xs-12 nonpadding-left nonpadding-right">
							<div class="col-sm-4 col-xs-4 padding-0">
								<div class="titleVideoOption">Download:</div>
								<div class="col-sm-2 col-xs-2 iconDownload">
									<p class="first"><i class="fa fa-film"></i></p>
									<p class="last">HD</p>
								</div>
								<div class="col-sm-10 col-xs-10 rightBlockDownload">
									<cfif rc.qry_galleryDetails.video_hd_res_name NEQ "" OR rc.qry_galleryDetails.video_mp4_hd_name NEQ "">
										<cftry>
											<cfset k_size = Trim(replaceNoCase(rc.qry_galleryDetails.video_hd_res_filesize,"mb","")) * 1000>
											<cfcatch type="Any">
												<cfset k_size = 0>
											</cfcatch>
										</cftry>
										<p>
											<strong>HD Res Video Download</strong>
										</p>

										<cfloop from="1" to="#rc.qry_galleryDetails.video_no_clips#" step="1" index="loop_count">
											<cfif Len(rc.qry_galleryDetails.video_mp4_hd_name) GT 0>
												<p>
													<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_mp4_hd_name#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_mp4_hd_name#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);">
														<i class="fa fa-download"></i><strong>HD Res video<cfif rc.qry_galleryDetails.video_no_clips GT 1> - clip #loop_count#</cfif></strong> (#rc.qry_galleryDetails.video_mp4_hd_filesize#)
													</a>
												</p>
											<cfelse>
												<p>
													<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_hd_res_name##loop_count#.#rc.qry_galleryDetails.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_hd_res_name##loop_count#.#rc.qry_galleryDetails.video_format#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);">
														<i class="fa fa-download"></i><strong>HD Res video<cfif rc.qry_galleryDetails.video_no_clips GT 1> - clip #loop_count#</cfif></strong> (#rc.qry_galleryDetails.video_hd_res_filesize#)
													</a>
												</p>
											</cfif>
										</cfloop>
									</cfif>
									<cfif rc.qry_galleryDetails.video_high_res_name NEQ "" OR rc.qry_galleryDetails.video_mp4_sd_name NEQ "">
										<cftry>
											<cfset k_size = Trim(replaceNoCase(rc.qry_galleryDetails.video_high_res_filesize,"mb","")) * 1000>
											<cfcatch type="Any">
												<cfset k_size = 0>
											</cfcatch>
										</cftry>
										<p>
											<strong>SD Res Video Download</strong>
										</p>
										<cfloop from="1" to="#rc.qry_galleryDetails.video_no_clips#" step="1" index="loop_count">
											<cfif Len(rc.qry_galleryDetails.video_mp4_sd_name) GT 0>
												<p>
													<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_mp4_sd_name#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_mp4_sd_name#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);">
														<i class="fa fa-download"></i><strong>High Res video<cfif rc.qry_galleryDetails.video_no_clips GT 1> - clip #loop_count#</cfif></strong> (#rc.qry_galleryDetails.video_mp4_sd_filesize#)
													</a>
												</p>
											<cfelse>
												<p>
													<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_high_res_name##loop_count#.#rc.qry_galleryDetails.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_high_res_name##loop_count#.#rc.qry_galleryDetails.video_format#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);">
														<i class="fa fa-download"></i><strong>SD Res video<cfif rc.qry_galleryDetails.video_no_clips GT 1> - clip #loop_count#</cfif></strong> (#rc.qry_galleryDetails.video_high_res_filesize#)
													</a>
												</p>
											</cfif>
										</cfloop>
										<cfif rc.qry_galleryDetails.video_hd_res_name EQ "">
											<cfif rc.qry_galleryDetails.video_medium_res_name NEQ "">
												<cftry>
													<cfset k_size = Trim(replaceNoCase(rc.qry_galleryDetails.video_medium_res_filesize,"mb","")) * 1000>
													<cfcatch type="Any">
														<cfset k_size = 0>
													</cfcatch>
												</cftry>
												<p><strong>Medium Res Video Download</strong></p>
												<cfif qry_galleryDetails.video_no_clips EQ 1>
													<p>
														<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_medium_res_name#1.#rc.qry_galleryDetails.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_medium_res_name#1.#rc.qry_galleryDetails.video_format#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);">
															<i class="fa fa-download"></i><strong>Medium Res video</strong> (#rc.qry_galleryDetails.video_medium_res_filesize#)
														</a>
													</p>
												<cfelse>
													<cfloop from="1" to="#rc.qry_galleryDetails.video_no_clips#" step="1" index="loop_count">
														<p>
															<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_medium_res_name##loop_count#.#rc.qry_galleryDetails.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_medium_res_name##loop_count#.#rc.qry_galleryDetails.video_format#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);">
																<i class="fa fa-download"></i><strong>Medium Res video - clip #loop_count#</strong> (#rc.qry_galleryDetails.video_medium_res_filesize#)
															</a>
														</p>
													</cfloop>
												</cfif>
											</cfif>
										</cfif>
										<cfif rc.qry_galleryDetails.video_low_res_name NEQ "">
											<cftry>
												<cfset k_size = Trim(replaceNoCase(rc.qry_galleryDetails.video_low_res_filesize,"mb","")) * 1000>
												<cfcatch type="Any">
													<cfset k_size = 0>
												</cfcatch>
											</cftry>
											<strong>Low Res Video Download</strong>
											<cfif rc.qry_galleryDetails.video_no_clips EQ 1>
												<p>
													<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_low_res_name#1.#rc.qry_galleryDetails.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_low_res_name#1.#rc.qry_galleryDetails.video_format#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);">
														<i class="fa fa-download"></i><strong>Low Res video</strong> (#rc.qry_galleryDetails.video_low_res_filesize#)
													</a>
												</p>
											<cfelse>
												<cfloop from="1" to="#rc.qry_galleryDetails.video_no_clips#" step="1" index="loop_count">
													<p>
														<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_low_res_name##loop_count#.#rc.qry_galleryDetails.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_low_res_name##loop_count#.#rc.qry_galleryDetails.video_format#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);">
															<i class="fa fa-download"></i><strong>Low Res video - clip</strong> (#rc.qry_galleryDetails.video_low_res_filesize#)
														</a>
													</p>
												</cfloop>
											</cfif>
										</cfif>
									</cfif>
								</div>
							</div>
							<div class="col-sm-4 col-xs-4 padding-0">
								<div class="titleVideoOption">Streaming:</div>
								<div class="col-sm-2 col-xs-2 iconDownload">
									<p class="first"><i class="fa fa-film"></i></p>
									<p class="last">FlashHD</p>
								</div>
								<div class="col-sm-10 col-xs-10 rightBlockDownload">
									<cfif Len(rc.qry_galleryDetails.video_mp4_hd_name) GT 0>
										<p><strong>Streaming this video</strong></p>
										<p>
											<a href="http://members.onlytase.com/members/playVideo.cfm?video_model_id=#rc.qry_galleryDetails.model_id#&gallery_id=#URL.gallery_id#&video_dir=#rc.qry_galleryDetails.video_directory_name#&res=hd" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Play','Video gallery id: #URL.gallery_id# HD');">
												<i class="fa fa-video-camera"></i><strong>HD Res video</strong>
											</a>
										</p>
									<cfelse>
										<cfif rc.qry_galleryDetails.video_hd_res_name NEQ "">
											<p><strong>HD Video Streaming</strong></p>
											<p>
												<a href="http://members.onlytase.com/members/playVideo.cfm?video_model_id=#rc.qry_galleryDetails.model_id#&gallery_id=#gallery_id#&video_dir=#rc.qry_galleryDetails.video_directory_name#&res=hd" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Play','Video gallery id: #gallery_id# HD');">
												<i class="fa fa-video-camera"></i><strong> HD Res video</strong></a>
											</p>
										</cfif>
											<p><strong>SD Video Streaming</strong></p>
											<p>
												<a href="http://members.onlytase.com/members/playVideo.cfm?video_model_id=#rc.qry_galleryDetails.model_id#&gallery_id=#gallery_id#&video_dir=#rc.qry_galleryDetails.video_directory_name#&res=hi" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Play','Video gallery id: #gallery_id# Hi');">
												<i class="fa fa-video-camera"></i><strong> SD Res video</strong></a>
											</p>
										<cfif rc.qry_galleryDetails.video_hd_res_name EQ "">
											<p><strong>Low Res Video Streaming</strong></p>
											<p>
												<a href="http://members.onlytase.com/members/playVideo.cfm?video_model_id=#rc.qry_galleryDetails.model_id#&gallery_id=#gallery_id#&video_dir=#rc.qry_galleryDetails.video_directory_name#&res=low" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Play','Video gallery id: #gallery_id# Low');">
												<i class="fa fa-video-camera"></i><strong> Low Res video</strong></a>
											</p>
										</cfif>
									</cfif>
								</div>
							</div>
							<div class="col-sm-4 col-xs-4 padding-0">
								<div class="titleVideoOption">Ipod Video Download:</div>
								<div class="col-sm-2 col-xs-2 iconDownload">
									<p class="first"><i class="fa fa-film"></i></p>
									<p class="last">IpodHD</p>
								</div>
								<div class="col-sm-10 col-xs-10 rightBlockDownload">
									<cfif rc.qry_galleryDetails.video_ipod_res_name NEQ "" AND rc.qry_galleryDetails.video_hd_res_name NEQ "" AND rc.qry_galleryDetails.video_mp4_hd_name EQ "">
										<cftry>
											<cfset k_size = Trim(replaceNoCase(rc.qry_galleryDetails.video_ipod_res_filesize,"mb","")) * 1000>
											<cfcatch type="Any">
												<cfset k_size = 0>
											</cfcatch>
										</cftry>
										<p>
											<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_ipod_res_name#.m4v" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_ipod_res_name#.m4v');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);">
												<i class="fa fa-video-camera"></i><strong>iPod video</strong> (#rc.qry_galleryDetails.video_ipod_res_filesize#)
											</a>
										</p>
									</cfif>
								</div>
							</div>
						</div>
					</cfcase>
					<cfdefaultcase>
						<div class="col-sm-12 col-xs-12">
							<div class="col-sm-6 col-xs-6 blockDownload">
								<div class="titleVideoOption">Download:</div>
								<div class="col-sm-2 col-xs-2 iconDownload">
									<p class="first"><i class="fa fa-film"></i></p>
									<p class="last">HD</p>
								</div>
								<div class="col-sm-10 col-xs-10 rightBlockDownload">
									<cftry>
										<cfset k_size = Trim(replaceNoCase(rc.qry_galleryDetails.video_mp4_hd_name,"mb","")) * 1000>
										<cfcatch type="Any">
											<cfset k_size = 0>
										</cfcatch>
									</cftry>
									<p>
										<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_mp4_hd_name#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_mp4_hd_name#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);" style="font-weight:bold;">
											<i class="fa fa-download"></i><strong>MP4 - HD Res Video</strong> (#rc.qry_galleryDetails.video_mp4_hd_filesize#)
										</a>
									</p>
									<p>
										<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_mp4_sd_name#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_mp4_sd_name#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);" style="font-weight:bold;">
											<i class="fa fa-download"></i><strong>MP4 - SD Res Video</strong> (#rc.qry_galleryDetails.video_mp4_sd_filesize#)
										</a>
									</p>
									<p>
										<a href="#rc.strServerVideoLink#/members/#rc.qry_galleryDetails.video_directory_name#/#rc.qry_galleryDetails.video_webm_name#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#rc.qry_galleryDetails.video_webm_filesize#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);" style="font-weight:bold;">
											<i class="fa fa-download"></i><strong>WEBM - HD Res Video</strong> (#rc.qry_galleryDetails.video_webm_filesize#)
										</a>
									</p>
								</div>
							</div>
							<div class="col-sm-6 col-xs-6 blockStream">
								<div class="titleVideoOption">Stream:</div>
								<div class="col-sm-2 col-xs-2 iconDownload">
									<p class="first"><i class="fa fa-film"></i></p>
									<p class="last">FlashHD</p>
								</div>
								<div class="col-sm-10 col-xs-10 rightBlockDownload">
									<p>
										<a href="http://members.onlytease.com/members/playVideo.cfm?video_model_id=#rc.qry_galleryDetails.model_id#&gallery_id=#URL.gallery_id#&video_dir=#rc.qry_galleryDetails.video_directory_name#&res=hd" title="Click to stream the video in your browser" onclick="pageTracker._trackEvent('Video','Play','Video gallery id: #URL.gallery_id# HD');">
											<i class="fa fa-video-camera"></i><strong>HD Video</strong>
										</a>
									</p>
									<p>
										<a href="http://members.onlytease.com/members/playVideo.cfm?video_model_id=#rc.qry_galleryDetails.model_id#&gallery_id=#URL.gallery_id#&video_dir=#rc.qry_galleryDetails.video_directory_name#&res=hi" title="Click to stream the video in your browser" onclick="pageTracker._trackEvent('Video','Play','Video gallery id: #URL.gallery_id# SD');">
											<i class="fa fa-video-camera"></i><strong>SD Video</strong>
										</a>
									</p>
								</div>
							</div>
						</div>
					</cfdefaultcase>
				</cfswitch>
			</div>
		</cfif>
	</cfif>

	<cfif rc.qry_galleryimages.recordcount gt rc.paging_config.DISPLAYCOUNT>
		<div class="row clearfix becky-sets">
			<cfif URL.view_image eq true>
				<div class="col-md-3 col-sm-3 col-xs-7 left title-back-gallery">
					<cfset curPage = Int(rc.current_position/24) + 1/>
					<a href="/index.cfm/model.view_gallery?gallery_id=#URL.gallery_id#&CurrentPage=#curPage#">
						<i class="fa fa-th-large"></i> Back to Gallery    
					</a>
				</div>
				<cfset newcol = "9">
				<cfset newcolxs = "5">
			<cfelse>
				<cfset newcol = "12">
				<cfset newcolxs = "12">
			</cfif>

			<div class="col-md-#newcol# col-sm-#newcol# right desktop">
		    	#rc.UIHelperService.paging(rc.paging_config,'ul')#
			</div>
			<div class="col-md-#newcol# col-sm-#newcol# col-xs-#newcolxs# right mobile" style="padding-right: 0px;">
				#rc.UIHelperService.paging(rc.paging_config,'dd')#
			</div>
		</div>
	</cfif>
	
	<div class="view-gallery-container">
		<cfif URL.view_image eq true>
			<div class="row selectSize right">
				<div class="col-md-3 col-sm-3 col-xs-3 blockpic">
					<cfif rc.qry_imageDetails.gallery_type NEQ 21 AND rc.qry_imageDetails.zipfile NEQ "">
						<a href="#rc.normal_url##rc.current_image#" target="_blank">
							<img src="/assets/img/normal.png" class="img-responsive"/>
						</a>
					<cfelse>
						<img src="/assets/img/normal-grey.png" class="img-responsive"/>
					</cfif>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-3 blockpic">
					<cfif rc.qry_imageDetails.gallery_type NEQ 21 AND rc.qry_imageDetails.lrg_zipfile NEQ "">
						<a href="#rc.large_url##rc.current_image#" target="_blank">
							<img src="/assets/img/large.png" class="img-responsive"/>
						</a>
					<cfelse>
						<img src="/assets/img/large-grey.png" class="img-responsive" />
					</cfif>
				</div> 
				<div class="col-md-3 col-sm-3 col-xs-3 blockpic">
					<cfif rc.qry_imageDetails.gallery_type NEQ 21 AND rc.qry_imageDetails.ultra_zipfile NEQ "">
						<a href="#rc.ultra_url##rc.current_image#" target="_blank">
							<img src="/assets/img/ultra.png" class="img-responsive"/>
						</a>
					<cfelse>
						<img src="/assets/img/ultra-grey.png" class="img-responsive" />
					</cfif>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-3 blockpic">
					<cfif rc.qry_imageDetails.gallery_type NEQ 21 AND rc.qry_imageDetails.super_zipfile NEQ "">
						<a href="#rc.super_url##rc.current_image#" target="_blank">
							<img src="/assets/img/super.png" class="img-responsive"/>
						</a>
					<cfelse>
						<img src="/assets/img/super-grey.png" class="img-responsive" />
					</cfif>
				</div>
			</div>	
			<div class="row picSize">
				<cfloop query="rc.qry_galleryimages" startrow="#rc.display_start_row#" maxrows="1">
					<cfif rc.qry_imageDetails.gallery_id GT application.gallery_change>
						<cfif rc.qry_imageDetails.gallery_type EQ 21>
							<a href="#rc.qry_imageDetails.guest_link_url#" target="_new"><img src="/members/#rc.sets_locat#/#rc.qry_imageDetails.directory_name#/#rc.image_dir#/#replaceNoCase(rc.qry_imageDetails.filename,"tn#rc.thumb_image_replace#","#rc.image_replace#")#"></a>
						<cfelse>
							<img src="http://members.onlytease.com/members/#rc.sets_locat#/#rc.qry_imageDetails.directory_name#/#rc.image_dir#/#replaceNoCase(rc.qry_imageDetails.filename,"tn#rc.thumb_image_replace#","#rc.image_replace#")#">
						</cfif>
					<cfelse>
						<img src="<cfif (rc.cs_use AND rc.qry_imageDetails.directory_name LTE rc.cs_max_directory AND rc.qry_imageDetails.directory_name DOES NOT CONTAIN "a")>#rc.cs_url#<cfelse>http://members.onlytease.com</cfif>/members/#rc.sets_locat#/#rc.qry_imageDetails.directory_name#/#rc.image_dir#/#replaceNoCase(rc.qry_imageDetails.filename,"tn","")#">
					</cfif>
				</cfloop>
			</div>
		<cfelse>
			<div class="row bg-white view-gallery-right">
				<div class="row custom-min-height">
					<cfloop query="rc.qry_galleryimages" startrow="#rc.display_start_row#" maxrows="#session.preferences.pref_thumbs#">
						<cfif rc.display_set>
							<cfset mobi_cols = "6"/>
						<cfelse>
							<cfset mobi_cols = "4"/>
						</cfif>
						<div class="col-md-2 col-xs-#mobi_cols# view-gallery-img marginTop #(video_image eq false) ? 'minHeigh190' : ''#">
							<cfif (session.preferences.pref_view AND URL.gallery_id GT application.gallery_change)>
								<cfset display_url = "#rc.view_url##ReplaceNoCase(rc.qry_galleryimages.filename,"tn#rc.thumb_image_replace#","#rc.image_replace#")#">
							<cfelseif (session.preferences.pref_view AND gallery_id LTE 6000)>
								<cfset display_url = "#rc.view_url##ReplaceNoCase(rc.qry_galleryimages.filename,"tn","")#">
							<cfelse>
								<cfset display_url = "#rc.view_url##rc.qry_galleryimages.image_id#&view_image=true">
							</cfif>
							<cfif rc.display_set>
								<cfif rc.customgal AND rc.qry_galleryDetails.gallery_type NEQ 4 AND rc.qry_galleryDetails.gallery_type NEQ 5 AND rc.qry_galleryDetails.gallery_type NEQ 6>
									<a class="view-gallery-img" href="##" onclick="js_toggleCheckBox(#rc.qry_galleryimages.image_id#);return false;">
								<cfelse>
									<a class="view-gallery-img" href="#display_url#" <cfif session.preferences.pref_window>target="_blank"<cfelse>target="_parent"</cfif>>
								</cfif>
							</cfif>
								<cfif rc.display_set>
									<img src="<cfif (rc.cs_use AND rc.qry_galleryDetails.directory_name LTE rc.cs_max_directory AND rc.qry_galleryDetails.directory_name DOES NOT CONTAIN "a")>#rc.cs_url#<cfelse>http://members.onlytease.com</cfif>/members/#rc.sets_locat#/#rc.qry_galleryDetails.directory_name#/thumbnails/#rc.qry_galleryimages.filename#" width="260" class="img-responsive #(rc.qry_galleryimages.image_width GT 0 AND rc.qry_galleryimages.image_height GT 0 AND rc.qry_galleryimages.image_width GT rc.qry_galleryimages.image_height AND video_image eq false) ? 'marginTop48' : ''#">
								<cfelse>
									<cfif rc.tracked_set>
										<cfset enc = Encrypt("/members/#rc.sets_locat#/tr_#rc.qry_galleryDetails.directory_name#/thumbnails/#rc.qry_galleryimages.filename#", application.key, application.algorithm, application.encoding)>
									<cfelse>
										<cfset enc = Encrypt("/members/#rc.sets_locat#/#rc.qry_galleryDetails.directory_name#/thumbnails/#rc.qry_galleryimages.filename#", application.key, application.algorithm, application.encoding)>
									</cfif>
									<img src="http://members.onlytease.com/members/display.cfm?i=#enc#" width="260" class="img-responsive #(rc.qry_galleryimages.image_width GT 0 AND rc.qry_galleryimages.image_height GT 0 AND rc.qry_galleryimages.image_width GT rc.qry_galleryimages.image_height AND video_image eq false) ? 'marginTop48' : ''#">
								</cfif>
							<cfif rc.display_set></a>
								<cfif rc.customgal AND rc.qry_galleryDetails.gallery_type NEQ 4 AND rc.qry_galleryDetails.gallery_type NEQ 5 AND rc.qry_galleryDetails.gallery_type NEQ 6>
									<input id="img_#rc.qry_galleryimages.image_id#" type="checkbox"<cfif rc.qry_galleryimages.in_customgal EQ 1> checked="checked"</cfif> onchange="js_updateCustomGal(#rc.qry_galleryimages.image_id#,#customgal_id#)" />
								</cfif>
							</cfif>
						</div>
					</cfloop>
				</div>			
			</div>
		</cfif>
			
	</div>	

	<cfif rc.qry_galleryimages.recordcount gt rc.paging_config.DISPLAYCOUNT>
		<div class="row clearfix becky-sets">
			<div class="col-md-12 col-sm-12 right desktop">
		    	#rc.UIHelperService.paging(rc.paging_config,'ul')#
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 right mobile" style="padding-right: 0px;">
				#rc.UIHelperService.paging(rc.paging_config,'dd')#
			</div>
		</div>
	</cfif>

	<cfif rc.qryComments.RecordCount GT 0>
		<div class="row bg-gray content-comment bg-white">
			<h5>Comments for this gallery</h5>
			<h6>
				<a href="http://members.onlytease.com/gallery_comments_add.cfm?gid=#URL.gallery_id#&CurrentPage=#URL.CurrentPage#&serial_code=#rc.qry_galleryDetails.serial_code#"><i class="fa fa-pencil-square"></i>Add New Comment</a>
			</h6>
			<div class="clearfix"></div>
			<div class="time-content">
				<form name="report_comment" action="http://members.onlytease.com/reporting_comment.cfm">
				<input type="hidden" name="id" value="#URL.gallery_id#">
				<cfloop query="rc.qryComments" startrow="1">
					<div class="row form-group">
						<cfif rc.report GT 0>
							<label class="col-sm-2 col-xs-2"><input type="radio" name="comment_id" value="#rc.qryComments.galleries_comment_id#"></label>
						</cfif>
						<label class="col-sm-4 col-xs-4">#DateFormat(rc.qryComments.galleries_comment_date_added)# - #rc.qryComments.forum_nickname#</label>
						<div class="col-sm-#rc.report GT 0 ? '6' : '8'# col-xs-#rc.report GT 0 ? '6' : '8'#">
							#rc.qryComments.galleries_comment_text#
							<cfif rc.qryComments.galleries_comment_added_by EQ session.pref_id>
								- <span class="editComment"><a href="http://members.onlytease.com/members/gallery_comments_edit.cfm?gid=#URL.gallery_id#&CurrentPage=#URL.CurrentPage#&serial_code=#rc.qry_galleryDetails.serial_code#&comment_id=#rc.qryComments.galleries_comment_id#"><i class="fa fa-pencil"></i></a></span>
							</cfif>
						</div>
					</div>
				</cfloop>
				<div class="row form-group textCenter">
					<cfif rc.blocked_reporter.blocked_reporting_comments EQ 0>
						<cfif rc.report EQ 0>
							<div class="col-sm-12 col-xs-12 right">
								<a href="/index.cfm/model.view_gallery?gallery_id=#URL.gallery_id#&report=true##report"><i class="fa fa-times-circle"></i>Report an inappropriate Comment</a>
							</div>
						<cfelse>
							<div class="formType">
								<label class="col-sm-6 col-xs-6 right">
									<b>Type This Below:&nbsp;</b>
								</label>
								<div class="col-sm-6 col-xs-6 textLeft">
									 <iframe name="iframe_captcha" id="iframe_captcha" src="http://members.onlytease.com/members/register_captcha.cfm" frameborder="0" width="200" height="34" scrolling="No">please wait</iframe>
									 <a href="" id="btn-refresh-capcha"><i class="fa fa-refresh"></i></a>
								</div>
							</div>
							<div class="formReport">
								<label class="col-sm-6 col-xs-6 right">
									  
								</label>

								<div class="clearBoth"></div>

								<div class="col-sm-6 col-xs-6 textLeft">
									<input type="Text" width="300" name="captcha_value" id="captcha_value" value="" tabindex="" onblur="validateField('captcha_value', this.value);"></input>
									<input type="submit" width="250" class="report_comment" value="Report Selected Comment"></form>
								</div>
							</div>
						</cfif>
					<cfelse>
						<div class="col-sm-12 col-xs-12 right">
							<i class="fa fa-times-circle"></i>To report a comment, please contact an admin using <a href="mailto:info@onlytease.com?subject=Reporting a comment on OnlyTease">info@onlytease.com</a>
						</div>
					</cfif>
				</div>
			</div>
		</div>
	</cfif>
</cfoutput>

