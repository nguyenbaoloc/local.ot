<cfoutput>
<cfif model_id gt 0>
	<cfif rc.qry_model.recordcount gt 0>
		<div class="row clearfix becky-sets name-profile">
			<div class="col-md-6 col-sm-12 col-xs-12 left">
				#rc.qry_model.model_display_name#'s Profile	
			</div>	
			<div class="col-md-6 col-sm-12 col-xs-12 right"></div>
		</div>
		<div class="row clearfix vitalStats-profile">
			<div class="col-md-6 col-sm-12 col-xs-12 left">
				<div class="vitalStats-title">Vital Stats:</div>
				<div class="form-horizontal form-details">
					<cfif rc.qry_model.model_height NEQ "">
					  	<div class="form-group">
						    <label class="col-sm-3 col-xs-3 control-label">Height:</label>
						    <div class="col-sm-9 col-xs-9">
						      	<p class="form-control-static">#rc.qry_model.model_height#</p>
						    </div>
					  	</div>
				  	</cfif>
				  	<cfif rc.qry_model.model_star_sign NEQ "">
					  	<div class="form-group">
						    <label class="col-sm-3 col-xs-3 control-label">Star Sign:</label>
						    <div class="col-sm-9 col-xs-9">
						    	<p class="form-control-static">#rc.qry_model.model_star_sign#</p>
						    </div>
					  	</div>
				  	</cfif>
				  	<cfif rc.qry_model.model_dob NEQ "" AND rc.qry_model.model_dob_active EQ 1>
					  	<div class="form-group">
						    <label class="col-sm-3 col-xs-3 control-label">D.O.B:</label>
						    <div class="col-sm-9 col-xs-9">
						    	<p class="form-control-static">#LSDateFormat(rc.qry_model.model_dob, "mmmm d, yyyy")#</p>
						    </div>
					  	</div>
				  	</cfif>
				  	<cfif (rc.qry_model.model_bust NEQ "" AND rc.qry_model.model_waist NEQ "" AND rc.qry_model.model_hips NEQ "")>
					  	<div class="form-group">
						    <label class="col-sm-3 col-xs-3 control-label">Stats:</label>
						    <div class="col-sm-9 col-xs-9">
						    	<p class="form-control-static">Bust: #rc.qry_model.model_bust# - Waist: #rc.qry_model.model_waist# - Hips: #rc.qry_model.model_hips#</p>
						    </div>
					  	</div>
				  	</cfif>
				  	<cfif rc.qry_model.model_shoe_size NEQ "" OR rc.qry_model.model_dress_size NEQ "">
					  	<div class="form-group">
						    <label class="col-sm-3 col-xs-3 control-label">Sizes:</label>
						    <div class="col-sm-9 col-xs-9">
						    	<p class="form-control-static">Dress: #rc.qry_model.model_dress_size# - Shoe: #rc.qry_model.model_shoe_size#</p>
						    </div>
					  	</div>
				  	</cfif>
				  	<div class="form-group blockListFont margin-bottom30">
			  			<label class="col-sm-4 control-label"><i class="fa fa-th-list"></i>#rc.qry_model.gallery_count#
			  			</label>		  		
			  			<label class="col-sm-5 control-label"><i class="fa fa-camera"></i>#NumberFormat(rc.qry_model.image_count,"9,999,999")#
			  			</label>		  		
			  			<label class="col-sm-3 control-label"><i class="fa fa-video-camera"></i>#rc.qry_model.video_count# 
			  			</label>		  		
				  	</div>
				  	<cfif rc.facebook.answer_text NEQ "">
				  		<div class="form-group">
					  		<label class="col-sm-1 control-label">
					  			<i class="fa fa-facebook-square blue"></i>
					  		</label>		  		
				  			<label class="col-sm-11 control-label">
				  				<a title="Click to see #rc.qry_model.model_display_name#'s Facebook Page" STYLE="text-decoration:none" href="#rc.facebook.answer_text#" target="_new">
				  					#left(rc.facebook.answer_text,40)#
				  				</a>
				  			</label>	
			  			</div>
				  	</cfif>
				  	<cfif rc.twitter.answer_text NEQ "">
				  		<div class="form-group">
					  		<label class="col-sm-1 control-label">
					  			<i class="fa fa-twitter-square blue"></i>
					  		</label>		  		
				  			<label class="col-sm-11 control-label">
				  				<a title="Click to see #rc.qry_model.model_display_name#'s Twitter Page" STYLE="text-decoration:none" href="http://twitter.com/#rc.twitter.answer_text#" target="_new">
				  					http://twitter.com/#rc.twitter.answer_text#
			  					</a>
				  			</label>
			  			</div>
				  	</cfif>
				</div>	
			</div>

			<cfif rc.qry_model.model_flash_profile NEQ "">
				<div class="col-md-6 col-sm-12 col-xs-12 right modelprofile-content">
					<div class="vitalStats-titleRight">Profile Video:</div>	
					<div class="col-md-12 form-horizontal form-details">
						<div class="form-group">
							<label class="control-label videoPlay">
								<cfif rc.qry_videoDetails.video_string NEQ "">
									<cfset videoPath = "http://cdn.content.onlytease.com/members/#rc.qry_videoDetails.directory_name#/#rc.qry_videoDetails.video_string#-#rc.qry_videoDetails.directory_name#-hi.flv">
								<cfelse>
									<cfset videoPath = "http://cdn.content.onlytease.com/members/#rc.qry_videoDetails.directory_name#/#rc.qry_videoDetails.directory_name#-hi.flv">
								</cfif>
								<input type="hidden" id="flv-video-path" value="#videoPath#">
								<div id='mediaspace'>Loading the player ...</div>
								<!--- <div class="viewVideo">
									<p><i class="fa fa-film"></i></p>
									<label>View Video</label>
								</div> --->
							</label>
						</div>
					</div>
				</div>
			<cfelse>
				<div class="col-md-6 col-sm-12 col-xs-12 right">
					<div class="vitalStats-titleRight">Latest Gallery:</div>	
					<div class="form-horizontal form-details">
						<div class="form-group">
							<label class="col-sm-5 col-xs-6 control-label">
								<img class="pic-profile" src="/assets/img/img-small-profile.jpg">
							</label>
							<div class="col-sm-7 col-xs-6 blockText-votingDate">
								<cfif rc.any_released.recordcount GT 0>
									<div class="text-voting-date">Voting results for the set</div>	
									<div class="form-group">
									    <label class="col-sm-3 col-xs-3 control-label">Model:</label>
									    <label class="col-sm-9 col-xs-9">
									    	<p class="form-control-static">
									    		#NumberFormat("#rc.released_gal.average_model#", "0.0")# / 10
									    	</p>
									    </label>
								  	</div>
								  	<div class="form-group margin-bottom30">
									    <label class="col-sm-3 col-xs-3 control-label">Outfit:</label>
									    <label class="col-sm-9 col-xs-9">
									    	<p class="form-control-static">
									    		#NumberFormat("#rc.released_gal.average_outfit#", "0.0")# / 10
									    	</p>
									    </label>
								  	</div>

								  	<div class="form-group margin-bottom30">
									    <label class="col-sm-12 col-xs-12 text-voting-date">
									    	Date Released
									    </label>
									    <label class="col-sm-12 col-xs-12">
									    	<p class="form-control-static">
									    		#LSDateFormat(rc.released_gal.release_date, "ddd, d mmmm yyyy")#
									    	</p>
									    </label>
								  	</div>
							  	</cfif>
							  	<div class="form-group">
								    <p class="col-sm-9">
								    	<a class="btn btn-success btn-blue-view no-border" href="/index.cfm/model.view_gallery?gallery_id=#rc.released_gal.gallery_id#">
								    		VIEW GALLERY
								    	</a>
								    </p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</cfif>
			
		</div>
		<cfif rc.answer_counter GT 0>
			<div class="row clearfix blockQuestionnaire">
				<div class="col-sm-12">
					<div class="vitalStats-title">Questionnaire:</div>	
					<div class="col-sm-12 form-horizontal formQuestionnaire">
						<cfset count = 0>
						<cfloop query="rc.get_answers">
							<cfset get_questions = rc.modelProfileService.get_questions(rc.get_answers.answer_question_id)/>
							<cfif rc.get_answers.answer_text GT "">
								<div class="form-group">
						    		<label class="control-label">
						    			#get_questions.question_text#
						    		</label>
						    		<cfset namelen=LEN(rc.get_answers.answer_text)>
									<cfset answer = '#UCASE(Left(rc.get_answers.answer_text, 1))##LCASE(MID(rc.get_answers.answer_text, 2, namelen-1))#'>
							    	<p class="form-control-static">
							    		#answer#
							    	</p>
							  	</div>
							</cfif>
						</cfloop>
					</div>
				</div>
			</div>
		</cfif>
	</cfif>
	
<cfelse>
</cfif>
</cfoutput>