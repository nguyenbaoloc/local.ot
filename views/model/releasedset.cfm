<cfoutput>
<cfset video_info = "">
<div class="row">
    <div class="col-md-12 col-sm-12">
        <cfif ListFind(application.video_set_types, data.gallery_type)>
            <cfset image_set = FALSE>
        <cfelse>
            <cfset image_set = TRUE>
        </cfif>
       
        <div class="row" style="margin-top:10px;">
            <div class="row custom-min-height">
                #view('common/images_set', {image_set: image_set, data: data})#
            </div>
        </div>
        <div class="<cfif image_set>row clearfix download-zone<cfelse>row clearfix view-video-zone-pink</cfif>">
            <cfset rc.ft_history = rc.QueryExtendService.getFT_history(data.gallery_id)>
            <cfif data.gallery_type EQ 21>
                <cfset rc.qry_guestGallery = rc.QueryExtendService.getGuestGallery(data.gallery_id)>
                <a href="#rc.qry_guestGallery.guest_link_url#" target="_new" alt="Click Here To Visit #rc.qry_guestGallery.model_display_name#" title="Click Here To Visit #rc.qry_guestGallery.model_display_name#"><img src="/images/guest_galleries/#rc.qry_guestGallery.model_link#_cover_#rc.qry_guestGallery.model_id#.jpg" border="0" width="260" height="120"></a>
                <br>
                <cfif rc.ft_history.recordcount GT 0 AND data.gallery_release EQ 1>
                    <strong><span style="color:red;">Previously FastTracked on: #rc.ft_history.ft_date_added_formarted#</span></strong>
                </cfif>
            <cfelse>
                <cfif rc.ft_history.recordcount GT 0 AND qry_galleries.gallery_release EQ 1>
                    <strong><span style="color:red;">Previously FastTracked on: #rc.ft_history.ft_date_added_formarted#</span></strong>
                </cfif>
                <cfif NOT image_set>
                    <div class="row model-info-pink">
                        <div class="row col-md-9 col-sm-9 row-info-pink">
                            <p>
                                <label class="model-name">#rc.UtilService.udf_modelNames(data.gallery_id)#</label><span class="border-left"><i class="fa fa-film"></i> Video length: #data.video_length# </span><span class="border-left desktop">#rc.UIHelperService.set_TextOfGalleryType(data.gallery_type)#</span>
                            </p>
                            #view('common/displayVideoModelSets', {pdata: data, pis_bts:"", pbts_id: data.gallery_id, pcolor: "pink", pstrServerVideoLink: rc.strServerVideoLink})#
                        </div>
                        <div class="col-md-3 col-sm-3 btn-view-area">
                            <a class="btn btn-success btn-pink-view no-border" href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>"> VIEW FULL <cfif image_set>SET<cfelse>VIDEO</cfif></a>
                        </div>
                    </div>
                <cfelse>
                    <cfset release_date = #data.release_date#>
                    <cfset from_date = #createODBCDate(DateAdd("d",-30,now()))#>
                    <div class="row model-info-pink">
                        <div class="row col-md-9 col-sm-9 row-info-pink">
                            <p>
                                <label class="model-name">
                                    #rc.UtilService.udf_modelNames(data.gallery_id)#<cfif data.gallery_type EQ 21> - Guest Gallery</cfif>
                                </label> 
                                <span class="border-left"><i class="fa fa-camera"></i> #NumberFormat(data.image_count,"9,999")#</span>
                                <span class="border-left border-none-mobi size-text">
                                    #rc.UIHelperService.set_TextOfGalleryType(data.gallery_type)# 
                                    <cfif data.video_clip>
                                        <br/><img src="/img/icon_XL_Purple.png" align="absmiddle" alt=""> <label class="xl-size size-text">Plus behind the scenes video available!</label>
                                    </cfif>
                                </span>
                            </p>
                            <cfif data.zipfile NEQ "" OR data.lrg_zipfile NEQ "" OR data.ultra_zipfile NEQ "" OR data.super_zipfile NEQ "">
                                <p class="desktop">
                                    <cfif data.zipfile NEQ "">
                                        <cftry>
                                        <cfset k_size = Trim(replaceNoCase(data.zipfile_size,"mb","")) * 1000>
                                            <cfcatch type="Any"><cfset k_size = 0></cfcatch>
                                        </cftry>
                                        <label class="standard-size size-text"><i class="fa fa-download pink"></i><a href="#rc.strServerZipLink#/members/zips/#data.zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#data.zipfile#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">Standard Size</a></label> 
                                    </cfif>
                                    <cfif data.lrg_zipfile NEQ "">
                                        <cftry>
                                        <cfset k_size = Trim(replaceNoCase(data.lrg_zipfile_size,"mb","")) * 1000>
                                            <cfcatch type="Any"><cfset k_size = 0></cfcatch>
                                        </cftry>
                                        <label class="xl-size size-text"><i class="fa fa-download pink"></i><a href="#rc.strServerZipLink#/members/largezips/#data.lrg_zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#data.lrg_zipfile#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">XL Size</a></label> 
                                        
                                    </cfif>
                                    <cfif data.ultra_zipfile NEQ "">
                                        <cftry>
                                        <cfset k_size = Trim(replaceNoCase(data.ultra_zipfile_size,"mb","")) * 1000>
                                            <cfcatch type="Any"><cfset k_size = 0></cfcatch>
                                        </cftry>
                                        <label class="u-xl-size size-text"><i class="fa fa-download pink"></i><a href="#rc.strServerZipLink#/members/ultrazips/#data.ultra_zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#data.ultra_zipfile#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">Ultra Xl Size</a></label> 
                                    </cfif>
                                    <cfif data.super_zipfile NEQ "">
                                        <cftry>
                                        <cfset k_size = Trim(replaceNoCase(data.super_zipfile_size,"mb","")) * 1000>
                                            <cfcatch type="Any"><cfset k_size = 0></cfcatch>
                                        </cftry>
                                        <label class="super-size size-text"><i class="fa fa-download pink"></i><a href="#rc.strServerZipLink#/members/superzips/#data.super_zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#data.super_zipfile#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">Super Size</a></label> 
                                    </cfif>
                                </p>
                                <cfif video_clip>
                                    #view('common/displayVideoModelSets', {pdata: data, pis_bts:"bts", pbts_id: video_clip, pcolor: "pink", pstrServerVideoLink: rc.strServerVideoLink})#
                                </cfif>
                            <cfelse>
                                #view('common/displayVideoModelSets', {pdata: data, pis_bts:"", pbts_id: video_clip, pcolor: "pink", pstrServerVideoLink: rc.strServerVideoLink})#
                            </cfif>
                        </div>
                        <div class="col-md-3 col-sm-3 btn-view-area">
                            <a class="btn btn-success btn-pink-view no-border" href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>"> VIEW FULL <cfif image_set>SET<cfelse>VIDEO</cfif></a>
                        </div>
                    </div>
                </cfif>
            </cfif>
        </div>
    </div>
</div>
</cfoutput>