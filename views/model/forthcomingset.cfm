<cfoutput>
<cfset video_info = "">
<div class="row">
    <div class="col-md-12 col-sm-12">
        <cfif ListFind(application.video_set_types, data.gallery_type)>
            <cfset image_set = FALSE>
        <cfelse>
            <cfset image_set = TRUE>
        </cfif>
       	<cfif data.gallery_type NEQ 7>
			<cfif (data.ft_pref_id NEQ "" AND data.ft_month EQ month(now()) AND data.ft_year EQ year(now()) ) >
				<cfset link_header = '<a href="">'>
				<cfset link_footer = '</a>'>
				<cfset link_alt = "Click to view this fasttracked set">
			<cfelseif data.pft_pref_id NEQ "">
				<cfset link_header = '<a href="">'>
				<cfset link_footer = "</a>">
				<cfset link_alt = "Click to view this fasttracked set">
			<cfelse>
				<cfset link_header = '<a href="">'>
				<cfset link_footer = "</a>">
				<cfset link_alt = "Click to preview the set">
			</cfif>
		<cfelse>
				<cfset link_header = "">
				<cfset link_footer = "">
				<cfset link_alt = "">
				<cfif data.pft_pref_id NEQ "">
					<cfset link_header = '<a href="">'>
					<cfset link_footer = "</a>">
					<cfset link_alt = "Click to preview the set">
				</cfif>
			</cfif>
        <div class="row" style="margin-top:10px;">
            <div class="row custom-min-height">
                <cfif NOT image_set>
					<cfif data.video_hd_res_name EQ "" AND data.video_mp4_hd_name EQ "">
						<div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic">
							#link_header#<img src="/images/v2/tour_images/#replace(data.tour_pic3_thumb,'tn','tn260')#" class="displayThumb" border="0" alt="#link_alt#" title="#link_alt#">#link_footer#
							<div class="opacity-vid-pic">
			                	<p>
			                		<i class="fa fa-film"></i>
			                	</p>	
			                	<label>Video</label>
			                </div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic desktop">
							#link_header#<img src="/images/v2/mailing_list/#replace(data.mailing_list_pic_thumb,'tn','tn260')#" class="displayThumb" border="0" alt="#link_alt#" title="#link_alt#">#link_footer#
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic desktop">
							#link_header#<img src="/images/v2/updates/tn260#data.updates_pic_thumb#" class="displayThumb" border="0" alt="#link_alt#" title="#link_alt#">#link_footer#
						</div>
					<cfelse>
						<div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic">
							#link_header#<img src="/images/v2/tour_images/#replace(data.tour_pic3_thumb,'tn','tn260')#" class="displayThumb" border="0" alt="#link_alt#" title="#link_alt#">#link_footer#
							<div class="opacity-vid-pic">
			                	<p>
			                		<i class="fa fa-film"></i>
			                	</p>	
			                	<label>Video</label>
			                </div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic desktop">
							#link_header#<img src="/images/v2/mailing_list/#replace(data.mailing_list_pic_thumb,'tn','tn260')#" class="displayThumb" border="0" alt="#link_alt#" title="#link_alt#">#link_footer#
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic desktop">
							#link_header#<img src="/images/v2/updates/tn260#data.updates_pic_thumb#" class="displayThumb" border="0" alt="#link_alt#" title="#link_alt#">#link_footer#
						</div>
					</cfif>
				</cfif>
				<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic'#">
					#link_header#<img src="/images/v2/tour_images/#replace(data.tour_pic1_thumb,'tn','tn260')#" class="displayThumb" border="0" alt="#link_alt#" title="#link_alt#">#link_footer#
				</div>
				<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic'#">
					#link_header#<img src="/images/v2/tour_images/#replace(data.tour_pic2_thumb,'tn','tn260')#" class="displayThumb" border="0" alt="#link_alt#" title="#link_alt#">#link_footer#
				</div>
				<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic'# #image_set ? 'desktop' : ''#">
					#link_header#<img src="/images/v2/tour_images/#replace(data.tour_pic3_thumb,'tn','tn260')#" class="displayThumb" border="0" alt="#link_alt#" title="#link_alt#">#link_footer#
				</div>
            </div>
        </div>
        <div class="<cfif image_set>row clearfix download-zone<cfelse>row clearfix view-video-zone-pink</cfif>" style="margin-bottom:10px;">
        	<div class="row model-info-pink">
	    		<div class="row col-md-12 col-sm-12 row-info-pink" style="margin-bottom:10px;">
	    			<div class="col-md-4 col-sm-4" style="float:left">
	    				<label class="model-name"><a href="/index.cfm/model.model_set?model_id=#url.model_id#">#rc.qrymodel.model_display_name#</a></label>
	    			</div>
	    			<div class="col-md-8 col-sm-8" style="float:right;">
	    				<cfif data.gallery_type NEQ 7>
							<cfif (data.ft_pref_id NEQ "" AND data.ft_month EQ month(now()) AND data.ft_year EQ year(now()) ) >
								<div class="col-md-6 col-sm-6" align="center">
									<a href="">
										<img src="/images/ft/view_now_white.gif" border="0" width="160" height="24" alt="Click to view this fasttracked set" title="Click to view this fasttracked set">
									</a>
								</div>
							<cfelseif data.pft_pref_id NEQ "">
								<div class="col-md-6 col-sm-6" align="center">
									<a href="">
										<img src="/images/ft/permanentFT_view_now_white.gif" border="0" width="160" height="24" alt="Click to view this fasttracked set" title="Click to view this fasttracked set">
									</a>
								</div>
							<cfelse>
								<cfif rc.allow_fasttrack>
									<div class="col-md-6 col-sm-6" align="center">
										<img src="/images/ft/fasttrack_white.gif" onClick="fastrackRequest(#session.pref_id#,#gallery_id#);" class="fasttrackCursor" border="0" width="160" height="24" alt="Click here to fasttrack this set" title="Click here to fasttrack this set">
									</div>
									<div class="col-md-1 col-sm-1" align="center" style="font-size: 14px ! important; line-height: 230%;">OR</div>
								</cfif>
								<cfif rc.allow_permfasttrack >
									<div class="col-md-6 col-sm-6" align="center">
										<img src="/images/ft/permanentFT_this set_white.gif" onClick="permFastrackRequest(#session.pref_id#,#gallery_id#);" class="fasttrackCursor" border="0" width="160" height="24" alt="Click here to permanently fasttrack this set, this costs #data.PFT_cost# PFT<cfif data.PFT_cost NEQ 1>'s</cfif>" title="Click here to permanently fasttrack this set, this costs #data.PFT_cost# PFT<cfif data.PFT_cost NEQ 1>'s</cfif>">
									</div>
								<cfelse>
									<div class="col-md-3 col-md-offset-0" align="center">
										<a href="/members/help.cfm?type=PFT">
											<img src="/images/ft/permanentFT_this set_white.gif" border="0" class="fasttrackCursor" border="0" width="160" height="24" alt="Click here to buy permanent fasttracks, this set costs #data.PFT_cost# PFT<cfif data.PFT_cost NEQ 1>'s</cfif>" title="Click here to buy permanent fasttracks, this set costs #data.PFT_cost# PFT<cfif data.PFT_cost NEQ 1>'s</cfif>">
										</a>
									</div>
								</cfif>
							</cfif>
						<cfelse>
							<cfif data.pft_pref_id NEQ "">
								<div class="col-md-6 col-sm-6" align="center">
									<a href="">
										<img src="/images/ft/permanentFT_view_now_white.gif" border="0" width="160" height="24" alt="Click to view this fasttracked set" title="Click to view this fasttracked set">
									</a>
								</div>
							<cfelse>
								<cfif rc.allow_permfasttrack>
									<div class="col-md-6 col-sm-6" align="center">
										<img src="/images/ft/permanentFT_this set_white.gif" onClick="permFastrackRequest(#session.pref_id#,#gallery_id#);" class="fasttrackCursor" border="0" width="160" height="24" alt="Click here to permanently fasttrack this set, this costs #data.PFT_cost# PFT<cfif data.PFT_cost NEQ 1>'s</cfif>" title="Click here to permanently fasttrack this set, this costs #data.PFT_cost# PFT<cfif data.PFT_cost NEQ 1>'s</cfif>">
									</div>
								<cfelse>
									<div class="col-md-6 col-sm-6" align="center">
										<a href="/members/help.cfm?type=PFT">
											<img src="/images/ft/permanentFT_this set_white.gif" border="0" class="fasttrackCursor" border="0" width="160" height="24" alt="Click here to buy permanent fasttracks, this set costs #data.PFT_cost# PFT<cfif data.PFT_cost NEQ 1>'s</cfif>" title="Click here to buy permanent fasttracks, this set costs #data.PFT_cost# PFT<cfif data.PFT_cost NEQ 1>'s</cfif>">
										</a>
									</div>
								</cfif>
							</cfif>
						</cfif>
					</div>
	    		</div>
	    	</div>
        </div>
    </div>
</div>
</cfoutput>