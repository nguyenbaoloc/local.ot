<cfoutput>
	<div class="row">
		<h3 class="titleSearch">Search OnlyTease by Category</div>
		<input type="hidden" id="galType" value="load">
		<input type="hidden" id="url_keyword_id" value="<cfif isDefined("url.keyword_id")><cfoutput>#url.keyword_id#</cfoutput></cfif>">
	</div>	
	<cfif findNoCase('Android', cgi.http_user_agent,1) or findNoCase('iPhone', cgi.http_user_agent,1)>
		<div class="row blockSort mobile" id="blockSortMobi">
			<div class="col-sm-4 col-xs-4">
				<div class="title-sortSearch">Filter:</div>
			</div>
			<div class="col-sm-8 col-xs-8">
				<select id="ddFiler-mobile-search" class="form-control left">
					<option value="a">All sets</option>
					<option value="p">Picture sets only</option>
					<option value="v">Video sets only</option>
					<option value="f">Fasttrack sets only</option>
				</select>
			</div>
			<div class="col-sm-12 col-xs-12"><hr/></div>
			<div class="col-sm-4 col-xs-4">
				<div class="title-sortSearch">Categories:</div>
			</div>
			<div class="col-sm-8 col-xs-8 blockCategories" id="filter_container">
				
			</div>
			<div class="col-sm-12 col-xs-12"><hr/></div>
			<div class="col-sm-4 col-xs-4">
				<div class="title-sortSearch">Sort By:</div>
			</div>
			<div class="col-sm-8 col-xs-8">
				<select id="ddSort-mobile-search" class="form-control left">
					<option value="1">Date released (newest first)</option>
					<option value="2">Date released (oldest first)</option>
					<option value="3">Model name (A to Z)</option>
					<option value="4">Model name (Z to A)</option>
					<option value="5">Random</option>
				</select>
			</div>
		</div>
	<cfelse>
		<div class="row blockSort desktop" id="blockSortDesk">
			<div class="col-md-4 col-sm-4 col-xs-4">
				<div class="title-sortSearch">Filter:</div>
				<div class="radio">
				  	<label>
					    <input type="radio" name="galTypeRadio" id="gt_a" onclick="galFilterType('a')" checked>
					    All sets
				  	</label>
				</div>
				<div class="radio">
				  	<label>
					    <input type="radio" name="galTypeRadio" id="gt_p" onclick="galFilterType('p')">
					    Picture sets only
				  	</label>
				</div>
				<div class="radio">
				  	<label>
					    <input type="radio" name="galTypeRadio" id="gt_v" onclick="galFilterType('v')">
					    Video sets only
				  	</label>
				</div>
				<div class="radio">
				  	<label>
					    <input type="radio" name="galTypeRadio" id="gt_f" onclick="galFilterType('f')">
					    Fasttrack sets only
				  	</label>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 blockCategories" id="filter_container">
				<div class="title-sortSearch">Categories:</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4">
				<div class="title-sortSearch">Sort By:</div>
				<div class="radio">
				  	<label>
					    <input type="radio" name="sortOrder" value="1" checked onclick="filterUpdate(1)">
					    Date released (newest first)
				  	</label>
				</div>
				<div class="radio">
				  	<label>
					    <input type="radio" name="sortOrder" value="2" onclick="filterUpdate(1)">
					    Date released (oldest first)
				  	</label>
				</div>
				<div class="radio">
				  	<label>
					    <input type="radio" name="sortOrder" value="3" onclick="filterUpdate(1)">
					    Model name (A to Z)
				  	</label>
				</div>
				<div class="radio">
				  	<label>
					    <input type="radio" name="sortOrder" value="4" onclick="filterUpdate(1)">
					    Model name (Z to A)
				  	</label>
				</div>
				<div class="radio">
				  	<label>
					    <input type="radio" name="sortOrder" value="5" onclick="filterUpdate(1)">
					    Random
				  	</label>
				</div>
			</div>
		</div>
	</cfif>
	<div class="row clearfix becky-sets paging-search marginTop">
	</div>	
	<div class="row custom-min-height marginTop marginTop" id="search-results">
		<div class="col-md-12 col-sm-12 col-xs-12 rs-text">
			Please select from the categories above to search OnlyTease by category
		</div>
	</div>
	<div id="loadingDiv-search" class="row custom-min-height rs-text">
		<img src="/images/loading_txt.gif" alt="Loading"><br>
		<img src="/images/loading_ani.gif">
	</div>
	<div class="row clearfix becky-sets becky-sets-video paging-search marginTop">
	</div>
	<script type="text/javascript">
		var keyword_ref = new Array();
		#rc.kw_ref#
		var keyword_first = #rc.kw_array#;
		window.onload=function(){
			// On load reset gtype
			<cfif NOT isDefined("url.keyword_id") OR NOT IsNumeric("url.keyword_id")>
				document.getElementById('galType').value = 'load';
			</cfif>
			filterUpdate(1);
			<cfif isDefined("url.keyword_id")>filterFromUrl(#url.keyword_id#);</cfif>
		}
	</script>
</cfoutput>