<cfoutput>
<cfset video_info = "">
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="row marginTop">
            <div class="row custom-min-height">
            	#view('common/images_set', {image_set: image_set, data: rc.qry_sets})# 
            </div>                           
        </div>
        <div class="marginBottom <cfif image_set>row clearfix download-zone<cfelse>row clearfix view-video-zone-pink</cfif>">
        	<div class="row model-info-pink">
	    		<div class="row col-md-12 col-sm-12 marginBottom row-info-pink">
	    			<div class="col-md-4 col-sm-4 floatLeft">
					 	<p>
				            <label class="model-name">
				                #rc.modelService.getModelNames(rc.qry_sets)#
				            </label> 
				            <span class="border-left">
				            	<cfif image_set>
				            		<i class="fa fa-camera"></i> #data.image_count GT 0 ? NumberFormat(data.image_count,"9,999") : 0#
				            	<cfelse>
				            		<i class="fa fa-film"></i> Video length: #data.video_length#
				            	</cfif>
				            </span>
				        </p>
					</span>
	    			</div>
	    			<div class="col-md-8 col-sm-8 floatRight">
    					<cfif data.gallery_type NEQ 7>
						<cfif (data.ft_pref_id NEQ "" AND data.ft_month EQ month(now()) AND data.ft_year EQ year(now()) ) >
							<div class="col-md-6 col-sm-6" align="center">
								<a href="/members/view_gallery.cfm?gallery_id=#qry_sets.gallery_id#"><img src="/images/ft/view_now_white.gif" border="0" width="160" height="24" alt="Click to view this fasttracked set" title="Click to view this fasttracked set"></a>
							</div>
						<cfelseif data.pft_pref_id NEQ "">
							<div class="col-md-6 col-sm-6" align="center">
								<a href="/members/view_gallery.cfm?gallery_id=#qry_sets.gallery_id#"><img src="/images/ft/permanentFT_view_now_white.gif" border="0" width="160" height="24" alt="Click to view this fasttracked set" title="Click to view this fasttracked set"></a>
							</div>
						<cfelse>
							<cfif rc.allow_fasttrack>
								<div class="col-md-6 col-xs-6" align="center">
									<img src="/images/ft/fasttrack_white.gif" onClick="fastrackRequest(#session.pref_id#,#gallery_id#);" class="fasttrackCursor" border="0" width="160" height="24" alt="Click here to fasttrack this set" title="Click here to fasttrack this set">
								</div>
								<div class="col-md-1 col-sm-1 textOr" align="center">OR</div>
							</cfif>
							<cfif rc.allow_permfasttrack >
								<div class="col-md-5 col-xs-5" align="center">
									<img src="/images/ft/permanentFT_this set_white.gif" onClick="permFastrackRequest(#session.pref_id#,#gallery_id#);" class="fasttrackCursor" border="0" width="160" height="24" alt="Click here to permanently fasttrack this set" title="Click here to permanently fasttrack this set">
								</div>
							<cfelse>
								<div class="col-md-5 col-xs-5" align="center">
									<a href="/members/help.cfm?type=PFT"><img src="/images/ft/permanentFT_this set_white.gif" border="0" class="fasttrackCursor" border="0" width="160" height="24" alt="Click here to buy permanent fasttracks" title="Click here to buy permanent fasttracks"></a>
								</div>
							</cfif>
						</cfif>
						</cfif>
					</div>
	    		</div>
	    	</div>
        </div>
    </div>
</div>
</cfoutput>