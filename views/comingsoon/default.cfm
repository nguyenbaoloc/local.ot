<cfoutput>
	<div class="row clearfix commingsoon-title">
		<div class="col-md-12 col-sm-12 left">
			<label><cfif viewFT EQ 0>Releases Coming Soon !<cfelse>All sets available for FastTrack</cfif></label>
		</div>
	</div>
	<form action="/index.cfm/comingsoon.default" method="get" id="filterForm"> 
		<div class="row clearfix commingsoon-filter" style="#viewFT eq 0 ? 'padding-bottom: 10px;' : ''#">
			<div class="col-md-5 col-sm-5 left">
				<cfif viewFT EQ 1>
					<div id="" class="form-inline filterForm left" method="post">
						<div class="form-group">
							<label>Sort order:</label>
							<select id="updatesSortOrder" class="filterSelect" name="updatesSortOrder" onchange="document.getElementById('filterForm').submit();">
								<option value="0" <cfif updatesSortOrder EQ 0>selected</cfif>>Date added (newest first)</option>
								<option value="1" <cfif updatesSortOrder EQ 1>selected</cfif>>Date added (oldest first)</option>
								<option value="2" <cfif updatesSortOrder EQ 2>selected</cfif>>Model name (A-Z)</option>
								<option value="3" <cfif updatesSortOrder EQ 3>selected</cfif>>Model name (Z-A)</option>
							</select>
						</div> 
					</div>
				</cfif>
			</div>
			<div class="col-md-7 col-sm-7 right">
				<div id="" class="form-inline filterForm" method="post">
					<div class="form-group">
						<label>Update filter:</label>
						<select id="viewFT" name="viewFT" class="filterSelect" onchange="document.getElementById('filterForm').submit();">
							<option value="0" <cfif viewFT EQ 0>selected</cfif>>View releases coming soon</option>
							<option value="1" <cfif viewFT EQ 1>selected</cfif>>View all sets available for FastTrack</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<cfif viewFT eq 1>
			<div class="row clearfix commingsoon-filter border-bottom-comming">
				<div class="col-md-5 col-sm-5 left">
					<div id="" class="form-inline filterForm left" method="post">
						<div class="form-group">
							<label>Filter by model name:</label>
							<select id="updatesFilterModelName" name="updatesFilterModelName" class="filterSelect" onchange="document.getElementById('filterForm').submit();">
								<option value="all" <cfif updatesFilterModelName EQ "all">selected</cfif>>View all models</option>
								<cfloop query="rc.qry_ModelList">
									<option value="#rc.qry_ModelList.model_id#" <cfif updatesFilterModelName EQ rc.qry_ModelList.model_id>selected</cfif>>#rc.qry_ModelList.model_display_name#</option>
								</cfloop>
							</select>
						</div> 
					</div>
				</div>

				<div class="col-md-7 col-sm-7 right desktop">
					<cfif rc.qry_sets.RecordCount GT 0>#rc.UIHelperService.paging(rc.stc_config,'ul')#</cfif>
				</div>
				<div class="col-md-7 col-sm-7 right mobile">
					<cfif rc.qry_sets.RecordCount GT 0>#rc.UIHelperService.paging(rc.stc_config,'dd')#</cfif>
				</div>
			</div>
		</cfif>
	</form>
	<cfset current_date = "">
	<cfset day_count = 1>
	<cfset next_gallery_count = 1>
	<cfif rc.qry_sets.RecordCount GT 0>
		<cfloop query="rc.qry_sets" startrow="#rc.display_start_row#" maxrows="10">
				<cfif current_date NEQ rc.qry_sets.release_date AND viewFT EQ 0>
					<!--- border start --->
					<div class="row clearfix becky-image-sets">
						<div class="right">
							#rc.qry_sets.release_date_formarted#	
						</div>
					</div>
					<cfset day_count = day_count + 1>
					<cfset current_date = rc.qry_sets.release_date>
				<cfelseif rc.qry_sets.currentRow NEQ rc.display_start_row>
					<div class="dividerBar"><img alt="" height="2" src="/images/spacer.gif"/></div>
				</cfif>
				<!--- display details for the correct type of set --->
				<cfif ListFind(application.video_set_types, rc.qry_sets.gallery_type)>
					<cfset display_style = "setContentVideoText">
					<cfset image_set = FALSE>
				<cfelse>
					<cfset display_style = "setContentImageText">
					<cfset image_set = TRUE>
				</cfif>
				#view('comingsoon/comingset', {data: rc.qry_sets})#
		</cfloop>
	<cfelse>
		<div class="detailsContainer">
			There were no <cfif viewFT EQ 0>updates<cfelse>sets available for FastTrack</cfif> found
		</div>
	</cfif>
</cfoutput>