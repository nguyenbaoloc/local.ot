<cfoutput>
<h5 class="text-center desktop textWellcomeModel">
	<b>WELCOME TO THE MEMBERS AREA</b>
</h5>
<div class="row">
	<div id="view-gallery" class="col-md-12 left-side-block detail-models view-gallery">
		<div class="panel-heading left-side-black-bg">
			<cfswitch expression="#rc.qry_galleryDetails.gallery_type#">
				<cfcase value="21">
					#rc.model_names# - Guest Gallery
				</cfcase>
				<cfdefaultcase>
					#rc.model_names#
				</cfdefaultcase>
			</cfswitch>
		</div>	
		<div class="panel-border-bottom"></div>
		<div class="panel-body left-side-gray-bg">
			<div class="prestige-tease">
				<h3>
					<cfswitch expression="#rc.qry_galleryDetails.set_type#">
						<cfcase value="-1">Set Preview</cfcase>
						<cfcase value="0">Special Release</cfcase>
						<cfcase value="2">FastTracked set</cfcase>
						<cfcase value="3">Permanent FastTrack</cfcase>
						<cfdefaultcase></cfdefaultcase>
					</cfswitch>
				</h3>
			</div>
			<div class="release-time">
				<h4>Release Date</h4>
				<div class="release-date">#DateFormat(rc.qry_galleryDetails.release_date, "dddd")# #rc.qry_galleryDetails.release_date_formarted#</div>
				<p class="caption-left-model-set caption-left-view-gallery">
					<cfswitch expression="#rc.qry_galleryDetails.gallery_type#">
						<cfcase value="1,2,3,7,10,11">
							<span><i class="fa fa-camera"></i>#NumberFormat(rc.qry_galleryimages.RecordCount,"9,999")#</span>
						</cfcase>
						<cfcase value="14,4,5,6,12,13,16">
							<span><i class="fa fa-film"></i>#rc.qry_galleryDetails.video_length#</span>
						</cfcase>
						<cfdefaultcase></cfdefaultcase>
					</cfswitch>
				</p>
			</div>
			<cfif rc.qry_galleryDetails.gallery_type NEQ 21>
				<cfif rc.allow_fasttrack>
					<cfif rc.qry_galleryDetails.VIDEO_DIRECTORY_NAME NEQ "">
						<div class="prestige-tease">
							<h5>The Following files will be downloadable once you have fasttracked this set:</h5>
							
								<cfif rc.qry_galleryDetails.video_hd_res_name NEQ "">
									<p class="standard-size font-regular-text">
										HD Res video (#rc.qry_galleryDetails.video_hd_res_filesize#)
									</p>
									<p class="standard-size font-regular-text">
									 	High Res video (#rc.qry_galleryDetails.video_high_res_filesize#)
									</p>
								<cfelse>
									<p class="standard-size font-regular-text">
									 	High Res video (#rc.qry_galleryDetails.video_high_res_filesize#)
									</p>
									<p class="standard-size font-regular-text">
									 	Low Res video (#rc.qry_galleryDetails.video_low_res_filesize#)
									</p>
								</cfif>
								<cfif rc.qry_galleryDetails.video_ipod_res_name NEQ "">
									<p class="standard-size font-regular-text">
									 	iPod Video (#rc.qry_galleryDetails.video_ipod_res_filesize#)
									</p>
								</cfif>
							</p>
						</div>
					</cfif>
				</cfif>
				<cfif rc.display_set AND (rc.qry_galleryDetails.zipfile NEQ "" OR rc.qry_galleryDetails.lrg_zipfile NEQ "" OR rc.qry_galleryDetails.ultra_zipfile NEQ "" OR rc.qry_galleryDetails.super_zipfile NEQ "")>
					<div class="prestige-tease">
						<h5>Zip Download</h5>
						<cfif rc.qry_galleryDetails.zipfile NEQ "">
							<p class="standard-size font-regular-text">
								<cftry>
									<cfset k_size = Trim(replaceNoCase(rc.qry_galleryDetails.zipfile_size,"mb","")) * 1000>
									<cfcatch type="Any">
										<cfset k_size = 0>
									</cfcatch>
								</cftry>
								<a href="#rc.strServerZipLink#/members/zips/#rc.qry_galleryDetails.zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#rc.qry_galleryDetails.zipfile#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);"><i class="fa fa-download pink"></i>Standard Size</a>
							</p>
						</cfif>
						<cfif rc.qry_galleryDetails.lrg_zipfile NEQ "">
							<p class="xl-size font-regular-text">
								<cftry>
									<cfset k_size = Trim(replaceNoCase(rc.qry_galleryDetails.lrg_zipfile_size,"mb","")) * 1000>
									<cfcatch type="Any">
										<cfset k_size = 0>
									</cfcatch>
								</cftry>
								<a href="#rc.strServerZipLink#/members/largezips/#rc.qry_galleryDetails.lrg_zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#rc.qry_galleryDetails.lrg_zipfile#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);"><i class="fa fa-download pink"></i>XL Size</a>
							</p>
						</cfif>
						<cfif rc.qry_galleryDetails.ultra_zipfile NEQ "">
							<p class="u-xl-size font-regular-text">
								<cftry>
									<cfset k_size = Trim(replaceNoCase(rc.qry_galleryDetails.ultra_zipfile_size,"mb","")) * 1000>
									<cfcatch type="Any">
										<cfset k_size = 0>
									</cfcatch>
								</cftry>
								<a href="#rc.strServerZipLink#/members/ultrazips/#rc.qry_galleryDetails.ultra_zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#rc.qry_galleryDetails.ultra_zipfile#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);"><i class="fa fa-download pink"></i>Ultra XL Size</a>
							</p>
						</cfif>
						<cfif rc.qry_galleryDetails.super_zipfile NEQ "">
							<p class="super-size font-regular-text">
								<cftry>
									<cfset k_size = Trim(replaceNoCase(rc.qry_galleryDetails.super_zipfile_size,"mb","")) * 1000>
									<cfcatch type="Any">
										<cfset k_size = 0>
									</cfcatch>
								</cftry>
								<a href="#rc.strServerZipLink#/members/superzips/#rc.qry_galleryDetails.super_zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#rc.qry_galleryDetails.super_zipfile#');pageTracker._trackEvent('User','Download','#trim(cgi.remote_user)#',#k_size#);"><i class="fa fa-download pink"></i>Super Size</a>
							</p>
						</cfif>
					</div>
				</cfif>
			</cfif>
			
			<div class="block-set-option">
				<h6>Set Options</h6>
				<ul class="list-unstyled list-set-option">
					<cfif rc.display_set AND rc.qry_galleryDetails.gallery_type AND rc.qry_galleryDetails.gallery_type NEQ 4 AND rc.qry_galleryDetails.gallery_type NEQ 5 AND rc.qry_galleryDetails.gallery_type NEQ 6>
						<li>
							<a href="http://members.onlytease.com/members/view_slideshow.cfm?gallery_id=#URL.gallery_id#">
								<i class="fa fa-play-circle"></i>View Slideshow</a>
						</li>
					</cfif>
					
					<cfif rc.favourite_gallery>
						<li>
							<a href="javascript:ConfirmDelete('#URL.gallery_id#', '#URL.CurrentPage#');">
								<i class="fa fa-gratipay pink"></i>Delete From Favourite Sets
							</a>
						</li>
					<cfelse>
						<li>
							<a href="http://members.onlytease.com/members/processes/favourites_functions.cfm?action=addgallery&gid=#URL.gallery_id#&CurrentPage=#URL.CurrentPage#">
								<i class="fa fa-gratipay pink"></i>Add to my favourite models
							</a>
						</li>
					</cfif>

					<li>
						<a href="http://members.onlytease.com/members/preferences.cfm?type=favourites">
							<i class="fa fa-check-circle green"></i>All favourite Models/Sets
						</a>
					</li>

					
					<li>
						<a href="http://members.onlytease.com/members/preferences.cfm">
							<i class="fa fa-question-circle blue-view-gallery"></i>My gallery prefefences
						</a>
					</li>

					<cfif rc.qry_galleryDetails.gallery_type NEQ 21>
						<li>
							<a href="http://members.onlytease.com/members/gallery_comments_add.cfm?gid=#URL.gallery_id#&CurrentPage=#URL.CurrentPage#&serial_code=#rc.qry_galleryDetails.serial_code#">
								<i class="fa fa-pencil-square red"></i>Add New Comment
							</a>
						</li>
					</cfif>

					<cfif rc.display_set AND NOT ListFind(application.stc_set_types.str_videos,rc.qry_galleryDetails.gallery_type) AND rc.qry_galleryDetails.set_type NEQ 2 AND rc.qry_galleryDetails.set_type NEQ 3 AND rc.qry_galleryDetails.set_type NEQ -1>
						<cfif rc.customgal>
							
						<cfelse>
							<li>
								<a href="" id="btn-add-img-cus-set">
									<i class="fa fa-plus-circle yellow"></i>Add image to Custom Set
								</a>
							</li>
							<div class="col-md-12 col-sm-12 col-xs-12 hide" id="customgal-div">
								<div class="customgal-div-content">
									<img src="/images/x.gif" id="x-close" class="right"/>
									<cfif rc.qry_customgal.RecordCount GT 0>
										<form action="/index.cfm/model.view_gallery" method="get">
											<input type="hidden" name="URL.gallery_id" value="#URL.gallery_id#" />
											<input type="hidden" name="URL.CurrentPage" value="#URL.CurrentPage#" />
											<label>Select custom set</label>
											<select name="total_votes_model">
												<cfloop query="rc.qry_customgal">
													<option value="#rc.qry_customgal.customgal_id#">#rc.qry_customgal.customgal_name#</option>
												</cfloop>
											</select><br/>
											<input class="btn btn-success btn-pink-view no-border btn-action-cus-set" type="submit" value="Select set" /><br />
										</form>
									</cfif>
									<form action="/index.cfm/model.view_gallery?gallery_id=#URL.gallery_id#&CurrentPage=#URL.CurrentPage#&newCustGal=1" method="post">
										<label>Create new custom set</label>
										<input name="newcustomgal_name" id="newcustomgal_name" type="text"/><br/>
										<input class="btn btn-success btn-pink-view no-border btn-action-cus-set" type="submit" value="Create set" />
									</form>
								</div>
							</div>
						</cfif>
					</cfif>
				</ul>
			</div>
			<cfif rc.qryComments.recordcount gt 0>
				<div class="block-latest-comment">
					<h6>Latest Comment</h6>
					<div class="content-lastComment">
						<p class="blockquote">&ldquo;</p>	
						<strong>#DateFormat(rc.qryComments.galleries_comment_date_added)# - #rc.qryComments.forum_nickname#</strong>
						<p class="text-detail">#rc.qryComments.galleries_comment_text#</p>
					</div>
					<div class="row">
						<div class="col-sm-9 col-xs-9 btn-view-area btn-see-all-comment">
					        <a class="btn btn-success btn-pink-view no-border" href="##comment" title="Click here to view this set">See All Comments </a>
					    </div>
					</div>
				</div>
			</cfif>
			
			<div class="block-see-sets">
				<h6>See Other Sets Containing:</h6>
				<ul class="list-unstyled list-see-sets">
					<cfloop query="rc.qry_keywords">
						<li>
							<a href="/members/search.cfm?type=keyword&keyword_id=#rc.qry_keywords.gallery_keyword_id#" title="Click to view all sets matching this keyword">#rc.qry_keywords.gallery_keyword_description#
							</a>
						</li>
					</cfloop>
				</ul>
			</div>
			<cfif rc.qry_galleryDetails.gallery_type NEQ 21>
					<cfswitch expression="#rc.qry_galleryDetails.set_type#">
						<cfcase value="-1">
							<!--- <div class="block-set-option">
								<h6>My Reminders options: </h6>
								<ul class="list-unstyled list-set-option">
									<cfif rc.qry_Reminder_check.RecordCount GT 0>
										<li>
											<a href="javascript:ConfirmReminderDelete('#rc.qry_Reminder_check.mr_keyid#','#URL.gallery_id#', '#URL.CurrentPage#');">Remove reminder</a>
										</li>
									<cfelse>
										<li>
											<a href="http://members.onlytease.com/members/processes/reminders_functions.cfm?action=addgallery&id=#URL.gallery_id#&CurrentPage=#URL.CurrentPage#"><i class="fa fa-info-circle"></i> Add reminder</a>
										</li>
									</cfif>
								</ul>
							</div> --->

							<cfif rc.qry_galleryDetails.gallery_type NEQ "7">
								<cfset oService = createObject("component", "model.services.ViewGallery")>
								<cfset qry_pfts = oService.qry_pfts()>
								
								<cfif qry_pfts.pft_remaining GT 0>
									<cfset rc.allow_permfasttrack="TRUE">
								</cfif>
								<cfset rc.allow_fasttrack="TRUE">

								<div class="row block-fastrack">
									<cfif rc.allow_fasttrack>
										<cfset ft_history = oService.ft_history(URL.gallery_id)>

										<cfif ft_history.recordcount GT 0>
											<div class="fastrack-text">
												<a href="">Previously FastTracked on: #DateFormat(ft_history.ft_date_added,"d mmmm yyyy")#</a>
											</div>
										</cfif>

										<div class="fastrack-text">
											<a href="" onclick="fastrackRequest(#session.pref_id#,#URL.gallery_id#);">Fasttrack This Set<i class="fa fa-fast-forward"></i></a>
										</div>
									</cfif>
									<cfif rc.allow_permfasttrack>
										<div class="fastrack-text marginTop5">
											<a href="" onclick="permFastrackRequest(#session.pref_id#,#URL.gallery_id#);">Permanent Fasttract This Set<i class="fa fa-fast-forward"></i></a>
										</div>
									<cfelse>
										<div class="fastrack-text marginTop5">
											<a href="http://members.onlytease.com/members/help.cfm?type=PFT">Permanent Fasttract This Set<i class="fa fa-fast-forward"></i></a>
										</div>
									</cfif>
								</div>
							</cfif>
						</cfcase>
						<cfcase value="1">
							<div class="row block-tease-factor">
								<cfif rc.qry_galleryDetails.gallery_type NEQ "14" AND rc.qry_galleryDetails.gallery_type NEQ "19" AND rc.qry_galleryDetails.gallery_type NEQ "20">
									<cfif rc.voted_old or rc.voted_new>
										<h6>Current voting results for this set: </h6>	
										<cfif rc.qry_galleryDetails.total_votes_cast_outfit GT application.number_minimum_votes>
											<div class="col-sm-12 col-xs-12 form-group">
												<label class="col-sm-12 col-xs-12">Votes for Model : #DecimalFormat(rc.qry_galleryDetails.average_model)#  / 10</label>
												<label class="col-sm-12 col-xs-12">Votes for Outfit : #DecimalFormat(rc.qry_galleryDetails.average_outfit)# / 10</label>
												<cfif rc.qry_galleryDetails.total_votes_cast_tease GT application.number_minimum_votes>
													<label class="col-sm-12 col-xs-12">
														Votes for Tease Factor : #DecimalFormat(rc.qry_galleryDetails.average_tease)# / 10
													</label>
												</cfif>
											</div>
										<cfelse>
											<div class="col-sm-12 col-xs-12 form-group">
												<label class="col-sm-12 col-xs-12">
													Votes are not displayed until a minimum number of votes have<br>been cast by the members
												</label>
											</div>
										</cfif>
									</cfif>
									<cfif rc.not_voted or rc.voted_new>
										<cfif not rc.voted_new>
											<h6>Vote on Model/Outfit/Tease  Factor:</h6>
											<cfset button_text = "Submit Votes" />
										<cfelse>
											<h6>Change your vote for this <br>Outfit / Model / Tease Factor</h6>
											<cfset button_text = "Change Votes" />
										</cfif>
										<form action="index.cfm/model.vote" method="POST">
											<input type="hidden" name="gallery_id" value="#URL.gallery_id#">
											<input type="hidden" name="CurrentPage" value="#URL.CurrentPage#">
											<cfif isDefined("voteError")>
												<h6>Please vote for Outfit, Model and Tease Factor</h6>
											</cfif>
											<div class="col-sm-12 col-xs-12 form-group">
												<label class="col-sm-4 col-xs-4">Model:</label>
												<select name="total_votes_model" class="col-sm-8 col-xs-8 selectpicker">
													<option value="0">Select</option>
													<option value="1" <cfif rc.model_vote eq 1>selected="selected"</cfif>>1 - poor</option>
													<cfloop from="2" to="9" step="1" index="loop_count">
														<option value="#loop_count#" <cfif rc.model_vote eq loop_count>selected="selected"</cfif>>#loop_count#</option>
													</cfloop>
													<option value="10" <cfif rc.model_vote eq 10>selected="selected"</cfif>>10 - best</option>
												</select>
											</div>
											<div class="col-sm-12 col-xs-12 form-group">
												<label class="col-sm-4 col-xs-4">Outfit:</label>
											  	<select name="total_votes_outfit" class="col-sm-8 col-xs-8 selectpicker">
													<option value="0">Select</option>
													<option value="1" <cfif rc.outfit_vote eq 1>selected="selected"</cfif>>1 - poor</option>
													<cfloop from="2" to="9" step="1" index="loop_count">
														<option value="#loop_count#" <cfif rc.outfit_vote eq loop_count>selected="selected"</cfif>>#loop_count#</option>
													</cfloop>
													<option value="10" <cfif rc.outfit_vote eq 10>selected="selected"</cfif>>10 - best</option>
												</select>
											</div>
											<div class="col-sm-12 col-xs-12 form-group">
												<label class="col-sm-4 col-xs-4">Tease:</label>
												<select name="total_votes_tease" class="col-sm-8 col-xs-8 selectpicker">
													<option value="0">Select</option>
													<option value="1" <cfif rc.tease_vote eq 1>selected="selected"</cfif>>1 - poor</option>
													<cfloop from="2" to="9" step="1" index="loop_count">
														<option value="#loop_count#" <cfif rc.tease_vote eq loop_count>selected="selected"</cfif>>#loop_count#</option>
													</cfloop>
													<option value="10" <cfif rc.tease_vote eq 10>selected="selected"</cfif>>10 - best</option>
												</select>
											</div>
											<div class="col-sm-9 col-xs-9 btn-view-area">
												<input type="Submit" value="#button_text#" class="btn btn-success btn-pink-view no-border" title="Click to submit your votes">
										    </div>	
										</form>
									</cfif>
								</cfif>
							</div>
						</cfcase>
						<cfdefaultcase></cfdefaultcase>
					</cfswitch>
			</cfif>
		</div>
		<!--- <div class="row">
			<div class="col-md-12 recommend-set view-gallery-recommended marginTop">
				<div class="panel-heading head-forthcoming-shoots">
					recommended sets
				</div>
				<div class="panel-body body-forthcoming-shoots">                    
                    <div class="row clearfix">                   
                		<div class="col-md-6 col-sm-6 col-xs-6">                       
                    		<a href="" target="_blank"><img src="http://members.onlytease.com/images/site_model_covers/Amy-Allen_cover_695_thumb.jpg" width="100%"></a>
                		</div>
	                    <div class="col-md-6 col-sm-6 col-xs-6">
	                        
	                        <a href="" target="_blank"><img src="http://members.onlytease.com/images/site_model_covers/Ancilla-Tilia_cover_693_thumb.jpg" width="100%"></a>
	                    </div>
                    </div>
                    <div class="row clearfix">        
	                    <div class="col-md-6 col-sm-6 col-xs-6"> 
	                        <a href="" target="_blank"><img src="http://members.onlytease.com/images/site_model_covers/Catherine_cover_692_thumb.jpg" width="100%"></a>
	                    </div>   
	                    <div class="col-md-6 col-sm-6 col-xs-6">		                        
	                        <a href="" target="_blank"><img src="http://members.onlytease.com/images/site_model_covers/Fifi-R_cover_299_thumb.jpg" width="100%"></a>
	                    </div>      
                    </div>	
        		</div>
			</div>
		</div> --->
		<div class="row suggestViewGallery">
			#view("common/suggest_model_site_form")#
			<div class="row">
			<div class="col-md-12 recommend-set view-gallery-random-model marginTop">	
				<div class="panel-heading head-forthcoming-shoots">
					random   model
				</div>
				<div class="panel-body left-side-gray-bg">
					<div class="content-image">
						<img src="http://members.onlytease.com/tour/images/models/260x390#replace(rc.qry_modelRandom.model_shortcut," ","-","ALL")#_cover_#rc.qry_modelRandom.model_id#.jpg" class="picLeftModel" alt="" title="" border="0">
					</div>
					<div class="name"><a href="/index.cfm/model.model_set?model_id=#rc.qry_modelRandom.model_id#">#rc.qry_modelRandom.model_display_name#</a></div>
					<p class="caption-left-model-set">
						<cfswitch expression="#rc.qry_galleryDetails.gallery_type#">
							<cfcase value="1,2,3,7,10,11">
								<span class="">
									<i class="fa fa-camera"></i>
									 #NumberFormat(rc.qry_modelRandom.image_count,"9,999")#
								</span>
							</cfcase>
							<cfcase value="14,4,5,6,12,13,16">
								<span class="">
									<i class="fa fa-film"></i>
									 #NumberFormat(rc.qry_modelRandom.video_count,"9,999")#
								</span>
							</cfcase>
							<cfdefaultcase></cfdefaultcase>
						</cfswitch>
						<span class="border-left">
							<i class="fa fa-th-list"></i>
							#NumberFormat(rc.qry_modelRandom.gallery_count,"9,999")#
						</span>
					</p>
					<div class="row">
						<div class="col-sm-9 col-xs-9 btn-view-area  btn-see-all-comment">
				        	<a class="btn btn-success btn-pink-view no-border" href="/index.cfm/model.model_set?model_id=#rc.qry_modelRandom.model_id#" title="Click here to view this set">View this SET </a>
				    	</div>	
				    </div>	
				</div>
			</div>	
		</div>
		</div>
	</div>
</div>
</cfoutput>

