<cfoutput>
    <div>
        <!--- TWITTER FEED --->
        #view('common/twitter_widget')#
        <!--- END OF TWITTER FEED --->

        <!--- LATEST NEWS --->
        #view('common/latest_news', {data: rc.qryNews})#
        <!--- END OF LATEST NEWS --->

        <!--- MODELS OF THE MONTH --->
        <div class="clearfix"></div>
    </div>
    <div class="row bottom-45 model-of-month left-side-block">
        <div class="col-md-12 desktop">
            <div class="panel-heading head-model-of-month">
                <p><img src="/assets/img/head_model_of_month.png"></p><br>
                <p class="vote-here">NOVEMBER<hr/></p>
            </div>
            <div class="panel-body body-model-of-month">
                #view('poll/view_model_poll', {data: #rc.view_model_poll#})#
            </div>
        </div>
    </div>
    <!--- END OF MODELS OF THE MONTH --->


    <div class="row bottom-45 forthcoming-shoots">
        <div class="col-md-12 left-side-block desktop">
            <div class="panel-heading head-forthcoming-shoots">
                FORTHCOMING SHOOTS
            </div>
            <div class="panel-body body-forthcoming-shoots">
                <cfset flag = 1>
                <cfset row_num = 0>
                <cfset qry_forthcoming_shoots = rc.QueryService.getFortcomingShoots()>
                <cfloop query="qry_forthcoming_shoots">
                    <cfif flag == 1>
                        <div class="row clearfix">
                    </cfif>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <cfset model_name = Trim(Replace(qry_forthcoming_shoots.model_display_name, " ", "-", "All"))>
                        <a href='#replace(replace(qry_forthcoming_shoots.fs_link_request,"members/new_forum/","new_forum."),".cfm","")#' target="_blank"><img src="http://members.onlytease.com/images/site_model_covers/#model_name#_cover_#qry_forthcoming_shoots.model_id#_thumb.jpg"  width="100%"></a>
                    </div>
                    <cfset row_num += 1>
                    <cfif row_num%2 eq 0 and flag neq qry_forthcoming_shoots.recordcount>
                        </div>
                        <div class="row clearfix">
                    </cfif>
                    <cfif flag eq qry_forthcoming_shoots.recordcount>
                        </div>
                    </cfif>
                    <cfset flag += 1>
                </cfloop>
                <div class="row clearfix">
                    <a href="/index.cfm/members.forthcoming_shoots" class="btn btn-success btn_forthcomingshoots">VIEW ALL</a>
                </div>
            </div>
        </div>
    </div>
</cfoutput>