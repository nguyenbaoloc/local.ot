<cfoutput>
	<div class="row model-info-pink">
	    <div class="row col-md-9 col-sm-9 row-info-pink">
	        <cfif image_set>
	            <p>
	                <label class="model-name">
	                    #model_names#<cfif data.gallery_type EQ 21> - Guest Gallery</cfif>
	                </label> 
	                <span class="border-left"><i class="fa fa-camera"></i> #NumberFormat(data.image_count,"9,999")#</span>
	                <span class="border-left border-none-mobi size-text">#UIHelperService.set_TextOfGalleryType(data.gallery_type)#</span>
	            </p>
	            <p class="desktop">
	                <cfif data.zipfile NEQ "">
	                    <cftry>
	                    <cfset k_size = Trim(replaceNoCase(data.zipfile_size,"mb","")) * 1000>
	                        <cfcatch type="Any"><cfset k_size = 0></cfcatch>
	                    </cftry>
	                    <label class="standard-size size-text">
	                    	<i class="fa fa-download pink"></i>
	                    	<a href="#strServerZipLink#/members/zips/#data.zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#data.zipfile#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">Standard Size</a>
	                    </label> 
	                </cfif>
	                <cfif data.lrg_zipfile NEQ "">
	                    <cftry>
	                    <cfset k_size = Trim(replaceNoCase(data.lrg_zipfile_size,"mb","")) * 1000>
	                        <cfcatch type="Any"><cfset k_size = 0></cfcatch>
	                    </cftry>
	                    <label class="xl-size size-text">
	                    	<i class="fa fa-download pink"></i>
	                    	<a href="#strServerZipLink#/members/largezips/#data.lrg_zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#data.lrg_zipfile#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">XL Size</a>
	                    </label> 
	                    
	                </cfif>
	                <cfif data.ultra_zipfile NEQ "">
	                    <cftry>
	                    <cfset k_size = Trim(replaceNoCase(data.ultra_zipfile_size,"mb","")) * 1000>
	                        <cfcatch type="Any"><cfset k_size = 0></cfcatch>
	                    </cftry>
	                    <label class="u-xl-size size-text">
	                    	<i class="fa fa-download pink"></i>
	                    	<a href="#strServerZipLink#/members/ultrazips/#data.ultra_zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#data.ultra_zipfile#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">Ultra Xl Size</a>
	                    </label> 
	                </cfif>
	                <cfif data.super_zipfile NEQ "">
	                    <cftry>
	                    <cfset k_size = Trim(replaceNoCase(data.super_zipfile_size,"mb","")) * 1000>
	                        <cfcatch type="Any"><cfset k_size = 0></cfcatch>
	                    </cftry>
	                    <label class="super-size size-text">
	                    	<i class="fa fa-download pink"></i>
	                    	<a href="#strServerZipLink#/members/superzips/#data.super_zipfile#" onclick="pageTracker._trackEvent('Zip','Download','#data.super_zipfile#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">Super Size</a>
	                    </label> 
	                </cfif>
	            </p>
	            <cfif video_clip>
	            	#view('common/displayVideo', {pdata: data, pis_bts:"bts", pbts_id: data.gallery_id, pcolor: "pink", pstrServerVideoLink: rc.strServerVideoLink})#
	            </cfif>
	        <cfelse>
	            #video_info#
            	#view('common/displayVideo', {pdata: data, pis_bts:"", pbts_id: data.gallery_id, pcolor: "pink", pstrServerVideoLink: rc.strServerVideoLink})#
	        </cfif>
	    </div>
	    <div class="col-md-3 col-sm-3 btn-view-area">
	        <a class="btn btn-success btn-pink-view no-border" href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>"> VIEW FULL <cfif image_set>SET<cfelse>VIDEO</cfif></a>
	    </div>
	</div>
</cfoutput>