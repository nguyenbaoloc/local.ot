<cfoutput>
<nav class="nav" >
    <div class="container">
        <div class="navbar-header homemenubrand">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="##homemenu" aria-expanded="false" aria-controls="homemenu">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse hide clhomemenu" id="homemenu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/index.cfm/members">HOME</a>
                </li>
                <li>
                    <a href="/index.cfm/gallery">GALLERY</a>
                </li>
                <li>
                    <a href="/index.cfm/model">MODELS</a>
                </li>
                <li>
                    <a href="/index.cfm/newforum.default">FORUMS</a>
                </li>
                <li>
                    <a href="/index.cfm/members.preferences">My OT</a>
                </li>
                <li role="presentation" class="dropdown subhomemenu">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="" role="button" aria-expanded="false">
                        EXTRAS <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="">Custom Shoots</a>
                        </li>
                        <li>
                            <a href="">Purchase Permanent FastTracks</a>
                        </li>
                        <li>
                            <a href="">Prestige Tease</a>
                        </li>
                        <li>
                            <a href="">Model of the Year</a>
                        </li>
                        <li>
                            <a href="">Behind the Scenes</a>
                        </li>
                        <li>
                            <a href="">OnlyTease Extras</a>
                        </li>
                        <li>
                            <a href="">OnlyTease Top Lists</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/index.cfm/search">SEARCH</a>
                </li>
                <li>
                    <a href="/index.cfm/comingsoon">COMING SOON</a>
                </li>
            </ul>
            <div class="mobile">
                <div class="">
                    <ul class="nav navbar-nav custom-xs-menu">
                        <li>
                            <a class="item active" href="http://www.onlytease.com/" id="tease" title="OnlyTease">VIP </a>
                        </li>
                        <li>
                            <a class="item" href="http://www.only-opaques.com/" id="opaque" title="OnlyOpaques">OO </a>
                        </li>
                        <li>
                            <a class="item" href="http://www.only-secretaries.com/" id="sec" title="OnlySecretaries">OS </a>
                        </li>
                        <li>
                            <a class="item" href="http://www.onlysilkandsatin.com/" id="silk" title="OnlySilkAndSatin">OSS </a>
                        </li>
                        <li>
                            <a class="item" href="http://www.onlyallsites.com/" id="vip" title="Vip.OnlyTease">OC </a>
                        </li>
                        <li>
                            <a class="item" href="http://www.onlyallsites.com/" id="melanie" title="OnlyMelanie">OM </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
</cfoutput>