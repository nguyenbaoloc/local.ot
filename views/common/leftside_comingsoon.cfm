<cfoutput>
	<div class="row bottom-45">
		<div id="comming-soon" class="col-md-12 left-side-block suggest-site comming-soon">
			<div class="panel-body left-side-gray-bg">
				<p class="textCenter text-normal-black">
					This is a list of the current planned releases over the next few weeks, we'll try to keep this page as updated as possible to let you know whats coming up.
				</p>
				<p class="textCenter text-normal-black">
					However you also have the ability to preview all fasttracks sets available using the dropdown menu. If you can't wait to see a set you can use one of your fasttracks to view the whole set ahead of it's release date.	
				</p>	
			</div>
		</div>
	</div>
	<!--- TWITTER FEED --->
    #view('common/twitter_widget')#
    <!--- END OF TWITTER FEED --->

    <!--- LATEST NEWS --->
    #view('common/latest_news', {data: rc.qryNews})#
    <!--- END OF LATEST NEWS --->
</cfoutput>