<cfoutput>
<cfinclude template="/views/members/otcams/getdata_otcams.cfm">
<nav class="nav navbar-default" id="tophead">
    <div class="container navbar-ipad desktop">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="##navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand ournetwork" href="/index.cfm">OUR NETWORK: </a>
        </div>
        <div class="navbar-inverse navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li>
                    <a class="active" href="http://www.onlytease.com/" id="tease" title="OnlyTease">ONLY TEASE </a>
                </li>
                <li>
                    <a class="" href="http://www.only-opaques.com/" id="opaque" title="OnlyOpaques">ONLY OPAQUES </a>
                </li>
                <li>
                    <a class="" href="http://www.only-secretaries.com/" id="sec" title="OnlySecretaries">ONLY SECRETARIES </a>
                </li>
                <li>
                    <a class="" href="http://www.onlysilkandsatin.com/" id="silk" title="OnlySilkAndSatin">ONLY SILK AND SATIN </a>
                </li>
                <li>
                    <a class="" href="http://www.onlyallsites.com/" id="vip" title="Vip.OnlyTease">VIP.ONLY TEASE </a>
                </li>
                <li>
                    <a class="" href="http://www.onlyallsites.com/" id="melanie" title="OnlyMelanie">ONLY MELANIE </a>
                </li>
                <li>
                    <a class="last-child" href="http://www.onlyallsites.com/" id="carla" title="OnlyCarla">ONLY CARLA </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<nav class="nav" id="camsticker">
    <div class="container">
        <div class="row clearfix" id="camslink">
            <div class="col-md-1 col-xs-1"></div>
            <div class="col-md-2 col-xs-2" id="logo_camsticker">
                <img src="/assets/img/logo_topmenu.png" class="img-responsive"/>
            </div>
            <div class="col-md-7 col-xs-7" id="statustext">
                
            </div>
            <div class="col-md-2 col-xs-2" id="btn_webcam">
                <a class="btn btn-success btn-xs" id="ctw"><b>Click To Watch</b></a>
            </div>
        </div>
    </div>
</nav>
<nav class="nav nav-bg" id="sociallinks">
    <div class="container">
        <div class="col-md-9 col-sm-6">
            <img src="/assets/img/logo.png" class="site-logo">
        </div>
        <div class="col-md-3 col-sm-6 listicon desktop">
            <ul class="list-inline ulSocialLink">
                <li><a href="https://twitter.com/onlyallsites"><i class="fa fa-2x fa-twitter"></i></a></li>
                <li><a href="https://www.facebook.com/onlyallsites"><i class="fa fa-2x fa-facebook"></i></a></li>
                <li><a href="https://www.youtube.com/user/OnlyAllsites1"><i class="fa fa-2x fa-youtube"></i></a></li>
                <li><a href="https://instagram.com/onlyallsites"><i class="fa fa-2x fa-instagram"></i></a></li>
                <li><a href="https://plus.google.com/wm/1/+Onlyallsites/posts"><i class="fa fa-2x fa-google-plus"></i></a></li>
            </ul>
        </div>
    </div>
</nav>
#view('common/main_menu')#
</cfoutput>
<script type="text/javascript" src="/assets/js/otcams/showdata_otcams.js"></script>
<script type="text/javascript" src="/assets/js/menu/menu_actions.js"></script>