<cfoutput>
	<cfif NOT image_set>
	    <cfif data.video_hd_res_name EQ "" AND data.video_mp4_hd_name EQ "">
	        <div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic">
	            <a href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#">
	                <img width="260" class="img-responsive" src="/images/v2/tour_images/#replace(data.tour_pic3_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
	                <div class="opacity-vid-pic">
	                	<p>
	                		<i class="fa fa-film"></i>
	                	</p>	
	                	<label>Video</label>
	                </div>
	            </a>
	        </div>
	        <div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic desktop">
	            <a href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#">
	                <img  class="img-responsive" src="/images/v2/mailing_list/#replace(data.mailing_list_pic_thumb,'tn','tn260')#"  alt="Click here to view this video" title="Click here to view this video">
	            </a>
	        </div>
	        <div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic desktop">
	            <a href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#">
	                <img  class="img-responsive" src="/images/v2/updates/tn260#data.updates_pic_thumb#" alt="Click here to view this video" title="Click here to view this video">
	            </a>
	        </div>
	    <cfelse>
	        <div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic">
	            <a href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#">
	                <img width="260" class="img-responsive" src="/images/v2/tour_images/#replace(data.tour_pic3_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
	                <div class="opacity-vid-pic">
	                	<p><i class="fa fa-film"></i></p>	
	                	<label>Video</label>
	                </div>
	            </a>
	        </div>
	        <div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic desktop">
	            <a href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#">
	                <img  class="img-responsive" src="/images/v2/mailing_list/#replace(data.mailing_list_pic_thumb,'tn','tn260')#"  alt="Click here to view this video" title="Click here to view this video">
	            </a>
	        </div>
	        <div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic desktop">
	            <a href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#">
	                <img  class="img-responsive" src="/images/v2/updates/tn260#data.updates_pic_thumb#" alt="Click here to view this video" title="Click here to view this video">
	            </a>
	        </div>
	    </cfif>
	</cfif>
	<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic release-pic-homepge'#">
	    <a href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#">
	        <img width="260" class="img-responsive" src="/images/v2/tour_images/#replace(data.tour_pic1_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
	    </a>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic release-pic-homepge'#">
	    <a href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#">
	        <img width="260" class="img-responsive" src="/images/v2/tour_images/#replace(data.tour_pic2_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
	    </a>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic release-pic-homepge #!findNoCase('ipad', cgi.http_user_agent,1) ? 'desktop' : ''#'#">
	    <a href="/index.cfm/model.view_gallery?gallery_id=#data.gallery_id#">
	        <img width="260" class="img-responsive" src="/images/v2/tour_images/#replace(data.tour_pic3_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
	    </a>
	</div>
</cfoutput>