<cfoutput>
	<h5 class="text-center desktop leftside-intro-text"><b>WELCOME TO THE MEMBERS AREA</b></h5>
	<div class="desktop">
		<div class="row">
		<cfif rc.qry_model.recordcount GT 0>
			<div id="detailModel-profile" class="col-md-12 detailModel-profile">
				<div class="panel-heading left-side-black-bg">
					<a href="/index.cfm/model.model_set?model_id=#model_id#">#rc.qry_model.model_display_name#</a>
				</div>	
				<div class="panel-border-bottom"></div>
				<div class="panel-body left-side-gray-bg">
					<div class="content-image">
						<img class="picLeftModel" src="http://members.onlytease.com/tour/images/models/260x390/#replace(rc.qry_model.model_display_name,' ','-','ALL')#_cover_#model_id#.jpg"/>
					</div>	
					<div class="titleList">Model Options</div>
					<ul class="list-unstyled profile-list modelProfile">
						<li>
							<cfif rc.favourite_gallery>
								<a href="javascript:ConfirmDelete('#model_id#');">
									<i class="fa fa-check-circle green"></i>Remove From Favourite Models
								</a>
							<cfelse>
								<a href="http://members.onlytease.com/members/processes/favourites_functions.cfm?action=addmodel&id=#model_id#&CurrentPage=#url.CurrentPage#">
									<i class="fa fa-check-circle green"></i>Add To My Favourite Models
								</a>
							</cfif>
						</li>
						<li>
							<a href="http://members.onlytease.com/members/preferences.cfm?type=favourites">
								<i class="fa fa-plus-circle yellow"></i>All Favourite Models/Sets
							</a>
						</li>
						<li>
							<a href="http://members.onlytease.com/members/preferences.cfm">
								<i class="fa fa-question-circle blue"></i>My Gallery Preferences
							</a>
						</li>
						<li>
							<a href="/index.cfm/model.model_set?model_id=#model_id#">
								<i class="fa fa-info-circle"></i>Return to this models sets
							</a>
						</li>
					</ul>
					<div class="titleList">Highest Voted Gallery</div>
					<p class="ratingPoint">
						#NumberFormat("#rc.top_gallery.average_combined#", "0.0")# / 10
					</p>
					<div class="content-image">
						<a href="/index.cfm/model.view_gallery?gallery_id=#rc.top_gallery.gallery_id#">
							<img class="picLeftModel" src="/images/v2/tour_images/#replace(rc.top_gallery.tour_pic1_thumb,'tn','tn260')#"/>
						</a>
					</div>	
				</div>
			</div>
		</cfif>
		</div>
		<div class="row suggestModelSet">
			#view("common/suggest_model_site_form", { rc.qrymodel: rc.qry_model})#
		</div>
	</div>
	<div class="mobile">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-6">
				<div id="detailModel-profile" class="col-md-12 detailModel-profile">
					<div class="panel-heading left-side-black-bg">
						<a href="/index.cfm/model.model_set?model_id=1335">Sarah Turner</a>
					</div>	
					<div class="panel-border-bottom"></div>
					<div class="panel-body left-side-gray-bg">
						<div class="content-image">
							<img class="picLeftModel" src="http://members.onlytease.com/tour/images/models/260x390/Sarah-Turner_cover_1335.jpg">
						</div>	
						<div class="titleList">Model Options</div>
						<ul class="list-unstyled profile-list modelProfile">
							<li>
								<a href="http://members.onlytease.com/members/processes/favourites_functions.cfm?action=addmodel&amp;id=1335&amp;CurrentPage=1">
									<i class="fa fa-check-circle green"></i>Add To My Favourite Models
								</a>								
							</li>
							<li>
								<a href="http://members.onlytease.com/members/preferences.cfm?type=favourites">
									<i class="fa fa-plus-circle yellow"></i>All Favourite Models/Sets
								</a>
							</li>
							<li>
								<a href="http://members.onlytease.com/members/preferences.cfm">
									<i class="fa fa-question-circle blue"></i>My Gallery Preferences
								</a>
							</li>
							<li>
								<a href="/index.cfm/model.model_set?model_id=1335">
									<i class="fa fa-info-circle"></i>Return to this models sets
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6">
				<div class="blockPic-rating">
					<div class="titleList">Highest Voted Gallery</div>
					<p class="ratingPoint">
						0.0 / 10
					</p>
					<div class="content-image">
						<a href="/index.cfm/model.view_gallery?gallery_id=17692">
							<img class="picLeftModel" src="/images/v2/tour_images/tn26014224-l38x0m0Qur.jpg">
						</a>
					</div>	
				</div>
				<div class="suggestModelSet">
					<div id="suggest-site" class="col-md-12 suggest-site view-gallery-suggest-site">
						<div class="panel-body left-side-gray-bg">							
							<p class="text-pink">Do You Know Of Another Site Where Sarah Turner Appears?</p>
							<p class="text-normal-black">Tell us where else you've seen Sarah Turner and you can win a fast-track</p>
							<div class="col-md-8 col-sm-8 col-xs-8 btn-view-area btn-suggest-modelSet div_suggestButton">
								<a href="" class="btn btn-success btn-pink-view no-border" id="div_suggestButton">Suggest Site</a>
							</div>
							<div id="div_suggestForm" class="col-md-12 col-sm-12 col-xs-12">
								<form action="/members/suggest_model_site.cfm" method="post" onsubmit="return valForm(this);">
									<input type="Hidden" name="model_id" value="1335">
									<input type="Hidden" name="model_name" value="Sarah Turner">
									<input type="Hidden" name="model_shortcut" value="/Sarah-Turner">
									<input type="Hidden" name="gallery_id" value="0">
									<div class="row form-group">
										<label class="col-sm-5 col-xs-5">Site name:</label>
										<input type="Text" name="site_name" value="" class="col-sm-7 col-xs-7">
									</div>
									<div class="row form-group">
										<label class="col-sm-5 col-xs-5">Site URL:</label>
										<input type="Text" name="site_url" value="" class="col-sm-7 col-xs-7">
									</div>
									<div class="row form-group">
										<label class="col-sm-5 col-xs-5">Model name:</label>
										<input type="Text" name="model_aka" value="if not Sarah Turner" class="col-sm-7 col-xs-7" onfocus="clearField(this);">
									</div>
									<div class="row form-group">
										<input type="Submit" value="Add" class="btn btn-success btn-pink-view no-border">
										<input type="Button" value="Cancel" class="btn btn-success btn-pink-view no-border" id="btn-cancel-suggest">
									</div>
								</form>
							</div>
							<div class="clearfix"></div>
							<p class="text-normal-black">And don't forget, you can join these sites and earn points at <br><a href="http://www.porn-rewards.com/" target="_blank">Porn-Rewards.com</a></p>
							<p><a href="http://www.porn-rewards.com/" target="_blank"><img src="../images/200x50-pr.jpg" class="img-responsive width100percen" border="0"></a></p>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</cfoutput>