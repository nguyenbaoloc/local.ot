<cfoutput>
	<div class="row bottom-45 lastNews">
        <div class="col-md-12 block-lastNew left-side-block">
            <div class="panel-heading left-side-black-bg">
                LATEST NEWS
            </div>
            <div class="panel-body left-side-grey-bg carousel slide" id="carousel-latest-news">
                <!-- Indicators -->
                <ol class="carousel-indicators dots_carousel desktop">
                    <cfif findNoCase('Android', cgi.http_user_agent,1) or findNoCase('iPhone', cgi.http_user_agent,1)>
                        <cfloop from="1" to="#data.recordcount#" index="i">
                            <li data-target="##carousel-latest-news" data-slide-to="#i-1#" class="#(i-1 eq 0) ? 'active' : ''#"></li>
                        </cfloop>
                    <cfelse>
                        <li data-target="##carousel-latest-news" data-slide-to="0" class="active"></li>
                        <li data-target="##carousel-latest-news" data-slide-to="1"></li>
                        <li data-target="##carousel-latest-news" data-slide-to="2"></li>
                    </cfif>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <cfset flag = 1>
                    <cfif data.recordcount GT 0>
                        <cfloop query="data">
                            <cfif flag eq 1>
                                <div class="item active">
                                    <div class="row">
                            </cfif>
                            <article class="article_content">
                                <div class="row">
                                    <div class="col-md-4 col-xs-4 text-center calendarItem">
                                        <p class="news_date">#DateFormat(data.item_release_date, "d")#</p>
                                        <p class="text-center custom-month-canler">#rc.arr_month[DateFormat(data.item_release_date, "mm")].name#</p>
                                    </div>
                                    <div class="col-md-8 col-xs-8 detailLastNews">                                   
                                        <p class="text-title-lastNew">#data.item_content#</p>
                                        <cfif data.item_optional_link NEQ "">
                                            <a class="custom-style-a readMore" href="#data.item_optional_link#" target="#data.item_link_target#">Read More <i class="fa fa-angle-right"></i></a>
                                        </cfif>
                                    </div>
                                </div>
                                <cfif findNoCase('Android', cgi.http_user_agent,1) or findNoCase('iPhone', cgi.http_user_agent,1)>
                                    
                                <cfelse>
                                    #(flag%3 != 0) ? '<hr/>' : ''#
                                </cfif>
                            </article>
                            <cfif findNoCase('Android', cgi.http_user_agent,1) or findNoCase('iPhone', cgi.http_user_agent,1)>
                                <cfif flag neq data.recordcount>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="row">
                                </cfif>
                            <cfelse>
                                <cfif flag%3 eq 0 and flag neq data.recordcount>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="row">
                                </cfif>
                            </cfif>
                            <cfif flag eq data.recordcount>
                                    </div>
                                </div>
                            </cfif>
                            <cfset flag += 1>
                        </cfloop>
                        <cfif data.recordcount LTE 5><cfset rc.display_random = rc.display_random + 1></cfif>

                    <cfelse>
                        <cfset rc.display_random = rc.display_random + 2>
                    </cfif>
                    <cfif rc.display_random GTE 1>
                        <div class="marginRandom">
                            <cfinclude template="/views/members/random_model.cfm">
                        </div>
                    </cfif>
                    <cfif rc.display_random GTE 1>
                        <div class="marginRandom">
                            <cfinclude template="/views/members/random_gallery.cfm">
                        </div>
                    </cfif>
                </div>
            </div>
        </div>
    </div>
</cfoutput>