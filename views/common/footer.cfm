<cfoutput>
	#view('common/main_menu')#
	<nav class="nav paddingTop mobile">
		<ul id="nav-icon-footer" class="list-inline paddingRight list-inline-footer">
            <li><a href="https://twitter.com/onlyallsites"><i class="fa fa-2x fa-twitter"></i></a></li>
            <li><a href="https://www.facebook.com/onlyallsites"><i class="fa fa-2x fa-facebook"></i></a></li>
            <li><a href="https://www.youtube.com/user/OnlyAllsites1"><i class="fa fa-2x fa-youtube"></i></a></li>
            <li><a href="https://instagram.com/onlyallsites"><i class="fa fa-2x fa-instagram"></i></a></li>
            <li><a href="https://plus.google.com/wm/1/+Onlyallsites/posts"><i class="fa fa-2x fa-google-plus"></i></a></li>
        </ul>
	</nav>
    <nav class="nav footer">
    	<div class="container">
	        <div class="col-md-9 col-sm-12 footer_info">
	            <p>To contact us, email info@onlyallsites.com<br/>
	            OT Publishing 2002 - 2014 | Webmasters | Privacy Policy | 18 U.S.C.2257 Record-Keeping Requirements Compliance Statement | Cancel my Membership | Lost my Password | Index</p>
	        </div>
	        <div class="col-md-3 col-sm-12 footer_sociallinks desktop">
	    	 	<ul class="list-inline icon-footer ulSocial">
	                <li><a href="https://twitter.com/onlyallsites"><i class="fa fa-2x fa-twitter"></i></a></li>
	                <li><a href="https://www.facebook.com/onlyallsites"><i class="fa fa-2x fa-facebook"></i></a></li>
	                <li><a href="https://www.youtube.com/user/OnlyAllsites1"><i class="fa fa-2x fa-youtube"></i></a></li>
	                <li><a href="https://instagram.com/onlyallsites"><i class="fa fa-2x fa-instagram"></i></a></li>
	                <li><a href="https://plus.google.com/wm/1/+Onlyallsites/posts"><i class="fa fa-2x fa-google-plus"></i></a></li>
	            </ul>
	    	</div>
    	</div>
    </nav>

    
</cfoutput>