<cfoutput>
	<h5 class="text-center desktop textWellcomeModel">
		<b>WELCOME TO THE MEMBERS AREA</b>
	</h5>

	<div class="row modelsetLeft">
		<div id="detail-models" class="col-md-12 left-side-block detail-models">
			<div class="panel-heading left-side-black-bg">
				<a href="/index.cfm/model.model_set?model_id=#url.model_id#">#rc.qrymodel.model_display_name#</a>
			</div>
			<div class="panel-border-bottom"></div>
			<div class="panel-body left-side-gray-bg">
				<div class="content-image">
					<img src="http://members.onlytease.com/tour/images/models/260x390/#replace(rc.qrymodel.model_display_name,' ','-','ALL')#_cover_#model_id#.jpg" class="picLeftModel" alt="" title="" border="0" class="img-responsive"/>
				</div>
				<p class="caption-left-model-set">
					<span class="">
						<i class="fa fa-camera"></i>
						#rc.qrymodel.image_count GT 0 ? NumberFormat(rc.qrymodel.image_count,"9,999") : 0#
					</span>
					<span class="border-left">
						<i class="fa fa-th-list"></i>
						#rc.qrymodel.video_count GT 0 ? NumberFormat(rc.qrymodel.video_count,"9,999") : 0#
					</span>
				</p>
				<div class="vital-stats">
					<h6>vital stats</h6>
					<div class="row detail-list">
						<div class="col-md-6 col-sm-6 col-xs-6 detail-list-name">
							<p>Bust</p>
							<p>Waist</p>
							<p>Hips</p>
							<p>Height</p>
							<p>Stat sign</p>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 detail-list-number">
							<p>#rc.qrymodel.model_bust#</p>
							<p>#rc.qrymodel.model_waist#</p>
							<p>#rc.qrymodel.model_hips#</p>
							<p>#rc.qrymodel.model_height NEQ "" ? rc.qrymodel.model_height : 'N/A'#</p>
							<p>#rc.qrymodel.model_star_sign NEQ "" ? rc.qrymodel.model_star_sign : 'N/A'#</p>
						</div>
					</div>
				</div>
				<cfif rc.qrymodel.model_random_fact NEQ "">
					<div class="fland-fact">
						<p class="title">Random fact</p>
						<p class="col-md-10 col-sm-10 col-xs-10 text">
							#rc.qrymodel.model_random_fact#
						</p>
					</div>
				</cfif>
			</div>
		</div>	
		<div id="read-profile" class="col-md-12 left-side-block read-profile">
			<div class="panel-heading left-side-pink-bg">
				<a href="/index.cfm/model.model_profile?model_id=#url.model_id#">READ HER FULL PROFILE <i class="fa fa-long-arrow-right"></i></a>
			</div>
			<div class="panel-body left-side-gray-bg">
				<ul class="list-unstyled profile-list">
					<li><a href=""><i class="fa fa-check-circle green"></i>Add To My Favourite Models</a></li>
					<li><a href=""><i class="fa fa-plus-circle yellow"></i>All Favourite Models/Sets</a></li>
					<li><a href=""><i class="fa fa-question-circle blue"></i>My Gallery Preferences</a></li>
					<li><a href=""><i class="fa fa-info-circle"></i>Add reminder</a></li>
				</ul>
			</div>
		</div>		
	</div>

	<!--- <div class="row">
		<div id="suggest-site" class="col-md-12 left-side-block suggest-site">
			<div class="panel-body left-side-gray-bg">
				<h3>#rc.qrymodel.model_display_name# ALSO APPEARS ON THE FOLLOWING SITES</h3>

				<cfif rc.qry_modelLinks.recordcount GT 0>
					<cfloop query="rc.qry_modelLinks">
						<cfset str_display = Iif(rc.qry_modelLinks.CurrentRow GT 50, "'none'", "'block'")>
						<div id="div_otherSite_#rc.qry_modelLinks.CurrentRow#" style="display:#str_display#;">
							<div>
								<a href="#rc.qry_modelLinks.ml_affiliate_url#" target="_blank"><img src="http://www.porn-rewards.com/images/site_images/thumb2_#rc.qry_modelLinks.sponsor_site_img_name#" class="img-responsive borderCCC" border="0" /></a>
							</div>
							<div class="clearfix"></div>
							<p>
								<a class="text-underline" href="#rc.qry_modelLinks.ml_affiliate_url#" target="_blank"><b>#rc.qry_modelLinks.sponsor_site_name#</b></a>
								<cfif rc.qry_modelLinks.ml_model_name IS NOT "">
									<div class="textLeftModelItalic">(as #rc.qry_modelLinks.ml_model_name#)</div>
								</cfif>
							</p>
						</div>
					</cfloop>
					<cfif rc.qry_modelLinks.RecordCount GT 50>
						<div>
							<input type="Hidden" value="#rc.qry_modelLinks.RecordCount#" id="hdModelLinkCount">
							<input id="div_otherSitesLabel" type="Button" value="Show More" class="searchButton width120" />
						</div>
					</cfif>
				</cfif>
				
				<p class="text-pink">Do You Know Of Another Site Where #rc.qrymodel.model_display_name# Appears?</p>
				<p class="text-normal-black">Tell us where else you've seen #rc.qrymodel.model_display_name# and you can win a fast-track</p>
				<div class="col-md-8 col-sm-8 col-xs-8 btn-view-area btn-suggest-modelSet">
					<a href="" class="btn btn-success btn-pink-view no-border">Suggest Site</a>
				</div>
				<div class="clearfix"></div>
				<p class="text-normal-black">And don't forget, you can join these sites and earn points at <br><a href="http://www.porn-rewards.com/" target="_blank">Porn-Rewards.com</a></p>
				<p><a href="http://www.porn-rewards.com/" target="_blank"><img src="../images/200x50-pr.jpg" class="img-responsive width100percen" border="0"></a></p>
			</div>
		</div>
	</div> --->
	<div class="row suggestModelSet">
		#view("common/suggest_model_site_form")#
	</div>
	#view('common/search_model', {letter_list: rc.letter_list})#
</cfoutput>