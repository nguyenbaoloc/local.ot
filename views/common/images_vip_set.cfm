<cfoutput>
	<cfif NOT image_set>
	    <cfif data.video_hd_res_name NEQ "">
	        <div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic">
	        	<cfif session.bolVerifiedVIP EQ 1>
					<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
				<cfelse>
					<a href="#application.vip_location#/error.cfm" target="_blank">
				</cfif>
						<img src="#application.vip_location#/images/mailing_list/#replace(data.mailing_list_pic_thumb,'tn','tn260')#" class="img-responsive" border="0" alt="Click here to view this video" title="Click here to view this video">
					</a>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic desktop">
				<cfif session.bolVerifiedVIP EQ 1>
					<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
				<cfelse>
					<a href="#application.vip_location#/error.cfm" target="_blank">
				</cfif>
					<img src="#application.vip_location#/images/updates/tn260#data.updates_pic_thumb#" class="img-responsive" border="0" alt="Click here to view this video" title="Click here to view this video">
				</a>
			</div>
		</cfif>
		<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic'#">
			<cfif session.bolVerifiedVIP EQ 1>
				<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
			<cfelse>
				<a href="#application.vip_location#/error.cfm" target="_blank">
			</cfif>
					<img src="#application.vip_location#/images/mailing_list/#replace(data.mailing_list_pic_thumb,'tn','tn260')#" class="img-responsive" border="0" alt="Click here to view this video" title="Click here to view this video">
				</a>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-6 release-vid-pic desktop">
			<cfif session.bolVerifiedVIP EQ 1>
				<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
			<cfelse>
				<a href="#application.vip_location#/error.cfm" target="_blank">
			</cfif>
				<img src="#application.vip_location#/images/updates/tn260#data.updates_pic_thumb#" class="img-responsive" border="0" alt="Click here to view this video" title="Click here to view this video">
			</a>
		</div>
	</cfif>
	<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic release-pic-homepge'#">
		<cfif session.bolVerifiedVIP EQ 1>
			<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
		<cfelse>
			<a href="#application.vip_location#/error.cfm" target="_blank">
		</cfif>
		<img width="260" class="img-responsive" src="#application.vip_location#/images/tour_images/#replace(data.tour_pic1_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
			</a>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic release-pic-homepge'#">
		<cfif session.bolVerifiedVIP EQ 1>
			<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
		<cfelse>
			<a href="#application.vip_location#/error.cfm" target="_blank">
		</cfif>
				<img width="260" class="img-responsive" src="#application.vip_location#/images/tour_images/#replace(data.tour_pic2_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
			</a>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-6 #NOT image_set ? 'release-vid-pic' : 'release-pic release-pic-homepge'# #image_set ? 'desktop' : ''#">
		<cfif session.bolVerifiedVIP EQ 1>
			<a href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank">
		<cfelse>
			<a href="#application.vip_location#/error.cfm" target="_blank">
		</cfif>
				<img width="260" class="img-responsive" src="#application.vip_location#/images/tour_images/#replace(data.tour_pic3_thumb,'tn','tn260')#" alt="Click here to view this <cfif image_set>set<cfelse>video</cfif>" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
			</a>
	</div>
</cfoutput>