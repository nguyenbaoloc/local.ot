<cfoutput>
    <cfif ListFind(application.stc_set_types.str_videos, data.gallery_type)>
        <cfset image_set = FALSE>
    <cfelse>
        <cfset image_set = TRUE>
    </cfif>
	<div class="row model-info-pink model-info-yellow">
	    <div class="row col-md-9 col-sm-9 row-info-pink row-vip-no-border">
	        <p>
	            <label class="model-name model-name-vip">
	                #model_names#<cfif data.gallery_type EQ 21> - Guest Gallery</cfif>
	            </label> 
	            <span class="border-left border-left-vip">
	            	<cfif image_set>
	            		<i class="fa fa-camera"></i> #NumberFormat(data.image_count,"9,999")#
	            	<cfelse>
	            		<i class="fa fa-film"></i> Video length: #data.video_length#
	            	</cfif>
	            </span>
	        </p>
	        <p>
	        	<img src="#application.vip_location#/images/vip-include-logo.jpg" width="194" height="37">
	        </p>
	    </div>
	    <div class="col-md-3 col-sm-3 btn-view-area btn-view-vip">
			<cfif session.bolVerifiedVIP EQ 1>
				<a class="btn btn-success btn-pink-view no-border btn-vipContent" href="#application.vip_location#/members/view_gallery.cfm?gallery_id=#data.gallery_id#" target="_blank" title="Click here to view this <cfif image_set>set<cfelse>video</cfif>">
			<cfelse>
				<a href="#application.vip_location#/error.cfm" target="_blank">
			</cfif>View this <cfif image_set>Set<cfelse>Video</cfif></a>
	    </div>
	</div>
</cfoutput>