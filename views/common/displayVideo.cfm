<cfoutput>
	<cfset data = rc.UIHelperService.displayVideo(qry_sets: pdata, is_bts: pis_bts, bts_id: pbts_id, strServerVideoLink: pstrServerVideoLink)>
	<p class="desktop">
		<cfif pis_bts EQ "bts">
			<label class="xl-size size-text">Behind the scenes video available for download:</label>
		<cfelse>
			<label class="xl-size size-text">Video available for download:</label>
		</cfif>
		<cfif pdata.video_hd_res_name NEQ "">
			<cfloop from="1" to="#pdata.video_no_clips#" index="i">
				<cfset k_size = 0>
				<cftry>
					<cfset k_size = Trim(replaceNoCase(pdata.video_hd_res_filesize,"mb","")) * 1000>
					<cfcatch type="Any"><cfset k_size = 0></cfcatch>
				</cftry>
	    		<label class='u-xl-size'><i class='fa fa-download #pcolor#'></i>
	            	<a class="xl-size size-text" href="#pstrServerVideoLink#/members/#pdata.video_directory_name#/#pdata.video_hd_res_name##i#.#pdata.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#pdata.video_hd_res_name##i#.#pdata.video_format#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">HD Resolution</a>
	            </label>
			</cfloop>
		<cfelse>
			<cfif pdata.video_medium_res_name NEQ "">
				<cfloop from="1" to="#pdata.video_no_clips#" step="1" index="j">
					<cftry>
					<cfset k_size = Trim(replaceNoCase(pdata.video_medium_res_filesize,"mb","")) * 1000>
						<cfcatch type="Any"><cfset k_size = 0></cfcatch>
					</cftry>
					<label class='u-xl-size'><i class='fa fa-download #pcolor#'></i>
						<a class="xl-size size-text" href="#pstrServerVideoLink#/members/#pdata.video_directory_name#/#pdata.video_medium_res_name##j#.#pdata.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#pdata.video_medium_res_name##j#.#pdata.video_format#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">Medium Resolution</a>
					</label>
				</cfloop>
			</cfif>
			<cfif pdata.video_low_res_name NEQ "">
				<cfloop from="1" to="#pdata.video_no_clips#" step="1" index="l">
					<cftry>
					<cfset k_size = Trim(replaceNoCase(pdata.video_low_res_filesize,"mb","")) * 1000>
						<cfcatch type="Any"><cfset k_size = 0></cfcatch>
					</cftry>
					<label class='u-xl-size'><i class='fa fa-download #pcolor#'></i>
						<a class="xl-size size-text" href="#pstrServerVideoLink#/members/#pdata.video_directory_name#/#pdata.video_low_res_name##l#.#pdata.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#pdata.video_low_res_name##l#.#pdata.video_format#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">Low Resolution</a>
					</label>
				</cfloop>
			</cfif>
			<cfif pdata.video_mp4_hd_name NEQ "">
				<cfloop from="1" to="#pdata.video_no_clips#" step="1" index="n">
					<cftry>
					<cfset k_size = Trim(replaceNoCase(pdata.video_mp4_hd_filesize,"mb","")) * 1000>
						<cfcatch type="Any"><cfset k_size = 0></cfcatch>
					</cftry>
					<label class='u-xl-size'><i class='fa fa-download #pcolor#'></i>
						<a class="xl-size size-text" href="#pstrServerVideoLink#/members/#pdata.video_directory_name#/#pdata.video_mp4_hd_name##n#.#pdata.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#pdata.video_mp4_hd_name##n#.#pdata.video_format#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">HD Resolution</a>
					</label>
				</cfloop>
			</cfif>
			<cfif pdata.video_mp4_sd_name NEQ "">
				<cfloop from="1" to="#pdata.video_no_clips#" step="1" index="m">
					<cftry>
					<cfset k_size = Trim(replaceNoCase(pdata.video_mp4_sd_filesize,"mb","")) * 1000>
						<cfcatch type="Any"><cfset k_size = 0></cfcatch>
					</cftry>
					<label class='u-xl-size'><i class='fa fa-download #pcolor#'></i>
						<a class="xl-size size-text" href="#pstrServerVideoLink#/members/#pdata.video_directory_name#/#pdata.video_mp4_sd_name##m#.#pdata.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#pdata.video_mp4_sd_name##m#.#pdata.video_format#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">SD Resolution</a>
					</label>
				</cfloop>
			</cfif>
		</cfif>
		<cfif pdata.video_high_res_name NEQ "">
			<cfloop from="1" to="#pdata.video_no_clips#" step="1" index="j">
				<cftry>
				<cfset k_size = Trim(replaceNoCase(pdata.video_high_res_filesize,"mb","")) * 1000>
					<cfcatch type="Any"><cfset k_size = 0></cfcatch>
				</cftry>
				<label class='u-xl-size'><i class='fa fa-download #pcolor#'></i>
					<a class="xl-size size-text" href="#pstrServerVideoLink#/members/#pdata.video_directory_name#/#pdata.video_high_res_name##j#.#pdata.video_format#" title="Right click link and download video to your harddrive." onclick="pageTracker._trackEvent('Video','Download','#pdata.video_high_res_name##j#.#pdata.video_format#');pageTracker._trackEvent('User','Download',remoteuser,#k_size#);">High Resolution</a>
				</label>
			</cfloop>
		</cfif>
		
	</p>
</cfoutput>