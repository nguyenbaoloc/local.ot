<cfoutput>
<cfset arr_length = ArrayLen(data.arr_otherDaysSets)>
<cfif arr_length NEQ 0>
    <div class="row clearfix daily-strip">
        <div class="col-md-12 column padding-0">
            <div class="tabbable">
                <ul class="nav nav-tabs daily-strip-tap-menu desktop daily-tabmenu-desktop">
                    <cfif showvipset>
                        <li class="col-md-3 tab-action padding-0">
                            <a href="##tab-#data.str_releaseDate#-#data.uui_release#-vip" data-toggle="tab"><i class="fa fa-star fa-1"></i>&nbsp;&nbsp; VIP &nbsp;&nbsp;<i class="fa fa-star fa-1"></i></a>
                        </li>
                    </cfif>
                    <cfset list_nametab = ""/>
                    <cfloop from="1" to="#arr_length#" index="int_site_index">
                        <cfset nametab = data.arr_otherDaysSets[int_site_index].str_standsforsite>
                        <li class="col-md-#showvipset? '3' : '4'# #nametab# tab-action padding-0">
                            <a href="##tab-#data.str_releaseDate#-#data.uui_release#-#nametab#" data-toggle="tab" class="#(nametab eq 'OO') ? 'tab-active' : ''#">#data.arr_otherDaysSets[int_site_index].str_site#</a>
                        </li>
                        <cfif nametab neq "OO">
                            <cfset list_nametab &= "#data.str_releaseDate#-#data.uui_release#-#nametab#,"/>
                        </cfif>
                    </cfloop>
                </ul>
                <ul class="nav nav-tabs daily-strip-tap-menu mobile daily-tabmenu-ipad">
                    <cfif showvipset>
                        <li class="col-xs-3 tab-action padding-0">
                            <a href="##tab-#data.str_releaseDate#-#data.uui_release#-vip" data-toggle="tab">VIP</a>
                        </li>
                    </cfif>
                    <cfloop from="1" to="#arr_length#" index="int_site_index">
                        <cfset nametab = data.arr_otherDaysSets[int_site_index].str_standsforsite>
                        <li class="col-xs-#showvipset? '3' : '4'# #nametab# tab-action padding-0">
                            <a href="##tab-#data.str_releaseDate#-#data.uui_release#-#nametab#" data-toggle="tab" class="#(nametab eq 'OO') ? 'tab-active' : ''#">#nametab#</a>
                        </li>
                    </cfloop>
                </ul>    
                <div class="tab-content dailystrip-content">
                    <cfif showvipset>
                        <div class="tab-pane" id="tab-#data.str_releaseDate#-#data.uui_release#-vip">
                            <cfset dataVip = rc.UIHelperService.udf_Vipsets(dat_release:data.dat_release,uui_release:data.uui_release)>
                            #view('common/udf_Vipsets', {data: dataVip})#
                        </div>
                    </cfif>
                    <cfloop from="1" to="#arr_length#" index="int_site_index">
                        <cfset str_standsforsite = data.arr_otherDaysSets[int_site_index].str_standsforsite/>
                        <div class="tab-pane #(int_site_index == 1) ? 'active' : ''#" id='tab-#data.str_releaseDate#-#data.uui_release#-#str_standsforsite#'>
                            <div class="row">
                                <div class="col-md-12 col-lg-offset-2 col-sm-offset-1 dailystrip-children-row"><div class="tab-release-date">SETS RELEASED ON OUR OTHER SITES, #DateFormat(data.dat_release, "d mmmm yyyy")#.</div></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-offset-2 dailystrip-children-row">
                                        <ul class="set-#data.str_releaseDate#-#data.uui_release#-#str_standsforsite#">
                                            <cfswitch expression="#data.arr_otherDaysSets[int_site_index].str_standsforsite#">
                                                <cfcase value="OO">
                                                    <cfloop from="1" to="#arraylen(data.arr_OO)#" index="item">
                                                        <li>
                                                            <a href="#data.arr_OO[item].link#">
                                                                <img alt="" src="#replace(data.arr_OO[item].image,'tn','tn120')#" class="dailystrip-sample-image">
                                                                <cfif ListFind(application.video_set_types, data.arr_OO[item].gallery_type)>
                                                                    <div class="box-pic-height">
                                                                        <p class="box-pic-icon">
                                                                            <i class="fa fa-film"></i>
                                                                        </p>
                                                                        <p class="box-pic-text">Video</p>
                                                                    </div>
                                                                </cfif>
                                                            </a>
                                                        </li>
                                                    </cfloop>
                                                </cfcase>
                                                <cfcase value="OS">
                                                    <cfloop from="1" to="#arraylen(data.arr_OS)#" index="item">
                                                       <li>
                                                            <a href="#data.arr_OS[item].link#">
                                                                <img alt="" src="#replace(data.arr_OS[item].image,'tn','tn120')#" class="dailystrip-sample-image">
                                                                <cfif ListFind(application.video_set_types, data.arr_OS[item].gallery_type)>
                                                                    <div class="box-pic-height">
                                                                        <p class="box-pic-icon">
                                                                            <i class="fa fa-film"></i>
                                                                        </p>
                                                                        <p class="box-pic-text">Video</p>
                                                                    </div>
                                                                </cfif>
                                                            </a>
                                                        </li>
                                                    </cfloop>
                                                </cfcase>
                                                <cfcase value="OSS">
                                                    <cfloop from="1" to="#arraylen(data.arr_OSS)#" index="item">
                                                        <li>
                                                            <a href="#data.arr_OSS[item].link#">
                                                                <img alt="" src="#replace(data.arr_OSS[item].image,'tn','tn120')#" class="dailystrip-sample-image">
                                                                <cfif ListFind(application.video_set_types, data.arr_OSS[item].gallery_type)>
                                                                    <div class="box-pic-height">
                                                                        <p class="box-pic-icon">
                                                                            <i class="fa fa-film"></i>
                                                                        </p>
                                                                        <p class="box-pic-text">Video</p>
                                                                    </div>
                                                                </cfif>
                                                            </a>
                                                        </li>
                                                    </cfloop>
                                                </cfcase>
                                            </cfswitch>
                                        </ul>
                                </div>
                            </div>
                        </div>
                        <!--- generate slider script --->
                        <cfif str_standsforsite neq "OO">
                            <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    var maxslides = 4;
                                    if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                                        maxslides = 2;
                                    }
                                    $('a[href="##tab-#data.str_releaseDate#-#data.uui_release#-#str_standsforsite#"]').one('shown.bs.tab', function (e) {
                                        $('.set-#data.str_releaseDate#-#data.uui_release#-#str_standsforsite#').bxSlider({
                                            infiniteLoop: false,
                                            hideControlOnEnd: true,
                                            pager: false,
                                            minSlides: 1,
                                            maxSlides: maxslides,
                                            slideWidth: 125,
                                            slideMargin: 10
                                        });
                                    });

                                    $('a[href="##tab-#data.str_releaseDate#-#data.uui_release#-vip"]').css("color","##FFA500");
                                    $('a[href="##tab-#data.str_releaseDate#-#data.uui_release#-OS"]').css("color","##662E91");
                                    $('a[href="##tab-#data.str_releaseDate#-#data.uui_release#-OSS"]').css("color","##9CBA62");
                                    $('.set-#data.str_releaseDate#-#data.uui_release#-OS > li').each(function() {
                                        $(this).css("border","1px solid ##662E91");
                                    });
                                    $('.set-#data.str_releaseDate#-#data.uui_release#-OSS > li').each(function() {
                                        $(this).css("border","1px solid ##9CBA62");
                                    });
                                });
                            </script>
                        </cfif>
                    </cfloop>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var maxslides = 4;
            if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                maxslides = 2;
            }

            $('.set-#data.str_releaseDate#-#data.uui_release#-OO').bxSlider({
                infiniteLoop: false,
                hideControlOnEnd: true,
                pager: false,
                minSlides: 1,
                maxSlides: maxslides,
                slideWidth: 125,
                slideMargin: 10
            });

            $('##tab-#data.str_releaseDate#-#data.uui_release#-OS').attr("color","##662E91");
            $('##tab-#data.str_releaseDate#-#data.uui_release#-OSS').attr("color","##9CBA62");
            $('##tab-#data.str_releaseDate#-#data.uui_release#-OO').attr("color","##EE008E");
            $('a[href="##tab-#data.str_releaseDate#-#data.uui_release#-OO"]').css("color","##EE008E");
            $('.set-#data.str_releaseDate#-#data.uui_release#-OO > li').each(function() {
                $(this).css("border","1px solid ##EE008E");
            });
            $('.tab-content').css("border-bottom-color","##EE008E");
            $('##tab-#data.str_releaseDate#-#data.uui_release#-vip').attr("color","##FFA500");
        });
    </script>
</cfif>
</cfoutput>