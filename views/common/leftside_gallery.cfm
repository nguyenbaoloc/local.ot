<cfoutput>
	<div class="row">
		<div id="current-month" class="col-md-12 left-side-block current-month">
			<div class="panel-heading text-center left-side-pink-bg">
				CURRENT MONTH
			</div>
			<div class="panel-body left-side-gray-bg">
				<div class="text-center text-underline">
					<a href="/index.cfm/gallery.default?m=#month(now())#&y=#year(now())#&view=#URL.view#">#DateFormat(now(),"mmmm yyyy")#</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div id="previous-month" class="col-md-12 left-side-block previous-month">
			<div class="panel-heading text-center left-side-pink-bg">
				PREVIOUS MONTHS
			</div>
			<div class="panel-body left-side-gray-bg">
				<div class="text-center text-underline desktop">
					<div class="table-responsive">
						<table id="calendar" class="table">
							<thead>
						        <tr>
						          	<th>
						          		<a href="" class="available-month-nav" data-type="prev" data-view="#URL.view#" data-year="#rc.pYear - 1#">
						          			<i class="fa fa-chevron-left"></i>
						          		</a>
						          	</th>
						          	<th colspan="2" class="center" id="current-available-month">#rc.pYear#</th>
						          	<input type="Hidden" id="hd-current-available-month" value="#rc.pYear#">
						          	<th class="right">
						          		<a href="" class="available-month-nav" data-type="next" data-view="#URL.view#" data-year="#rc.pYear + 1#">
						          			<i class="fa fa-chevron-right"></i>
							          	</a>
						          	</th>
						        </tr>
					      	</thead>
					      	<tbody id="calendar-content">
					      		<cfloop  index="item" from="1" to="12">
			      					<cfif item eq 1>
		      							<tr>
			      					</cfif>
			      					<cfif rc.arrAvailableMonths[item].disabled eq 0>
			      						<cfset activeclass = rc.arrAvailableMonths[item].year eq rc.pYear and rc.arrAvailableMonths[item].month eq rc.pMonth ? 'active' : ''>
					      				<td class="#activeclass#"><a href="/index.cfm/gallery.default?pMonth=#rc.arrAvailableMonths[item].month#&pYear=#rc.arrAvailableMonths[item].year#&view=#URL.view#" data-month="#item#" data-year="#rc.pYear#">#DateFormat(CreateDate(rc.pYear,item,1), "mmm")#</a></td>
						      		<cfelse>
						      			<td><div>#DateFormat(CreateDate(rc.pYear,item,1), "mmm")#</div></td>
			      					</cfif>
			      					<cfif item % 4 eq 0 and item neq 12>
			      						</tr><tr>
			      					</cfif>
			      					<cfif item eq 12>
			      						</tr>
			      					</cfif>
			      				</cfloop>
					      	</tbody>
						</table>
					</div>
				</div>
				<form class="form-inline filterForm">
					<div class="form-group form-select-gallery mobile">
						<label class="col-xs-5">Choose a month:</label>
						<select id="ddPreMonths" class="col-xs-7 filterSelect">
							<option <cfif rc.pMonth eq rc.qry_available_months.month and rc.pYear eq rc.qry_available_months.year>selected</cfif>  value="/index.cfm/gallery.default?m=#month(now())#&y=#year(now())#&view=#URL.view#">#DateFormat(now(),"mmmm yyyy")#</option>
							<cfloop query="rc.qry_available_months">
								<cfif NOT (rc.qry_available_months.month EQ month(now()) AND rc.qry_available_months.year EQ year(now()))>
									<option <cfif rc.pMonth eq rc.qry_available_months.month and rc.pYear eq rc.qry_available_months.year>selected</cfif> value="/index.cfm/gallery.default?pMonth=#rc.qry_available_months.month#&pYear=#rc.qry_available_months.year#&view=#URL.view#">
										#DateFormat(CreateDate(rc.qry_available_months.year,rc.qry_available_months.month,1),"mmmm yyyy")#
									</option>
								</cfif>
							</cfloop>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
</cfoutput>