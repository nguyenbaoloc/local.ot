<cfoutput>
    <div class="row bottom-45">
        <div id="block-follow-news" class="col-md-12 block-follow-news left-side-block">
            <div class="panel-heading left-side-black-bg">
                <a href="https://twitter.com/onlyallsites" target="_blank">FOLLOW US</a>
                <span class="pull-right"><a href="https://twitter.com/onlyallsites" target="_blank"><i class="fa fa-2x fa-twitter"></i></a></span>
            </div>
            
            <div class="panel-body left-side-grey-bg carousel slide latest_news_body">
                <a class="twitter-timeline" href="https://twitter.com/OnlyAllSites" data-widget-id="577390012104208385" data-chrome="noheader nofooter noborders transparent">Tweets by @OnlyAllSites</a>
                
            </div>
        </div>
    </div>
</cfoutput>