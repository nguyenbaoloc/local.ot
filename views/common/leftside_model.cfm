<cfoutput>
	<!--- search model --->	
	#view('common/search_model', {letter_list: rc.letter_list})#

	<!--- TWITTER FEED --->
    #view('common/twitter_widget')#
    <!--- END OF TWITTER FEED --->

    <!--- LATEST NEWS --->
    #view('common/latest_news', {data: rc.qryNews})#
    <!--- END OF LATEST NEWS --->
</cfoutput>