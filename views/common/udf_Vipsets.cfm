<cfoutput query="data.vipUpdates">
    <cfif data.vipUpdates.recordcount GT 0>
        <div class="row">
            <div class="col-md-12 col-lg-offset-2 col-sm-offset-1">
                <div class="tab-release-date">SETS RELEASED ON OUR OTHER SITES, #DateFormat(data.str_releaseDate, "d mmmm yyyy")#.</div>
            </div>
        </div>

        <!--- set image list --->
        <cfset imageList_vip = "">
        <cfset arr_vip = []>
        <cfset arrayAppend(arr_vip,{link:'#application.vip_location#/index.cfm/members.view_gallery?gallery_id=#data.vipUpdates.gallery_id#', image: '/assets/img/sets-released-other-sites-VIP.jpg', gallery_type: 1})>
        <cfif NOT data.image_set>
            <cfif data.vipUpdates.video_hd_res_name NEQ "">
                <cfset arrayAppend(arr_vip,{link:'#application.vip_location#/index.cfm/members.view_gallery?gallery_id=#data.vipUpdates.gallery_id#', image: '#application.vip_location#/images/video_iconhd2.gif', gallery_type: 1})>
            </cfif>
            <cfset arrayAppend(arr_vip,{link:'#application.vip_location#/index.cfm/members.view_gallery?gallery_id=#data.vipUpdates.gallery_id#', image: '#application.vip_location#/images/mailing_list/#data.vipUpdates.mailing_list_pic_thumb#', gallery_type: data.vipUpdates.gallery_type})>

            <cfset arrayAppend(arr_vip,{link:'#application.vip_location#/index.cfm/members.view_gallery?gallery_id=#data.vipUpdates.gallery_id#', image: '#application.vip_location#/images/updates/#data.vipUpdates.updates_pic_thumb#', gallery_type: data.vipUpdates.gallery_type})>
        </cfif>
        <cfset arrayAppend(arr_vip,{link:'#application.vip_location#/index.cfm/members.view_gallery?gallery_id=#data.vipUpdates.gallery_id#', image: '#application.vip_location#/images/tour_images/#data.vipUpdates.tour_pic1_thumb#', gallery_type: data.vipUpdates.gallery_type})>

        <cfset arrayAppend(arr_vip,{link:'#application.vip_location#/index.cfm/members.view_gallery?gallery_id=#data.vipUpdates.gallery_id#', image: '#application.vip_location#/images/tour_images/#data.vipUpdates.tour_pic2_thumb#', gallery_type: data.vipUpdates.gallery_type})>

        <cfset arrayAppend(arr_vip,{link:'#application.vip_location#/index.cfm/members.view_gallery?gallery_id=#data.vipUpdates.gallery_id#', image: '#application.vip_location#/images/tour_images/#data.vipUpdates.tour_pic3_thumb#', gallery_type: data.vipUpdates.gallery_type})>

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                var maxslides = 4;
                if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    maxslides = 1;
                }
                $('a[href="##tab-#data.str_releaseDate#-#data.uui_release#-vip"]').one('shown.bs.tab', function (e) {
                    $('.set-#data.str_releaseDate#-slider-#data.uui_release#-vip').bxSlider({
                        infiniteLoop: false,
                        hideControlOnEnd: true,
                        pager: false,
                        minSlides: 1,
                        maxSlides: maxslides,
                        slideWidth: 125,
                        slideMargin: 10
                    });
                });

                $('##tab-#data.str_releaseDate#-slider-#data.uui_release#-vip').attr("color","##FFA500");
                $('.set-#data.str_releaseDate#-slider-#data.uui_release#-vip > li').each(function() {
                    $(this).css("border","1px solid ##FFA500");
                });
            });
        </script>

        <div class="row">
            <div class="col-md-12 col-lg-offset-2 col-sm-offset-1 bxwapper-model">
                <ul class="set-#data.str_releaseDate#-slider-#data.uui_release#-vip">
                    <cfloop from="1" to="#arraylen(arr_vip)#" index="item">
                        <li>
                            <a href="#arr_vip[item].link#">
                                <img alt="" src="#arr_vip[item].image#" class="dailystrip-sample-image">
                                <cfif ListFind(application.video_set_types, arr_vip[item].gallery_type)>
                                    <div class="box-pic-height">
                                        <p class="box-pic-icon">
                                            <i class="fa fa-film"></i>
                                        </p>
                                        <p class="box-pic-text">Video</p>
                                    </div>
                                </cfif>
                            </a>
                        </li>
                    </cfloop>
                </ul>
            </div>
        </div>
    </cfif>
</cfoutput>
