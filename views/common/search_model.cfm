<cfoutput>
	<div class="row bottom-45">
		<div class="col-md-12 left-side-block">
			<div class="title-search">Search models by first letter:</div>
			<div class="box-search-alphabet">
				<p class="">
					<cfloop from="65" to="73" index="this_letter">
						<cfif ListContains(letter_list, "#Chr(this_letter)#")>
							<span><a href="/index.cfm/model?letter=#Chr(this_letter)#">#Chr(this_letter)#</a></span>
						<cfelse>
							<span class="letter-model">#Chr(this_letter)#</span>
						</cfif>
					</cfloop>
				</p>
				<p class="">
					<cfloop from="74" to="82" index="this_letter">
						<cfif ListContains(letter_list, "#Chr(this_letter)#")>
							<span><a href="/index.cfm/model?letter=#Chr(this_letter)#">#Chr(this_letter)#</a></span>
						<cfelse>
							<span class="letter-model">#Chr(this_letter)#</span>
						</cfif>
					</cfloop>
				</p>
				<p class="">
					<cfloop from="83" to="90" index="this_letter">
						<cfif ListContains(letter_list, "#Chr(this_letter)#")>
							<span><a href="/index.cfm/model?letter=#Chr(this_letter)#">#Chr(this_letter)#</a></span>
						<cfelse>
							<span class="letter-model">#Chr(this_letter)#</span>
						</cfif>
					</cfloop>
				</p>
			</div>
			<div class="title-search">Search models:</div>
			<form action="/index.cfm/model" method="get" class="search-form">
				<input type="hidden" name="type" value="model">
				<input type="Hidden" name="search" value="TRUE">
				<input type="text" name="search_text" class="search-models">
				<button type="submit" name="submit" value="Search" class=""><i class="fa fa-search"></i></button>
			</form>
		</div>
	</div>
</cfoutput>