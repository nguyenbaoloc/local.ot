component accessors="true" extends="framework.one" {

	this.name				= "onlytease_application";
	this.clientmanagement	= "Yes";
	this.sessionmanagement	= "Yes";
	this.setclientcookies	= "Yes";
	this.sessiontimeout		= "#CreateTimeSpan(0,0,30,0)#";
	this.applicationtimeout	= "#CreateTimeSpan(0,1,0,0)#";
	this.datasource = "ot2004_data";
	this.tag.cflocation.addtoken = false;

	this.ormEnabled = true;
	this.invokeImplicitAccessor = true;

	variables.framework.usingSubsystems = false;
	variables.framework.defaultSection = 'members';
	variables.framework.defaultItem = 'default';
	variables.framework.reloadApplicationOnEveryRequest = false;
	// variables.framework.error="main.error";
	function setupRequest () {
		cgi.remote_user = "gDr2sGwd";
		param name="session.bolVerifiedVIP" default="0";
		param name="session.website" default="";
		param name="remote_user" default="#cgi.remote_user#";

		var oQueryService = createObject('component','model.services.Query');
		var oQueryExtendService = createObject('component','model.services.QueryExtend');

		if (cgi.SERVER_NAME CONTAINS "local") {
			remote_user = "Bruce";
			session.bol_allSitesMember = 1;
		}
		else {
			if (NOT IsDefined("session.bol_allSitesMember")) {
				var qry_authUser = oQueryService.authUser(remote_user);
				session.bol_allSitesMember = qry_authUser.bol_member;
			}
		}

		if(NOT isDefined("session.pref_id") OR (isDefined("session.remote_user") AND remote_user NEQ session.remote_user) 
			 OR NOT isDefined('session.preferences.download_server') 
			 OR (NOT isDefined('session.preferences.widget_display')) 
			 OR (NOT isDefined('session.preferences.suggested_sites'))) {
			qry_checkPreferences = oQueryService.checkPreferences(remote_user);
			if(qry_checkPreferences.recordcount GT 0) {
				session.remote_user = remote_user;
				session.pref_id = qry_checkPreferences.pref_id;
			}
			else {
				qry_pref_id = oQueryService.getMaxPrefId(remote_user);
				session.pref_id = qry_pref_id.new_prefid;
			}

			qry_checkPreferences = oQueryExtendService.checkPreferencesBy_PrefId(session.pref_id);
			session.preferences = {};
			session.preferences.pref_imagesize 	= qry_checkPreferences.pref_imagesize;
			session.preferences.pref_thumbs 		= qry_checkPreferences.pref_thumbs;
			session.preferences.pref_view 		= qry_checkPreferences.pref_view;
			session.preferences.pref_window 		= qry_checkPreferences.pref_window;
			session.preferences.pref_modellist 	= qry_checkPreferences.pref_modellist;
			session.preferences.pref_sets 		= qry_checkPreferences.pref_sets;
			session.preferences.archive_display 	= qry_checkPreferences.archive_display;
			session.preferences.download_server 	= qry_checkPreferences.download_server;
			session.preferences.slideshow_autoresize = qry_checkPreferences.pref_ss_autoresize;
			session.preferences.widget_display = qry_checkPreferences.widget_display;
			session.preferences.suggested_sites = qry_checkPreferences.suggested_sites;
			session.preferences.cdn_option = qry_checkPreferences.cdn_option;

		}
		if ( NOT session.bolVerifiedVIP) {
			vipMemberCheck = oQueryService.checkVipMember(session.pref_id,DateAdd('m', -3, now()));
			if (vipMemberCheck.recordcount > 0) {
				session.bolVerifiedVIP = 1;
			}
		}

		//forum
		var oNewForumService = createObject('component','model.services.NewForum');

		if (session.website EQ "") {
			var datalist = "oas_users,ot_users,oo_users,os_users,oss_users,oc_users,om_users";
			loop list="#datalist#" index="this_datatable" {
				var qry_site = oNewForumService.qry_site(this_datatable);
				if (qry_site.recordcount GT 0) {
					session.website = this_datatable;
					break;
				}
			}
		}

		var checkdatetime = DateAdd("n", -15, now());
		var forum_qry_lastupdate = oNewForumService.qry_forum_lastupdate(session.pref_id);

		if (forum_qry_lastupdate.forum_check_date LT checkdatetime ) {
			// oNewForumService.qry_forum_update_lastupdate(session.pref_id);
			application.last_post_date = forum_qry_lastupdate.forum_check_date;
		}
		else {
			application.last_post_date = forum_qry_lastupdate.forum_last_access_date;
		}

	}
	function setupApplication() {
		
		application.queryTimeCache   = CreateTimeSpan(0,0,1,0);
		application.dsn_name 		 = "ot2004_data";
		application.forum_datasource = "ot_fullforum";
		application.vip_location 	 = "http://vip.onlytease.com";

		application.file_locat_url 				= "http://cdn.content.onlytease.com";
		application.direct_file_locat_url 		= "http://content.onlytease.com";
		application.file_locat_url_zip 			= "http://cdnzips.onlytease.com";
		application.direct_file_locat_url_zip 	= "http://otzips.onlytease.com";

		application.picture_set_types 		= "1,2,3,7,10,11,17,19";
		application.video_set_types 		= "14,4,5,6,12,13,16,18,20";
		application.all_set_types 			= "1,2,3,4,5,6,7,10,11,12,13,14,17,18,19,20";
		application.number_coming_soon 		= 10;
		application.number_minimum_votes 	= "5";
		application.max_customzip_images 	= "100";
		application.gallery_change = 6000;
		application.key = "B7E6CF571C8461AA040BAD6DBE071214";
		application.algorithm = "DESEDE";
		application.encoding = "hex";
		application.rootdir_this_site = "/web/sites/onlytease/members.onlytease.com/public_html";
		application.forum_threads_shown = 20;

		//for forum configuration
		<!--- Get Application Settings --->
		var oNewForumService = createObject('component','model.services.NewForum');
		var get_forum_settings = oNewForumService.get_forum_settings();
		if (get_forum_settings.AsPercent eq "Y") {
			application.AppWidth="#get_forum_settings.TableWidth#%";
		}
		else {
			application.AppWidth="#get_forum_settings.TableWidth#";
		}
		application.ImgPath="#get_forum_settings.ImgPath#";
		application.AppPath="#get_forum_settings.AppPath#";
		application.AppName="#get_forum_settings.AppName#";
		
		var oQueryExtendService = createObject('component','model.services.QueryExtend');
		var forums_config = oQueryExtendService.qry_defaults_forum();
		application.ip 		= forums_config.ip;
		application.rootdir 	= forums_config.rootdir;
		application.dsn_name_forum 		= forums_config.dsn_name;

		if(NOT StructKeyExists(application, "arr_otherSites")){
			lock scope="Application" timeout="10" type="Exclusive" {
				var strDataSource = "";
				application.arr_otherSites = [];
				<!--- Only Tease --->
				strDataSource = "ot2004_data";
				var stc_siteData = {
					str_standsforsite = "OT"
					,str_siteName = "Only Tease"
					,str_datasource = "ot2004_data"
					,str_siteUrlTour = "http://www.onlytease.com"
					,str_siteUrlMembers = "http://members.onlytease.com"
					,str_siteLogo = "/assets/img/sets-released-other-sites-OT.jpg"
					,str_siteOverlay = "http://members.onlytease.com/images/fasttrack_overlay_OT.png"
					,str_siteThumb = "/assets/img/sets-released-other-sites-OT.jpg"
					,str_siteColour = "8ec0fd"
					,str_picsPath = "http://cdn.onlytease.com/images/xpromotion"
					,bol_display = Iif(	strDataSource EQ application.dsn_name, 0, 1)
				};
				ArrayAppend(application.arr_otherSites, stc_siteData);
				<!--- Only Opaques --->
				strDataSource = "oo_data";
				var stc_siteData = {
					str_standsforsite = "OO"
					,str_siteName = "Only Opaques"
					,str_datasource = "oo_data"
					,str_siteUrlTour = "http://www.only-opaques.com"
					,str_siteUrlMembers = "http://members.only-opaques.com"
					,str_siteLogo = "/assets/img/sets-released-other-sites-OO.jpg"
					,str_siteOverlay = "http://members.onlytease.com/images/fasttrack_overlay_OO.png"
					,str_siteThumb = "/assets/img/sets-released-other-sites-OO.jpg"
					,str_siteColour = "ec008c"
					,str_picsPath = "http://cdn.only-opaques.com/images/xpromotion"
					,bol_display = Iif(	strDataSource EQ application.dsn_name, 0, 1)
				};
				ArrayAppend(application.arr_otherSites, stc_siteData);
				<!--- Only Secretaries --->
				strDataSource = "os_data";
				var stc_siteData = {
					str_standsforsite = "OS"
					,str_siteName = "Only Secretaries"
					,str_datasource = "os_data"
					,str_siteUrlTour = "http://www.only-secretaries.com"
					,str_siteUrlMembers = "http://members.only-secretaries.com"
					,str_siteLogo = "/assets/img/sets-released-other-sites-OS.jpg"
					,str_siteOverlay = "http://members.onlytease.com/images/fasttrack_overlay_OS.png"
					,str_siteThumb = "/assets/img/sets-released-other-sites-OS.jpg"
					,str_siteColour = "b66edc"
					,str_picsPath = "http://cdn.only-secretaries.com/images/xpromotion"
					,bol_display = Iif(	strDataSource EQ application.dsn_name, 0, 1)
				};
				ArrayAppend(application.arr_otherSites, stc_siteData);
				<!--- Only Silk and Satin --->
				strDataSource = "oss_data";
				var stc_siteData = {
					str_standsforsite = "OSS"
					,str_siteName = "Only Silk and Satin"
					,str_datasource = "oss_data"
					,str_siteUrlTour = "http://www.onlysilkandsatin.com"
					,str_siteUrlMembers = "http://members.onlysilkandsatin.com"
					,str_siteLogo = "/assets/img/sets-released-other-sites-OSS.jpg"
					,str_siteOverlay = "http://members.onlytease.com/images/fasttrack_overlay_OSS.png"
					,str_siteThumb = "/assets/img/sets-released-other-sites-OSS.jpg"
					,str_siteColour = "25c079"
					,str_picsPath = "http://cdn.onlysilkandsatin.com/images/xpromotion"
					,bol_display = Iif(strDataSource EQ application.dsn_name, 0, 1)
				}
				ArrayAppend(application.arr_otherSites, stc_siteData);
			}
		}

		var oUtilService = createObject('component','model.services.Util')
		application.ticker_content = oUtilService.getTickerContent();

		if (NOT StructKeyExists(application, "stc_set_types")) {
			application.stc_set_types = {};
			application.stc_set_types.lastReload = dateAdd("d",-1,now());
		}

		if (dateAdd("h",12,application.stc_set_types.lastReload) LTE now()) {
			<!--- reset the structure data --->
			application.stc_set_types.str_pictures = "";
			application.stc_set_types.str_videos = "";
			application.stc_set_types.str_all = "";
			application.stc_set_types.desc = ArrayNew(1);

			<!--- reload the latest set types --->
			var oGalleryService = createObject('component','model.services.Gallery');
			var qry_typelist = oGalleryService.getTypeList();

			<!--- loop and update the data --->
			loop query="qry_typelist" {
				<!--- add to the picture list --->
				if (qry_typelist.gallery_type_picture EQ 1) {
					application.stc_set_types.str_pictures = ListAppend(application.stc_set_types.str_pictures,qry_typelist.gallery_type_id);
				}
				<!--- add to the video list --->
				if (qry_typelist.gallery_type_video EQ 1) {
					application.stc_set_types.str_videos = ListAppend(application.stc_set_types.str_videos,qry_typelist.gallery_type_id);
				}
				<!--- add to the all sets list --->
				application.stc_set_types.str_all = ListAppend(application.stc_set_types.str_all,qry_typelist.gallery_type_id);
				<!--- create the description array --->
				application.stc_set_types.desc[qry_typelist.gallery_type_id] = qry_typelist.gallery_type_display_text;
			}

			<!--- set the last reload time --->
			application.stc_set_types.lastReload = now();
		}

	}

	function setupView() {
		var obj = createObject('component','model.services.News')
		rc.qryNews = obj.getLatestNews();

		obj = createObject("component","model.services.Model");
		qry_letter_list = obj.getLetters();
		rc.letter_list = ValueList(qry_letter_list.model_initial);

		obj = createObject("component","model.services.Util");
		rc.arr_month = obj.InitArrayOfMonth();
	}

	// function onError(required rc){
	//   	return view('main/error');
	// }

	// function onMissingView(required rc){
	//   	controller(main.error);
	// }
}
